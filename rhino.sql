/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : rhino

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-11-09 15:52:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for assignments
-- ----------------------------
DROP TABLE IF EXISTS `assignments`;
CREATE TABLE `assignments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `project` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of assignments
-- ----------------------------
INSERT INTO `assignments` VALUES ('51', '3', '1');
INSERT INTO `assignments` VALUES ('52', '3', '30');
INSERT INTO `assignments` VALUES ('54', '3', '24');
INSERT INTO `assignments` VALUES ('92', '3', '22');
INSERT INTO `assignments` VALUES ('93', '3', '20');
INSERT INTO `assignments` VALUES ('95', '3', '27');
INSERT INTO `assignments` VALUES ('96', '3', '26');
INSERT INTO `assignments` VALUES ('97', '3', '28');
INSERT INTO `assignments` VALUES ('98', '3', '29');
INSERT INTO `assignments` VALUES ('99', '1', '22');
INSERT INTO `assignments` VALUES ('101', '1', '20');
INSERT INTO `assignments` VALUES ('102', '1', '24');
INSERT INTO `assignments` VALUES ('103', '1', '21');
INSERT INTO `assignments` VALUES ('104', '1', '30');
INSERT INTO `assignments` VALUES ('105', '1', '27');
INSERT INTO `assignments` VALUES ('106', '1', '26');
INSERT INTO `assignments` VALUES ('107', '1', '28');
INSERT INTO `assignments` VALUES ('108', '1', '29');
INSERT INTO `assignments` VALUES ('110', '1', '1');

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `state` varchar(60) DEFAULT NULL,
  `image` varchar(60) DEFAULT NULL,
  `vat` varchar(255) DEFAULT NULL,
  `currency` int(1) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of clients
-- ----------------------------
INSERT INTO `clients` VALUES ('1', '19', 'Douglas Fowler', '+1-202-555-0164', 'Premsuite.com', ' 14 av , 1003 ', '1055020', 'USA', 'New', 'NYC', 'f536eab2b448.jpg', 'US', '29', 'Premsuite', 'CEO');
INSERT INTO `clients` VALUES ('2', '20', 'Katherine Pena', '+1-202-555-0164', '', '83 av , 1003', '660145', 'USA', 'Miami', 'FL', 'b300b0a27f36.jpg', 'US766318813721', '29', '', '');
INSERT INTO `clients` VALUES ('3', '21', 'Daniel Newman', '077 4438 7096', null, '54 Great North Road', 'PL152YG', 'UK', 'HAYTON', 'LANDFORD', '7b762721b320.jpg', 'UKSP52YN', '10', 'Knockout Kickboxing', 'Caster');
INSERT INTO `clients` VALUES ('4', '22', 'Dominic Müller', '(159) 749 5286', '', '', '', '', '', '', null, '', '3', '', '');
INSERT INTO `clients` VALUES ('5', '24', 'Joshua Beck', '(265) 127 3897', '', '', '', 'USA', '', 'IL', 'e84820e2de7d.jpg', '', '29', '', '');

-- ----------------------------
-- Table structure for currencies
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of currencies
-- ----------------------------
INSERT INTO `currencies` VALUES ('1', 'AUD', 'Australia Dollar');
INSERT INTO `currencies` VALUES ('2', 'BGN', 'Bulgaria Lev');
INSERT INTO `currencies` VALUES ('3', 'BRL', 'Brazil Real');
INSERT INTO `currencies` VALUES ('4', 'CAD', 'Canada Dollar');
INSERT INTO `currencies` VALUES ('5', 'CHF', 'Switzerland Franc');
INSERT INTO `currencies` VALUES ('6', 'CNY', 'China Yuan Renminbi');
INSERT INTO `currencies` VALUES ('7', 'CZK', 'Czech Republic Koruna');
INSERT INTO `currencies` VALUES ('8', 'DKK', 'Denmark Krone');
INSERT INTO `currencies` VALUES ('9', 'EUR', 'Euro Member Countries');
INSERT INTO `currencies` VALUES ('10', 'GBP', 'United Kingdom Pound');
INSERT INTO `currencies` VALUES ('11', 'HKD', 'Hong Kong Dollar');
INSERT INTO `currencies` VALUES ('12', 'HRK', 'Croatia Kuna');
INSERT INTO `currencies` VALUES ('13', 'HUF', 'Hungary Forint');
INSERT INTO `currencies` VALUES ('14', 'IDR', 'Indonesia Rupiah');
INSERT INTO `currencies` VALUES ('15', 'ILS', 'Israel Shekel');
INSERT INTO `currencies` VALUES ('16', 'INR', 'India Rupee');
INSERT INTO `currencies` VALUES ('17', 'JPY', 'Japan Yen');
INSERT INTO `currencies` VALUES ('18', 'KRW', 'Korea (South) Won');
INSERT INTO `currencies` VALUES ('19', 'MXN', 'Mexico Peso');
INSERT INTO `currencies` VALUES ('20', 'MYR', 'Malaysia Ringgit');
INSERT INTO `currencies` VALUES ('21', 'NOK', 'Norway Krone');
INSERT INTO `currencies` VALUES ('22', 'NZD', 'New Zealand Dollar');
INSERT INTO `currencies` VALUES ('23', 'PHP', 'Philippines Peso');
INSERT INTO `currencies` VALUES ('24', 'PLN', 'Poland Zloty');
INSERT INTO `currencies` VALUES ('25', 'RON', 'Romania New Leu');
INSERT INTO `currencies` VALUES ('26', 'RUB', 'Russia Ruble');
INSERT INTO `currencies` VALUES ('27', 'SEK', 'Sweden Krona');
INSERT INTO `currencies` VALUES ('28', 'SGD', 'Singapore Dollar');
INSERT INTO `currencies` VALUES ('29', 'USD', 'American Dollar');

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `department` varchar(60) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES ('7', 'All Members');
INSERT INTO `departments` VALUES ('8', 'Backend');
INSERT INTO `departments` VALUES ('9', 'Frontend');
INSERT INTO `departments` VALUES ('10', 'Design');

-- ----------------------------
-- Table structure for department_members
-- ----------------------------
DROP TABLE IF EXISTS `department_members`;
CREATE TABLE `department_members` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `department` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department_members
-- ----------------------------
INSERT INTO `department_members` VALUES ('8', '8', '1');
INSERT INTO `department_members` VALUES ('9', '8', '24');
INSERT INTO `department_members` VALUES ('106', '9', '22');
INSERT INTO `department_members` VALUES ('107', '9', '1');
INSERT INTO `department_members` VALUES ('108', '9', '20');
INSERT INTO `department_members` VALUES ('109', '9', '21');
INSERT INTO `department_members` VALUES ('110', '7', '22');
INSERT INTO `department_members` VALUES ('112', '7', '1');
INSERT INTO `department_members` VALUES ('113', '7', '20');
INSERT INTO `department_members` VALUES ('114', '7', '24');
INSERT INTO `department_members` VALUES ('115', '7', '21');
INSERT INTO `department_members` VALUES ('116', '7', '30');
INSERT INTO `department_members` VALUES ('117', '7', '27');
INSERT INTO `department_members` VALUES ('118', '7', '26');
INSERT INTO `department_members` VALUES ('119', '7', '28');
INSERT INTO `department_members` VALUES ('120', '7', '29');
INSERT INTO `department_members` VALUES ('121', '10', '21');
INSERT INTO `department_members` VALUES ('122', '10', '30');
INSERT INTO `department_members` VALUES ('123', '10', '29');

-- ----------------------------
-- Table structure for estimate_items
-- ----------------------------
DROP TABLE IF EXISTS `estimate_items`;
CREATE TABLE `estimate_items` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `estimate` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of estimate_items
-- ----------------------------
INSERT INTO `estimate_items` VALUES ('6', 'ITM5465465', '8', 'Lacinia unc llam risus', 'Lacinia unc llam risus', '66', '2395');
INSERT INTO `estimate_items` VALUES ('7', 'ITM5465466', '12', 'fadsfadsf', 'adsfsdfa', '10', '230');
INSERT INTO `estimate_items` VALUES ('8', 'ITM5465469', '1974735', 'TEST ITEM', 'HELLO', '9', '100');
INSERT INTO `estimate_items` VALUES ('11', 'ITM5465462', '13', 'sdfhjhj', 'jhjkfhj', '66', '220');
INSERT INTO `estimate_items` VALUES ('13', 'ITM8768143', '15', 'Nislnam ', 'Nislnam egestas laoreet nam entum odiophas iam ibulum aesent rosed. Lacinia conseq convalli tempusp ipsump sed. Maurisin facilis tur magnis mauris massama. Ris duis ras tdonec neque mi ulla ctetur sceleris egesta gsed lacusp.', '7', '777');
INSERT INTO `estimate_items` VALUES ('14', 'ITM17734.4', '12', 'suspendi ', 'Scum lum suspendi oin ipsumnam mstut. Odio ut disse quam quamin proin congued sociis itor. Sapiendo aptent laoreet nulla venenat tempus liquam. Justonul orem vel ctetur pulvinar felisq facilis nas luctus bus auctorpr lobortis.', '15', '199');
INSERT INTO `estimate_items` VALUES ('16', 'ITM48c65c3', '3', 'dasdsadasdas', 'dasdasdsa', '5', '5134');
INSERT INTO `estimate_items` VALUES ('17', 'ITMcf0eb98', '4', 'item 01', 'Mus viverr laciniap nullam ipsumin lectusn.', '7', '1499');
INSERT INTO `estimate_items` VALUES ('19', 'ITMfc7a4a8', '4', 'dfdsf', 'dsf', '7', '66');

-- ----------------------------
-- Table structure for estimate_requests
-- ----------------------------
DROP TABLE IF EXISTS `estimate_requests`;
CREATE TABLE `estimate_requests` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) DEFAULT NULL,
  `project` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `budget` int(10) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estimate_requests
-- ----------------------------
INSERT INTO `estimate_requests` VALUES ('2', 'asdasd', '3', 'Faucibus temporin', 'Faucibus temporin aliquama massan sodalesm aesent nunc. Ante rutrumnu lum quispr enean massama isque quam semper enean. Egetal egestas lus laoreet facilis eratphas ras himena lectuss liquam pretiu lacusaen ulum sodales sapienma tur lacinia.', '150000', '', '2016-10-21 16:37:53', '2');
INSERT INTO `estimate_requests` VALUES ('3', 'sdaasdddd', '3', 'senectus', 'Nas pellente aliquet tfusce ac ris naeos faucibus. Primis nunc snam nulla seminteg nunc ris turpiset feugiat. Aliquama diampr nislae molest nislsed nean imperdie turpis magnap senectus uisque justo nec viverr egestas turpisn aliquam.', '76800', '', '2016-10-21 16:37:55', '1');
INSERT INTO `estimate_requests` VALUES ('4', 'REQd101ac5', '3', 'asdad', 'ada', '24213', '', '2016-11-09 16:06:40', '1');
INSERT INTO `estimate_requests` VALUES ('5', 'REQ62d4b8c', '3', 'Esque ipsumves', 'Esque ipsumves congue lla hendre tiam mnulla nec. Odioin enimdon mauris volutpa ntum mattisae semnunc. Euismod teger ellus natoque mollis teger litora odio dolorve facili platea lus quamal nislin feugiatn volutpat ad malesu eget.', '24213', '', '2016-11-09 16:06:45', '1');
INSERT INTO `estimate_requests` VALUES ('6', 'REQ9a593c0', '3', 'dsffdfsf', 'asfd', '24213', '', '2016-11-09 16:07:00', '1');
INSERT INTO `estimate_requests` VALUES ('7', 'REQ8580c48', '3', 'Quamsusp ger natis ullamcor', 'Metiam scras ullam arcualiq ligulam eu isse necsed teger laoreet. Velitsed daut que amet sed molest gravida. Odioin bibendu aliquet maurisma placerat liberos lacinia usce diam nas nislnam massacra nec sodalesm malesu arcucura faucib.', '24213', '', '2016-11-09 16:09:27', '2');

-- ----------------------------
-- Table structure for estimate_reviews
-- ----------------------------
DROP TABLE IF EXISTS `estimate_reviews`;
CREATE TABLE `estimate_reviews` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `estimate` int(10) NOT NULL,
  `text` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estimate_reviews
-- ----------------------------
INSERT INTO `estimate_reviews` VALUES ('1', '12', 'Gravida quam disse nulla porta lus volutp enimsed. Lobortis volutpa purusp proin congued pornam dumin. Rfusce urnavest tate lum tristi nislin lus himenaeo lobortis leoetiam idnulla egestas ipsumma adipis imperdie iaculi mauris ulum uis.', '2016-09-20 21:11:43');
INSERT INTO `estimate_reviews` VALUES ('2', '8', 'Waaaaaaaay to pricy', '2016-09-20 21:51:09');
INSERT INTO `estimate_reviews` VALUES ('3', '8', 'Justov ent lacusp quiscras metusd eratetia magnap teger. Liberout tudin ger rutruma vestib sodale metusqui iquam eger. Leo nislae sed tiam miquis lectusa natoque quamut proin liquam diamin leofusce erossed quam snam aliquet conubia.', '2016-09-20 21:55:59');
INSERT INTO `estimate_reviews` VALUES ('4', '3', 'Curae accumsan laut quam cusut torquent congue accumsan. Metusd nec nisiinte oin laoreet elitphas. Entum ent quamnunc llaut uam amus lacusp ellus maurisve turpisp volutp msed faucibus tur disse iain dictumst aenean onec llus.', '2016-09-28 20:12:25');
INSERT INTO `estimate_reviews` VALUES ('5', '3', 'Isse enimlo suscip sed massacra bus abitur. Eroscu quamin ullam naeos rutruma adipis liberout. Sque sodale ras ssed nas donec sapiendo ornare liquam etsed bibendu disse esent dignis egesta nullap tsed proin vel ultrici.', '2016-09-28 20:28:49');

-- ----------------------------
-- Table structure for expenses
-- ----------------------------
DROP TABLE IF EXISTS `expenses`;
CREATE TABLE `expenses` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `amount` int(60) NOT NULL,
  `category` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `file` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of expenses
-- ----------------------------
INSERT INTO `expenses` VALUES ('2', 'EXPc9cf9b9', '25055', '1', '2016-10-12 00:00:00', 'sdasdasd', 'Orci odio cing himena mauris ras facili. ', null);
INSERT INTO `expenses` VALUES ('3', 'EXP9084dbe', '39999', '2', '2016-11-01 00:00:00', '6565', 'Unc ligulam quamve proin liberos odioin aliqua. Maurisve imperdie ecenas morbi maurisin teger orciduis. Potenti amet sed ante urna nulla leosed. Nas estnunc urnavest roin ulum laoree rutrum rfusce tellus tur sapien auris feugiatm.', null);
INSERT INTO `expenses` VALUES ('5', 'EXP5440d24', '32040', '1', '2016-07-21 00:00:00', 'Nonmorbi ullam lisis lla', 'Nas tincid cursusve culus malesu lacuse ullamcor sollic tesque. Nonmorbi ullam lisis lla egetal commodo leoetiam. Metiam nean duis sapien ', null);
INSERT INTO `expenses` VALUES ('6', 'EXP65b07b4', '16000', '1', '2016-11-04 00:00:00', 'gdsgdf', 'fgdfgdfgdfg', null);

-- ----------------------------
-- Table structure for expense_categories
-- ----------------------------
DROP TABLE IF EXISTS `expense_categories`;
CREATE TABLE `expense_categories` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of expense_categories
-- ----------------------------
INSERT INTO `expense_categories` VALUES ('1', 'Miscellaneous');
INSERT INTO `expense_categories` VALUES ('2', 'Salary');
INSERT INTO `expense_categories` VALUES ('3', 'Software');

-- ----------------------------
-- Table structure for invoice_items
-- ----------------------------
DROP TABLE IF EXISTS `invoice_items`;
CREATE TABLE `invoice_items` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `invoice` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `issued` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of invoice_items
-- ----------------------------
INSERT INTO `invoice_items` VALUES ('1', 'ITM12356', '4', 'Lacini id inproin teger donec orciut itor lla dis', 'Nec mattisae metussed arcused massased roin molesti erdum maurisd ibulum. Pharetr facili lectusn nislqu teger euismo ulum gravidas. Turpisut eleifen ent naeos pretium ris enim ctetur aesent facili fringi dictumst blandit tempusp commodo sedlorem.', '3', '15000', '2016-09-06 16:41:59');
INSERT INTO `invoice_items` VALUES ('2', 'ITM123567', '4', 'Portad mnulla nuncn', 'Antenull consect isised iennam maurisin et erat dictumst fusce. Quam onec malesuad nequenu sapienv sociis ras. Miquis aesent convall aenean anteduis ger lum etiam magnaqu enean aesent scras rproin eratquis llam massa quamal posuere.', '10', '1500', '2016-09-06 17:39:50');
INSERT INTO `invoice_items` VALUES ('4', 'ITM123569', '4', 'test', 'test', '3', '10000', '2016-09-06 18:02:52');
INSERT INTO `invoice_items` VALUES ('5', 'ITM123564', '3', 'dfsadf', 'agfagdfa', '64', '4700', '2016-09-06 18:05:28');
INSERT INTO `invoice_items` VALUES ('11', 'ITMFIN', '12', 'fadsfadsf', 'adsfsdfa', '10', '230', null);
INSERT INTO `invoice_items` VALUES ('12', 'ITMINF', '12', 'suspendi ', 'Scum lum suspendi oin ipsumnam mstut. Odio ut disse quam quamin proin congued sociis itor. Sapiendo aptent laoreet nulla venenat tempus liquam. Justonul orem vel ctetur pulvinar felisq facilis nas luctus bus auctorpr lobortis.', '15', '199', null);
INSERT INTO `invoice_items` VALUES ('14', 'ITMad92a74', '5', 'sdfdfaadsf', 'Tur phasel sapienna disse ntum tortor. Posuered odioin loremnul tellus eroscu lacusp lacusp elemen turpisut. Oin himena quam sdonec necinte tristi tortor. Maurisd tcras uisque lla mauris rutrumnu ndisse liberoa elit egestas varius auctorpr.', '17', '195', '2016-09-28 21:19:50');
INSERT INTO `invoice_items` VALUES ('29', 'ITMb5bbfcf', '24', 'fadsfadsf', 'adsfsdfa', '10', '230', '2016-09-30 19:45:34');
INSERT INTO `invoice_items` VALUES ('30', 'ITMcfb5bbf', '24', 'suspendi ', 'Scum lum suspendi oin ipsumnam mstut. Odio ut disse quam quamin proin congued sociis itor. Sapiendo aptent laoreet nulla venenat tempus liquam. Justonul orem vel ctetur pulvinar felisq facilis nas luctus bus auctorpr lobortis.', '15', '199', '2016-09-30 19:45:34');
INSERT INTO `invoice_items` VALUES ('31', 'ITM0eeeab8', '25', 'item 01', 'Mus viverr laciniap nullam ipsumin lectusn.', '7', '1499', null);
INSERT INTO `invoice_items` VALUES ('32', 'ITM0ea8bee', '25', 'dfdsf', 'dsf', '7', '66', null);
INSERT INTO `invoice_items` VALUES ('33', 'ITMd637cb2', '26', 'fadsfadsf', 'adsfsdfa', '10', '230', '2016-11-03 13:26:14');
INSERT INTO `invoice_items` VALUES ('34', 'ITM6bc32d7', '26', 'suspendi ', 'Scum lum suspendi oin ipsumnam mstut. Odio ut disse quam quamin proin congued sociis itor. Sapiendo aptent laoreet nulla venenat tempus liquam. Justonul orem vel ctetur pulvinar felisq facilis nas luctus bus auctorpr lobortis.', '15', '199', '2016-11-03 13:26:14');
INSERT INTO `invoice_items` VALUES ('35', 'ITM6372dcb', '27', 'fadsfadsf', 'adsfsdfa', '10', '230', '2016-11-03 13:26:14');
INSERT INTO `invoice_items` VALUES ('36', 'ITM2c7d36b', '27', 'suspendi ', 'Scum lum suspendi oin ipsumnam mstut. Odio ut disse quam quamin proin congued sociis itor. Sapiendo aptent laoreet nulla venenat tempus liquam. Justonul orem vel ctetur pulvinar felisq facilis nas luctus bus auctorpr lobortis.', '15', '199', '2016-11-03 13:26:14');
INSERT INTO `invoice_items` VALUES ('37', 'ITM4ce467a', '28', 'fadsfadsf', 'adsfsdfa', '10', '230', '2016-11-03 13:26:14');
INSERT INTO `invoice_items` VALUES ('38', 'ITMca474e6', '28', 'suspendi ', 'Scum lum suspendi oin ipsumnam mstut. Odio ut disse quam quamin proin congued sociis itor. Sapiendo aptent laoreet nulla venenat tempus liquam. Justonul orem vel ctetur pulvinar felisq facilis nas luctus bus auctorpr lobortis.', '15', '199', '2016-11-03 13:26:14');
INSERT INTO `invoice_items` VALUES ('39', 'ITM47c6ae4', '29', 'fadsfadsf', 'adsfsdfa', '10', '230', '2016-11-03 13:26:14');
INSERT INTO `invoice_items` VALUES ('40', 'ITMc464ea7', '29', 'suspendi ', 'Scum lum suspendi oin ipsumnam mstut. Odio ut disse quam quamin proin congued sociis itor. Sapiendo aptent laoreet nulla venenat tempus liquam. Justonul orem vel ctetur pulvinar felisq facilis nas luctus bus auctorpr lobortis.', '15', '199', '2016-11-03 13:26:14');

-- ----------------------------
-- Table structure for invoice_payments
-- ----------------------------
DROP TABLE IF EXISTS `invoice_payments`;
CREATE TABLE `invoice_payments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `project` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `amount` int(60) NOT NULL,
  `method` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `payer` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of invoice_payments
-- ----------------------------
INSERT INTO `invoice_payments` VALUES ('2', 'PAYfdef13c', '3', '12', '5565', '3', '2016-10-24 20:12:39', '21');
INSERT INTO `invoice_payments` VALUES ('3', 'PAY0d957f1', '3', '4', '93960', '3', '2016-10-24 20:26:54', '21');
INSERT INTO `invoice_payments` VALUES ('4', 'PAYcfec5d8', '3', '5', '3477', '3', '2016-10-25 14:18:10', '21');
INSERT INTO `invoice_payments` VALUES ('5', 'PAY583cdfd', '3', '24', '5285', '3', '2016-10-25 14:53:16', '21');
INSERT INTO `invoice_payments` VALUES ('6', 'PAYe5b87c1', '3', '3', '100', '1', '2016-10-26 00:00:00', '1');
INSERT INTO `invoice_payments` VALUES ('7', 'PAY4b88e62', '3', '29', '5285', '3', '2016-11-03 19:23:38', '21');
INSERT INTO `invoice_payments` VALUES ('8', 'PAY4be6f1c', '3', '25', '11338', '3', '2016-11-04 20:30:03', '21');

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `modifier` int(10) NOT NULL,
  `receiver` int(10) NOT NULL,
  `text` text,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `href` varchar(255) DEFAULT NULL,
  `new` int(10) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notifications
-- ----------------------------
INSERT INTO `notifications` VALUES ('1', '1', '1', 'Hudson Nicoletti, updated personal status for the task #TSKf1848f3!', '2016-11-07 18:46:42', '/task/TSKf1848f3/overview', '0');
INSERT INTO `notifications` VALUES ('2', '1', '20', 'Hudson Nicoletti, updated personal status for the task #TSKf1848f3!', '2016-11-07 18:46:42', '/task/TSKf1848f3/overview', '1');
INSERT INTO `notifications` VALUES ('3', '1', '26', 'Hudson Nicoletti, updated personal status for the task #TSKf1848f3!', '2016-11-07 18:46:42', '/task/TSKf1848f3/overview', '1');
INSERT INTO `notifications` VALUES ('4', '1', '28', 'Hudson Nicoletti, updated personal status for the task #TSKf1848f3!', '2016-11-07 18:46:42', '/task/TSKf1848f3/overview', '1');
INSERT INTO `notifications` VALUES ('5', '1', '29', 'Hudson Nicoletti, updated personal status for the task #TSKf1848f3!', '2016-11-07 18:46:42', '/task/TSKf1848f3/overview', '1');
INSERT INTO `notifications` VALUES ('6', '1', '1', ', responded in Task #TSKf1848f3!', '2016-11-07 18:59:01', '/task/overview/TSKf1848f3', '0');
INSERT INTO `notifications` VALUES ('7', '1', '20', ', responded in Task #TSKf1848f3!', '2016-11-07 18:59:01', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('8', '1', '26', ', responded in Task #TSKf1848f3!', '2016-11-07 18:59:01', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('9', '1', '28', ', responded in Task #TSKf1848f3!', '2016-11-07 18:59:01', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('10', '1', '29', ', responded in Task #TSKf1848f3!', '2016-11-07 18:59:01', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('11', '1', '1', 'Responded in Task #TSKf1848f3!', '2016-11-07 19:00:52', '/task/overview/TSKf1848f3', '0');
INSERT INTO `notifications` VALUES ('12', '1', '20', 'Responded in Task #TSKf1848f3!', '2016-11-07 19:00:52', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('13', '1', '26', 'Responded in Task #TSKf1848f3!', '2016-11-07 19:00:52', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('14', '1', '28', 'Responded in Task #TSKf1848f3!', '2016-11-07 19:00:52', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('15', '1', '29', 'Responded in Task #TSKf1848f3!', '2016-11-07 19:00:52', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('16', '1', '1', 'Responded in Task #TSKf1848f3!', '2016-11-07 20:24:42', '/task/overview/TSKf1848f3', '0');
INSERT INTO `notifications` VALUES ('17', '1', '20', 'Responded in Task #TSKf1848f3!', '2016-11-07 20:24:42', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('18', '1', '26', 'Responded in Task #TSKf1848f3!', '2016-11-07 20:24:42', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('19', '1', '28', 'Responded in Task #TSKf1848f3!', '2016-11-07 20:24:43', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('20', '1', '29', 'Responded in Task #TSKf1848f3!', '2016-11-07 20:24:43', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('21', '1', '1', 'Canceled Task #TSKf1848f3!', '2016-11-08 13:36:41', '/task/overview/TSKf1848f3', '0');
INSERT INTO `notifications` VALUES ('22', '1', '20', 'Canceled Task #TSKf1848f3!', '2016-11-08 13:36:41', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('23', '1', '26', 'Canceled Task #TSKf1848f3!', '2016-11-08 13:36:41', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('24', '1', '28', 'Canceled Task #TSKf1848f3!', '2016-11-08 13:36:41', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('25', '1', '29', 'Canceled Task #TSKf1848f3!', '2016-11-08 13:36:41', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('26', '1', '1', 'Updated Task #TSKf1848f3 status!', '2016-11-08 13:36:49', '/task/overview/TSKf1848f3', '0');
INSERT INTO `notifications` VALUES ('27', '1', '20', 'Updated Task #TSKf1848f3 status!', '2016-11-08 13:36:49', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('28', '1', '26', 'Updated Task #TSKf1848f3 status!', '2016-11-08 13:36:49', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('29', '1', '28', 'Updated Task #TSKf1848f3 status!', '2016-11-08 13:36:49', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('30', '1', '29', 'Updated Task #TSKf1848f3 status!', '2016-11-08 13:36:49', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('31', '1', '1', 'Updated Task #TSKcfcea86 status!', '2016-11-08 13:54:16', '/task/overview/TSKcfcea86', '0');
INSERT INTO `notifications` VALUES ('32', '1', '24', 'Updated Task #TSKcfcea86 status!', '2016-11-08 13:54:16', '/task/overview/TSKcfcea86', '1');
INSERT INTO `notifications` VALUES ('33', '1', '1', 'Updated personal status for the task #TSKcd31d7a!', '2016-11-08 14:40:46', '/task/overview/TSKcd31d7a', '0');
INSERT INTO `notifications` VALUES ('34', '1', '24', 'Updated personal status for the task #TSKcd31d7a!', '2016-11-08 14:40:46', '/task/overview/TSKcd31d7a', '1');
INSERT INTO `notifications` VALUES ('35', '1', '30', 'Updated personal status for the task #TSKcd31d7a!', '2016-11-08 14:40:46', '/task/overview/TSKcd31d7a', '1');
INSERT INTO `notifications` VALUES ('36', '1', '30', 'Task information updated! #', '2016-11-08 14:42:56', '/task/overview/TSKcd31d7a', '1');
INSERT INTO `notifications` VALUES ('37', '1', '29', 'Task information updated! #', '2016-11-08 15:15:11', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('38', '1', '1', 'Task information updated! #', '2016-11-08 15:15:57', '/task/overview/TSKf1848f3', '0');
INSERT INTO `notifications` VALUES ('39', '1', '20', 'Task information updated! #', '2016-11-08 15:15:57', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('40', '1', '26', 'Task information updated! #', '2016-11-08 15:15:57', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('41', '1', '28', 'Task information updated! #', '2016-11-08 15:15:58', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('42', '1', '29', 'Task information updated! #', '2016-11-08 15:15:58', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('43', '1', '1', 'Task information updated! #', '2016-11-08 15:21:11', '/task/overview/TSKf1848f3', '0');
INSERT INTO `notifications` VALUES ('44', '1', '20', 'Task information updated! #', '2016-11-08 15:21:11', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('45', '1', '26', 'Task information updated! #', '2016-11-08 15:21:11', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('46', '1', '28', 'Task information updated! #', '2016-11-08 15:21:11', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('47', '1', '29', 'Task information updated! #', '2016-11-08 15:21:11', '/task/overview/TSKf1848f3', '1');
INSERT INTO `notifications` VALUES ('48', '1', '1', 'Task information updated! #', '2016-11-08 16:12:38', '/task/overview/TSK825cd49', '0');
INSERT INTO `notifications` VALUES ('49', '1', '30', 'Task information updated! #', '2016-11-08 16:12:38', '/task/overview/TSK825cd49', '1');
INSERT INTO `notifications` VALUES ('50', '1', '1', 'Updated personal status for the task #TSK825cd49!', '2016-11-08 16:13:56', '/task/overview/TSK825cd49', '0');
INSERT INTO `notifications` VALUES ('51', '1', '30', 'Updated personal status for the task #TSK825cd49!', '2016-11-08 16:13:56', '/task/overview/TSK825cd49', '1');
INSERT INTO `notifications` VALUES ('52', '1', '1', 'Task information updated! #', '2016-11-08 16:14:26', '/task/overview/TSK825cd49', '0');
INSERT INTO `notifications` VALUES ('53', '1', '30', 'Task information updated! #', '2016-11-08 16:14:26', '/task/overview/TSK825cd49', '1');
INSERT INTO `notifications` VALUES ('54', '1', '21', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 16:58:51', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('55', '21', '1', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:25', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('56', '21', '3', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:25', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('57', '21', '5', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:25', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('58', '21', '6', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:25', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('59', '21', '8', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:25', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('60', '21', '10', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:26', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('61', '21', '11', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:26', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('62', '21', '12', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:26', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('63', '21', '17', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:26', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('64', '21', '23', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:09:26', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('65', '21', '1', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:02', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('66', '21', '3', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:08', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('67', '21', '5', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:08', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('68', '21', '6', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:14', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('69', '21', '8', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:20', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('70', '21', '10', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:26', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('71', '21', '11', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:32', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('72', '21', '12', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:38', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('73', '21', '17', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:43', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('74', '21', '23', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:11:48', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('75', '21', '1', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:28:20', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('76', '21', '3', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:28:34', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('77', '21', '5', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:28:34', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('78', '21', '6', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:28:50', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('79', '21', '8', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:29:04', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('80', '21', '10', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:29:16', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('81', '21', '11', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:29:29', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('82', '21', '12', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:29:41', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('83', '21', '17', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:29:51', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('84', '21', '23', 'A response has been made to the ticket #SUP00083ff!', '2016-11-08 17:29:59', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('85', '1', '1', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('86', '1', '3', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('87', '1', '5', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('88', '1', '6', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('89', '1', '8', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('90', '1', '10', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('91', '1', '11', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('92', '1', '12', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('93', '1', '17', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('94', '1', '23', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('95', '1', '21', 'Status of the ticket #SUP00083ff has been set to closed!', '2016-11-08 17:41:15', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('96', '1', '1', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('97', '1', '3', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('98', '1', '5', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('99', '1', '6', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('100', '1', '8', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('101', '1', '10', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('102', '1', '11', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('103', '1', '12', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:32', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('104', '1', '17', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:33', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('105', '1', '23', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:33', '/ticket/overview/SUP00083ff', '1');
INSERT INTO `notifications` VALUES ('106', '1', '21', 'Status of the ticket #SUP00083ff has been set to reopened!', '2016-11-08 17:41:33', '/ticket/overview/SUP00083ff', '0');
INSERT INTO `notifications` VALUES ('107', '1', '24', 'Your account information was updated!', '2016-11-08 18:50:25', '/account', '1');
INSERT INTO `notifications` VALUES ('108', '1', '24', 'Your account information was updated!', '2016-11-08 18:50:46', '/account', '1');
INSERT INTO `notifications` VALUES ('109', '1', '24', 'Your account information was updated!', '2016-11-08 18:52:02', '/account', '1');

-- ----------------------------
-- Table structure for paypal_payments
-- ----------------------------
DROP TABLE IF EXISTS `paypal_payments`;
CREATE TABLE `paypal_payments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `ppid` varchar(255) NOT NULL,
  `value` int(60) DEFAULT NULL,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paypal_payments
-- ----------------------------
INSERT INTO `paypal_payments` VALUES ('1', '21', '5', 'PAY-07U578769X4658432LAEMFKA', '3461', '2016-10-20 15:10:31');
INSERT INTO `paypal_payments` VALUES ('2', '21', '5', 'PAY-8DY828108X816904TLAEMHYQ', '3461', '2016-10-20 15:15:45');
INSERT INTO `paypal_payments` VALUES ('3', '21', '5', 'PAY-1P077810LS4081915LAEPHKY', '3461', '2016-10-20 18:39:38');
INSERT INTO `paypal_payments` VALUES ('4', '21', '4', 'PAY-77U45652YW223140TLAEPQMQ', '93600', '2016-10-20 18:58:57');
INSERT INTO `paypal_payments` VALUES ('5', '21', '4', 'PAY-7A950633H1476950ULAEPROQ', '93600', '2016-10-20 19:01:14');
INSERT INTO `paypal_payments` VALUES ('6', '21', '4', 'PAY-9MW37527T90157301LAEPTAI', '93600', '2016-10-20 19:04:33');
INSERT INTO `paypal_payments` VALUES ('7', '21', '24', 'PAY-5BD342031T581944RLAEQAGA', '5285', '2016-10-20 19:32:39');

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `type` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `client` int(10) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `finished` date DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of projects
-- ----------------------------
INSERT INTO `projects` VALUES ('1', 'PRO.974410', '2', 'Sportmart Website', '2', '2016-09-26', '2017-03-01', null, '1', 'Rfusce nequeal modnam sodale justov imperd ullamcor magnaves. Arcu erossed euismodd tristi neque liberos llaut egesta. Habitant orcivest musfusce lum ligulain lus. Necsed feugiat tortor oin tempusp sim ntum nislqu roin laut feugiatm tetiam.');
INSERT INTO `projects` VALUES ('3', 'PRO474019', '1', 'Knockout Kickboxing E-Commerce', '3', '2016-09-26', '2016-12-31', null, '1', 'Tempusp id ulla dolorma purusp nullam lus lum iam porttito. Enimphas ac daut condim ulum ipsumae lorem miin. Nec porttit pretium leofusce aliquete cing volutp semper quisut ut turpisp duinulla uscras hac ris noninte.');

-- ----------------------------
-- Table structure for project_activities
-- ----------------------------
DROP TABLE IF EXISTS `project_activities`;
CREATE TABLE `project_activities` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `project` int(10) NOT NULL,
  `text` text,
  `user` int(11) NOT NULL COMMENT 'user because client can be here',
  `date` datetime NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_activities
-- ----------------------------
INSERT INTO `project_activities` VALUES ('8', '3', 'Varius musetiam sed placerat malesu montes ent mattis esent mauris porttit ris feugiatn element sapien aliquama arcused.', '21', '2016-09-26 20:53:16');
INSERT INTO `project_activities` VALUES ('9', '3', 'Requested an estimate.', '21', '2016-09-26 20:54:21');
INSERT INTO `project_activities` VALUES ('10', '3', 'Requested an estimate.', '21', '2016-09-28 19:56:47');
INSERT INTO `project_activities` VALUES ('11', '3', 'Requested an estimate.', '21', '2016-09-28 19:59:03');
INSERT INTO `project_activities` VALUES ('12', '3', 'Created a new estimate, \'Nam justop diamin condim vitae ris nuncsed. Tudin adipis turpisn scras nislae enimsus lum leo pharetra eduis. Isque dolornu temporin ornare facili eratphas uisque etiam eleifend ntum ornareve rutruma iam portad purusd lus euismo.\' to the project.', '1', '2016-09-28 20:10:43');
INSERT INTO `project_activities` VALUES ('13', '3', 'Added a new estimate item, \'dasdsadasdas\' to the project estimate \'#EST05a0377\'.', '1', '2016-09-28 20:11:47');
INSERT INTO `project_activities` VALUES ('14', '3', 'Estimate \'#EST05a0377\' sent to client.', '1', '2016-09-28 20:11:52');
INSERT INTO `project_activities` VALUES ('15', '3', 'Estimate \'#EST05a0377\' was declined and requested a review.', '21', '2016-09-28 20:12:26');
INSERT INTO `project_activities` VALUES ('16', '3', 'Estimate \'#EST05a0377\' sent to client.', '1', '2016-09-28 20:28:23');
INSERT INTO `project_activities` VALUES ('17', '3', 'Estimate \'#EST05a0377\' was declined and requested a review.', '21', '2016-09-28 20:28:49');
INSERT INTO `project_activities` VALUES ('18', '3', 'Estimate \'#EST05a0377\' sent to client.', '1', '2016-09-28 20:29:40');
INSERT INTO `project_activities` VALUES ('19', '3', 'Estimate \'#EST05a0377\' was declined.', '21', '2016-09-28 20:29:57');
INSERT INTO `project_activities` VALUES ('20', '3', 'Created a new invoice item, \'sdfdfaadsf\' to the project invoce \'#INV1731841\'.', '1', '2016-09-28 21:19:50');
INSERT INTO `project_activities` VALUES ('21', '3', 'Estimate \'#EST05a0377\' sent to client.', '1', '2016-09-28 21:21:22');
INSERT INTO `project_activities` VALUES ('22', '3', 'Added a new Payment \'#PAY0492fc6\'.', '1', '2016-09-29 21:52:16');
INSERT INTO `project_activities` VALUES ('23', '3', 'Added a new Payment \'#PAY06c7f32\'.', '1', '2016-09-29 21:52:49');
INSERT INTO `project_activities` VALUES ('24', '3', 'Created a new invoice, \'TEST\' to the project.', '1', '2016-09-30 16:28:03');
INSERT INTO `project_activities` VALUES ('25', '3', 'Created a new invoice item, \'sadasd\' to the project invoce \'#INVb850248\'.', '1', '2016-09-30 16:28:40');
INSERT INTO `project_activities` VALUES ('26', '3', 'Created a new invoice item, \'fasfdffasfdsaf\' to the project invoce \'#INVb850248\'.', '1', '2016-09-30 17:12:41');
INSERT INTO `project_activities` VALUES ('27', '3', 'Updated a invoice item, \'sadasd\' to the project invoce \'#INVb850248\'.', '1', '2016-09-30 19:37:46');
INSERT INTO `project_activities` VALUES ('28', '3', 'Created a new estimate, \'Tiam hac lectusin puru\' to the project.', '1', '2016-09-30 20:23:41');
INSERT INTO `project_activities` VALUES ('29', '3', 'Updated a estimate item, \'item 01\' to the project estimate \'#EST26ced60\'.', '1', '2016-09-30 20:27:31');
INSERT INTO `project_activities` VALUES ('30', '3', 'Estimate \'#EST26ced60\' sent to client.', '1', '2016-09-30 20:28:10');
INSERT INTO `project_activities` VALUES ('31', '3', 'Estimate \'#EST26ced60\' was accepted and invoiced as \'#INVfd306d3\' ', '21', '2016-09-30 20:29:33');
INSERT INTO `project_activities` VALUES ('32', '3', 'Estimate \'#EST26ced60\' was accepted and invoiced as \'#INV5c56265\' ', '21', '2016-09-30 20:32:35');
INSERT INTO `project_activities` VALUES ('33', '3', 'Uploaded \'teste\' to the project\'s files.', '1', '2016-09-30 20:54:46');
INSERT INTO `project_activities` VALUES ('34', '3', 'Removed \'teste\' from the project\'s files.', '1', '2016-09-30 20:55:04');
INSERT INTO `project_activities` VALUES ('35', '3', 'Added a new Payment \'#PAY7d41b04\'.', '1', '2016-09-30 21:42:16');
INSERT INTO `project_activities` VALUES ('36', '3', 'Updated project status!', '5', '2016-10-07 19:45:33');
INSERT INTO `project_activities` VALUES ('37', '3', 'Updated project status!', '5', '2016-10-07 19:45:41');
INSERT INTO `project_activities` VALUES ('38', '3', '', '21', '2016-10-18 14:49:56');
INSERT INTO `project_activities` VALUES ('39', '3', 'Vitaef sapiendo telluss afusce quis justop oin oin duifusce liberout sellus uis bibendum dis convalli hendre quat.', '1', '2016-10-21 14:29:39');
INSERT INTO `project_activities` VALUES ('40', '3', 'Requested an estimate.', '21', '2016-10-21 21:12:09');
INSERT INTO `project_activities` VALUES ('41', '3', '(159) 749 5286', '1', '2016-10-26 13:48:36');
INSERT INTO `project_activities` VALUES ('42', '3', 'Uploaded \'ddfdggf\' to the project\'s files.', '1', '2016-10-26 13:51:25');
INSERT INTO `project_activities` VALUES ('43', '3', 'Removed \'ddfdggf\' from the project\'s files.', '1', '2016-10-26 13:58:35');
INSERT INTO `project_activities` VALUES ('44', '3', 'Created a new estimate, \'0sdfjashfjsd\' to the project.', '1', '2016-10-26 14:34:53');
INSERT INTO `project_activities` VALUES ('45', '3', 'Estimate \'#ESTc1f7501\' sent to client.', '1', '2016-10-26 14:35:16');
INSERT INTO `project_activities` VALUES ('46', '3', 'Created a new invoice, \'NEW!\' to the project.', '1', '2016-10-26 14:37:21');
INSERT INTO `project_activities` VALUES ('47', '3', 'Created a new invoice item, \'item\' to the project invoce \'#INV33ec650\'.', '1', '2016-10-26 14:37:48');
INSERT INTO `project_activities` VALUES ('48', '3', 'Added a new Payment \'#PAYe5b87c1\'.', '1', '2016-10-26 14:40:21');
INSERT INTO `project_activities` VALUES ('49', '3', 'Requested an estimate \'#REQ8580c48\'.', '21', '2016-11-09 16:09:27');

-- ----------------------------
-- Table structure for project_estimates
-- ----------------------------
DROP TABLE IF EXISTS `project_estimates`;
CREATE TABLE `project_estimates` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(60) NOT NULL,
  `project` int(10) NOT NULL,
  `status` int(10) NOT NULL COMMENT 'open , invoiced , reviesd , accepted ',
  `title` varchar(255) DEFAULT NULL,
  `issue` datetime DEFAULT NULL,
  `due` datetime DEFAULT NULL,
  `recurring` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of project_estimates
-- ----------------------------
INSERT INTO `project_estimates` VALUES ('3', 'EST05a0377', '3', '2', 'Ger sapien nostra', '2016-09-28 00:00:00', '2016-09-30 00:00:00', '1');
INSERT INTO `project_estimates` VALUES ('4', 'EST26ced60', '3', '3', 'Tiam hac lectusin puru', '2016-09-27 00:00:00', '2016-09-30 00:00:00', '0');

-- ----------------------------
-- Table structure for project_files
-- ----------------------------
DROP TABLE IF EXISTS `project_files`;
CREATE TABLE `project_files` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `project` int(10) NOT NULL,
  `user` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(60) NOT NULL,
  `uploaded` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_files
-- ----------------------------

-- ----------------------------
-- Table structure for project_invoices
-- ----------------------------
DROP TABLE IF EXISTS `project_invoices`;
CREATE TABLE `project_invoices` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(60) NOT NULL,
  `project` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `issue` datetime DEFAULT NULL,
  `due` datetime DEFAULT NULL,
  `recurring` int(10) NOT NULL,
  `last_recursion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_invoices
-- ----------------------------
INSERT INTO `project_invoices` VALUES ('3', 'INV2641647', '3', '5', 'Amet lacusp lum eleifen', '2016-08-31 00:00:00', '2016-08-27 00:00:00', '0', null);
INSERT INTO `project_invoices` VALUES ('4', 'INV6834171', '3', '3', 'Sit suscip felis aesent liberout arcualiq aesent', '2016-09-06 00:00:00', '2016-09-10 00:00:00', '0', null);
INSERT INTO `project_invoices` VALUES ('5', 'INV1731841', '3', '3', 'Quamsusp ger natis ullamcor', '2016-09-04 00:00:00', '2016-09-06 00:00:00', '0', null);
INSERT INTO `project_invoices` VALUES ('6', 'INV2148713', '3', '4', 'Volutpat estmorbi lacus nas', '2016-09-06 00:00:00', '2016-09-06 00:00:00', '0', null);
INSERT INTO `project_invoices` VALUES ('12', 'INVIFN', '3', '3', 'Egesta erdum disse msed enas bibendu', '2016-08-08 00:00:00', '2016-09-01 00:00:00', '1', '2016-11-03 13:26:14');
INSERT INTO `project_invoices` VALUES ('24', 'RECbbfbf5c', '3', '3', 'Recursion from #INVIFN ', '2016-09-30 19:45:34', '2016-10-30 19:45:34', '0', '2016-09-30 19:45:34');
INSERT INTO `project_invoices` VALUES ('25', 'INV5c56265', '3', '3', 'Tiam hac lectusin puru', '2016-09-27 00:00:00', '2016-09-30 00:00:00', '0', '2016-09-30 00:00:00');
INSERT INTO `project_invoices` VALUES ('26', 'RECc3bd276', '3', '1', 'Recursion from #INVIFN ', '2016-11-03 13:26:14', '2016-12-03 13:26:14', '0', '2016-11-03 13:26:14');
INSERT INTO `project_invoices` VALUES ('27', 'RECc6d3b27', '3', '1', 'Recursion from #INVIFN ', '2016-11-03 13:26:14', '2016-12-03 13:26:14', '0', '2016-11-03 13:26:14');
INSERT INTO `project_invoices` VALUES ('28', 'REC2db67c3', '3', '1', 'Recursion from #INVIFN ', '2016-11-03 13:26:14', '2016-12-03 13:26:14', '0', '2016-11-03 13:26:14');
INSERT INTO `project_invoices` VALUES ('29', 'REC4647cae', '3', '3', 'Recursion from #INVIFN ', '2016-11-03 13:26:14', '2016-12-03 13:26:15', '0', '2016-11-03 13:26:14');

-- ----------------------------
-- Table structure for project_recursions
-- ----------------------------
DROP TABLE IF EXISTS `project_recursions`;
CREATE TABLE `project_recursions` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `project` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  `due` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `last_recursion` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_recursions
-- ----------------------------

-- ----------------------------
-- Table structure for project_types
-- ----------------------------
DROP TABLE IF EXISTS `project_types`;
CREATE TABLE `project_types` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_types
-- ----------------------------
INSERT INTO `project_types` VALUES ('1', 'Web Development');
INSERT INTO `project_types` VALUES ('2', 'Graphic Design');
INSERT INTO `project_types` VALUES ('3', 'Marketing');
INSERT INTO `project_types` VALUES ('4', 'Social Media');
INSERT INTO `project_types` VALUES ('5', 'Others');
INSERT INTO `project_types` VALUES ('6', 'Mobile Development');

-- ----------------------------
-- Table structure for request_codes
-- ----------------------------
DROP TABLE IF EXISTS `request_codes`;
CREATE TABLE `request_codes` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `code` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `expires` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of request_codes
-- ----------------------------

-- ----------------------------
-- Table structure for stripe_payments
-- ----------------------------
DROP TABLE IF EXISTS `stripe_payments`;
CREATE TABLE `stripe_payments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `spid` varchar(255) NOT NULL,
  `value` int(60) DEFAULT NULL,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of stripe_payments
-- ----------------------------
INSERT INTO `stripe_payments` VALUES ('1', '21', '25', 'ch_196kNsF1kx10X3ORae0Mzmb6', '11174', '2016-10-20 19:29:40');
INSERT INTO `stripe_payments` VALUES ('2', '21', '12', 'ch_196klCF1kx10X3OR5JK8ytZZ', '5544', '2016-10-20 19:53:46');
INSERT INTO `stripe_payments` VALUES ('3', '21', '12', 'ch_196kqPF1kx10X3ORunyxx8Lb', '5544', '2016-10-20 19:59:09');
INSERT INTO `stripe_payments` VALUES ('4', '21', '12', 'ch_1971BAF1kx10X3ORKU9OYe0q', '5549', '2016-10-21 13:25:38');
INSERT INTO `stripe_payments` VALUES ('5', '21', '12', 'ch_1971JDF1kx10X3OR730gNRvy', '5549', '2016-10-21 13:33:56');
INSERT INTO `stripe_payments` VALUES ('6', '21', '5', 'ch_1971j8F1kx10X3ORundMfIDa', '3464', '2016-10-21 14:00:44');
INSERT INTO `stripe_payments` VALUES ('7', '21', '24', 'ch_1972d7F1kx10X3ORboRKXIQI', '5285', '2016-10-21 14:58:34');
INSERT INTO `stripe_payments` VALUES ('8', '21', '4', 'ch_1988c9F1kx10X3OR8AfYXU0m', '93960', '2016-10-24 15:34:02');
INSERT INTO `stripe_payments` VALUES ('9', '21', '12', 'ch_198BrwF1kx10X3ORWcI7BBjB', '5565', '2016-10-24 19:02:34');
INSERT INTO `stripe_payments` VALUES ('10', '21', '12', 'ch_198CxkF1kx10X3ORGSDXyXVu', '5565', '2016-10-24 20:12:38');
INSERT INTO `stripe_payments` VALUES ('11', '21', '4', 'ch_198DBYF1kx10X3ORuH8Skbvf', '93960', '2016-10-24 20:26:54');
INSERT INTO `stripe_payments` VALUES ('12', '21', '5', 'ch_198TuJF1kx10X3ORnmCcbzZD', '3477', '2016-10-25 14:18:10');
INSERT INTO `stripe_payments` VALUES ('13', '21', '24', 'ch_198USHF1kx10X3OROb62aDvH', '5285', '2016-10-25 14:53:16');
INSERT INTO `stripe_payments` VALUES ('14', '21', '29', 'ch_19Bpu7F1kx10X3ORkN6mCyVp', '5285', '2016-11-03 19:23:38');
INSERT INTO `stripe_payments` VALUES ('15', '21', '25', 'ch_19CDPxF1kx10X3ORLN9Rm4eE', '11338', '2016-11-04 20:30:03');

-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `project` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `deadline` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `assigned` int(10) DEFAULT NULL,
  `completed` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tasks
-- ----------------------------
INSERT INTO `tasks` VALUES ('2', 'TSKcfcea86', '3', 'Orcivest ris ris nulla', 'Massacra ultric onec metusdo nulla ulum vulput. Orcivest ris ris nulla ligulain cursus uam. Maecenas nislae mus convall egetlor mauris quam varius sapien eroscu cursusve llam sodale massa orem ris llam nislnam convall magnis.', '2016-10-05 00:00:00', '2', '2016-09-30 22:07:50', '1', null);
INSERT INTO `tasks` VALUES ('3', 'TSK825cd49', '3', 'Lectusn curabitu ', 'Lectusn curabitu esque liquam llam oin mauris nibhnul rutrumnu erat. Vel lus lacusnam felisq sapienna pulvinar. Odio loremn onec justov enas lum. Venenat elemen que tristi tortor liberos volutp cras loremin nec sedlorem nas.', '2016-11-07 00:00:00', '3', '2016-10-26 13:50:30', '1', null);
INSERT INTO `tasks` VALUES ('4', 'TSKcd31d7a', '3', 'Lacini enas cursusve ', 'Teger condim isse inproin tempor oin. Lacini enas cursusve nunc bibend rhoncus. Platea iumsed vulput orciut lisis oin aptent faucibus nam. Pharetr laciniai ullamcor lum potenti ipsump uscras leopelle tortor orci elit vitaef itor.', '2016-11-09 00:00:00', '3', '2016-11-04 12:51:24', '27', null);
INSERT INTO `tasks` VALUES ('5', 'TSKf1848f3', '3', 'Aeger estsusp duis.', 'Leo nullam turpisn velitsed quam cras. Ut facilis blandit nibhnull ultrices que lus mattisae ultricie. Quam rhoncus aesent cursusve eger neque laciniai ger sedinteg ras leocur vestib semper amus rutrum scras porttit tesque id', '2016-11-06 00:00:00', '3', '2016-11-04 19:20:00', '1', null);

-- ----------------------------
-- Table structure for task_collaborators
-- ----------------------------
DROP TABLE IF EXISTS `task_collaborators`;
CREATE TABLE `task_collaborators` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `task` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_collaborators
-- ----------------------------
INSERT INTO `task_collaborators` VALUES ('8', '2', '1', '2');
INSERT INTO `task_collaborators` VALUES ('10', '2', '24', '1');
INSERT INTO `task_collaborators` VALUES ('12', '3', '1', '2');
INSERT INTO `task_collaborators` VALUES ('15', '3', '30', '1');
INSERT INTO `task_collaborators` VALUES ('16', '4', '1', '2');
INSERT INTO `task_collaborators` VALUES ('19', '4', '24', '1');
INSERT INTO `task_collaborators` VALUES ('20', '4', '30', '1');
INSERT INTO `task_collaborators` VALUES ('21', '5', '1', '2');
INSERT INTO `task_collaborators` VALUES ('22', '5', '20', '1');
INSERT INTO `task_collaborators` VALUES ('23', '5', '26', '1');
INSERT INTO `task_collaborators` VALUES ('24', '5', '28', '1');
INSERT INTO `task_collaborators` VALUES ('25', '5', '29', '1');
INSERT INTO `task_collaborators` VALUES ('26', '4', '22', '1');

-- ----------------------------
-- Table structure for task_responses
-- ----------------------------
DROP TABLE IF EXISTS `task_responses`;
CREATE TABLE `task_responses` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `task` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  `text` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_responses
-- ----------------------------
INSERT INTO `task_responses` VALUES ('1', '2', '1', 'test', '2016-10-25 20:56:25');
INSERT INTO `task_responses` VALUES ('2', '2', '1', 'Tellus elementu ornare sisut bulum uis lus pretiu nibhphas eratphas. Nonmorbi nequesed ecenas nec enas id lla lectus ultric. Proin enimphas maurisve quamsusp malesu pulvina nequen bus. Isised miquis accumsan sodale vitaef metuscra arcucura mussed. Ac libero aliquete mauris nunc lacini teger vehicula dolornu onec eger auctorcr aesent.', '2016-10-26 13:12:48');
INSERT INTO `task_responses` VALUES ('3', '5', '1', 'Vitaenul rutrumnu mattisae roin uisque tur. Llam oin varius nullam tempusp porta nostra quispr. Lum lum facili enimsed duifusce sisut ligula. Uada tfusce enimlo abitur lus massama culus quam. Auris eclass elitphas antenull orciduis onec morbi iennam gravida duinulla gravidas nas sapiendo feugiat varius ullam dum commodo loremin.', '2016-11-07 18:59:01');
INSERT INTO `task_responses` VALUES ('4', '5', '1', 'Scum ger primis mauris scras liberoa condim illa metus consec. Sapienma rosed naeos et nean ctetur aesent mus. Metusves noninte duis oin oin magna aliquet teger. Tortor lacini urient feugiat lectusa gsed. Eratetia dictumst curae laoreet lectusn teger mauris cursusve que nequen ullam velit tempor ligulain esque musfusce.', '2016-11-07 19:00:52');
INSERT INTO `task_responses` VALUES ('5', '5', '1', 'Posuere blandit tfusce sed potent que nunccras quamnunc malesuad urient. Suscipit maurisin leoetiam dictum sollic portamor ipsump magnaves aliquam abitur. Leocras ligulain accumsan ies atein metusd nibhphas. Sfusce litora dapibus noninte quisque mauris utcras oin cras. Laoree volutpat ullam quamin roin afusce rissed malesu molesti duinulla loremn ulla.', '2016-11-07 20:24:42');

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `image` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of team
-- ----------------------------
INSERT INTO `team` VALUES ('1', '1', 'Hudson Nicoletti', '+55-43-9679-9631', 'd2c0474dd10c.jpg');
INSERT INTO `team` VALUES ('20', '3', 'Jhon Doe', '+1-202-555-0148', '092c9d5d8747.jpg');
INSERT INTO `team` VALUES ('21', '5', 'Leandro Onofre', '+1-404-555-0174', '031595e25efe.jpg');
INSERT INTO `team` VALUES ('22', '6', 'Andrew Hughes', '+1-225-555-0149', '14bd12eefd02.jpg');
INSERT INTO `team` VALUES ('24', '8', 'Kyle Freeman', '+1-614-555-0188', 'ad54fe48d8b6.jpg');
INSERT INTO `team` VALUES ('26', '10', 'Raymond Harper', '+1-789-346-652', '093ba2d137b8.jpg');
INSERT INTO `team` VALUES ('27', '11', 'Mark Hunter', '+1-614-555-0188', '1bbdbbe4894f.jpg');
INSERT INTO `team` VALUES ('28', '12', 'Timothy Martinez', '+55-43-7777-9876', '064f0769df6d.jpg');
INSERT INTO `team` VALUES ('29', '17', 'Vincent Wells', '', null);
INSERT INTO `team` VALUES ('30', '23', 'Lennard Martin', '+55 44 9999-9999', 'c428e5310f0e.jpg');

-- ----------------------------
-- Table structure for tickets
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `unique` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `category` int(10) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tickets
-- ----------------------------
INSERT INTO `tickets` VALUES ('1', '21', 'SUP00083ff', 'Acum teger eleifend oin aptent element esent felisq', '2', '1', '2016-11-08 17:41:32', '2016-10-04 21:45:47');

-- ----------------------------
-- Table structure for ticket_categories
-- ----------------------------
DROP TABLE IF EXISTS `ticket_categories`;
CREATE TABLE `ticket_categories` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket_categories
-- ----------------------------
INSERT INTO `ticket_categories` VALUES ('1', 'General');
INSERT INTO `ticket_categories` VALUES ('2', 'Bug Report');
INSERT INTO `ticket_categories` VALUES ('3', 'Invoice');

-- ----------------------------
-- Table structure for ticket_responses
-- ----------------------------
DROP TABLE IF EXISTS `ticket_responses`;
CREATE TABLE `ticket_responses` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `ticket` int(10) NOT NULL,
  `uid` int(10) NOT NULL,
  `text` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket_responses
-- ----------------------------
INSERT INTO `ticket_responses` VALUES ('1', '1', '21', 'Augueph tincid oin nec faucibus aesent eratquis ent. Cursusve luctus egesta leocras liquam faucibus. Ullam loremnul arcualiq nullam montes lum cras potent sodale egestasm lectusn uam duis nostra nibhnul aliquet arcused sedlorem dumin tur.', '2016-10-04 21:45:47');
INSERT INTO `ticket_responses` VALUES ('2', '1', '1', 'Erat ridicul orciduis turpisp egetlor vivamus iquam natoque congue. Naeos sque utcras leopelle duis mauris. Malesu molesti nas diam consequa lisis puruscra. Acum rhoncusv in lus turpisve nec. Vulput congue magnapro porta metuscra oin. Quis metusqui egestasm sceler magnap nec onec ullamco portamor nibhphas felis morbi ndisse viverr.', '2016-10-04 21:48:35');
INSERT INTO `ticket_responses` VALUES ('3', '1', '1', 'Uris porta cursus eget ipsumma turpisve natis disse arcucura. Dictumst elitphas vehicula massacra nascetur ent lum penatib. Ies magnis quam urnaut cras facili imperd vel nulla. Lum ligulam rhoncusv nislae aliquam eratfus. Accums sed accumsan euismo ullam vulput afusce velit nonmorbi iaculis nulla congue sse commodo llus nec.', '2016-10-26 13:13:38');
INSERT INTO `ticket_responses` VALUES ('4', '1', '1', 'Alterações feitas.', '2016-10-26 13:14:10');
INSERT INTO `ticket_responses` VALUES ('5', '1', '1', 'f', '2016-10-26 13:16:19');
INSERT INTO `ticket_responses` VALUES ('6', '1', '1', 'qq', '2016-10-26 13:17:36');
INSERT INTO `ticket_responses` VALUES ('7', '1', '21', 'Esent liquam sapienv miquis ies snam sagittis rhoncusv nuncsed. Mauris commodo luctus semnunc tempor afusce euismod sceleris tur. Nean llam sedlorem turpisp volutp facili vivamus auguesed lum justo. Estnulla lectuss dumin onec vulput ntum. Iquam sfusce nisi rerit hac purus semper convall arcucura faucibus metusves egestas arcuduis lacini.', '2016-11-08 16:48:42');
INSERT INTO `ticket_responses` VALUES ('8', '1', '21', 'Esent liquam sapienv miquis ies snam sagittis rhoncusv nuncsed. Mauris commodo luctus semnunc tempor afusce euismod sceleris tur. Nean llam sedlorem turpisp volutp facili vivamus auguesed lum justo. Estnulla lectuss dumin onec vulput ntum. Iquam sfusce nisi rerit hac purus semper convall arcucura faucibus metusves egestas arcuduis lacini.', '2016-11-08 16:49:33');
INSERT INTO `ticket_responses` VALUES ('9', '1', '21', 'Nislqu inceptos mauris mattis vitae loreme ger nequesed. Laoreet ris leocras que justonul ullam feugiatn commodo nam enimsus. Iain et maurisma eger tortorp ras. Ulla lacusnam tdonec posuered leocur rhoncus sed. Turpisp porttito nislin ent lus pretiu morbi esque blandit conubia lacusp sellus mipraese congue aptent ent enim.', '2016-11-08 16:50:19');
INSERT INTO `ticket_responses` VALUES ('10', '1', '21', 'Proin imperd bibendu aenean quisque sed. Quisut bibend musfusce egestas tesque liquam feugiatm. Aesent risusm maurisd gravida oin ipsumae varius auguesed eu itnunc. Tempus metusves malesuad nequenu penatib erdum adipis sit que. Nunc mauris bulum varius sed tate lum sellus telluset urnaut ellus laoree iam massacra culus morbi.', '2016-11-08 16:52:43');
INSERT INTO `ticket_responses` VALUES ('11', '1', '1', 'Quam faucibus tristiq libero amus tiam. Rutrumnu insuspen duis abitur nas pellente varius. Orbi magna leocras nibhnull ullamco mauris luctus urna lobortis. Faucibu dictum vitae mattiss ultricie ies nislin sociosqu. Ris feugiat nean nullam pharetr euismod. Oin ridicul onec amus aduis nibh netus feugiatn rhoncus nislsed ipsumin snam.', '2016-11-08 16:53:04');
INSERT INTO `ticket_responses` VALUES ('12', '1', '21', 'Nec uam pulvinar eduis orcivest telluset usce eget justonul esent. Ante dolordo necmae laoree estmorbi egestas anunc. Blandit utcras amus ligula ellus loremin aliquete mauris ris etsed. Vitaenul ris ecenas orciduis molesti convalli cras. Himena snam sellus que adipisci accums isised metusd sapiendo roin itnunc himena orciut lus.', '2016-11-08 16:54:20');
INSERT INTO `ticket_responses` VALUES ('13', '1', '1', 'fs', '2016-11-08 16:58:51');
INSERT INTO `ticket_responses` VALUES ('14', '1', '21', 'Iaculi purus entum egestas blandit odio quam. Quamphas arcuduis sollic sollici cursusve nulla estnulla magna scum. Laoreet que posuered sed disse liberos bibend atein elit. Tempus utcras duifusce lectusa ent idnulla. Augue loreme quisut iaculis ulum roin lectuss estmorbi libero quam facilisi sceleris ibulum dolordo varius vitaenu afusce.', '2016-11-08 17:01:38');
INSERT INTO `ticket_responses` VALUES ('15', '1', '21', 'Urnain dictum adipis facilis enimdon llaut. Tsed aliquet enas lacusaen ultricie congue. Blandit orcivest laciniai puruscra nec ipsump. Leofusce curabitu facilis isque elitphas esent min laoree nisi ger enim bulum quispr nuncnunc euismo vulput oin.', '2016-11-08 17:09:25');
INSERT INTO `ticket_responses` VALUES ('16', '1', '21', 'Teger diampr diam teger ligula uscras egestas turpisp parturi rutrum. Tristi imperd aenean nunc massan velitsed nislnam iaculis atein. Augue ris nulla nunccras quat facilis arcuduis ullam sque gravida potent rsed sque montes aliquam ipsumves.', '2016-11-08 17:11:02');
INSERT INTO `ticket_responses` VALUES ('17', '1', '21', 'Cras uscras nislsed maurisin enimsus uis sim lla. Augueph telluss ullamcor miin nas anteduis iam facili. Sellus eger metusves rfusce etsed egestas habitant. Molestie urnaut teger noninte loremnul uis quis maurisve mus ad convall onec.', '2016-11-08 17:28:20');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(255) NOT NULL,
  `permission` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '$2y$10$gabEFku4DUe48sMZBAQnd.q0jxa4W1FBvtbBgYk04RTKgbsu6Tk4q', 'desenvolvimento@dzoe.com.br', '3');
INSERT INTO `users` VALUES ('5', 'criacao', '$2y$10$VaNrAuzM.bCZSO2EhlPGgOmGKBjnc7cWN3QJJpgCsiPR1L2QXGOhq', 'criacao@dzoe.com.brd', '2');
INSERT INTO `users` VALUES ('6', 'hughes', '$2y$10$laNL8N9tDb.Rp3.ZiauyH.3v0avoKv0sdC8eDfXqwMgIioYJNqR3W', 'hughes@hughes.com', '2');
INSERT INTO `users` VALUES ('8', 'freeman', '$2y$10$N57GGtWvbTnqslljBYywyucOxWZfZnF.Iuhq5nKX8awU/jRqiImwK', 'freeman@freeman.com', '2');
INSERT INTO `users` VALUES ('10', 'harper', '$2y$10$89g2QFkwXr0Os4TiRRiz7Op8N9IHFWBqimjX.rJSNr2a5/PZ9sKzO', 'harper@harper.com', '2');
INSERT INTO `users` VALUES ('11', 'hunter', '$2y$10$Zjep56ej4/f/pGe3Utsn9eNt.NnkWbUldDeiqvj5V4ZiUntJCO6Ba', 'mark@hunter.com', '2');
INSERT INTO `users` VALUES ('12', 'martinez', '$2y$10$uBM.5W7fBS8JUbu65OZAn.AIe6UMlJ5.YkIQ/sv8c9Yh.q6sKYWOi', 'timothy@martinez.com', '3');
INSERT INTO `users` VALUES ('13', 'Danielle', '$2y$10$I0L.1302Q2VGjHDZNImdj.60CJ2T2j/vgDe.Ee5eqOHBzKxYsddeK', 'Danielle@Sanchez.com', '1');
INSERT INTO `users` VALUES ('14', 'kathy', '$2y$10$eWUQ6G0KiMEjuuKFeL8tEuF7s3BzBZr8VKsKE9SrZ8YMgt89mwrJW', 'Kathy@gmail.com', '1');
INSERT INTO `users` VALUES ('15', 'bryan', '$2y$10$fM7TikgtMlQwJxpswqDuIOfYWqx.irFKgdy0oPcLlTBwEkQOWXeHu', 'BryanPrice@gmail.com', '1');
INSERT INTO `users` VALUES ('16', 'ethan', '$2y$10$6SR24838MajSB73awPoqP.t08TAkQgAjGYk4EePvDGW3mvXj.vcza', 'EthanParker@parker.com', '1');
INSERT INTO `users` VALUES ('17', 'vincent', '$2y$10$xnG.KXc4nSks0IL2QJcod.qkz6c9iJ3IbMNEci8EmgUFrF7SDsqqy', 'VincentWells@gmail.com', '2');
INSERT INTO `users` VALUES ('18', 'Hayes', '$2y$10$9lyCw5.4M1/6M.Ti8/hz8uZEz12wFesxuTqTxv6kt.d4AaYeVk7yu', 'EthanHayes@gmail.com', '1');
INSERT INTO `users` VALUES ('19', 'Douglas', '$2y$10$9gXMJUvlt24Y6DuWPI5KPeY/Gq6R8z/zOYsyMGQCTBJOGNY3ytHVS', 'DouglasFowler@gmail.com', '1');
INSERT INTO `users` VALUES ('20', 'Katherine', '$2y$10$Ou1sTfvka8RsLE64Ux3XFO9rtVyUfiVXktzRuSYCEUdIoqTtGnn7.', 'KatherinePena@gmail.com', '1');
INSERT INTO `users` VALUES ('21', 'Daniel', '$2y$10$vQ0.xaShu1JnvrVY4YVRl.F1Pug8O0oqWJI74Cs5Zj/rT7AlqZNOq', 'DanielNewman@gmail.com', '1');
INSERT INTO `users` VALUES ('22', 'Dominic', '$2y$10$LZEeUnPstnSh4JEZ.kok8OEbJLdpehjg8d2p2R9sJPUgqWFZkoiJG', 'DominicMuller@yahoo.com', '1');
INSERT INTO `users` VALUES ('23', 'Lennard', '$2y$10$E4g9KSzXn15NJztYUfyjXO22d/5p04SnucD5WsCXPOCDSAIS6xwxS', 'Lennard@Martin.com', '2');
INSERT INTO `users` VALUES ('24', 'Joshua', '$2y$10$PiN2jv0eoBY4RzQHjm5gCOlCIhT/8YzCAEVSHnMTuW8fH0B1LnYP2', 'yoderivut@maileme101.com', '1');

-- ----------------------------
-- Event structure for Check Project Recursions
-- ----------------------------
DROP EVENT IF EXISTS `Check Project Recursions`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `Check Project Recursions` ON SCHEDULE EVERY 1 DAY STARTS '2016-09-29 14:26:41' ON COMPLETION NOT PRESERVE ENABLE DO INSERT INTO project_recursions (invoice,project,status,due,last_recursion) SELECT _,project,status,due,last_recursion from project_invoices where recurring = 1 and  NOW() >= last_recursion + INTERVAL 1 MONTH
;;
DELIMITER ;

-- ----------------------------
-- Event structure for Request Codes Reset
-- ----------------------------
DROP EVENT IF EXISTS `Request Codes Reset`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `Request Codes Reset` ON SCHEDULE EVERY 1 MINUTE STARTS '2016-09-26 16:28:15' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM request_codes WHERE NOW() <= expires
;;
DELIMITER ;
