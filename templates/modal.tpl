<div id="dynamic-modal" class="modal modal-fixed-footer {{#bottom}}bottom-sheet{{/bottom}}">

  {{#form}}
    <form action="{{action}}" method="post" accept-charset="utf8" role="AjaxForm" enctype="multipart/form-data">
  {{/form}}

	<div class="modal-content">
		<h4>{{header}}</h4>
    <br>
    {{#card}}
    <div class="row">
	    <div class="col s12">
		    <div class="card-panel blue-grey darken-4">
          {{{content}}}
		    </div>
	    </div>
    </div>
    {{/card}}

    <div class="row">
    {{#inputs}}
      {{^file}}
      {{#switch}}
      <span>{{{label}}}</span><br><br>
      <div class="switch">
        <label>
          No
          {{{input}}}
          <span class="lever"></span>
          Yes
        </label>
      </div>
      {{/switch}}
      {{^switch}}
      <div class="input-field col s12 {{cl}}">
        {{{input}}}
        <label {{#active}}class="active"{{/active}} >{{{label}}}</label>
      </div>
      {{/switch}}
      {{/file}}
      {{#break}}<hr>{{/break}}
    {{/inputs}}
    </div>
    {{#inputs}}
      {{#file}}
      <div class="file-field input-field">
      	<div class="btn">
      		<span>{{{label}}}</span>
      		{{{input}}}
      	</div>
      	<div class="file-path-wrapper">
      		<input class="file-path validate" type="text">
      	</div>
      </div>
      {{/file}}
    {{/inputs}}

    {{#info}}
      <table class="responsive-table">
        <thead>
          <tr>
            {{#titles}} <th class="color: #26a69a">{{{title}}}</th> {{/titles}}
          </tr>
        </thead>
        <tbody>
          <tr>
            {{#contents}} <td>{{{content}}}</td> {{/contents}}
          </tr>
        </tbody>
      </table>
    {{/info}}
  </div>

	<div class="modal-footer">
    {{#form}}<button type="submit" class="btn blue-grey darken-4 waves-effect waves-light">Submit</button>{{/form}}
    <button type="button" class="btn blue-grey darken-1 modal-action modal-close waves-effect waves-light">Cancel</button>
	</div>

  {{#form}}
    </form>
  {{/form}}
</div>
