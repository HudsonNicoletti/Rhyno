{{#nav}}
<li {{#active}}class="active"{{/active}} >
  <a href="{{href}}">{{label}}</a>
</li>
{{/nav}}
