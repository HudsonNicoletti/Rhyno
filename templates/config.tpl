[database]
adapter   = Mysql
host      = {{{db_host}}}
username  = {{{db_user}}}
password  = {{{db_pass}}}
dbname    = {{{db_name}}}
charset   = utf8

[mail]
host      = {{{mail_host}}}
username  = {{{mail_user}}}
password  = {{{mail_pass}}}
security  = {{{mail_secu}}}
port      = {{{mail_port}}}
charset   = UTF-8
email     = {{{mail_mail}}}
name      = {{{mail_name}}}

[application]
baseUri          = {{{app_base}}}
logo             = {{{app_logo}}}
title            = {{{app_title}}}
token            = {{{app_token}}}
currency         = {{{app_currency}}}
date_format      = {{{app_date}}}
datetime_format  = {{{app_datetime}}}

[paypal]
active    = {{paypal}}
mode      = {{paypal_mode}}
clientID  = {{paypal_clientID}}
secret    = {{paypal_secret}}

[stripe]
active      = {{stripe}}
secret      = {{stripe_secret}}
publishable = {{stripe_publishable}}

[taxes]
active  = {{taxes}}
tax     = {{tax}}
type    = {{taxtype}}
taxtime = {{taxtime}}

[permissions]
user    = 0
client  = 1
team    = 2
admin   = 3

[team_crud]
members_page    = {{members_page}}
members_create  = {{members_create}}
members_update  = {{members_update}}
members_delete  = {{members_delete}}

tickets_page    = {{tickets_page}}
tickets_update  = {{tickets_update}}
tickets_delete  = {{tickets_delete}}

clients_page    = {{clients_page}}
clients_create  = {{clients_create}}
clients_update  = {{clients_update}}
clients_delete  = {{clients_delete}}

projects_page   = {{projects_page}}
projects_create = {{projects_create}}
projects_update = {{projects_update}}
projects_delete = {{projects_delete}}

projecttasks_page   = {{projecttasks_page}}
projecttasks_create = {{projecttasks_create}}
projecttasks_update = {{projecttasks_update}}
projecttasks_delete = {{projecttasks_delete}}

projectfiles_page   = {{projectfiles_page}}
projectfiles_create = {{projectfiles_create}}
projectfiles_delete = {{projectfiles_delete}}

projectrequests_page   = {{projectrequests_page}}
projectrequests_delete = {{projectrequests_delete}}

projectestimates_page   = {{projectestimates_page}}
projectestimates_send   = {{projectestimates_send}}
projectestimates_create = {{projectestimates_create}}
projectestimates_update = {{projectestimates_update}}
projectestimates_delete = {{projectestimates_delete}}

projectinvoices_page   = {{projectinvoices_page}}
projectinvoices_create = {{projectinvoices_create}}
projectinvoices_update = {{projectinvoices_update}}
projectinvoices_delete = {{projectinvoices_delete}}

projectpayments_page   = {{projectpayments_page}}
projectpayments_create = {{projectpayments_create}}
projectpayments_delete = {{projectpayments_delete}}

[client_crud]
tickets_page    = {{clienttickets_page}}
tickets_create  = {{clienttickets_create}}
tickets_reopen  = {{clienttickets_reopen}}
tickets_delete  = {{clienttickets_delete}}

projectfiles_page    = {{clientprojectfiles_page}}
projectfiles_create  = {{clientprojectfiles_create}}
projectfiles_delete  = {{clientprojectfiles_delete}}

projectrequests_page    = {{clientprojectrequests_page}}
projectrequests_create  = {{clientprojectrequests_create}}
projectrequests_delete  = {{clientprojectrequests_delete}}

[team_notification]
member_info_email          = {{member_info_email}}
member_info_inner          = {{member_info_inner}}
member_permission_email    = {{member_permission_email}}
member_permission_inner    = {{member_permission_inner}}

project_assigned_email     = {{project_assigned_email}}
project_assigned_inner     = {{project_assigned_inner}}
project_activities_email   = {{project_activities_email}}
project_activities_inner   = {{project_activities_inner}}

task_assigned_email       = {{task_assigned_email}}
task_assigned_inner       = {{task_assigned_inner}}
task_response_email       = {{task_response_email}}
task_response_inner       = {{task_response_inner}}
task_memberstatus_email   = {{task_memberstatus_email}}
task_memberstatus_inner   = {{task_memberstatus_inner}}
task_status_email         = {{task_status_email}}
task_status_inner         = {{task_status_inner}}
task_info_email           = {{task_info_email}}
task_info_inner           = {{task_info_inner}}

ticket_created_email      = {{ticket_created_email}}
ticket_created_inner      = {{ticket_created_inner}}
ticket_response_email     = {{ticket_response_email}}
ticket_response_inner     = {{ticket_response_inner}}
ticket_status_email       = {{ticket_status_email}}
ticket_status_inner       = {{ticket_status_inner}}

[client_notification]
client_info_email               = {{client_info_email}}
client_info_inner               = {{client_info_inner}}
project_create_email            = {{clientproject_create_email}}
project_create_inner            = {{clientproject_create_inner}}
project_activities_email        = {{clientproject_activities_email}}
project_activities_inner        = {{clientproject_activities_inner}}
project_status_email            = {{clientproject_status_email}}
project_status_inner            = {{clientproject_status_inner}}
projectinvoice_status_email     = {{projectinvoice_status_email}}
projectinvoice_status_inner     = {{projectinvoice_status_inner}}
ticket_response_email           = {{clientticket_response_email}}
ticket_response_inner           = {{clientticket_response_inner}}
ticket_status_email             = {{clientticket_status_email}}
ticket_status_inner             = {{clientticket_status_inner}}

[logs]
create  = 1
update  = 2
delete  = 3
info    = 4

[debug]
active  = {{{debug}}}
installer  = {{{installer}}}
