<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <style>
  body{
    background-color: #e5e5e5; height: 100%; font-family: sans-serif; margin: 0px;
  }
  *, *:after, *:before {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
  }
  * {
    font-family: Roboto, sans-serif;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    -o-transition: all .3s ease;
    transition: all .3s ease;
  }
  .wrapper {
    width: 100%; margin: 0 auto;
  }
  .card {
    width: 60%;
    background-color: #fff;
    margin: auto;
    margin-top: 5%;
    position: relative;
    padding: 50px;
    border-top-left-radius: 2px;
    border-top-right-radius: 2px;
    border-bottom-right-radius: 2px;
    border-bottom-left-radius: 2px;box-shadow: 0 2px 5px 0 #00000029, 0 2px 10px 0 #0000001f;
  }
  .card .logo{
    width: 152px;    height: 152px;    margin-right: 2em; float: left;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
  .card .title-container
  {
    display: flex;
    flex-direction: column;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    padding: 15px;
  }
  .card .title-container h3
  {
    font-size: 2.5em;
    font-weight: 300;
    color: #555;
    display: inline-block;
    margin: 0;
    padding: 0;
  }
  .card .title-container span
  {
    font-size: 1.2em;
    font-weight: 400;
    color: #777;
    display: inline-block;
    padding-top: 10px;
  }
  .card .description
  {
    width: 100%;
    display: block;
    clear: both;
    padding: 45px 0;
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
  }
  .card .description p{
    padding: 0;
    margin: 0;
    color: #010101;
    font-size: 1.1em;
    line-height: 1.5em;
    word-wrap: break-word;
    font-weight: 400;
  }
  .card .details {

  }
  .card .details h3{
    font-weight: 300;
    display: inline-block;
    margin: 0;
    padding: 0;
    font-size: 2em;
    font-weight: 300;
    color: #333;
  }
  .card .details ul{
    list-style: none;
    margin: 0;
    padding: 0;
  }
  .card .details ul li i{
    padding: 15px;
    color: #26A69A;
  }
  .card .details ul li a{
    padding: 15px 0;
    color: #104944;
    display: inline-block;
  }
  </style>
</head>
<body>

{{#PASSWORD_RESET}}
<div class="wrapper">

  <div class="card">
    <img src="{{{LOGO}}}" class="logo"></img>
    <div class="title-container">
      <h3>Password Recovery</h3>
      <span>Sent by {{SENDER}}</span>
      <span>{{DATE}}</span>
    </div>
    <div class="description">
      <p>A request has been made to reset you're password , if this was not you just ignore this e-mail , your account will be safe ;)</p>
      <p>If you did made this request please input the security code along with your new password to to the panel.</p>
      <p>In case you closed the window , link to the panel is bellow this message along with your security code.</p>
      <p>Keep in mind that this request is valid fo only <strong>25 minutes</strong>.</p>
    </div>
    <div class="details">
      <h3>Details</h3>
      <ul>
        <li>
          <i class="material-icons">vpn_key</i>
          <a>{{{CODE}}}</a>
        </li>
        <li>
          <i class="material-icons">language</i>
          <a href="{{{LINK}}}">{{{LINK}}}</a>
        </li>
      </ul>
    </div>
  </div>

</div>
{{/PASSWORD_RESET}}

{{#NOTIFICATION}}
<div class="wrapper">

  <div class="card">
    <img src="{{{LOGO}}}" class="logo"></img>
    <div class="title-container">
      <h3>{{TITLE}}</h3>
      <span>Sent by {{SENDER}}</span>
      <span>{{DATE}}</span>
    </div>
    <div class="description">
      <p>{{TEXT}}</p>
    </div>
    {{#LINK}}
    <div class="details">
      <ul>
        <li>
          <i class="material-icons">language</i>
          <a href="{{{LINK}}}">{{{LINK}}}</a>
        </li>
      </ul>
    </div>
    {{/LINK}}
  </div>

</div>
{{/NOTIFICATION}}

{{#TEST}}
<div class="wrapper">

  <div class="card">
    <img src="{{{LOGO}}}" class="logo"></img>
    <div class="title-container">
      <h3>It Works !</h3>
      <span>Sent by {{SENDER}}</span>
      <span>{{DATE}}</span>
    </div>
    <div class="description">
      <p>This is an e-mail test sent by the server! It works !</p>
    </div>
  </div>

</div>
{{/TEST}}

</body>
</html>
