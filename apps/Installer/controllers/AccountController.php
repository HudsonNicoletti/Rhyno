<?php

namespace Installer\Controllers;

use Mustache_Engine as Mustache;

require __DIR__ . "/../../Manager/controllers/RhynoException.php";

use \Manager\Controllers\RhynoException;

use Installer\Models\Users as Users,
    Installer\Models\Team as Team;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Hidden;

class AccountController extends ControllerBase
{
  public function CreateAction()
  {
      $this->response->setContentType("application/json");

      $username = preg_replace('/\s+/', '', $this->request->getPost("username","string"));
      $password = preg_replace('/\s+/', '', $this->request->getPost("password","string"));
      $email    = $this->request->getPost("email","email");
      # catch any form errors
      try
      {
        $check_email    = Users::findFirstByEmail($email);
        $check_username = Users::findFirstByUsername($username);

        if(!$this->request->isPost() || !$this->request->isAjax()):
          return RhynoException::InvalidRequestMethod();

        elseif(!$username || !$password):
          return RhynoException::EmptyInput(["Username","Password"]);

        elseif(!$this->request->getPost("email")):
          return RhynoException::EmptyInput("E-mail");

        elseif(!$this->isEmail($email)):
          return RhynoException::InvalidEmailAddress();

        elseif($check_email):
          return RhynoException::RegisteredEmailAddress();

        elseif($check_username):
          return RhynoException::RegisteredUsername();

        elseif(!$this->security->checkToken()):
          return RhynoException::InvalidCsrfToken();
        endif;
      }
      catch (\Exception $e)
      {
        $this->flags['status'] = false ;
        $this->flags['toast']  = "error";
        $this->flags['title']  = $e->getMessage();
      }

      if($this->flags['status']):
        try
        {
          $user = new Users;
            $user->username   = $username;
            $user->password   = password_hash($password, PASSWORD_BCRYPT );
            $user->email      = $email;
            $user->permission = $this->permissions->admin;
          if($user->save())
          {
            $member = new Team;
              $member->uid    = $user->_;
              $member->name   = $this->request->getPost("name","string");
              $member->phone  = $this->request->getPost("phone","string");
              $member->image  = null;
            if(!$member->save())
            {
              return RhynoException::DBError();
            }
          }
          else
          {
            return RhynoException::DBError();
          }


          $update = $this->UpdateConfigFile([
            "installer"   => "false",
          ]);

          if(!$update){ return RhynoException::CustomError("Unable to update application configuration file."); }

          $this->flags['toast']      = "success";
          $this->flags['title']      = "Account successfully created! Redirecting.";
          $this->flags['redirect']   = "{$this->rhyno_url}/index";
        }
        catch(\Exception $e)
        {
          $this->flags['toast']  = "warning";
          $this->flags['title']  = $e->getMessage();
        }

      endif;

      return $this->response->setJsonContent([
        "toast"     =>  $this->flags['toast'],
        "title"     =>  $this->flags['title'],
        "redirect"  =>  $this->flags['redirect'],
        "time"      =>  $this->flags['time']
      ]);

      $this->response->send();
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
