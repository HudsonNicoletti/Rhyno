<?php

namespace Installer\Controllers;

use Mustache_Engine as Mustache;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
  protected $rhyno_url;
  protected $rhyno_token;
  protected $rhyno_date;
  protected $rhyno_datetime;
  protected $flags = [
    "status"    =>  true,
    "toast"     =>  "default",
    "title"     =>  null,
    "redirect"  =>  false,
    "time"      =>  2200
  ];

  public function Initialize()
  {
    $this->assets
         ->addCss('assets/manager/addons/Materialize/dist/css/materialize.css')
         ->addCss('assets/manager/addons/font-awesome/css/font-awesome.css')
         ->addCss('assets/manager/addons/code-prettify/src/prettify.css')
         ->addCss('assets/manager/css/admin.css')
         ->addCss('assets/manager/css/custom.css')
         ->addCss('assets/manager/css/themes/light.css')
         ->addCss('assets/manager/css/themes/main/materialize-red.css')
         ->addCss('assets/manager/css/themes/alternative/red.css')
         ->addCss("assets/manager/addons/Materialize/extras/noUiSlider/nouislider.css");

    $this->assets
         ->addJs('assets/manager/addons/jquery/dist/jquery.js')
         ->addJs('assets/manager/addons/Materialize/dist/js/materialize.js')
         ->addJs('assets/manager/addons/code-prettify/src/prettify.js')
         ->addJs('assets/manager/js/main.js');

   #    SETS SESSION TOKENS GLOBALY ACCESSABLE
   $this->rhyno_url      = rtrim($this->url->getStatic(),'/');
   $this->rhyno_token    = $this->configuration->app->token;

   $this->rhyno_date     = $this->configuration->app->date_format;
   $this->rhyno_datetime = $this->configuration->app->datetime_format;

   $this->view->logo      = $this->configuration->app->logo;
   $this->view->pagetitle = $this->configuration->app->title;
   $this->view->staticUrl = $this->rhyno_url;

   if(!$this->configuration->debug->installer)
   {
     return $this->response->redirect("{$this->rhyno_url}/index",true);
   }

  }

  protected function isEmail($str)
  {
    return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $str );
  }

  protected function UpdateConfigFile(array $line)
  {
    $template = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/config.tpl"), [
      'db_host'       => $line['db_host'] ?: $this->configuration->database->host,
      'db_user'       => $line['db_user'] ?: $this->configuration->database->username,
      'db_pass'       => $line['db_pass'] ?: $this->configuration->database->password,
      'db_name'       => $line['db_name'] ?: $this->configuration->database->dbname,

      'mail_host'     => $line['mail_host'] ?: $this->configuration->mail->host,
      'mail_user'     => $line['mail_user'] ?: $this->configuration->mail->username,
      'mail_pass'     => $line['mail_pass'] ?: $this->configuration->mail->password,
      'mail_secu'     => $line['mail_secu'] ?: $this->configuration->mail->security,
      'mail_port'     => $line['mail_port'] ?: $this->configuration->mail->port,
      'mail_mail'     => $line['mail_mail'] ?: $this->configuration->mail->email,
      'mail_name'     => $line['mail_name'] ?: $this->configuration->mail->name,

      'app_base'      => $line['app_base']      ?: $this->configuration->app->baseUri,
      'app_logo'      => $line['app_logo']      ?: $this->configuration->app->logo,
      'app_title'     => $line['app_title']     ?: $this->configuration->app->title,
      'app_token'     => $line['app_token']     ?: $this->configuration->app->token,
      'app_currency'  => $line['app_currency']  ?: $this->configuration->app->currency,
      'app_date'      => $line['app_date']      ?: $this->configuration->app->date_format,
      'app_datetime'  => $line['app_datetime']  ?: $this->configuration->app->datetime_format,

      'paypal'          => $line['paypal']          ?: ($this->configuration->paypal->active ? 'true' : 'false'),
      'paypal_mode'     => $line['paypal_mode']     ?: $this->configuration->paypal->mode,
      'paypal_clientID' => $line['paypal_clientID'] ?: $this->configuration->paypal->clientID,
      'paypal_secret'   => $line['paypal_secret']   ?: $this->configuration->paypal->secret,

      'stripe'              => $line['stripe']              ?: ($this->configuration->stripe->active ? 'true' : 'false'),
      'stripe_secret'       => $line['stripe_secret']       ?: $this->configuration->stripe->secret,
      'stripe_publishable'  => $line['stripe_publishable']  ?: $this->configuration->stripe->publishable,

      'taxes'   => $line['taxes']    ?: ($this->configuration->taxes->active ? 'true' : 'false'),
      'tax'     => $line['tax']      ?: $this->configuration->taxes->tax,
      'taxtype' => $line['taxtype']  ?: $this->configuration->taxes->type,
      'taxtime' => $line['taxtime']  ?: $this->configuration->taxes->taxtime,

      'members_page'            => $line['members_page']            ?: ($this->configuration->team_crud->members_page ? 'true' : 'false'),
      'members_create'          => $line['members_create']          ?: ($this->configuration->team_crud->members_create ? 'true' : 'false'),
      'members_update'          => $line['members_update']          ?: ($this->configuration->team_crud->members_update ? 'true' : 'false'),
      'members_delete'          => $line['members_delete']          ?: ($this->configuration->team_crud->members_delete ? 'true' : 'false'),
      'tickets_page'            => $line['tickets_page']            ?: ($this->configuration->team_crud->tickets_page ? 'true' : 'false'),
      'tickets_update'          => $line['tickets_update']          ?: ($this->configuration->team_crud->tickets_update ? 'true' : 'false'),
      'tickets_delete'          => $line['tickets_delete']          ?: ($this->configuration->team_crud->tickets_delete ? 'true' : 'false'),
      'clients_page'            => $line['clients_page']            ?: ($this->configuration->team_crud->clients_page ? 'true' : 'false'),
      'clients_create'          => $line['clients_create']          ?: ($this->configuration->team_crud->clients_create ? 'true' : 'false'),
      'clients_update'          => $line['clients_update']          ?: ($this->configuration->team_crud->clients_update ? 'true' : 'false'),
      'clients_delete'          => $line['clients_delete']          ?: ($this->configuration->team_crud->clients_delete ? 'true' : 'false'),
      'projects_page'           => $line['projects_page']           ?: ($this->configuration->team_crud->projects_page ? 'true' : 'false'),
      'projects_create'         => $line['projects_create']         ?: ($this->configuration->team_crud->projects_create ? 'true' : 'false'),
      'projects_update'         => $line['projects_update']         ?: ($this->configuration->team_crud->projects_update ? 'true' : 'false'),
      'projects_delete'         => $line['projects_delete']         ?: ($this->configuration->team_crud->projects_delete ? 'true' : 'false'),
      'projecttasks_page'       => $line['projecttasks_page']       ?: ($this->configuration->team_crud->projecttasks_page ? 'true' : 'false'),
      'projecttasks_create'     => $line['projecttasks_create']     ?: ($this->configuration->team_crud->projecttasks_create ? 'true' : 'false'),
      'projecttasks_update'     => $line['projecttasks_update']     ?: ($this->configuration->team_crud->projecttasks_update ? 'true' : 'false'),
      'projecttasks_delete'     => $line['projecttasks_delete']     ?: ($this->configuration->team_crud->projecttasks_delete ? 'true' : 'false'),
      'projectfiles_page'       => $line['projectfiles_page']       ?: ($this->configuration->team_crud->projectfiles_page ? 'true' : 'false'),
      'projectfiles_create'     => $line['projectfiles_create']     ?: ($this->configuration->team_crud->projectfiles_create ? 'true' : 'false'),
      'projectfiles_delete'     => $line['projectfiles_delete']     ?: ($this->configuration->team_crud->projectfiles_delete ? 'true' : 'false'),
      'projectrequests_page'    => $line['projectrequests_page']    ?: ($this->configuration->team_crud->projectrequests_page ? 'true' : 'false'),
      'projectrequests_delete'  => $line['projectrequests_delete']  ?: ($this->configuration->team_crud->projectrequests_delete ? 'true' : 'false'),
      'projectestimates_page'   => $line['projectestimates_page']   ?: ($this->configuration->team_crud->projectestimates_page ? 'true' : 'false'),
      'projectestimates_send'   => $line['projectestimates_send']   ?: ($this->configuration->team_crud->projectestimates_send ? 'true' : 'false'),
      'projectestimates_create' => $line['projectestimates_create'] ?: ($this->configuration->team_crud->projectestimates_create ? 'true' : 'false'),
      'projectestimates_update' => $line['projectestimates_update'] ?: ($this->configuration->team_crud->projectestimates_update ? 'true' : 'false'),
      'projectestimates_delete' => $line['projectestimates_delete'] ?: ($this->configuration->team_crud->projectestimates_delete ? 'true' : 'false'),
      'projectinvoices_page'    => $line['projectinvoices_page']    ?: ($this->configuration->team_crud->projectinvoices_page ? 'true' : 'false'),
      'projectinvoices_create'  => $line['projectinvoices_create']  ?: ($this->configuration->team_crud->projectinvoices_create ? 'true' : 'false'),
      'projectinvoices_update'  => $line['projectinvoices_update']  ?: ($this->configuration->team_crud->projectinvoices_update ? 'true' : 'false'),
      'projectinvoices_delete'  => $line['projectinvoices_delete']  ?: ($this->configuration->team_crud->projectinvoices_delete ? 'true' : 'false'),
      'projectpayments_page'    => $line['projectpayments_page']    ?: ($this->configuration->team_crud->projectpayments_page ? 'true' : 'false'),
      'projectpayments_create'  => $line['projectpayments_create']  ?: ($this->configuration->team_crud->projectpayments_create ? 'true' : 'false'),
      'projectpayments_delete'  => $line['projectpayments_delete']  ?: ($this->configuration->team_crud->projectpayments_delete ? 'true' : 'false'),

      'clienttickets_page'            => $line['clienttickets_page']           ?: ($this->configuration->client_crud->tickets_page ? 'true' : 'false'),
      'clienttickets_create'          => $line['clienttickets_create']         ?: ($this->configuration->client_crud->tickets_create ? 'true' : 'false'),
      'clienttickets_reopen'          => $line['clienttickets_reopen']         ?: ($this->configuration->client_crud->tickets_reopen ? 'true' : 'false'),
      'clienttickets_delete'          => $line['clienttickets_delete']         ?: ($this->configuration->client_crud->tickets_delete ? 'true' : 'false'),
      'clientprojectfiles_page'       => $line['clientprojectfiles_page']      ?: ($this->configuration->client_crud->projectfiles_page ? 'true' : 'false'),
      'clientprojectfiles_create'     => $line['clientprojectfiles_create']    ?: ($this->configuration->client_crud->projectfiles_create ? 'true' : 'false'),
      'clientprojectfiles_delete'     => $line['clientprojectfiles_delete']    ?: ($this->configuration->client_crud->projectfiles_delete ? 'true' : 'false'),
      'clientprojectrequests_page'    => $line['clientprojectrequests_page']   ?: ($this->configuration->client_crud->projectrequests_page ? 'true' : 'false'),
      'clientprojectrequests_create'  => $line['clientprojectrequests_create'] ?: ($this->configuration->client_crud->projectrequests_create ? 'true' : 'false'),
      'clientprojectrequests_delete'  => $line['clientprojectrequests_delete'] ?: ($this->configuration->client_crud->projectrequests_delete ? 'true' : 'false'),

      'member_info_email'         =>  $line['member_info_email']        ?: ($this->configuration->team_notification->member_info_email ? 'true' : 'false'),
      'member_info_inner'         =>  $line['member_info_inner']        ?: ($this->configuration->team_notification->member_info_inner ? 'true' : 'false'),
      'member_permission_email'   =>  $line['member_permission_email']  ?: ($this->configuration->team_notification->member_permission_email ? 'true' : 'false'),
      'member_permission_inner'   =>  $line['member_permission_inner']  ?: ($this->configuration->team_notification->member_permission_inner ? 'true' : 'false'),
      'project_assigned_email'    =>  $line['project_assigned_email']   ?: ($this->configuration->team_notification->project_assigned_email ? 'true' : 'false'),
      'project_assigned_inner'    =>  $line['project_assigned_inner']   ?: ($this->configuration->team_notification->project_assigned_inner ? 'true' : 'false'),
      'project_activities_email'  =>  $line['project_activities_email'] ?: ($this->configuration->team_notification->project_activities_email ? 'true' : 'false'),
      'project_activities_inner'  =>  $line['project_activities_inner'] ?: ($this->configuration->team_notification->project_activities_inner ? 'true' : 'false'),
      'task_assigned_email'       =>  $line['task_assigned_email']      ?: ($this->configuration->team_notification->task_assigned_email ? 'true' : 'false'),
      'task_assigned_inner'       =>  $line['task_assigned_inner']      ?: ($this->configuration->team_notification->task_assigned_inner ? 'true' : 'false'),
      'task_response_email'       =>  $line['task_response_email']      ?: ($this->configuration->team_notification->task_response_email ? 'true' : 'false'),
      'task_response_inner'       =>  $line['task_response_inner']      ?: ($this->configuration->team_notification->task_response_inner ? 'true' : 'false'),
      'task_memberstatus_email'   =>  $line['task_memberstatus_email']  ?: ($this->configuration->team_notification->task_memberstatus_email ? 'true' : 'false'),
      'task_memberstatus_inner'   =>  $line['task_memberstatus_inner']  ?: ($this->configuration->team_notification->task_memberstatus_inner ? 'true' : 'false'),
      'task_status_email'         =>  $line['task_status_email']        ?: ($this->configuration->team_notification->task_status_email ? 'true' : 'false'),
      'task_status_inner'         =>  $line['task_status_inner']        ?: ($this->configuration->team_notification->task_status_inner ? 'true' : 'false'),
      'task_info_email'           =>  $line['task_info_email']          ?: ($this->configuration->team_notification->task_info_email ? 'true' : 'false'),
      'task_info_inner'           =>  $line['task_info_inner']          ?: ($this->configuration->team_notification->task_info_inner ? 'true' : 'false'),
      'ticket_created_email'      =>  $line['ticket_created_email']     ?: ($this->configuration->team_notification->ticket_created_email ? 'true' : 'false'),
      'ticket_created_inner'      =>  $line['ticket_created_inner']     ?: ($this->configuration->team_notification->ticket_created_inner ? 'true' : 'false'),
      'ticket_response_email'     =>  $line['ticket_response_email']    ?: ($this->configuration->team_notification->ticket_response_email ? 'true' : 'false'),
      'ticket_response_inner'     =>  $line['ticket_response_inner']    ?: ($this->configuration->team_notification->ticket_response_inner ? 'true' : 'false'),
      'ticket_status_email'       =>  $line['ticket_status_email']      ?: ($this->configuration->team_notification->ticket_status_email ? 'true' : 'false'),
      'ticket_status_inner'       =>  $line['ticket_status_inner']      ?: ($this->configuration->team_notification->ticket_status_inner ? 'true' : 'false'),

      'client_info_email'               => $line['client_info_email']               ?: ($this->configuration->client_notification->client_info_email ? 'true' : 'false'),
      'client_info_inner'               => $line['client_info_inner']               ?: ($this->configuration->client_notification->client_info_inner ? 'true' : 'false'),
      'clientproject_create_email'      => $line['clientproject_create_email']      ?: ($this->configuration->client_notification->project_create_email ? 'true' : 'false'),
      'clientproject_create_inner'      => $line['clientproject_create_inner']      ?: ($this->configuration->client_notification->project_create_inner ? 'true' : 'false'),
      'clientproject_activities_email'  => $line['clientproject_activities_email']  ?: ($this->configuration->client_notification->project_activities_email ? 'true' : 'false'),
      'clientproject_activities_inner'  => $line['clientproject_activities_inner']  ?: ($this->configuration->client_notification->project_activities_inner ? 'true' : 'false'),
      'clientproject_status_email'      => $line['clientproject_status_email']      ?: ($this->configuration->client_notification->project_status_email ? 'true' : 'false'),
      'clientproject_status_inner'      => $line['clientproject_status_inner']      ?: ($this->configuration->client_notification->project_status_inner ? 'true' : 'false'),
      'projectinvoice_status_email'     => $line['projectinvoice_status_email']     ?: ($this->configuration->client_notification->projectinvoice_status_email ? 'true' : 'false'),
      'projectinvoice_status_inner'     => $line['projectinvoice_status_inner']     ?: ($this->configuration->client_notification->projectinvoice_status_inner ? 'true' : 'false'),
      'clientticket_response_email'     => $line['clientticket_response_email']     ?: ($this->configuration->client_notification->ticket_response_email ? 'true' : 'false'),
      'clientticket_response_inner'     => $line['clientticket_response_inner']     ?: ($this->configuration->client_notification->ticket_response_inner ? 'true' : 'false'),
      'clientticket_status_email'       => $line['clientticket_status_email']       ?: ($this->configuration->client_notification->ticket_status_email ? 'true' : 'false'),
      'clientticket_status_inner'       => $line['clientticket_status_inner']       ?: ($this->configuration->client_notification->ticket_status_inner ? 'true' : 'false'),

      'debug'     => $line['debug'] ?: ($this->configuration->debug->active ? 'true' : 'false'),
      'installer' => $line['installer'] ?: ($this->configuration->debug->installer ? 'true' : 'false'),
    ]);

    if(file_put_contents(__DIR__."/../../../config/config.ini",$template))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
