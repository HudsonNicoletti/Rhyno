<?php

namespace Installer\Controllers;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Hidden;

class IndexController extends ControllerBase
{
  public function onConstruct()
  {
    $title    = explode(" ",$this->configuration->app->title);
    $title_2 = "";
    foreach($title as $i => $t)
    {
      if($i != 0)
      {
        $title_2 .= $t." ";
      }
    }

    $this->view->title   = (object)["first"=>$title[0],"all"=>$title_2];
  }

  public function IndexAction()
  {

  }

  public function ApplicationAction()
  {
    $form = new Form();

    $currencies = [
      "AUD" => "AUD",
      "BGN" => "BGN",
      "BRL" => "BRL",
      "CAD" => "CAD",
      "CHF" => "CHF",
      "CNY" => "CNY",
      "CZK" => "CZK",
      "DKK" => "DKK",
      "EUR" => "EUR",
      "GBP" => "GBP",
      "HKD" => "HKD",
      "HRK" => "HRK",
      "HUF" => "HUF",
      "IDR" => "IDR",
      "ILS" => "ILS",
      "INR" => "INR",
      "JPY" => "JPY",
      "KRW" => "KRW",
      "MXN" => "MXN",
      "MYR" => "MYR",
      "NOK" => "NOK",
      "NZD" => "NZD",
      "PHP" => "PHP",
      "PLN" => "PLN",
      "RON" => "RON",
      "RUB" => "RUB",
      "SEK" => "SEK",
      "SGD" => "SGD",
      "USD" => "USD"
    ];

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    $element['db_host'] = new Text( "db_host" ,[
      'required' => true,
      'value'    => $this->configuration->database->host
    ]);

    $element['db_user'] = new Text( "db_user" ,[
      'required' => true,
      'value'    => $this->configuration->database->username
    ]);

    $element['db_pass'] = new Text( "db_pass" ,[
      'value'    => $this->configuration->database->password
    ]);

    $element['db_database'] = new Text( "db_database" ,[
      'required' => true,
      'value'    => $this->configuration->database->dbname
    ]);

    $element['mail_host'] = new Text( "mail_host" ,[
      'required' => true,
      'value'    => $this->configuration->mail->host
    ]);

    $element['mail_user'] = new Text( "mail_user" ,[
      'required' => true,
      'value'    => $this->configuration->mail->username
    ]);

    $element['mail_port'] = new Text( "mail_port" ,[
      'required' => true,
      'value'    => $this->configuration->mail->port
    ]);

    $element['mail_type'] = new Select( "mail_type" , ["tls"=>"TLS","ssl"=>"SSL"] ,[
      'value' => $this->configuration->mail->security,
      'class' => "validate",
    ]);

    $element['mail_pass'] = new Text( "mail_pass" ,[
      'value'    => $this->configuration->mail->password
    ]);

    $element['mail_email'] = new Text( "mail_email" ,[
      'required' => true,
      'value'    => $this->configuration->mail->email
    ]);

    $element['mail_name'] = new Text( "mail_name" ,[
      'required' => true,
      'value'    => $this->configuration->mail->name
    ]);

    $element['app_base'] = new Text( "app_base" ,[
      'value'    => $this->configuration->app->baseUri
    ]);

    $element['app_title'] = new Text( "app_title" ,[
      'required' => true,
      'value'    => $this->configuration->app->title
    ]);

    $element['app_token'] = new Text( "app_token" ,[
      'required' => true,
      'value'    => $this->configuration->app->token
    ]);

    $element['app_currency'] = new Select( "app_currency" , $currencies ,[
      'class' => "validate",
      'value' => $this->configuration->app->currency
    ]);

    $element['app_date'] = new Select( "app_date" ,[
      "d-m-Y" => "dd-mm-yyyy",
      "m-d-Y" => "mm-dd-yyyy",
    ],[
      'class' => "validate",
      'value' => $this->configuration->app->date_format
    ]);

    $element['app_datetime'] = new Select( "app_datetime" ,[
      "d-m-Y H:i:s" => "dd-mm-yyyy Hour:min:sec",
      "m-d-Y H:i:s" => "mm-dd-yyyy Hour:min:sec",
    ],[
      'class' => "validate",
      'value' => $this->configuration->app->datetime_format
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }
    $this->view->form = $form;
  }

  public function AccountAction()
  {

    $form = new Form();
    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);
    foreach ($element as $e)
    {
      $form->add($e);
    }
    $this->view->form = $form;
  }

  public function SaveAction()
  {
    $this->response->setContentType("application/json");
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("db_host") || !$this->request->getPost("db_user") || !$this->request->getPost("db_database")):
        return RhynoException::EmptyInput(["Database Host","Database User","Database"]);

      elseif(!$this->request->getPost("mail_host") || !$this->request->getPost("mail_user") || !$this->request->getPost("mail_port") || !$this->request->getPost("mail_type") || !$this->request->getPost("mail_email") || !$this->request->getPost("mail_name")):
        return RhynoException::EmptyInput(["SMTP Host","SMTP User","SMTP Port","SMTP Security","Sender E-Mail","Sender Name"]);

      elseif(!$this->request->getPost("app_title") || !$this->request->getPost("app_token") || !$this->request->getPost("app_currency") || !$this->request->getPost("app_date") || !$this->request->getPost("app_datetime")):
        return RhynoException::EmptyInput(["Application Title","Application Token","Application Currency","Application Date/Time Format"]);

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $update = $this->UpdateConfigFile([
          "db_host"      => $this->request->getPost("db_host","string"),
          "db_user"      => $this->request->getPost("db_user","string"),
          "db_pass"      => $this->request->getPost("db_pass","string"),
          "db_name"      => $this->request->getPost("db_database","string"),

          "mail_host"    => $this->request->getPost("mail_host","string"),
          "mail_user"    => $this->request->getPost("mail_user","string"),
          "mail_pass"    => $this->request->getPost("mail_pass","string"),
          "mail_secu"    => $this->request->getPost("mail_type","string"),
          "mail_port"    => $this->request->getPost("mail_port","string"),
          "mail_mail"    => $this->request->getPost("mail_email","string"),
          "mail_name"    => $this->request->getPost("mail_name","string"),

          "app_base"     => ($this->request->getPost("app_base","string") ?: '/'),
          "app_title"    => $this->request->getPost("app_title","string"),
          "app_token"    => $this->request->getPost("app_token","string"),
          "app_currency" => $this->request->getPost("app_currency","string"),
          "app_date"     => $this->request->getPost("app_date","string"),
          "app_datetime" => $this->request->getPost("app_datetime","string"),
        ]);

        if(!$update):
          return RhynoException::CustomError("Unable to update application configuration file.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Application Configuration Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/installer/account";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
        $this->flags['redirect'] = "{$this->rhyno_url}/installer";
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
