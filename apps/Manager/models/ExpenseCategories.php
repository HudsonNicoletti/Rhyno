<?php

namespace Manager\Models;

class ExpenseCategories extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return "expense_categories";
    }

}
