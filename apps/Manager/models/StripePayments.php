<?php

namespace Manager\Models;

class StripePayments extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return "stripe_payments";
    }

}
