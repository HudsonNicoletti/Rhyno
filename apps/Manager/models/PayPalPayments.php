<?php

namespace Manager\Models;

class PayPalPayments extends \Phalcon\Mvc\Model
{

    public function getSource()
    {
        return "paypal_payments";
    }

}
