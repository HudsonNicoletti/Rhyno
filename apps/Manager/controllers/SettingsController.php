<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Currencies as Currencies;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class SettingsController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");
    $form = new Form();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    $element['title'] = new Text( "title" ,[
      'required' => true,
      'value'    => $this->configuration->app->title
    ]);

    $element['token'] = new Text( "token" ,[
      'required' => true,
      'value'    => $this->configuration->app->token
    ]);

    $element['currency'] = new Select( "currency" , Currencies::find(["order"=>"code asc"]) ,[
      'using' => ['code','code'],
      'class' => "validate",
      'value' => $this->configuration->app->currency
    ]);

    $element['date'] = new Select( "date" ,[
      "d-m-Y" => "dd-mm-yyyy",
      "m-d-Y" => "mm-dd-yyyy",
    ],[
      'class' => "validate",
      'value' => $this->configuration->app->date_format
    ]);

    $element['datetime'] = new Select( "datetime" ,[
      "d-m-Y H:i:s" => "dd-mm-yyyy Hour:min:sec",
      "m-d-Y H:i:s" => "mm-dd-yyyy Hour:min:sec",
    ],[
      'class' => "validate",
      'value' => $this->configuration->app->datetime_format
    ]);

    foreach ($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("title")):
        return RhynoException::EmptyInput("System Title");

      elseif(!$this->request->getPost("token")):
        return RhynoException::EmptyInput("System Token");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if($this->request->hasFiles()):
          foreach($this->request->getUploadedFiles() as $file):
            if($file->getError() != 0):
              return RhynoException::UploadError($file->getError());
            endif;
            $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
            $file->moveTo("assets/manager/images/{$filename}");
          endforeach;
          @unlink("assets/manager/images/{$this->configuration->app->logo}");
        endif;

        $update = $this->UpdateConfigFile([
          "app_logo"  => ($filename ?: $this->configuration->app->logo),
          "app_title" => $this->request->getPost("title","string"),
          "app_token" => $this->request->getPost("token","string"),
          "app_currency" => $this->request->getPost("currency","string"),
          "app_date"     => $this->request->getPost("date","string"),
          "app_datetime" => $this->request->getPost("datetime","string"),
        ]);

        if(!$update):
          return RhynoException::CustomError("Unable to update application configuration file.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Application Configuration Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/settings";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
        $this->flags['redirect'] = "{$this->rhyno_url}/settings";
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
