<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Projects          as Projects,
    Manager\Models\ProjectEstimates  as ProjectEstimates,
    Manager\Models\EstimateRequests  as EstimateRequests,
    Manager\Models\ProjectActivities as ProjectActivities;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectEstimateRequestsController extends ControllerBase
{
  private $uniqueP;
  private $uniqueE;
  private $uniqueR;
  private $project;
  private $estimate;
  private $rmethod;
  private $requess;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueE = $this->dispatcher->getParam("estimate","string");
    $this->uniqueR = $this->dispatcher->getParam("request","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project  = Projects::findFirstByUnique($this->uniqueP);
    $this->estimate = ProjectEstimates::findFirstByUnique($this->uniqueE);
    $this->requess  = EstimateRequests::findFirstByUnique($this->uniqueR);
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/requests");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projectrequests_page)
        {
          $this->view->pick("projects/team/requests");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        if($this->configuration->client_crud->projectrequests_page)
        {
          $this->view->pick("projects/client/requests");
          $this->view->crud = $this->configuration->client_crud;
        }
        else
        {
          $this->permissionHandler("team");
        }
      break;
    }

    $form = new Form;

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]);
    foreach($element as $e)
    {
      $form->add($e);
    }

    $requests = EstimateRequests::find([
      "project" => $this->project->_,
      "order"   => "status ASC , date ASC"
    ]);

    $this->view->requests = $requests;
    $this->view->form = $form;
    $this->view->project = $this->project;
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueR):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->requess):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      try
      {
        switch ($this->requess->status)
        {
          case 1: $this->requess->status = 2; break;
          case 2: $this->requess->status = 1; break;
        }
        if(!$this->requess->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Request status updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/requests";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueR):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->requess):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      try
      {
        if(!$this->requess->delete())
        {
          return RhynoException::Unreachable();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Request successfuly removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/requests";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form = new Form;
      $inputs = [];

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      if ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/requests/delete/{$this->uniqueR}";
        $modal_header = "Remove Project Request!";

        $modal_card = [
          "content"  => "Are you sure? The selected request will me permanently removed from the server."
        ];

      elseif ($this->rmethod == "view"):
          $modal_header = "Request Information!";
          $modal_info = [
            [
              "titles" => [
                ["title" => "Request Title"],
                ["title" => "Budget"],
                ["title" => "Reference"],
              ],
              "contents"  => [
                ["content" => $this->requess->title],
                ["content" => number_format(($this->requess->budget / 100),2,',','.')],
                ["content" => $this->requess->reference],
              ]
            ],
            [
              "titles" => [
                ["title" => "Request Description"],
              ],
              "contents"  => [
                ["content" => $this->requess->text],
              ]
            ],
          ];

      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()), "active" => $f->getAttribute("active"), "file" => $f->getAttribute("file"), "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
