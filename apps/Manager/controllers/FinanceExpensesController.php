<?php

namespace Manager\Controllers;

use Manager\Models\Expenses as Expenses,
    Manager\Models\ExpenseCategories as ExpenseCategories;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class FinanceExpensesController extends ControllerBase
{
  private $uniqueE;
  private $expense;
  private $rmthod;

  public function onConstruct()
  {
    $this->uniqueE = $this->dispatcher->getParam("expense","string");
    $this->expense = Expenses::findFirstByUnique($this->uniqueE) ?: null;
    $this->rmethod = $this->dispatcher->getParam("method","string");
  }

  public function IndexAction()
  {
    $this->permissionHandler("admin");

    $expenses = Expenses::query()
    ->columns([
      "Manager\Models\Expenses._",
      "Manager\Models\Expenses.unique",
      "Manager\Models\Expenses.amount",
      "Manager\Models\Expenses.date",
      "Manager\Models\Expenses.title",
      "Manager\Models\Expenses.text",
      "Manager\Models\Expenses.file",
      "Manager\Models\ExpenseCategories.name as category",
    ])
    ->innerJoin("Manager\Models\ExpenseCategories","Manager\Models\Expenses.category = Manager\Models\ExpenseCategories._")
    ->orderBy("date ASC")
    ->execute();

    $categories = ExpenseCategories::find();
    $this->view->categories = $categories;
    $this->view->expenses = $expenses;
    $this->view->pick("finance/admin/expenses");
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isPost() || !$this->request->isAjax()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";

    elseif(!$this->request->getPost("title","string")):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Please input a title.";

    elseif(!$this->request->getPost("amount","int")):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Please input a value.";

    elseif(!$this->request->getPost("date","int")):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Please select a date.";

    elseif(!$this->security->checkToken()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid CSRF Token.";
    endif;

    if($this->flags['status']):

      if($this->request->hasFiles()):
        foreach($this->request->getUploadedFiles() as $file):
          $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
          $file->moveTo("assets/manager/files/{$filename}");
        endforeach;
      endif;

      $date = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("date","string"));

      $exp = new Expenses;
        $exp->unique    = $this->uniqueCode("EXP");
        $exp->title     = $this->request->getPost("title","string");
        $exp->text      = $this->request->getPost("text","string");
        $exp->category  = $this->request->getPost("category","int");
        $exp->file      = $filename ?: null;
        $exp->amount    = $this->request->getPost("amount","int");
        $exp->date      = $date->format("Y-m-d H:i:s");
      $exp->save();

      $this->flags['toast']      = "success";
      $this->flags['title']      = "Expense successfully added!";
      $this->flags['redirect']   = "{$this->rhyno_url}/finance/expenses";

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isPost() || !$this->request->isAjax()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";

    elseif(!$this->uniqueE):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Wrong number of parameters.";

    elseif(!$this->expense):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Unable to locate in database.";

    elseif(!$this->request->getPost("title","string")):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Please input a title.";

    elseif(!$this->request->getPost("amount","int")):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Please input a value.";

    elseif(!$this->request->getPost("date","int")):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Please select a date.";

    elseif(!$this->security->checkToken()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid CSRF Token.";
    endif;

    if($this->flags['status']):

      if($this->request->hasFiles()):
        unlink("assets/manager/files/{$this->expense->file}");
        foreach($this->request->getUploadedFiles() as $file):
          $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
          $file->moveTo("assets/manager/files/{$filename}");
        endforeach;
      endif;

      $date = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("date","string"));

      $this->expense->title     = $this->request->getPost("title","string");
      $this->expense->text      = $this->request->getPost("text","string");
      $this->expense->category  = $this->request->getPost("category","int");
      $this->expense->file      = $filename ?: $this->expense->file;
      $this->expense->amount    = $this->request->getPost("amount","int");
      $this->expense->date      = $date->format("Y-m-d H:i:s");
      $this->expense->save();

      $this->flags['toast']      = "success";
      $this->flags['title']      = "Expense successfully updated!";
      $this->flags['redirect']   = "{$this->rhyno_url}/finance/expenses";

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isPost() || !$this->request->isAjax()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";

    elseif(!$this->uniqueE):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Wrong number of parameters.";

    elseif(!$this->expense):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Unable to locate in database.";

    elseif(!$this->security->checkToken()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid CSRF Token.";
    endif;

    if($this->flags['status']):

      unlink("assets/manager/files/{$this->expense->file}");
      $this->expense->delete();

      $this->flags['toast']      = "success";
      $this->flags['title']      = "Expense successfully removed!";
      $this->flags['redirect']   = "{$this->rhyno_url}/finance/expenses";

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form = new Form;
      $inputs = [];
      if($this->rmethod != "remove")
      {
        $element['title'] = new Text( "title" ,[
          'label' => "Expense Title",
        ]);
        $element['text'] = new Textarea( "text" ,[
          'label' => "Expense Description",
          'class' => "materialize-textarea"
        ]);
        $element['date'] = new Text( "date" ,[
          'label' => "Expense Date",
          'class' => "datepicker"
        ]);
        $element['category'] = new Select( "category" , ExpenseCategories::find() ,[
          'using' => ['_','name'],
          'label' => "Expense Category"
        ]);
        $element['amount'] = new Text( "amount" ,[
          'label' => "Expense Value",
          'data-mask'         => "000.000.000.000.000,00",
          'data-mask-reverse' => true,
        ]);
        $element['file'] = new File( "file" ,[
          'label' => "Attachment",
          'file'  => true
        ]);
      }

      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/finance/expenses/new";
        $modal_header = "Create Expense!";

      elseif( $this->rmethod == "modify" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/finance/expenses/update/{$this->uniqueE}";
        $modal_header = "Update Expense!";


        $element['title']->setAttribute("active",true)->setAttribute("value",$this->expense->title);
        $element['text']->setAttribute("active",true)->setAttribute("value",$this->expense->text);
        $element['amount']->setAttribute("active",true)->setAttribute("value",$this->expense->amount);
        $element['category']->setAttribute("active",false)->setAttribute("value",$this->expense->category);
        $element['date']->setAttribute("active",true)->setAttribute("value",(new \DateTime($this->expense->date))->format($this->rhyno_date));

      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/finance/expenses/delete/{$this->uniqueE}";
        $modal_header = "Remove Espense!";

        $modal_card = [
          "content"  => "Are you sure? The selected expense will me permanently removed from the server."
        ];
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
