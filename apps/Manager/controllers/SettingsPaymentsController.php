<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class SettingsPaymentsController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");
    $form = new Form;


    $element['pp_active'] = new Select( "pp_active" ,[
      "true"  => "Enabled",
      "false" => "Disabled",
    ],[
      'value'         => ($this->configuration->paypal->active ? "true" : "false"),
      'data-validate' => true
    ]);

    $element['pp_mode'] = new Select( "pp_mode" ,[
      "sandbox" => "Sandbox",
      "live"    => "Live",
    ],[
      'value'         => $this->configuration->paypal->mode,
      'data-validate' => true
    ]);

    $element['pp_clientID'] = new Text( "pp_clientID" ,[
      'class'         => "validate",
      'value'         => $this->configuration->paypal->clientID,
      'data-validate' => true
    ]);

    $element['pp_secret'] = new Text( "pp_secret" ,[
      'class'         => "validate",
      'value'         => $this->configuration->paypal->secret,
      'data-validate' => true
    ]);

    $element['sp_active'] = new Select( "sp_active" ,[
      "true"  => "Enabled",
      "false" => "Disabled",
    ],[
      'value'         => ($this->configuration->stripe->active ? "true" : "false"),
      'data-validate' => true
    ]);

    $element['sp_publishable'] = new Text( "sp_publishable" ,[
      'class'         => "validate",
      'value'         => $this->configuration->stripe->publishable,
      'data-validate' => true
    ]);

    $element['sp_secret'] = new Text( "sp_secret" ,[
      'class'         => "validate",
      'value'         => $this->configuration->stripe->secret,
      'data-validate' => true
    ]);

    $element['taxes_active'] = new Select( "taxes_active" ,[
      "true"  => "Enabled",
      "false" => "Disabled",
    ],[
      'value'         => ($this->configuration->taxes->active ? "true" : "false"),
      'data-validate' => true
    ]);

    $element['taxes_type'] = new Select( "taxes_type" ,[
      "exact"   => "Exact",
      "percent" => "Percent",
    ],[
      'value'         => $this->configuration->taxes->type,
      'data-validate' => true
    ]);

    $element['taxes_time'] = new Select( "taxes_time" ,[
      "daily"    => "Daily",
      "monthly"  => "Monthly",
      "annually" => "Annually",
    ],[
      'value'         => $this->configuration->taxes->taxtime,
      'data-validate' => true
    ]);

    $element['taxes_amount'] = new Text( "taxes_amount" ,[
      'class'         => "validate",
      'value'         => $this->configuration->taxes->tax,
      'data-validate' => true
    ]);

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]);

    foreach($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("settings/payments");
  }

  public function SaveAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->dispatcher->getParam("method")):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        switch ($this->dispatcher->getParam("method","string"))
        {
          case 'paypal':
            $params = [
              "paypal"          => $this->request->getPost("pp_active","string"),
              "paypal_mode"     => $this->request->getPost("pp_mode","string"),
              "paypal_clientID" => $this->request->getPost("pp_clientID","string"),
              "paypal_secret"   => $this->request->getPost("pp_secret","string"),
            ];
          break;
          case 'stripe':
            $params = [
              "stripe"              => $this->request->getPost("sp_active","string"),
              "stripe_secret"       => $this->request->getPost("sp_secret","string"),
              "stripe_publishable"  => $this->request->getPost("sp_publishable","string"),
            ];
          break;
          case 'taxes':
            $params = [
              "taxes"   => $this->request->getPost("taxes_active","string"),
              "tax"     => $this->request->getPost("taxes_amount","int"),
              "taxtype" => $this->request->getPost("taxes_type","string"),
              "taxtime" => $this->request->getPost("taxes_time","string"),
            ];
          break;
        }

        $update = $this->UpdateConfigFile($params);

        if(!$update):
          return RhynoException::CustomError("Unable to update application configuration file.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Payment Configuration Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/settings/payments";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
