<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Logs         as Logs,
    Manager\Models\ProjectTypes as ProjectTypes,
    Manager\Models\Projects     as Projects;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectCategoriesController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");

    $categories = ProjectTypes::query()
    ->columns([
      'Manager\Models\ProjectTypes._',
      'Manager\Models\ProjectTypes.title',
      "(SELECT COUNT(Manager\Models\Projects.type) FROM Manager\Models\Projects WHERE Manager\Models\Projects.type = Manager\Models\ProjectTypes._) as assigned"
    ])
    ->leftJoin('Manager\Models\Projects', 'Manager\Models\ProjectTypes._ = Manager\Models\Projects.type')
    ->groupBy('Manager\Models\ProjectTypes._')
    ->orderBy('Manager\Models\ProjectTypes.title')
    ->execute();

    $this->view->categories = $categories;
    $this->view->pick("projects/admin/categories");
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");
    $title = $this->request->getPost("title","string");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$title):
        return RhynoException::EmptyInput("title");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $category = new ProjectTypes;
          $category->title = $title;
        if(!$category->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project Category successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/categories";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");
    $id = $this->dispatcher->getParam("category","int");
    $title = $this->request->getPost("title","string");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$id):
        return RhynoException::WrongNumberOfParams();

      elseif(!$title):
        return RhynoException::EmptyInput("title");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $category = ProjectTypes::findFirst($id);
          $category->title = $title;
        if(!$category->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project Category successfully updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/categories";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");
    $id = $this->dispatcher->getParam("category","int");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$id):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $category = ProjectTypes::findFirst($id);
        if(!$category->delete())
        {
          return RhynoException::Unreachable();
        }

        foreach (Projects::findByType($id) as $project)
        {
          $project->type = $this->request->getPost("category","int");
          $project->save();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project Category successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/categories";

      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form = new Form();
      $inputs = [];
      $uid = $this->dispatcher->getParam("category","int");
      $method = $this->dispatcher->getParam("method","string");

      if($uid)
      {
        $category = ProjectTypes::findFirst($uid);
      }

      if($method == "remove"):
        $element['category'] = new Select( "category" , ProjectTypes::find(["_ != '{$uid}'"]) ,[
          'using' => ["_","title"],
          'label' => "Client",
        ]);
      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Category Title",
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $method == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/categories/new";
        $modal_header = "Create a new project category!";

        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO UPDATE POPULATE WITH VALJUE TO ELEMENT
      elseif ($method == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/categories/update/{$uid}";
        $modal_header = "Update project category information!";

        $element['title']->setAttribute("active",true)->setAttribute("value",$category->title);
        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($method == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/categories/delete/{$uid}";
        $modal_header = "Remove Project Category!";

        $modal_card = [
          "content"  => "Are you sure? Please select a category to transfer all projects associated with this category."
        ];

        foreach($element as $e)
        {
          $form->add($e);
        }

      endif;

      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
