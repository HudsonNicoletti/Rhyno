<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users              as Users,
    Manager\Models\Projects           as Projects,
    Manager\Models\Clients            as Clients,
    Manager\Models\Currencies         as Currencies,
    Manager\Models\ProjectActivities  as ProjectActivities,
    Manager\Models\ProjectEstimates   as ProjectEstimates,
    Manager\Models\ProjectInvoices    as ProjectInvoices,
    Manager\Models\InvoiceItems       as InvoiceItems,
    Manager\Models\EstimateItems      as EstimateItems,
    Manager\Models\EstimateReviews    as EstimateReviews,
    Manager\Models\EstimateRequests   as EstimateRequests;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectEstimatesController extends ControllerBase
{
  private $uniqueP;
  private $uniqueE;
  private $project;
  private $estimate;
  private $rmethod;
  private $items;
  private $requests;
  private $reviews;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueE = $this->dispatcher->getParam("estimate","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project  = Projects::findFirstByUnique($this->uniqueP);
    $this->estimate = ProjectEstimates::findFirstByUnique($this->uniqueE);
    $this->requests = EstimateRequests::findByProject($this->project->_);
    $this->reviews  = EstimateReviews::findByEstimate($this->estimate->_);
    $this->items    = EstimateItems::findByEstimate($this->estimate->_);
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/estimates");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projectestimates_page)
        {
          $this->view->pick("projects/team/estimates");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->view->pick("projects/client/estimates");
        $this->view->crud = $this->configuration->client_crud;
      break;
    }

    if($this->rhyno_user->permission >= $this->permissions->team):

      $estimates = ProjectEstimates::query()
      ->columns([
        "Manager\Models\ProjectEstimates._",
        "Manager\Models\ProjectEstimates.title",
        "Manager\Models\ProjectEstimates.unique",
        "Manager\Models\ProjectEstimates.status",
        "Manager\Models\ProjectEstimates.issue",
        "Manager\Models\ProjectEstimates.due",
        "Manager\Models\ProjectEstimates.recurring",
        "(SELECT SUM(Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as items",
        "(SELECT SUM(Manager\Models\EstimateItems.price * Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as value",
      ])
      ->where("Manager\Models\ProjectEstimates.project = :project:")
      ->bind([
        "project" => $this->project->_
      ])
      ->orderBy("Manager\Models\ProjectEstimates.status=1 DESC,
                 Manager\Models\ProjectEstimates.status=4 DESC,
                 Manager\Models\ProjectEstimates.status=3 DESC,
                 Manager\Models\ProjectEstimates.status=2 DESC,
                 Manager\Models\ProjectEstimates.status=5 DESC,
                 Manager\Models\ProjectEstimates.due      ASC")
      ->execute();

    elseif($this->rhyno_user->permission <= $this->permissions->client):

      $estimates = ProjectEstimates::query()
      ->columns([
        "Manager\Models\ProjectEstimates._",
        "Manager\Models\ProjectEstimates.title",
        "Manager\Models\ProjectEstimates.unique",
        "Manager\Models\ProjectEstimates.status",
        "Manager\Models\ProjectEstimates.issue",
        "Manager\Models\ProjectEstimates.due",
        "Manager\Models\ProjectEstimates.recurring",
        "(SELECT SUM(Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as items",
        "(SELECT SUM(Manager\Models\EstimateItems.price * Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as value",
      ])
      ->where("Manager\Models\ProjectEstimates.project = :project: AND Manager\Models\ProjectEstimates.status != :pending:")
      ->bind([
        "project" => $this->project->_,
        "pending" => 1
      ])
      ->orderBy("Manager\Models\ProjectEstimates.due ASC")
      ->execute();

      $this->view->currency = Currencies::findFirst($this->rhyno_user_info->currency);

    endif;

    $this->view->project   = $this->project;
    $this->view->estimates = $estimates;
  }

  public function OverviewAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/estimate-overview");
      break;
      case $this->permissions->team:
        $this->view->pick("projects/team/estimate-overview");
        $this->view->crud = $this->configuration->team_crud;
      break;
      case $this->permissions->client:
        $this->view->pick("projects/client/estimate-overview");
        $this->view->crud = $this->configuration->client_crud;
        $this->view->currency = Currencies::findFirst($this->rhyno_user_info->currency);
      break;
    }

    $form     = new Form;
    $estimate = ProjectEstimates::query()
    ->columns([
      "Manager\Models\ProjectEstimates._",
      "Manager\Models\ProjectEstimates.title",
      "Manager\Models\ProjectEstimates.unique",
      "Manager\Models\ProjectEstimates.status",
      "Manager\Models\ProjectEstimates.issue",
      "Manager\Models\ProjectEstimates.due",
      "Manager\Models\ProjectEstimates.recurring",
      "(SELECT SUM(Manager\Models\EstimateItems.price * Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as total",
    ])
    ->where("Manager\Models\ProjectEstimates.unique = :estimate:")
    ->bind([
      "estimate" => $this->uniqueE
    ])
    ->execute();

    $form->add(new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]));

    $this->view->project  = $this->project;
    $this->view->estimate = $estimate[0];
    $this->view->items    = $this->items;
    $this->view->reviews  = $this->reviews;
    $this->view->form     = $form;
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("issue","string") || !$this->request->getPost("due","string")):
        return RhynoException::EmptyInput(["issue date","due date"]);

      elseif((new \DateTime($this->request->getPost("issue","string"))) > (new \DateTime($this->request->getPost("due","string")))):
        return RhynoException::CustomError("Woah! Issue date can't be after the due date!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      $title   = $this->request->getPost("title","string");

      try
      {
        $issue = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("issue","string"));
        $due   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("due","string"));
        $est = new ProjectEstimates;
          $est->unique    = $this->uniqueCode("EST");
          $est->project   = $this->project->_;
          $est->status    = 1;
          $est->title     = $title;
          $est->issue     = $issue->format("Y-m-d H:i:s");
          $est->due       = $due->format("Y-m-d H:i:s");
          $est->recurring = ($this->request->getPost("recurring") == true ? 1 : 0);
        if(!$est->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Created a new estimate, '{$title}' to the project.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project estimate successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueE):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->estimate):
        return RhynoException::Unreachable();

      elseif(!$this->estimate->project == $this->project->_):
        return RhynoException::CustomError("Woah! Estimate is not part of this project!");

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("issue","string") || !$this->request->getPost("due","string")):
        return RhynoException::EmptyInput(["issue date","due date"]);

      elseif((new \DateTime($this->request->getPost("issue","string"))) > (new \DateTime($this->request->getPost("due","string")))):
        return RhynoException::CustomError("Woah! Issue date can't be after the due date!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $issue = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("issue","string"));
        $due = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("due","string"));
        $this->estimate->title      = $this->request->getPost("title","string");
        $this->estimate->issue      = $issue->format("Y-m-d H:i:s");
        $this->estimate->due        = $due->format("Y-m-d H:i:s");
        $this->estimate->recurring  = ($this->request->getPost("recurring") == true ? 1 : 0);
        if(!$this->estimate->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project estimate successfully updated!";
        $this->flags['redirect']   = $this->request->getHTTPReferer();
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueE):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->estimate):
        return RhynoException::Unreachable();

      elseif(!$this->estimate->project == $this->project->_):
        return RhynoException::CustomError("Woah! Estimate is not part of this project!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if(!$this->estimate->delete() || !$this->reviews->delete() || !$this->items->delete())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project estimate successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function SendAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueE):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->estimate):
        return RhynoException::Unreachable();

      elseif(!$this->estimate->project == $this->project->_):
        return RhynoException::CustomError("Woah! Estimate is not part of this project!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      try
      {
        $this->estimate->status  = 2;
        $this->estimate->save();

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Estimate '#{$this->uniqueE}' sent to client.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $client = Clients::findFirst($this->project->client);

        # system notification
        if($this->configuration->client_notification->project_activities_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => "New estimate #{$this->uniqueE} created for the project #{$this->uniqueP}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/estimates"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->configuration->client_notification->project_activities_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => "New estimate #{$this->uniqueE} created for the project #{$this->uniqueP}.",
            "text"     => "You are receiving this notification because a new estimate ( #{$this->uniqueE} ) was created for the project #{$this->uniqueP}!",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/estimates",
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project estimate successfully sent to client!";
        $this->flags['redirect']   = $this->request->getHTTPReferer();
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function SubmitAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->rmethod || !$this->uniqueP || !$this->uniqueE):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->estimate):
        return RhynoException::Unreachable();

      elseif(!$this->estimate->project == $this->project->_):
        return RhynoException::CustomError("Woah! Estimate is not part of this project!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      try
      {
        # case accept estimate
        if($this->rmethod == "accept")
        {
          $this->flags['title'] = "Project estimate successfully accepted & invoiced!";

          $this->estimate->status = 3;
          if(!$this->estimate->save())
          {
            return RhynoException::DBError();
          }

          $issue = \DateTime::createFromFormat($this->rhyno_date, $this->estimate->issue);
          $due   = \DateTime::createFromFormat($this->rhyno_date, $this->estimate->due);

          $inv = new ProjectInvoices;
            $inv->unique            = $this->uniqueCode("INV");
            $inv->project           = $this->project->_;
            $inv->status            = 1;
            $inv->title             = $this->estimate->title;
            $inv->issue             = $issue->format("Y-m-d H:i:s");
            $inv->due               = $due->format("Y-m-d H:i:s");
            $inv->last_recursion    = $due->format("Y-m-d H:i:s");
            $inv->recurring         = $this->estimate->recurring;
          if($inv->save())
          {
            foreach($this->items as $i)
            {
              $itm = new InvoiceItems;
                $itm->unique  = $this->uniqueCode("ITM");
                $itm->invoice = $inv->_;
                $itm->title   = $i->title;
                $itm->text    = $i->text;
                $itm->amount  = $i->amount;
                $itm->price   = $i->price;
                $itm->issued  = $i->issued;
              if(!$itm->save())
              {
                return RhynoException::DBError();
              }
            }
          }
          else
          {
            return RhynoException::DBError();
          }

          $activity = "Estimate '#{$this->uniqueE}' was accepted and invoiced as '#{$inv->unique}' ";
        }
        # if decline
        elseif($this->rmethod == "decline")
        {
          if($this->request->getPost("review"))
          {
            $this->flags['title'] = "Project estimate successfully declined & will be revised!";
            $activity = "Estimate '#{$this->uniqueE}' was declined and requested a review.";

            $this->estimate->status = 4;
            $review = new EstimateReviews;
              $review->estimate = $this->estimate->_;
              $review->text     = $this->request->getPost("text","string");
              $review->date     = (new \DateTime())->format("Y-m-d H:i:s");
            if(!$review->save())
            {
              return RhynoException::DBError();
            }
          }
          else
          {
            $this->flags['title'] = "Project estimate successfully declined!";
            $activity = "Estimate '#{$this->uniqueE}' was declined.";
            $this->estimate->status = 5;
          }

          if(!$this->estimate->save())
          {
            return RhynoException::DBError();
          }
        }
        else{}

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = $activity;
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $client = Clients::findFirst($this->project->client);

        # system notification
        if($this->configuration->client_notification->project_activities_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => $activity,
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/estimates"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->configuration->client_notification->project_activities_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => $activity,
            "text"     => "You are receiving this notification because: {$activity}",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/estimates",
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function RequestAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->request->getPost("title")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("text")):
        return RhynoException::EmptyInput("Description");

      elseif(!$this->request->getPost("budget")):
        return RhynoException::EmptyInput("Budget");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $currency = Currencies::findFirst($this->rhyno_user_info->currency);
        $convert  = $this->CurrencyConverter($this->request->getPost("budget","int"), $currency->code);
        $budget   = $convert->values->converted;

        if(!$budget)
        {
          return RhynoException::CustomError("Currency not converted!");
        }

        $req = new EstimateRequests;
          $req->unique    = $this->uniqueCode("REQ");
          $req->project   = $this->project->_;
          $req->title     = $this->request->getPost("title","string");
          $req->text      = $this->request->getPost("text","string");
          $req->budget    = $budget;
          $req->reference = $this->request->getPost("reference","string");
          $req->date      = (new \DateTime())->format("Y-m-d H:i:s");
          $req->status    = 1;
        if(!$req->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Requested an estimate. '#{$req->unique}'";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Estimate request sent!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form     = new Form;
      $inputs   = [];

      if($this->uniqueP)
      {
        $client = Clients::query()
        ->columns([
          "Manager\Models\Currencies.code",
        ])
        ->innerJoin("Manager\Models\Currencies","Manager\Models\Currencies._ =  Manager\Models\Clients.currency")
        ->where("Manager\Models\Clients._ = :client:")
        ->bind([
          "client" => $this->project->client
        ])
        ->execute();
      }

      if($this->rmethod == "remove"):

      elseif($this->rmethod  == "request"):
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Estimate Title",
        ]);
        $element['text'] = new Textarea( "text" ,[
          'class' => "materialize-textarea",
          'label' => "What you want done?",
        ]);
        $element['budget'] = new Text( "budget" ,[
          'label'             => "What budget do you have in mind? ({$client[0]->code})",
          'data-mask'         => "000.000.000.000.000,00",
          'data-mask-reverse' => true,
        ]);
        $element['reference'] = new Text( "reference" ,[
          'label' => "Do you have any reference link?",
        ]);

      elseif($this->rmethod  == "decline"):
        # CREATING ELEMENTS
        $element['text'] = new Textarea( "text" ,[
          'class' => "materialize-textarea",
          'label' => "Please describe whats wrong.",
        ]);
        $element['review'] = new Check( "review" ,[
          'label' => "Send a modification request?",
          'switch' => true
        ]);
      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Estimate Title",
        ]);
        $element['issue'] = new Text( "issue" ,[
          'label' => "Issue Date",
          'class' => "datepicker",
        ]);
        $element['due'] = new Text( "due" ,[
          'label' => "Due Date",
          'class' => "datepicker",
        ]);
        $element['recurring'] = new Check( "recurring" ,[
          'label'  => "Recurrent Invoice?",
          'switch' => true,
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod  == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/new";
        $modal_header = "Create a Estimate!";

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod  == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/update/{$this->uniqueE}";
        $modal_header = "Update Project estimate!";

        $element['title']->setAttribute("active",true)->setAttribute("value",$this->estimate->title);
        $element['issue']->setAttribute("active",true)->setAttribute("value",(new \DateTime($this->estimate->issue))->format($this->rhyno_date));
        $element['due']  ->setAttribute("active",true)->setAttribute("value",(new \DateTime($this->estimate->due))->format($this->rhyno_date));
        ($this->estimate->recurring == 0) ?: $element['recurring']->setAttribute("checked",true);

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod  == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/delete/{$this->uniqueE}";
        $modal_header = "Remove Project invoice!";

        $modal_card = [
          "content"  => "Are you sure? The selected estimate will me permanently removed from the server along with it's items."
        ];

      elseif ($this->rmethod  == "request"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/request";
        $modal_header = "Estimate Request";

        $modal_card = [
          "content"  => "Please fill out the form with all the details so we can analyze your request and send you an estimate."
        ];

      elseif ($this->rmethod  == "decline"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/decline/{$this->uniqueE}/submit";
        $modal_header = "What's Wrong?";

        $modal_card = [
          "content"  => "Hmm, seems that you're not satisfied with this estimate. You can request a modification to this estimate or just cancel and forget about it ;)"
        ];

      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[
          "label"  => $f->getAttribute("label"),
          "input"  => $f->render($f->getName()),
          "file"   => $f->getAttribute("file"),
          "active" => $f->getAttribute("active"),
          "switch" => $f->getAttribute("switch"),
          "cl"     => $f->getAttribute("cl")
        ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
