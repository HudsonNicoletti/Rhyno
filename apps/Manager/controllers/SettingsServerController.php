<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class SettingsServerController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");
    $form = new Form;

    $element['host'] = new Text( "host" ,[
      'class'         => "validate",
      'value'         => $this->configuration->database->host,
      'data-validate' => true
    ]);

    $element['username'] = new Text( "username" ,[
      'class'         => "validate",
      'value'         => $this->configuration->database->username,
      'data-validate' => true
    ]);

    $element['password'] = new Password( "password" ,[
      'class' => "validate",
      'value' => $this->configuration->database->password,
    ]);

    $element['database'] = new Text( "database" ,[
      'class'         => "validate",
      'value'         => $this->configuration->database->dbname,
      'data-validate' => true
    ]);

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]);

    foreach($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("settings/server");
  }

  public function SaveAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("host") || !$this->request->getPost("username") || !$this->request->getPost("database")):
        return RhynoException::EmptyInput(["Databse Host","Username","Database Name"]);

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $update = $this->UpdateConfigFile([
          "db_host" => $this->request->getPost("host","string"),
          "db_user" => $this->request->getPost("username","string"),
          "db_pass" => $this->request->getPost("password","string"),
          "db_name" => $this->request->getPost("database","string"),
        ]);

        if(!$update):
          return RhynoException::CustomError("Unable to update application configuration file.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Server Configuration Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/settings/server";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
