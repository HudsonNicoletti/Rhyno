<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Team         as Team,
    Manager\Models\Users        as Users,
    Manager\Models\Tasks        as Tasks,
    Manager\Models\Assignments  as Assignments,
    Manager\Models\Departments  as Departments,
    Manager\Models\DepartmentMembers  as DepartmentMembers;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class TeamController extends ControllerBase
{
  private $member_info_inner;
  private $member_info_email;

  public function onConstruct()
  {
    $this->member_info_inner = $this->configuration->team_notification->member_info_inner;
    $this->member_info_email = $this->configuration->team_notification->member_info_email;
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("team/admin/index");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->members_page)
        {
          $this->view->pick("team/team/index");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->permissionHandler("team");
      break;
    }

    $members = Users::query()
    ->columns([
      'Manager\Models\Users.email',
      'Manager\Models\Team.name',
      'Manager\Models\Team._',
      'Manager\Models\Team.image',
      'Manager\Models\Team.phone'
    ])
    ->innerJoin('Manager\Models\Team', 'Manager\Models\Team.uid = Manager\Models\Users._')
    ->orderBy("name ASC")
    ->execute();

    $this->view->members = $members;
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    $username = preg_replace('/\s+/', '', $this->request->getPost("username","string"));
    $password = preg_replace('/\s+/', '', $this->request->getPost("password","string"));
    $email    = $this->request->getPost("email","email");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("name","string")):
        return RhynoException::EmptyInput("Name");

      elseif(!$email):
        return RhynoException::EmptyInput("Email");

      elseif(!$username || !$password):
        return RhynoException::EmptyInput(["Username","Password"]);

      elseif(!$this->isEmail($email)):
        return RhynoException::InvalidEmailAddress();

      elseif( Users::findFirstByEmail($email) ):
        return RhynoException::CustomError("E-Mail already in use, please use a different e-mail.");

      elseif( Users::findFirstByUsername($username) ):
        return RhynoException::CustomError("Username already in use, please use a different username.");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if($this->request->hasFiles()):
          foreach($this->request->getUploadedFiles() as $file):
            if($file->getError() != 0):
              $this->flags['redirect']  = "{$this->rhyno_url}/team";
              return RhynoException::UploadError($file->getError());
            endif;
            $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
            $file->moveTo("assets/manager/images/avatar/{$filename}");
            $this->ResizeImage("assets/manager/images/avatar/{$filename}");
          endforeach;
        endif;

        $user = new Users;
          $user->username   = $username;
          $user->password   = password_hash($password, PASSWORD_BCRYPT );
          $user->email      = $email;
          $user->permission = $this->permissions->team;
        if($user->save())
        {
          $member = new Team;
            $member->uid    = $user->_;
            $member->name   = $this->request->getPost("name","string");
            $member->phone  = $this->request->getPost("phone","string");
            $member->image  = ($filename ?: null);
          $member->save();
        }
        else
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Member successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/team";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    $m = Team::findFirst($this->dispatcher->getParam("member","int"));
    $u = Users::findFirst($m->uid);

    $username = preg_replace('/\s+/', '', $this->request->getPost("username","string"));
    $password = preg_replace('/\s+/', '', $this->request->getPost("password","string"));
    $email    = $this->request->getPost("email","email");

    try
    {
      if(!$this->request->isPost() ):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("name","string")):
        return RhynoException::EmptyInput("Name");

      elseif(!$email):
        return RhynoException::EmptyInput("Email");

      elseif(!$username):
        return RhynoException::EmptyInput("Username");

      elseif(!$this->isEmail($email)):
        return RhynoException::InvalidEmailAddress();

      elseif( $email != $u->email && Users::findFirstByEmail($email) ):
        return RhynoException::CustomError("E-Mail already in use, please use a different e-mail.");

      elseif( $username != $u->username && Users::findFirstByUsername($username) ):
        return RhynoException::CustomError("Username already in use, please use a different username.");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if($this->request->hasFiles()):
          foreach($this->request->getUploadedFiles() as $file):
            if($file->getError() != 0):
              $this->flags['redirect']  = "{$this->rhyno_url}/team";
              return RhynoException::UploadError($file->getError());
            endif;
            $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
            $file->moveTo("assets/manager/images/avatar/{$filename}");
            $this->ResizeImage("assets/manager/images/avatar/{$filename}");
          endforeach;
          @unlink("assets/manager/images/avatar/{$m->image}");
        endif;

        $u->username   = $username;
        $u->password   = ($password != null ) ? password_hash($password, PASSWORD_BCRYPT ) : $u->password;
        $u->email      = $email;

        $m->name          = $this->request->getPost("name","string");
        $m->phone         = $this->request->getPost("phone","string");
        $m->image         = ($filename ?: $m->image);

        if(!$u->save()){ return RhynoException::DBError(); }
        if(!$m->save()){ return RhynoException::DBError(); }

        # system notification
        if($this->member_info_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $m->uid,
            "text"     => "Your account information was updated!",
            "href"     => "{$this->rhyno_url}/account"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->member_info_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $m->uid,
            "title"    => "Your account information was updated!",
            "text"     => "You are receiving this e-mail because your account information was updated by a admin!",
            "href"     => "{$this->rhyno_url}/account",
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']     = "success";
        $this->flags['title']     = "Member information successfully updated!";
        $this->flags['redirect']  = "{$this->rhyno_url}/team";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);


    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");
    $mid = $this->dispatcher->getParam('member','int');
    $new_member = $this->request->getPost("member","int");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$mid):
        return RhynoException::WrongNumberOfParams();

      elseif(!$new_member):
        return RhynoException::EmptyInput("Member");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        # REMOVE FROM Team & Users
        $t = Team::findFirst($mid);
        # REMOVE FROM Users
        $u = Users::findFirst($t->uid);
        # REMOVE FROM Assignments
        $a = Assignments::findByMember($mid);
        # REMOVE FROM DepartmentMembers
        $d = DepartmentMembers::findByMember($mid);

        if(!$t->delete()):
          return RhynoException::CustomError("Unable to remove team member.");
        elseif(!$u->delete()):
          return RhynoException::CustomError("Unable to remove user.");
        elseif(!$a->delete()):
          return RhynoException::CustomError("Unable to remove member's assignments.");
        elseif(!$d->delete()):
          return RhynoException::CustomError("Unable to remove member from department.");
        endif;

        # remove image from server
        @unlink("assets/manager/images/avatar/{$t->image}");

        # UPDATE Tasks
        foreach(Tasks::findByAssigned($mid) as $task)
        {
          $task->assigned = $new_member;
          if(!$task->save())
          {
            return RhynoException::CustomError("Unable to move assignments to new member.");
          }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Team member deleted!";
        $this->flags['redirect']   = "{$this->rhyno_url}/team";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form = new Form();
      $inputs = [];
      $uid = $this->dispatcher->getParam("member");

      if($uid)
      {
        $member = Users::query()
        ->columns([
          'Manager\Models\Users.email',
          'Manager\Models\Users.username',
          'Manager\Models\Team._',
          'Manager\Models\Team.name',
          'Manager\Models\Team.phone'
        ])
        ->innerJoin('Manager\Models\Team', 'Manager\Models\Team.uid = Manager\Models\Users._')
        ->where("Manager\Models\Team._ = :user:")
        ->bind([
          "user"  => $uid
        ])
        ->execute();
      }

      if($this->dispatcher->getParam("method") == "remove"):
        $element['member'] = new Select( "member" , Team::find([" uid != '{$uid}' "]) ,[
          'using' =>  ['_','name'],
        ]);
      else:

      # CREATING ELEMENTS
      $element['name'] = new Text( "name" ,[
        'class'         => "validate",
        'label'         => "Full Name",
      ]);

      $element['email'] = new Text( "email" ,[
        'class'         => "validate",
        'label'         => "E-Mail",
      ]);

      $element['phone'] = new Text( "phone" ,[
        'class'         => "validate",
        'label'         => "Phone Number",
      ]);

      $element['username'] = new Text( "username" ,[
        'class'         => "validate",
        'label'         => "Username",
        'data-validate' => true,
      ]);

      $element['password'] = new Password( "password" ,[
        'class'         => "validate",
        'label'         => "Password",
      ]);

      $element['image'] = new File( "image" ,[
        'class' => "validate",
        'label' => "User Image",
        'file'  => true
      ]);

      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->dispatcher->getParam("method") == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/team/new";
        $modal_header = "Create a new member!";

        $element['password']->setAttribute("data-validate",true)->setAttribute("data-empty","* Campo Obrigatório");
        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO UPDATE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->dispatcher->getParam("method") == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/team/update/{$uid}";
        $modal_header = "Update member information!";

        $modal_card = [
          ["content"  => "Please note that the fields are optional for updating: <i>Password</i> , <i>User Image</i> , <i>Phone</i>"]
        ];

        ($this->configuration->team_notification->member_info_email) ? array_push($modal_card,["content" => 'E-Mail Notification is enabled , this might take a sec depending on your connection.']) : null ;

        $element['name']        ->setAttribute("active",true)->setAttribute("value",$member[0]->name);
        $element['email']       ->setAttribute("active",true)->setAttribute("value",$member[0]->email);
        $element['phone']       ->setAttribute("active",true)->setAttribute("value",$member[0]->phone);
        $element['username']    ->setAttribute("active",true)->setAttribute("value",$member[0]->username);
        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->dispatcher->getParam("method") == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/team/delete/{$uid}";
        $modal_header = "Remove Member!";

        $modal_card = [
          "content"  => "Please select a member to transfer all projects and tasks assignments from this member."
        ];

        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO VIEW POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->dispatcher->getParam("method") == "view"):
        $modal_header = "Member Information!";
        $modal_info = [
          "titles" => [
            ["title" => "Full Name"],
            ["title" => "E-Mail"],
            ["title" => "Phone"],
            ["title" => "Username"],
          ],
          "contents"  => [
            ["content" => $member[0]->name],
            ["content" => $member[0]->email],
            ["content" => $member[0]->phone],
            ["content" => $member[0]->username],
          ]
        ];

      endif;

      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
