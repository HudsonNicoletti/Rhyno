<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden;

class SettingsNotificationsController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");
    $form = new Form;

    $element['member_info_email']         = new Check("member_info_email",        ["checked" => ($this->configuration->team_notification->member_info_email ?: null)]);
    $element['member_info_inner']         = new Check("member_info_inner",        ["checked" => ($this->configuration->team_notification->member_info_inner ?: null)]);
    $element['member_permission_email']   = new Check("member_permission_email",  ["checked" => ($this->configuration->team_notification->member_permission_email ?: null)]);
    $element['member_permission_inner']   = new Check("member_permission_inner",  ["checked" => ($this->configuration->team_notification->member_permission_inner ?: null)]);
    $element['project_assigned_email']    = new Check("project_assigned_email",   ["checked" => ($this->configuration->team_notification->project_assigned_email ?: null)]);
    $element['project_assigned_inner']    = new Check("project_assigned_inner",   ["checked" => ($this->configuration->team_notification->project_assigned_inner ?: null)]);
    $element['project_activities_email']  = new Check("project_activities_email", ["checked" => ($this->configuration->team_notification->project_activities_email ?: null)]);
    $element['project_activities_inner']  = new Check("project_activities_inner", ["checked" => ($this->configuration->team_notification->project_activities_inner ?: null)]);
    $element['task_assigned_email']       = new Check("task_assigned_email",      ["checked" => ($this->configuration->team_notification->task_assigned_email ?: null)]);
    $element['task_assigned_inner']       = new Check("task_assigned_inner",      ["checked" => ($this->configuration->team_notification->task_assigned_inner ?: null)]);
    $element['task_response_email']       = new Check("task_response_email",      ["checked" => ($this->configuration->team_notification->task_response_email ?: null)]);
    $element['task_response_inner']       = new Check("task_response_inner",      ["checked" => ($this->configuration->team_notification->task_response_inner ?: null)]);
    $element['task_memberstatus_email']   = new Check("task_memberstatus_email",  ["checked" => ($this->configuration->team_notification->task_memberstatus_email ?: null)]);
    $element['task_memberstatus_inner']   = new Check("task_memberstatus_inner",  ["checked" => ($this->configuration->team_notification->task_memberstatus_inner ?: null)]);
    $element['task_status_email']         = new Check("task_status_email",        ["checked" => ($this->configuration->team_notification->task_status_email ?: null)]);
    $element['task_status_inner']         = new Check("task_status_inner",        ["checked" => ($this->configuration->team_notification->task_status_inner ?: null)]);
    $element['task_info_email']           = new Check("task_info_email",          ["checked" => ($this->configuration->team_notification->task_info_email ?: null)]);
    $element['task_info_inner']           = new Check("task_info_inner",          ["checked" => ($this->configuration->team_notification->task_info_inner ?: null)]);
    $element['ticket_created_email']      = new Check("ticket_created_email",     ["checked" => ($this->configuration->team_notification->ticket_created_email ?: null)]);
    $element['ticket_created_inner']      = new Check("ticket_created_inner",     ["checked" => ($this->configuration->team_notification->ticket_created_inner ?: null)]);
    $element['ticket_response_email']     = new Check("ticket_response_email",    ["checked" => ($this->configuration->team_notification->ticket_response_email ?: null)]);
    $element['ticket_response_inner']     = new Check("ticket_response_inner",    ["checked" => ($this->configuration->team_notification->ticket_response_inner ?: null)]);
    $element['ticket_status_email']       = new Check("ticket_status_email",      ["checked" => ($this->configuration->team_notification->ticket_status_email ?: null)]);
    $element['ticket_status_inner']       = new Check("ticket_status_inner",      ["checked" => ($this->configuration->team_notification->ticket_status_inner ?: null)]);

    $element['client_info_email']               = new Check("client_info_email",              ["checked" => ($this->configuration->client_notification->client_info_email ?: null)]);
    $element['client_info_inner']               = new Check("client_info_inner",              ["checked" => ($this->configuration->client_notification->client_info_inner ?: null)]);
    $element['clientproject_create_email']      = new Check("clientproject_create_email",     ["checked" => ($this->configuration->client_notification->project_create_email ?: null)]);
    $element['clientproject_create_inner']      = new Check("clientproject_create_inner",     ["checked" => ($this->configuration->client_notification->project_create_inner ?: null)]);
    $element['clientproject_activities_email']  = new Check("clientproject_activities_email", ["checked" => ($this->configuration->client_notification->project_activities_email ?: null)]);
    $element['clientproject_activities_inner']  = new Check("clientproject_activities_inner", ["checked" => ($this->configuration->client_notification->project_activities_inner ?: null)]);
    $element['clientproject_status_email']      = new Check("clientproject_status_email",     ["checked" => ($this->configuration->client_notification->project_status_email ?: null)]);
    $element['clientproject_status_inner']      = new Check("clientproject_status_inner",     ["checked" => ($this->configuration->client_notification->project_status_inner ?: null)]);
    $element['projectinvoice_status_email']     = new Check("projectinvoice_status_email",    ["checked" => ($this->configuration->client_notification->projectinvoice_status_email ?: null)]);
    $element['projectinvoice_status_inner']     = new Check("projectinvoice_status_inner",    ["checked" => ($this->configuration->client_notification->projectinvoice_status_inner ?: null)]);
    $element['clientticket_response_email']     = new Check("clientticket_response_email",    ["checked" => ($this->configuration->client_notification->ticket_response_email ?: null)]);
    $element['clientticket_response_inner']     = new Check("clientticket_response_inner",    ["checked" => ($this->configuration->client_notification->ticket_response_inner ?: null)]);
    $element['clientticket_status_email']       = new Check("clientticket_status_email",      ["checked" => ($this->configuration->client_notification->ticket_status_email ?: null)]);
    $element['clientticket_status_inner']       = new Check("clientticket_status_inner",      ["checked" => ($this->configuration->client_notification->ticket_status_inner ?: null)]);

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    foreach($element as $e){ $form->add($e); }

    $this->view->form = $form;
    $this->view->pick("settings/notifications");
  }

  public function SaveAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->dispatcher->getParam("method","string")):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if($this->dispatcher->getParam("method","string") == "members"):
          $parameters = [
            'member_info_email'       => ($this->request->getPost("member_info_email") ? "true" : "false"),
            'member_info_inner'       => ($this->request->getPost("member_info_inner") ? "true" : "false"),
            'member_permission_email' => ($this->request->getPost("member_permission_email") ? "true" : "false"),
            'member_permission_inner' => ($this->request->getPost("member_permission_inner") ? "true" : "false"),
            'project_assigned_email'  => ($this->request->getPost("project_assigned_email") ? "true" : "false"),
            'project_assigned_inner'  => ($this->request->getPost("project_assigned_inner") ? "true" : "false"),
            'project_activities_email'=> ($this->request->getPost("project_activities_email") ? "true" : "false"),
            'project_activities_inner'=> ($this->request->getPost("project_activities_inner") ? "true" : "false"),
            'task_assigned_email'     => ($this->request->getPost("task_assigned_email") ? "true" : "false"),
            'task_assigned_inner'     => ($this->request->getPost("task_assigned_inner") ? "true" : "false"),
            'task_response_email'     => ($this->request->getPost("task_response_email") ? "true" : "false"),
            'task_response_inner'     => ($this->request->getPost("task_response_inner") ? "true" : "false"),
            'task_memberstatus_email' => ($this->request->getPost("task_memberstatus_email") ? "true" : "false"),
            'task_memberstatus_inner' => ($this->request->getPost("task_memberstatus_inner") ? "true" : "false"),
            'task_status_email'       => ($this->request->getPost("task_status_email") ? "true" : "false"),
            'task_status_inner'       => ($this->request->getPost("task_status_inner") ? "true" : "false"),
            'task_info_email'         => ($this->request->getPost("task_info_email") ? "true" : "false"),
            'task_info_inner'         => ($this->request->getPost("task_info_inner") ? "true" : "false"),
            'ticket_created_email'    => ($this->request->getPost("ticket_created_email") ? "true" : "false"),
            'ticket_created_inner'    => ($this->request->getPost("ticket_created_inner") ? "true" : "false"),
            'ticket_response_email'   => ($this->request->getPost("ticket_response_email") ? "true" : "false"),
            'ticket_response_inner'   => ($this->request->getPost("ticket_response_inner") ? "true" : "false"),
            'ticket_status_email'     => ($this->request->getPost("ticket_status_email") ? "true" : "false"),
            'ticket_status_inner'     => ($this->request->getPost("ticket_status_inner") ? "true" : "false"),
          ];
        elseif($this->dispatcher->getParam("method","string") == "clients"):
          $parameters = [
            'client_info_email'               => ($this->request->getPost("client_info_email") ? "true" : "false"),
            'client_info_inner'               => ($this->request->getPost("client_info_inner") ? "true" : "false"),
            'clientproject_create_email'      => ($this->request->getPost("clientproject_create_email") ? "true" : "false"),
            'clientproject_create_inner'      => ($this->request->getPost("clientproject_create_inner") ? "true" : "false"),
            'clientproject_activities_email'  => ($this->request->getPost("clientproject_activities_email") ? "true" : "false"),
            'clientproject_activities_inner'  => ($this->request->getPost("clientproject_activities_inner") ? "true" : "false"),
            'clientproject_complete_email'    => ($this->request->getPost("clientproject_complete_email") ? "true" : "false"),
            'clientproject_complete_inner'    => ($this->request->getPost("clientproject_complete_inner") ? "true" : "false"),
            'projectestimate_status_email'    => ($this->request->getPost("projectestimate_status_email") ? "true" : "false"),
            'projectestimate_status_inner'    => ($this->request->getPost("projectestimate_status_inner") ? "true" : "false"),
            'projectinvoice_status_email'     => ($this->request->getPost("projectinvoice_status_email") ? "true" : "false"),
            'projectinvoice_status_inner'     => ($this->request->getPost("projectinvoice_status_inner") ? "true" : "false"),
            'clientticket_response_email'     => ($this->request->getPost("clientticket_response_email") ? "true" : "false"),
            'clientticket_response_inner'     => ($this->request->getPost("clientticket_response_inner") ? "true" : "false"),
            'clientticket_status_email'       => ($this->request->getPost("clientticket_status_email") ? "true" : "false"),
            'clientticket_status_inner'       => ($this->request->getPost("clientticket_status_inner") ? "true" : "false"),
          ];
        endif;

        $update = $this->UpdateConfigFile($parameters);

        if(!$update):
          return RhynoException::CustomError("Unable to update application configuration file.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Notifications Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/settings/notifications";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
