<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users as Users,
    Manager\Models\Team as Team;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden;

class SettingsAdministratorsController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");

    $admins = Users::query()
    ->columns([
      'Manager\Models\Users._',
      'Manager\Models\Users.email',
      'Manager\Models\Team.name',
      'Manager\Models\Team.image',
    ])
    ->innerJoin("Manager\Models\Team","Manager\Models\Team.uid = Manager\Models\Users._")
    ->where("Manager\Models\Users.permission >= :permission:")
    ->bind([
      "permission" => $this->permissions->admin
    ])
    ->orderBy("Manager\Models\Team.name ASC")
    ->execute();

    $this->view->admins  = $admins;
    $this->view->pick("settings/administrators");
  }

  public function SaveAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(count($this->request->getPost("members")) == 0 ):
        return RhynoException::CustomError("Please Choose at least one member as an admin.");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $members = $this->request->getPost("members");

        # GET ALL Admins
        $admins = Users::find(["permission >= '{$this->permissions->admin}'"]);
        # Set array to push assigned admins
        $assignedAdmins = [];
        $exceptions     = ["inner"=>[],"email"=>[]];
        # Loop throgh already assigned admins
        foreach($admins as $a)
        {
          # Verify if admin is not in the post array
          if(!in_array($a->_,$members))
          {
            $a->permission = $this->permissions->team;
            if(!$a->save())
            {
              return RhynoException::DBError();
            }
          }
          # else push this user to array for comparision
          else
          {
            array_push($assignedAdmins, $a->_);
          }
        }
        # loop throgh post
        foreach($members as $member)
        {
          # if current member is not already collaborating , insert into database
          if(!in_array($member,$assignedAdmins))
          {
            $user = Users::findFirst($member);
            $user->permission = $this->permissions->admin;
            if(!$user->save())
            {
              return RhynoException::DBError();
            }
          }
        }
        foreach($members as $member)
        {
          # if current member is not already collaborating , insert into database
          if(!in_array($member,$assignedAdmins))
          {
            # system notification
            $receiver = Team::findFirst($member);
            $user_inf = Users::findFirst($receiver->uid);
            if($this->configuration->team_notification->member_permission_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "Your account permisson level was updated! You have an admin permission!",
              ]);

              if(!$inner){ array_push($exceptions["inner"],$receiver->name); }
            }

            if($this->configuration->team_notification->member_permission_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "Your account permission was updated!",
                "text"     => "You are receiving this e-mail because your account permission was updated! You now have admin permissions!"
              ]);

              if(!$email){ array_push($exceptions["email"],$user_inf->email); }
            }
          }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Admins have been updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/settings/administrators";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['title']  = "Invalid Method.";
      $this->flags['text']   = "Metodo Inválido.";
    endif;

    if($this->flags['status']):

      $form = new Form();
      $inputs = [];
      $method = $this->dispatcher->getParam("method");

      # CREATING ELEMENTS
      foreach(Team::find(["order"=>"name ASC"]) as $member)
      {
        $element[$member->uid] = new Check( $member->uid ,[
          'name'  => "members[]",
          'id'    => rand(),
          'value' => $member->uid,
          'label' => $member->name ,
          'cl'    => "m6 l4" # Classes
        ]);
      }

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $method == "manage" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/settings/administrators/save";
        $modal_header = "Manage Admininstrators";
        $modal_card = [];

        ($this->configuration->team_notification->member_permission_email) ? array_push($modal_card,["content" => 'E-Mail Notification is enabled , this might take a sec depending on your connection.']) : null ;

        foreach(Users::find(["permission >= '{$this->permissions->admin}'"]) as $admin)
        {
          $element[$admin->_]->setAttribute("checked",true);
        }
      endif;

      # ADD EACH ELEMENT TO FORM
      foreach($element as $e){ $form->add($e); }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
