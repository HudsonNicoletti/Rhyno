<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Team as Team,
    Manager\Models\Departments as Departments,
    Manager\Models\DepartmentMembers as DepartmentMembers;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden;

class DepartmentsController extends ControllerBase
{
  public function MembersAction()
  {
    $this->response->setContentType("application/json");

    $members = [];
    foreach ($this->request->getPost("content") as $c) {
      $dept = DepartmentMembers::findByDepartment($c);
      foreach ($dept as $d) {
        array_push($members, $d->member);
      }
    }

    return $this->response->setJsonContent([
      "data" => $members,
    ]);
    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function IndexAction()
  {
    $this->permissionHandler("admin");

    $departments = Departments::query()
    ->columns([
      'Manager\Models\Departments._',
      'Manager\Models\Departments.department',
      'COUNT( DISTINCT Manager\Models\DepartmentMembers.member) as count',
    ])
    ->leftJoin('Manager\Models\DepartmentMembers', 'Manager\Models\DepartmentMembers.department = Manager\Models\Departments._')
    ->groupBy("Manager\Models\Departments._")
    ->orderBy("Manager\Models\Departments.department ASC")
    ->execute();

    $this->view->departments  = $departments;
    $this->view->pick("team/admin/departments");
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");
    # catch any erros
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $exception = ['dm'=>[]];
        $d = new Departments();
          $d->department = $this->request->getPost("title","string");
        if($d->save())
        {
          foreach ($this->request->getPost("members") as $m) {
            $dm = new DepartmentMembers;
              $dm->department = $d->_;
              $dm->member = $m;
            if(!$dm->save()){ array_push($exception['dm'],Team::findFirst($m)->name); }
          }
        }
        else
        {
          return RhynoException::DBError();
        }

        if(count($exception['dm']) > 0)
        {
          return RhynoException::NotIncluded($exception['dm']);
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Department successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/departments";

      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");
    # catch any erros
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->dispatcher->getParam("department")):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $exception = ['dm'=>[]];
        $department = $this->dispatcher->getParam("department","int");

        $d = Departments::findFirst($department);
          $d->department = $this->request->getPost("title","string");
        if($d->save())
        {
          $members = DepartmentMembers::findByDepartment($department);
          foreach ($members as $member)
          {
            $member->delete();
          }
          foreach ($this->request->getPost("members") as $m) {
            $dm = new DepartmentMembers;
              $dm->department = $d->_;
              $dm->member = $m;
            if(!$dm->save()){ array_push($exception['dm'],Team::findFirst($m)->name); }
          }
        }
        else
        {
          return RhynoException::DBError();
        }

        if(count($exception['dm']) > 0)
        {
          return RhynoException::NotIncluded($exception['dm']);
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Department successfully updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/departments";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']      = "warning";
        $this->flags['title']      = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");
    # catch any erros
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->dispatcher->getParam("department")):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $department = $this->dispatcher->getParam("department","int");
        $new = $this->request->getPost("department");

        $d = Departments::findFirst($department);
        if($d->delete())
        {
          foreach (DepartmentMembers::findByDepartment($department) as $dm)
          {
            $verify_user = DepartmentMembers::findFirst(["department = '{$new}' AND member ='{$dm->member}' "]);
            if($verify_user == NULL)
            {
              $dm->department = $new;
              $dm->save();
            }
            else
            {
              $dm->delete();
            }
          }
        }
        else
        {
          return RhynoException::Unreachable();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Department successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/departments";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']      = "warning";
        $this->flags['title']      = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);
    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['title']  = "Invalid Method.";
      $this->flags['text']   = "Metodo Inválido.";
    endif;

    if($this->flags['status']):

      $form = new Form();
      $action = false;
      $alert = false;
      $inputs = [];
      $id = $this->dispatcher->getParam("department","int");
      $method = $this->dispatcher->getParam("method","string");

      if($id)
      {
        $department = Departments::findFirst($id);
      }

      if( $method == "remove" ):

        $element['department'] = new Select( "department" , Departments::find(["_ != '{$id}'"]) ,[
          'using' => ['_','department'],
          'label' => "Department Title",
        ]);

      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Department Title",
        ]);

        foreach(Team::find(["order"=>"name asc"]) as $team)
        {
          $element[$team->_] = new Check( $team->_ ,[
            'name'  => "members[]",
            'id'    => explode(" ",$team->name)[0],
            'value' => $team->_,
            'label' => $team->name ,
            'cl'    => "m6 l4" # Classes
          ]);
        }
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $method == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/department/new";
        $modal_header = "Create a new department!";
        $modal_card = [
          "content" => "Here you can create a new department and assign some members on the fly!"
        ];

        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO UPDATE POPULATE WITH VALJUE TO ELEMENT
      elseif ( $method == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/department/update/{$id}";
        $modal_header = "Update a department!";
        $modal_card = [
          "content" => "You can unassign any member an assign others from here !"
        ];

        $element["title"]->setAttribute("active",true)->setAttribute("value",$department->department);
        foreach(DepartmentMembers::findByDepartment($id) as $d)
        {
          $element[$d->member]->setAttribute("checked",true);
        }

        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO REMOVE POPULATE WITH VALJUE TO ELEMENT
      elseif ($method == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/department/delete/{$id}";
        $modal_header = "Remove Department!";
        $modal_card = [
          "content" => "Are you sure that you want to remove this department ? Every member involved will be affected. Please select a different deparment for data transfer."
        ];

        foreach($element as $e)
        {
          $form->add($e);
        }

      endif;

      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()), "active" => $f->getAttribute("active"), "file" => $f->getAttribute("file"), "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
