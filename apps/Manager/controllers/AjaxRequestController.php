<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users as Users,
    Manager\Models\Projects as Projects,
    Manager\Models\ProjectActivities as ProjectActivities;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class AjaxRequestController extends ControllerBase
{
  public function CurrencyRatesAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isGet() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false;
      return $this->response->redirect("/error/notfound",true);
    }

    if($this->flags['status']):
      return $this->response->setJsonContent($this->CurrencyRateCheck());
    endif;

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ClsnnAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false;
      return $this->response->redirect("/error/notfound",true);
    }

    if($this->flags['status']):

      $this->clearNewNotifications();
      return true;

    endif;

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ProjectActivitiesAction()
  {
    $this->response->setContentType("application/json");
    $unique = $this->dispatcher->getParam("project");
    $amount = $this->request->getQuery("amount");
    $project = Projects::findFirstByUnique($unique);

    try
    {
      if(!$this->request->isGet() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();
      elseif(!$unique || !$amount):
        return RhynoException::WrongNumberOfParams();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false;
      return $this->response->redirect("/error/notfound",true);
    }

    if($this->flags['status']):

      $activities = ProjectActivities::query()
      ->columns([
        "Manager\Models\ProjectActivities._",
        "Manager\Models\ProjectActivities.text",
        "Manager\Models\ProjectActivities.date",
        "COALESCE(Manager\Models\Team.name,Manager\Models\Clients.name) as name",
        "COALESCE(Manager\Models\Team.image,Manager\Models\Clients.image) as image",
      ])
      ->leftJoin('Manager\Models\Team', 'Manager\Models\ProjectActivities.user = Manager\Models\Team.uid')
      ->leftJoin('Manager\Models\Clients', 'Manager\Models\ProjectActivities.user = Manager\Models\Clients.uid')
      ->where("Manager\Models\ProjectActivities.project = :project:")
      ->bind([
        "project" => $project->_
      ])
      ->limit(10,$amount)
      ->orderBy("Manager\Models\ProjectActivities.date DESC")
      ->execute();

      if(count($activities) > 0):
        $status = true;
        $data   = $activities;
      else:
        $status = false;
        $data   = false;
      endif;

    endif;

    return $this->response->setJsonContent([
      "status" => $status,
      "data"   => $data
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
