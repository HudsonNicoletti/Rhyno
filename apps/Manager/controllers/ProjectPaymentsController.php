<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Projects          as Projects,
    Manager\Models\Clients           as Clients,
    Manager\Models\Currencies        as Currencies,
    Manager\Models\ProjectActivities as ProjectActivities,
    Manager\Models\ProjectInvoices   as ProjectInvoices,
    Manager\Models\InvoicePayments   as InvoicePayments,
    Manager\Models\PayPalPayments    as PayPalPayments,
    Manager\Models\StripePayments    as StripePayments;

use Mustache_Engine as Mustache;

use PayPal\Api\Payer            as Payer,
    PayPal\Api\Details          as Details,
    PayPal\Api\Item             as Item,
    PayPal\Api\ItemList         as ItemList,
    PayPal\Api\Amount           as Amount,
    PayPal\Api\Transaction      as Transaction,
    PayPal\Api\Payment          as Payment,
    PayPal\Api\RedirectUrls     as RedirectUrls,
    PayPal\Api\PaymentExecution as PaymentExecution,
    PayPal\Exeption\PPConnectionExeption as PPConnectionExeption;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectPaymentsController extends ControllerBase
{
  private $uniqueP;
  private $uniqueI;
  private $uniqueY;
  private $rmethod;
  private $project;
  private $invoice;
  private $payment;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueI = $this->dispatcher->getParam("invoice","string");
    $this->uniqueY = $this->dispatcher->getParam("payment","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project = Projects::findFirstByUnique($this->uniqueP);
    $this->invoice = ProjectInvoices::findFirstByUnique($this->uniqueI);
    $this->payment = InvoicePayments::findFirstByUnique($this->uniqueY);
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/payments");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projectpayments_page)
        {
          $this->view->pick("projects/team/payments");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->view->pick("projects/client/payments");
        $this->view->crud = $this->configuration->client_crud;
        $this->view->currency = Currencies::findFirst($this->rhyno_user_info->currency);
      break;
    }

    $payments = InvoicePayments::query()
    ->columns([
      "Manager\Models\InvoicePayments._",
      "Manager\Models\InvoicePayments.unique",
      "Manager\Models\InvoicePayments.invoice as invoiceid",
      "Manager\Models\ProjectInvoices.unique as invoice",
      "Manager\Models\ProjectInvoices.title",
      "Manager\Models\InvoicePayments.method",
      "Manager\Models\InvoicePayments.amount",
      "Manager\Models\InvoicePayments.date",
    ])
    ->innerJoin("Manager\Models\ProjectInvoices","Manager\Models\ProjectInvoices._ = Manager\Models\InvoicePayments.invoice")
    ->where("Manager\Models\InvoicePayments.project = :project:")
    ->bind([
      "project" => $this->project->_
    ])
    ->orderBy("date DESC")
    ->execute();

    $this->view->project  = $this->project;
    $this->view->payments = $payments;
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->request->getPost("value")):
        return RhynoException::EmptyInput("Value");

      elseif(!$this->request->getPost("date")):
        return RhynoException::EmptyInput("Date");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {

        $date   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("date","string"));

        $pay = new InvoicePayments;
          $pay->unique  = $this->uniqueCode("PAY");
          $pay->invoice = $this->request->getPost("invoice","int");
          $pay->project = $this->project->_;
          $pay->amount  = $this->request->getPost("value","int");
          $pay->method  = $this->request->getPost("method","int");
          $pay->payer   = $this->rhyno_user->_;
          $pay->date    = $date->format("Y-m-d H:i:s");
        if(!$pay->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Added a new Payment '#{$pay->unique}'.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $client = Clients::findFirst($this->project->client);

        # system notification
        if($this->configuration->client_notification->project_activities_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => "Added a new payment, '#{$pay->unique}' for the project #{$this->uniqueP}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/payments"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->configuration->client_notification->project_activities_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => "Added a new payment, '#{$pay->unique}' for the project #{$this->uniqueP}.",
            "text"     => "You are receiving this notification because a new payment, '{$pay->unique}' was created for the project #{$this->uniqueP}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/payments"
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Payment successfully added!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/payments";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueY):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->payment):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if(!$this->payment->delete())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Payment successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/payments";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function PayPalAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $item        = new Item();
        $payer       = new Payer();
        $itemList    = new ItemList();
        $details     = new Details();
        $amount      = new Amount();
        $payment     = new Payment();
        $redirect    = new RedirectUrls();
        $transaction = new Transaction();

        $invoice  = ProjectInvoices::query()
        ->columns([
          'Manager\Models\ProjectInvoices._',
          'Manager\Models\ProjectInvoices.due',
          "(SELECT SUM(Manager\Models\InvoiceItems.price * Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as total",
          "(SELECT SUM(Manager\Models\InvoicePayments.amount) FROM Manager\Models\InvoicePayments where Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._) as paid",
        ])
        ->where("Manager\Models\ProjectInvoices.unique = :invoice:")
        ->bind([
          "invoice" => $this->uniqueI
        ])
        ->execute();

        $orig_value  = $invoice[0]->total;
        $tax_value   = $this->calcTax($orig_value,$invoice[0]->due);
        $total_value = (($orig_value + $tax_value) /100);

        $item->setName("Invoice #{$this->uniqueI}")
        ->setCurrency($this->configuration->app->currency)
        ->setQuantity(1)
        ->setPrice(($orig_value/100));

        $itemList = new ItemList();
        $itemList->setItems([$item]);

        $payer
          ->setPaymentMethod("paypal");
        $details
          ->setShipping(0)
          ->setTax(($tax_value/100))
          ->setSubtotal(($orig_value/100));
        $amount
          ->setCurrency($this->configuration->app->currency)
          ->setTotal($total_value)
          ->setDetails($details);
        $transaction
          ->setAmount($amount)
          ->setItemList($itemList)
          ->setDescription("Invoice #{$this->uniqueI} payment.");
        $redirect
          ->setReturnUrl("http://".$this->request->getHttpHost()."{$this->rhyno_url}/payments/verification/paypal/{$this->uniqueP}/{$this->uniqueI}")
          ->setCancelUrl("http://".$this->request->getHttpHost()."{$this->rhyno_url}/project/{$this->uniqueP}/invoices/overview/{$this->uniqueI}");
        $payment
          ->setIntent("sale")
          ->setPayer($payer)
          ->setTransactions([$transaction])
          ->setRedirectUrls($redirect);

        $payment->create($this->paypal);

        $pp = New PayPalPayments;
          $pp->user    = $this->rhyno_user->_;
          $pp->invoice = $invoice[0]->_;
          $pp->ppid    = $payment->getId();
          $pp->value   = ($total_value*100);
          $pp->date    = (new \DateTime("now"))->format("Y-m-d H:i:s");
        if(!$pp->save())
        {
          return RhynoException::DBError();
        }

        foreach($payment->getLinks() as $link )
        {
          if($link->getRel() == 'approval_url')
          {
            $this->flags['toast']      = "success";
            $this->flags['title']      = "Redirecting to PayPal.";
            $this->flags['redirect']   = $link->getHref();
          }
        }

      }
      catch(\Exception $e)
      {
        $this->flags['toast'] = "warning";
        $this->flags['title'] = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function PayPalVerificationAction()
  {
    $paymentid = $this->request->getQuery("paymentId","string");
    $token     = $this->request->getQuery("token","string");
    $payerid   = $this->request->getQuery("PayerID","string");

    $pp = PayPalPayments::findFirstByPpid($paymentid);

    $execute = new PaymentExecution();
    $execute->setPayerId($payerid);
    $payment = Payment::get($paymentid, $this->paypal);
    $payment->execute($execute, $this->paypal);

    switch ($payment->state)
    {
      case 'approved':
        $pay = new InvoicePayments;
          $pay->unique  = $this->uniqueCode("PAY");
          $pay->project = $this->invoice->project;
          $pay->invoice = $this->invoice->_;
          $pay->amount  = $pp->value;
          $pay->method  = 2;
          $pay->payer   = $this->rhyno_user->_;
          $pay->date    = (new \DateTime("now"))->format("Y-m-d H:i:s");
        $pay->save();
        $redirect = "#success";
      break;
      case 'failed':
        $redirect = "#not-approved";
      break;
    }
    $this->response->redirect("{$this->rhyno_url}/project/{$this->uniqueP}/invoices/overview/{$this->uniqueI}{$redirect}",true);
  }

  public function StripeAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      $invoice  = ProjectInvoices::query()
      ->columns([
        'Manager\Models\ProjectInvoices._',
        'Manager\Models\ProjectInvoices.due',
        "(SELECT SUM(Manager\Models\InvoiceItems.price * Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as total",
        "(SELECT SUM(Manager\Models\InvoicePayments.amount) FROM Manager\Models\InvoicePayments where Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._) as paid",
      ])
      ->where("Manager\Models\ProjectInvoices.unique = :invoice:")
      ->bind([
        "invoice" => $this->uniqueI
      ])
      ->execute();

      $orig_value  = $invoice[0]->total;
      $tax_value   = $this->calcTax($orig_value,$invoice[0]->due);
      $total_value = ($orig_value + $tax_value);
      $fl = false;
      try
      {
        # need to move this to services
        \Stripe\Stripe::setApiKey($this->configuration->stripe->secret);
        $charge = \Stripe\Charge::create([
          "amount"    => $total_value,
          "currency"  => $this->configuration->app->currency,
          "source"    => $this->request->getPost("stripeToken"),
          "metadata"  => ["Invoice" => $this->uniqueI]
        ]);

        $sp = new StripePayments;
          $sp->user    = $this->rhyno_user->_;
          $sp->invoice = $invoice[0]->_;
          $sp->spid    = $charge->id;
          $sp->value   = $total_value;
          $sp->date    = (new \DateTime("now"))->format("Y-m-d H:i:s");
        $sp->save();

        $fl = true;
      }
      catch(\Stripe\Error\Card $e)
      {
        $redirect = "#card-error";
      }
      catch (\Stripe\Error\ApiConnection $e)
      {
        $redirect = "#api-error";
      }
      catch (\Exception $e)
      {
        $redirect = "#error";
      };

      if($fl)
      {
        if($charge->status == "succeeded")
        {
          $pay = new InvoicePayments;
            $pay->unique  = $this->uniqueCode("PAY");
            $pay->project = $this->invoice->project;
            $pay->invoice = $this->invoice->_;
            $pay->amount  = $total_value;
            $pay->method  = 3;
            $pay->payer   = $this->rhyno_user->_;
            $pay->date    = (new \DateTime("now"))->format("Y-m-d H:i:s");
          $pay->save();

          $redirect = "#success";
        }
        else { $redirect = "#error"; }
      }

      $this->response->redirect("{$this->rhyno_url}/project/{$this->uniqueP}/invoices/overview/{$this->uniqueI}{$redirect}",true);

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form     = new Form;
      $inputs   = [];

      if($this->rmethod == "remove"):

      else:
        # CREATING ELEMENTS
        $element['invoice'] = new Select( "invoice" , ProjectInvoices::findByProject($this->project->_) ,[
          'using' => ["_","title"],
          'label' => "Invoice",
        ]);
        $element['method'] = new Select( "method" ,[
          1 => "Cash",
          2 => "PayPal",
          3 => "Stripe"
        ],[
          'label' => "Payment Method",
        ]);
        $element['date'] = new Text( "date" ,[
          'label' => "Payment Date",
          'class' => "datepicker"
        ]);
        $element['value'] = new Text( "value" ,[
          'label'             => "Value",
          'data-mask'         => "000.000.000.000.000,00",
          'data-mask-reverse' => true,
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/payments/new";
        $modal_header = "Add a Payment!";
      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/payments/delete/{$this->uniqueY}";
        $modal_header = "Remove Payment!";

        $modal_card = [
          "content"  => "Are you sure? The selected payment will me permanently removed from the server."
        ];

      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }

      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
