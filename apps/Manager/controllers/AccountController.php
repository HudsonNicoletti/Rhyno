<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users       as Users,
    Manager\Models\Clients     as Clients,
    Manager\Models\Currencies  as Currencies,
    Manager\Models\Team        as Team;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class AccountController extends ControllerBase
{
  public function IndexAction()
  {
    $this->assets->addCss("assets/manager/css/pages/profile.css");
    $form = new Form;

    $element['currency'] = new Select( "currency" , Currencies::find(["order"=>"code asc"]) ,[
      'using' => ['_','code'],
      'class' => "validate",
    ]);

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]);

    # SELECT VIEW
    if($this->rhyno_user->permission >= $this->permissions->team):

      $member = Team::query()
      ->columns([
        "Manager\Models\Team.name",
        "Manager\Models\Team.image",
        "Manager\Models\Team.phone",
        "Manager\Models\Users.email",
        "Manager\Models\Users.username",
      ])
      ->innerJoin("Manager\Models\Users","Manager\Models\Team.uid = Manager\Models\Users._")
      ->where("Manager\Models\Team.uid = :user:")
      ->bind([
        "user" => $this->rhyno_user->_
      ])
      ->execute();

      $this->view->member = $member[0];
      $this->view->pick("account/member");

    elseif($this->rhyno_user->permission <= $this->permissions->client):

      $client = Clients::query()
      ->columns([
        'Manager\Models\Clients._',
        'Manager\Models\Clients.name',
        'Manager\Models\Clients.phone',
        'Manager\Models\Clients.domain',
        'Manager\Models\Clients.image',
        'Manager\Models\Clients.company',
        'Manager\Models\Clients.role',
        'Manager\Models\Clients.address',
        'Manager\Models\Clients.country',
        'Manager\Models\Clients.zip',
        'Manager\Models\Clients.currency',
        'Manager\Models\Clients.city',
        'Manager\Models\Clients.state',
        'Manager\Models\Clients.vat',
        'Manager\Models\Users.username',
        'Manager\Models\Users.email',
      ])
      ->innerJoin('Manager\Models\Users', 'Manager\Models\Users._ = Manager\Models\Clients.uid')
      ->where("Manager\Models\Clients.uid = :user: ")
      ->bind([
        "user" => $this->rhyno_user->_
      ])
      ->execute();

      $element['currency']->setAttribute("value",$client[0]->currency);

      $this->view->client = $client[0];
      $this->view->pick("account/client");
    endif;

    foreach($element as $e){ $form->add($e); }
    $this->view->form = $form;
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    $m = Team::findFirstByUid($this->rhyno_user->_);
    $c = Clients::findFirstByUid($this->rhyno_user->_);
    $u = Users::findFirst($this->rhyno_user->_);

    $username = preg_replace('/\s+/', '', $this->request->getPost("username","string"));
    $password = preg_replace('/\s+/', '', $this->request->getPost("password","string"));
    $email    = $this->request->getPost("email","email");

    # catch any erros
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$email):
        return RhynoException::EmptyInput("E-Mail");

      elseif(!$this->isEmail($email)):
        return RhynoException::InvalidEmailAddress();

      elseif($email != $u->email && Users::findFirstByEmail($email)->_ != NULL):
        return RhynoException::RegisteredEmailAddress();

      elseif($username != $u->username && Users::findFirstByUsername($username)->_ != NULL):
        return RhynoException::RegisteredUsername();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if($this->request->hasFiles()):
          foreach($this->request->getUploadedFiles() as $file):
            if($file->getError() != 0):
              $this->flags['redirect']  = "{$this->rhyno_url}/account";
              return RhynoException::UploadError($file->getError());
            endif;
            $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
            $file->moveTo("assets/manager/images/avatar/{$filename}");
            $this->ResizeImage("assets/manager/images/avatar/{$filename}");
          endforeach;
          @unlink("assets/manager/images/avatar/{$m->image}");
        endif;

        $u->username   = $username;
        $u->password   = ($password != null ) ? password_hash($password, PASSWORD_BCRYPT ) : $u->password;
        $u->email      = $email;
        if(!$u->save()) { return RhynoException::DBError(); }

        if($m)
        {
          $m->name    = $this->request->getPost("name","string");
          $m->phone   = $this->request->getPost("phone","string");
          $m->image   = ($filename ?: $m->image);
          if(!$m->save()) { return RhynoException::DBError(); }
        }
        elseif($c)
        {
          $c->name      = $this->request->getPost("name","string");
          $c->phone     = $this->request->getPost("phone","string");
          $c->domain    = $this->request->getPost("domain","string");
          $c->address   = $this->request->getPost("address","string");
          $c->zip       = $this->request->getPost("zip","string");
          $c->country   = $this->request->getPost("country","string");
          $c->city      = $this->request->getPost("city","string");
          $c->state     = $this->request->getPost("state","string");
          $c->vat       = $this->request->getPost("vat","string");
          $c->currency  = $this->request->getPost("currency","int");
          $c->company   = $this->request->getPost("company","string");
          $c->role      = $this->request->getPost("role","string");
          $c->image     = ($filename ?: $c->image);
          if(!$c->save()) { return RhynoException::DBError(); }
        }

        $this->flags['toast']     = "success";
        $this->flags['title']     = "Account information successfully updated!";
        $this->flags['redirect']  = "{$this->rhyno_url}/account";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
