<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users              as Users,
    Manager\Models\Clients            as Clients,
    Manager\Models\Projects           as Projects,
    Manager\Models\Currencies         as Currencies,
    Manager\Models\ProjectActivities  as ProjectActivities,
    Manager\Models\ProjectInvoices    as ProjectInvoices,
    Manager\Models\InvoicePayments    as InvoicePayments,
    Manager\Models\InvoiceItems       as InvoiceItems;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectInvoicesController extends ControllerBase
{
  private $uniqueP;
  private $uniqueI;
  private $rmethod;
  private $project;
  private $invoice;
  private $items;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueI = $this->dispatcher->getParam("invoice","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project = Projects::findFirstByUnique($this->uniqueP);
    $this->invoice = ProjectInvoices::findFirstByUnique($this->uniqueI);
    $this->items   = InvoiceItems::findByInvoice(@$this->invoice->_);

    # check recursions
    $this->RecurringInvoices($this->project);
    # check invoice status
    $this->VerifyInvoices($this->project);
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/invoices");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projectinvoices_page)
        {
          $this->view->pick("projects/team/invoices");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->view->pick("projects/client/invoices");
        $this->view->crud = $this->configuration->client_crud;
        $this->view->currency = Currencies::findFirst($this->rhyno_user_info->currency);
      break;
    }

    $invoices = ProjectInvoices::query()
    ->columns([
      "Manager\Models\ProjectInvoices._",
      "Manager\Models\ProjectInvoices.unique",
      "Manager\Models\ProjectInvoices.title",
      "Manager\Models\ProjectInvoices.unique",
      "Manager\Models\ProjectInvoices.status",
      "Manager\Models\ProjectInvoices.issue",
      "Manager\Models\ProjectInvoices.due",
      "Manager\Models\ProjectInvoices.recurring",
      'Manager\Models\InvoicePayments.date as paydate',
      "(SELECT SUM(Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as items",
      "(SELECT SUM(Manager\Models\InvoiceItems.price * Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as value",
      "(SELECT SUM(Manager\Models\InvoicePayments.amount) FROM Manager\Models\InvoicePayments where Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._) as paid",
    ])
    ->leftJoin("Manager\Models\InvoicePayments","Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._")
    ->where("Manager\Models\ProjectInvoices.project = :project:")
    ->bind([
      "project" => $this->project->_
    ])
    ->orderBy("Manager\Models\ProjectInvoices.status=1 DESC,
               Manager\Models\ProjectInvoices.status=4 DESC,
               Manager\Models\ProjectInvoices.status=3 DESC,
               Manager\Models\ProjectInvoices.status=2 DESC,
               Manager\Models\ProjectInvoices.status=5 DESC,
               Manager\Models\ProjectInvoices.due      ASC")
    ->execute();

    $this->view->project  = $this->project;
    $this->view->invoices = $invoices;
  }

  public function OverviewAction()
  {
    $this->assets->addCss("assets/manager/css/pages/profile.css")
                 ->addJs("assets/manager/js/stripe.checkout.js");

    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/invoice-overview");
      break;
      case $this->permissions->team:
        $this->view->pick("projects/team/invoice-overview");
        $this->view->crud = $this->configuration->team_crud;
      break;
      case $this->permissions->client:
        $this->view->pick("projects/client/invoice-overview");
        $this->view->crud = $this->configuration->client_crud;
        $this->view->currency = Currencies::findFirst($this->rhyno_user_info->currency);
      break;
    }

    $form    = new Form;
    $invoice = ProjectInvoices::query()
    ->columns([
      'Manager\Models\ProjectInvoices._',
      'Manager\Models\ProjectInvoices.unique',
      'Manager\Models\ProjectInvoices.project',
      'Manager\Models\ProjectInvoices.title',
      'Manager\Models\ProjectInvoices.status',
      'Manager\Models\ProjectInvoices.issue',
      'Manager\Models\ProjectInvoices.due',
      'Manager\Models\ProjectInvoices.recurring',
      'Manager\Models\InvoicePayments.date as paydate',
      "(SELECT SUM(Manager\Models\InvoiceItems.price * Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as total",
      "(SELECT SUM(Manager\Models\InvoicePayments.amount) FROM Manager\Models\InvoicePayments where Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._) as paid",
    ])
    ->leftJoin("Manager\Models\InvoicePayments","Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._")
    ->where("Manager\Models\ProjectInvoices.unique = :invoice:")
    ->bind([
      "invoice" => $this->uniqueI
    ])
    ->execute();

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]);
    foreach($element as $e)
    {
      $form->add($e);
    }

    $this->view->project  = $this->project;
    $this->view->invoice  = $invoice[0];
    $this->view->items    = $this->items;
    $this->view->form     = $form;
    $this->view->stripe   = $this->configuration->stripe->publishable;
    $this->view->tax      = $this->calcTax($invoice[0]->total,$invoice[0]->due,$invoice[0]->paydate);
    $this->view->payments = (object)["paypal"=>$this->configuration->paypal->active,"stripe"=>$this->configuration->stripe->active];

  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    $issue = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("issue","string"));
    $due   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("due","string"));

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("issue","string") || !$this->request->getPost("due","string")):
        return RhynoException::EmptyInput(["issue date","due date"]);

      elseif($issue > $due):
        return RhynoException::CustomError("Woah! Issue date can't be after the due date!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $title   = $this->request->getPost("title","string");

        $inv = new ProjectInvoices;
          $inv->unique         = $this->uniqueCode("INV");
          $inv->project        = $this->project->_;
          $inv->status         = 1;
          $inv->title          = $title;
          $inv->issue          = $issue->format("Y-m-d H:i:s");
          $inv->due            = $due->format("Y-m-d H:i:s");
          $inv->last_recursion = $issue->format("Y-m-d H:i:s");
          $inv->recurring      = ($this->request->getPost("recurring") == true ? 1 : 0);
        if(!$inv->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Created a new invoice, '{$title}' to the project.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $client = Clients::findFirst($this->project->client);

        # system notification
        if($this->configuration->client_notification->project_activities_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => "Created a new invoice, '{$title}' for the project #{$this->uniqueP}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/invoices"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->configuration->client_notification->project_activities_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => "Created a new invoice, '{$title}' for the project #{$this->uniqueP}.",
            "text"     => "You are receiving this notification because a new invoice, '{$inv->unique}' was created for the project #{$this->uniqueP}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/invoices"
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project invoice successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    $issue   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("issue","string"));
    $due   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("due","string"));

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueI):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->invoice):
        return RhynoException::Unreachable();

      elseif(!$this->invoice->project == $this->project->_):
        return RhynoException::CustomError("Woah, this invoice is not part of the project!");

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("issue","string") || !$this->request->getPost("due","string")):
        return RhynoException::EmptyInput(["issue date","due date"]);

      elseif($issue > $due):
        return RhynoException::CustomError("Woah! Issue date can't be after the due date!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $this->invoice->title           = $this->request->getPost("title","string");
        $this->invoice->issue           = $issue->format("Y-m-d H:i:s");
        $this->invoice->due             = $due->format("Y-m-d H:i:s");
        $this->invoice->recurring       = ($this->request->getPost("recurring") == true ? 1 : 0);
        $this->invoice->last_recursion  = ($this->request->getPost("recurring") == true ? (new \DateTime("now"))->format("Y-m-d H:i:s") : $this->invoice->last_recursion);
        if(!$this->invoice->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project invoice successfully updated!";
        $this->flags['redirect']   = $this->request->getHTTPReferer();
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueI):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->invoice):
        return RhynoException::Unreachable();

      elseif(!$this->invoice->project == $this->project->_):
        return RhynoException::CustomError("Woah, this invoice is not part of the project!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        # DELETE INVOICE & ITEMS
        if(!$this->items->delete()){ return RhynoException::DBError(); }
        if(!$this->invoice->delete()){ return RhynoException::DBError(); }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project invoice successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function StatusAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueI):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->invoice):
        return RhynoException::Unreachable();

      elseif(!$this->invoice->project == $this->project->_):
        return RhynoException::CustomError("Woah, this invoice is not part of the project!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        switch ($this->rmethod)
        {
          case 'cancel': $this->invoice->status = 5; break;
          case 'set':    $this->invoice->status = 1; break;
        }

        if(!$this->invoice->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project invoice successfully updated!";
        $this->flags['redirect']   = $this->request->getHTTPReferer();
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form    = new Form;
      $inputs  = [];

      if($this->rmethod  == "remove"):

      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Invoice Title",
        ]);
        $element['issue'] = new Text( "issue" ,[
          'label' => "Issue Date",
          'class' => "datepicker",
        ]);
        $element['due'] = new Text( "due" ,[
          'label' => "Due Date",
          'class' => "datepicker",
        ]);
        $element['recurring'] = new Check( "recurring" ,[
          'label'  => "Recurrent Invoice?",
          'switch' => true,
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod  == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/new";
        $modal_header = "Create a Invoice!";

        $element['issue']->setAttribute("active",true)->setAttribute("value",(new \DateTime())->format($this->rhyno_date));

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod  == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/update/{$this->uniqueI}";
        $modal_header = "Update Project invoice!";

        $element['title']->setAttribute("active",true)->setAttribute("value",$this->invoice->title);
        $element['issue']->setAttribute("active",true)->setAttribute("value",(new \DateTime($this->invoice->issue))->format($this->rhyno_date));
        $element['due']  ->setAttribute("active",true)->setAttribute("value",(new \DateTime($this->invoice->due))->format($this->rhyno_date));
        ($this->invoice->recurring == 0) ?: $element['recurring']->setAttribute("checked",true);

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/delete/{$this->uniqueI}";
        $modal_header = "Remove Project invoice!";

        $modal_card = [
          "content"  => "Are you sure? The selected invoice will me permanently removed from the server along with it's items."
        ];

      endif;


      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl"), "switch" => $f->getAttribute("switch") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
