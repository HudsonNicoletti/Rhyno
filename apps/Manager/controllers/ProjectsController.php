<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Team              as Team,
    Manager\Models\Clients           as Clients,
    Manager\Models\Assignments       as Assignments,
    Manager\Models\Tasks             as Tasks,
    Manager\Models\TaskResponses     as TaskResponses,
    Manager\Models\TaskCollaborators as TaskCollaborators,
    Manager\Models\Projects          as Projects,
    Manager\Models\ProjectTypes      as ProjectTypes,
    Manager\Models\ProjectFiles      as ProjectFiles,
    Manager\Models\ProjectInvoices   as ProjectInvoices,
    Manager\Models\ProjectEstimates  as ProjectEstimates,
    Manager\Models\ProjectActivities as ProjectActivities,
    Manager\Models\InvoiceItems      as InvoiceItems,
    Manager\Models\InvoicePayments   as InvoicePayments,
    Manager\Models\EstimateItems     as EstimateItems,
    Manager\Models\EstimateRequests  as EstimateRequests,
    Manager\Models\EstimateReviews   as EstimateReviews;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectsController extends ControllerBase
{
  private $uniqueP;
  private $rmethod;
  private $project;

  private $project_create_email;
  private $project_create_inner;
  private $project_status_email;
  private $project_status_inner;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project = Projects::findFirstByUnique($this->uniqueP);

    $this->project_create_email = $this->configuration->client_notification->project_create_email;
    $this->project_create_inner = $this->configuration->client_notification->project_create_inner;
    $this->project_status_email = $this->configuration->client_notification->project_status_email;
    $this->project_status_inner = $this->configuration->client_notification->project_status_inner;
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/index");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projects_page)
        {
          $this->view->pick("projects/team/index");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->view->pick("projects/client/index");
        $this->view->crud = $this->configuration->client_crud;
      break;
    }

    if($this->rhyno_user->permission >= $this->permissions->team):

      $projects = Projects::query()
      ->columns([
        'Manager\Models\Projects._',
        'Manager\Models\Projects.unique',
        'Manager\Models\Projects.title',
        'Manager\Models\Projects.description',
        'Manager\Models\Projects.deadline',
        'Manager\Models\Projects.status',
        'Manager\Models\Clients.name',
        'Manager\Models\Clients.company',
        'Manager\Models\Clients.image',
        'Manager\Models\ProjectTypes.title as type',
        "(((SELECT COUNT(Manager\Models\Tasks._) from Manager\Models\Tasks where Manager\Models\Tasks.status = 2 and Manager\Models\Tasks.project = Manager\Models\Projects._) * 100) / (SELECT  COUNT(Manager\Models\Tasks._) from Manager\Models\Tasks where Manager\Models\Tasks.project = Manager\Models\Projects._)) as percentage "
      ])
      ->innerJoin('Manager\Models\Clients', 'Manager\Models\Clients._ = Manager\Models\Projects.client')
      ->innerJoin('Manager\Models\ProjectTypes', 'Manager\Models\ProjectTypes._ = Manager\Models\Projects.type')
      ->orderBy("deadline ASC")
      ->execute();

      $this->view->projects = $projects;

    elseif($this->rhyno_user->permission <= $this->permissions->client):

      $projects = Projects::query()
      ->columns([
        'Manager\Models\Projects._',
        'Manager\Models\Projects.unique',
        'Manager\Models\Projects.title',
        'Manager\Models\Projects.description',
        'Manager\Models\Projects.deadline',
        'Manager\Models\Projects.status',
        'Manager\Models\ProjectTypes.title as type',
        "(((SELECT COUNT(Manager\Models\Tasks._) from Manager\Models\Tasks where Manager\Models\Tasks.status = 2 and Manager\Models\Tasks.project = Manager\Models\Projects._) * 100) / (SELECT  COUNT(Manager\Models\Tasks._) from Manager\Models\Tasks where Manager\Models\Tasks.project = Manager\Models\Projects._)) as percentage "
      ])
      ->innerJoin('Manager\Models\Clients', 'Manager\Models\Clients._ = Manager\Models\Projects.client')
      ->innerJoin('Manager\Models\ProjectTypes', 'Manager\Models\ProjectTypes._ = Manager\Models\Projects.type')
      ->where("Manager\Models\Clients.uid = :client:")
      ->bind([
        "client" => $this->rhyno_user->_
      ])
      ->orderBy("deadline ASC")
      ->execute();

      $this->view->projects = $projects;

    endif;
  }

  public function OverviewAction()
  {
    $this->assets
    ->addCss("assets/manager/css/timeline.css")
    ->addCss("assets/manager/css/progressbar.css")
    ->addCss("assets/manager/css/pages/profile.css");

    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/overview");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projects_page)
        {
          $this->view->pick("projects/team/overview");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->view->pick("projects/client/overview");
        $this->view->crud = $this->configuration->client_crud;
      break;
    }

    $form = new Form;

    # Project Query
    $project = Projects::query()
    ->columns([
      'Manager\Models\Projects._',
      'Manager\Models\Projects.unique',
      'Manager\Models\Projects.title',
      'Manager\Models\Projects.description',
      'Manager\Models\Projects.created',
      'Manager\Models\Projects.deadline',
      'Manager\Models\Projects.finished',
      'Manager\Models\Projects.status',
      'Manager\Models\Projects.client',
      'Manager\Models\Projects.type as filter',
      'Manager\Models\Clients.name',
      'Manager\Models\Clients.company',
      'Manager\Models\Clients.image',
      'Manager\Models\Clients.domain',
      'Manager\Models\Clients.role',
      'Manager\Models\Clients.domain',
      'Manager\Models\Clients.phone',
      'Manager\Models\Clients.city',
      'Manager\Models\Clients.state',
      'Manager\Models\Clients.country',
      'Manager\Models\Clients.vat',
      'Manager\Models\Clients.address',
      'Manager\Models\Clients.zip',
      'Manager\Models\Users.email',
      'Manager\Models\ProjectTypes.title as type',
    ])
    ->innerJoin('Manager\Models\Clients', 'Manager\Models\Clients._ = \Manager\Models\Projects.client')
    ->innerJoin('Manager\Models\ProjectTypes', 'Manager\Models\ProjectTypes._ = Manager\Models\Projects.type')
    ->innerJoin('Manager\Models\Users', 'Manager\Models\Clients.uid = Manager\Models\Users._')
    ->where("Manager\Models\Projects.unique = :project:")
    ->bind([
      "project" => $this->uniqueP
    ])
    ->execute();

    $assigned = Assignments::query()
    ->columns([
      'Manager\Models\Team._',
      'Manager\Models\Team.name',
      'Manager\Models\Team.image',
    ])
    ->innerJoin('Manager\Models\Team', 'Manager\Models\Assignments.member = Manager\Models\Team._')
    ->where("Manager\Models\Assignments.project = :project:")
    ->bind([
      "project" => $this->project->_
    ])
    ->orderBy("name asc")
    ->execute();

    $activities = ProjectActivities::query()
    ->columns([
      'Manager\Models\ProjectActivities._',
      'Manager\Models\ProjectActivities.text',
      'Manager\Models\ProjectActivities.date',
      'COALESCE(Manager\Models\Team.name,Manager\Models\Clients.name) as name',
      'COALESCE(Manager\Models\Team.image,Manager\Models\Clients.image) as image',
    ])
    ->leftJoin('Manager\Models\Team', 'Manager\Models\ProjectActivities.user = Manager\Models\Team.uid')
    ->leftJoin('Manager\Models\Clients', 'Manager\Models\ProjectActivities.user = Manager\Models\Clients.uid')
    ->where("Manager\Models\ProjectActivities.project = :project:")
    ->bind([
      "project" => $this->project->_
    ])
    ->limit(10,0)
    ->orderBy("Manager\Models\ProjectActivities.date DESC")
    ->execute();

    $done  = 0;
    $tasks = Tasks::findByProject($this->project->_);
    $count = ($tasks->count() == 0 ? 1 : $tasks->count());
    foreach($tasks as $task)
    {
      ($task->status == 2) ? $done = $done + 1 : $done = $done;
    }
    $progress = round(($done * 100) / $count);

    $form->add(new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]));

    $this->view->project    = $project[0];
    $this->view->assigned   = $assigned;
    $this->view->activities = $activities;
    $this->view->progress   = $progress;
    $this->view->form       = $form;
  }

  public function NotesAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->request->getPost("text","string")):
        return RhynoException::EmptyInput("Note");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = $this->request->getPost("text","string");
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$act->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project note created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/overview";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function StatusAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->rmethod || !$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        switch ($this->rmethod)
        {
          case 'incomplete': $this->project->status = 1; $status = "incomplete"; break;
          case 'complete': $this->project->status = 2; $status = "completed"; break;
          case 'cancel': $this->project->status = 3; $status = "canceled"; break;
        }
        if(!$this->project->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Updated project status to {$status}!";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $client = Clients::findFirst($this->project->client);

        # system notification
        if($this->project_status_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => "Project #{$this->uniqueP} status updated to {$status}!",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/overview"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->project_status_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => "Project #{$this->uniqueP} status updated!",
            "text"     => "You are receiving this notification because your project status ( #{$this->uniqueP} ) was updated to {$status}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/overview"
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project status updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/overview";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("deadline","string")):
        return RhynoException::EmptyInput("Deadline");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $deadline = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("deadline","string"));
        $project = new Projects;
          $project->unique        = $this->uniqueCode("PRO");
          $project->title         = $this->request->getPost("title","string");
          $project->type          = $this->request->getPost("type","int");
          $project->client        = $this->request->getPost("client","int");
          $project->created       = (new \DateTime())->format("Y-m-d H:i:s");
          $project->deadline      = $deadline->format("Y-m-d H:i:s");
          $project->status        = 1;
          $project->description   = $this->request->getPost("description","string");
        if(!$project->save())
        {
          return RhynoException::DBError();
        }

        $client = Clients::findFirst($this->request->getPost("client","int"));

        # system notification
        if($this->project_create_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => "Project #{$project->unique} was created!",
            "href"     => "{$this->rhyno_url}/project/{$project->unique}/overview"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->project_create_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => "Project #{$project->unique} was created!",
            "text"     => "You are receiving this notification because a new project ( #{$project->unique} ) was created.",
            "href"     => "{$this->rhyno_url}/project/{$project->unique}/overview"
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/projects";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("deadline","string")):
        return RhynoException::EmptyInput("Deadline");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $deadline = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("deadline","string"));
        $this->project->title         = $this->request->getPost("title","string");
        $this->project->type          = $this->request->getPost("type","int");
        $this->project->client        = $this->request->getPost("client","int");
        $this->project->deadline      = $deadline->format("Y-m-d H:i:s");
        $this->project->description   = $this->request->getPost("description","string");
        if(!$this->project->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project successfully updated!";
        $this->flags['redirect']   = $this->request->getHTTPReferer();
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        foreach(Tasks::findByProject($this->project->_) as $task)
        {
          $tc = TaskCollaborators::findByTask($task->_);
          $tr = TaskResponses::findByTask($task->_);

          if(!$tc->delete()):
            return RhynoException::CustomError("Unable to remove collaborators from task.");
          elseif(!$tr->delete()):
            return RhynoException::CustomError("Unable to remove responses from task.");
          elseif(!$task->delete()):
            return RhynoException::CustomError("Unable to remove task.");
          endif;
        }

        foreach(ProjectEstimates::findByProject($this->project->_) as $estimate)
        {
          $ei = EstimateItems::findByEstimate($estimate->_);
          $er = EstimateRequests::findByProject($this->project->_);
          $ev = EstimateReviews::findByEstimate($estimate->_);

          if(!$ei->delete()):
            return RhynoException::CustomError("Unable to remove items from estimate.");
          elseif(!$er->delete()):
            return RhynoException::CustomError("Unable to remove estimate requests.");
          elseif(!$ev->delete()):
            return RhynoException::CustomError("Unable to remove estimate reviews.");
          elseif(!$estimate->delete()):
            return RhynoException::CustomError("Unable to remove estimate.");
          endif;
        }

        foreach(ProjectInvoices::findByProject($this->project->_) as $invoice)
        {
          $ii = InvoiceItems::findByInvoice($invoice->_);
          $ip = InvoicePayments::findByInvoice($invoice->_);

          if(!$ii->delete()):
            return RhynoException::CustomError("Unable to remove items from invoice.");
          elseif(!$ip->delete()):
            return RhynoException::CustomError("Unable to remove invoice payments.");
          elseif(!$invoice->delete()):
            return RhynoException::CustomError("Unable to remove invoice.");
          endif;
        }

        foreach(ProjectFiles::findByProject($this->project->_) as $file)
        {
          @unlink("assets/manager/files/{$file->file}");
          if(!$file->delete()):
            return RhynoException::CustomError("Unable to remove file.");
          endif;
        }

        $as = Assignments::findByProject($this->project->_);
        $pa = ProjectActivities::findByProject($this->project->_);

        if(!$as->delete()):
          return RhynoException::CustomError("Unable to remove assigned members.");
        elseif(!$pa->delete()):
          return RhynoException::CustomError("Unable to remove project activities.");
        elseif(!$this->project->delete()):
          return RhynoException::CustomError("Unable to remove project.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/projects";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form   = new Form;
      $inputs = [];

      if($this->rmethod == "remove"):

      else:

        $clients = Clients::query()
        ->columns([
          "Manager\Models\Clients._",
          "CONCAT(Manager\Models\Clients.name, ' - ', Manager\Models\Clients.company) as name",
        ])
        ->orderBy("name asc")
        ->execute();

        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Project Title",
        ]);

        $element['deadline'] = new Text( "deadline" ,[
          'label' => "Deadline",
          'class' => "datepicker"
        ]);

        $element['description'] = new Textarea( "description" ,[
          'class' => "materialize-textarea",
          'label' => "Project Description",
        ]);

        $element['client'] = new Select( "client" , $clients ,[
          'using' => ["_","name"],
          'label' => "Client",
        ]);

        $element['type'] = new Select( "type" , ProjectTypes::find() ,[
          'using' =>  ['_','title'],
          'label' => "Project Type",
        ]);

      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/new";
        $modal_header = "Create a new project!";

      # IF REQUEST IS TO UPDATE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/update/{$this->uniqueP}";
        $modal_header = "Update project information!";

        $element['title']       ->setAttribute("active",true)->setAttribute("value",$this->project->title);
        $element['description'] ->setAttribute("active",true)->setAttribute("value",$this->project->description);
        $element['deadline']    ->setAttribute("active",true)->setAttribute("value",(new \DateTime($this->project->deadline))->format($this->rhyno_date));
        $element['client']      ->setAttribute("active",false)->setAttribute("value",$this->project->client);
        $element['type']        ->setAttribute("active",false)->setAttribute("value",$this->project->type);

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/delete/{$this->uniqueP}";
        $modal_header = "Remove Project!";

        $modal_card = [
          "content"  => "Are you sure? Apon removal all data such as Tasks etc. associated with this project will be also removed!"
        ];

      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
