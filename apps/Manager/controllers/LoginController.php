<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users        as Users,
    Manager\Models\Team         as Team,
    Manager\Models\Clients      as Clients,
    Manager\Models\RequestCodes as RequestCodes;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Hidden;

class LoginController extends ControllerBase
{
    public function IndexAction()
    {
      $this->assets->addCss("assets/manager/css/login.css");

      $form = new Form();

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken()
      ]);

      $element['username'] = new Text( "username" ,[
        'required'     => true,
      ]);

      $element['password'] = new Password( "password" ,[
        'required'     => true,
      ]);

      foreach ($element as $e)
      {
        $form->add($e);
      }

      $title    = explode(" ",$this->configuration->app->title);
      $title_2 = "";
      foreach($title as $i => $t)
      {
        if($i != 0)
        {
          $title_2 .= $t." ";
        }
      }

      $this->view->form    = $form;
      $this->view->installer    = $this->configuration->debug->installer;
      $this->view->title   = (object)["first"=>$title[0],"all"=>$title_2];
    }

    public function ResetAction()
    {
      $this->assets->addCss("assets/manager/css/login.css");

      $form = new Form();

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken()
      ]);

      $element['username'] = new Text( "username" ,[
        'required'     => true,
      ]);

      foreach ($element as $e)
      {
        $form->add($e);
      }

      $this->view->form = $form;
    }

    public function RequestCodeAction()
    {
      $this->response->setContentType("application/json");

      # catch any erros
      try
      {
        if(!$this->request->isPost() || !$this->request->isAjax()):
          return RhynoException::InvalidRequestMethod();

        elseif(!$this->request->getPost("username")):
          return RhynoException::EmptyInput("Username | E-Mail");

        elseif(!$this->isEmail($this->request->getPost("username")) && !Users::findFirstByUsername($this->request->getPost("username"))):
          return RhynoException::CustomError("Username does not exists.");

        elseif($this->isEmail($this->request->getPost("username")) && !Users::findFirstByEmail($this->request->getPost("username"))):
          return RhynoException::CustomError("E-Mail does not exists.");

        elseif(!$this->security->checkToken()):
          return RhynoException::InvalidCsrfToken();
        endif;
      }
      catch (\Exception $e)
      {
        $this->flags['status'] = false ;
        $this->flags['toast']  = "error";
        $this->flags['title']  = $e->getMessage();
      }

      if( $this->flags['status'] )
      {
        try
        {
          $username = preg_replace('/\s+/', '', $this->request->getPost("username"));
          $user = ( $this->isEmail( $username ) ) ? Users::findFirstByEmail($username) : Users::findFirstByUsername($username);

          $code = sha1(time());
          $hash = sha1($code);
          # Set Request Code
          $request_code = new RequestCodes;
            $request_code->user    = $user->_;
            $request_code->code    = password_hash($code, PASSWORD_BCRYPT );
            $request_code->hash    = $hash;
            $request_code->expires = (new \DateTime("+25 minutes"))->format("Y-m-d H:i:s");
          $request_code->save();

          # send email
          $this->mail->functions->From       = $this->configuration->mail->email;
          $this->mail->functions->FromName   = $this->configuration->mail->name;

          $this->mail->functions->addAddress($user->email,  $user->name);
          $this->mail->functions->Subject = "Reset Password confirmation";

          $this->mail->functions->Body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/mail.tpl"), [
            'PASSWORD_RESET' => true,
            'SENDER'  => $this->configuration->app->title,
            'LOGO'    => 'data: '.mime_content_type("assets/manager/images/{$this->configuration->app->logo}").';base64,'.base64_encode(file_get_contents("assets/manager/images/{$this->configuration->app->logo}")),
            'CODE'    => $code,
            'LINK'    => $this->request->getHttpHost().$this->rhyno_url."/login/confirmation/{$hash}",
            'DATE'    => (new \DateTime())->format($this->configuration->app->date_format)
          ]);

          if(!$this->mail->functions->send())
          {
            return RhynoException::EmailSendError($user->email);
          }
          $this->mail->functions->ClearAddresses();

          $this->flags['toast']  = "success";
          $this->flags['title']  = "An E-Mail was sent to you! Please follow the instructions to create a new password.";
          $this->flags['redirect'] = "{$this->rhyno_url}/login/confirmation/{$hash}";
        }
        catch (\Exception $e)
        {
          $this->flags['toast']  = "success";
          $this->flags['title']  = $e->getMessage();
        }
      }

      return $this->response->setJsonContent([
        "toast"     =>  $this->flags['toast'],
        "title"     =>  $this->flags['title'],
        "redirect"  =>  $this->flags['redirect'],
        "time"      =>  $this->flags['time']
      ]);

      $this->response->send();
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function ConfirmationAction()
    {
      $this->assets->addCss("assets/manager/css/login.css");
      $hash = $this->dispatcher->getParam("hash");

      if(!$hash):
        $this->response->redirect("{$this->rhyno_url}/login",true);

      elseif(!RequestCodes::findFirstByHash($hash)):
        $this->response->redirect("{$this->rhyno_url}/login",true);
      endif;

      $form = new Form();

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken()
      ]);

      $element['code'] = new Password( "code" ,[
        'required'     => true,
      ]);

      $element['password'] = new Password( "password" ,[
        'required'     => true,
      ]);

      foreach ($element as $e)
      {
        $form->add($e);
      }

      $this->view->form = $form;
      $this->view->hash = $hash;
    }

    public function VerifyAction()
    {
      $this->response->setContentType("application/json");

      $code = preg_replace('/\s+/', '', $this->request->getPost("code"));
      $hash = $this->dispatcher->getParam("hash");
      $pass = preg_replace('/\s+/', '', $this->request->getPost("password","string"));

      $data = RequestCodes::findFirstByHash($hash);

      try
      {
        if(!$this->request->isPost() || !$this->request->isAjax()):
          return RhynoException::InvalidRequestMethod();

        elseif(!$this->request->getPost("code")):
          return RhynoException::EmptyInput("code");

        elseif(!$data):
          return RhynoException::CustomError("This link is no longer available , please request a new tassword reset.");

        elseif(!password_verify($code, $data->code)):
          return RhynoException::CustomError("The code given is incorrect, please verify your security code.");

        elseif(!$this->security->checkToken()):
          return RhynoException::InvalidCsrfToken();
        endif;
      }
      catch(\Exception $e)
      {
        $this->flags['status'] = false ;
        $this->flags['toast']  = "error";
        $this->flags['title']  = $e->getMessage();
      }

      if( $this->flags['status'] )
      {
        try
        {
          # REMOVE THE REQUEST CODE
          $data->delete();

          # UPDATE Password
          $user = Users::findFirst($data->user);
            $user->password = password_hash($pass , PASSWORD_BCRYPT );
          if(!$user->save())
          {
            return RhynoException::DBError();
          }

          $this->flags['toast']  = "success";
          $this->flags['title']  = "Your password has been reset! You will be redirected to the login screen.";
          $this->flags['redirect'] = "{$this->rhyno_url}/login";
        }
        catch(\Exception $e)
        {
          $this->flags['toast']  = "warning";
          $this->flags['title']  = $e->getMessage();
        }
      }

      return $this->response->setJsonContent([
        "toast"     =>  $this->flags['toast'],
        "title"     =>  $this->flags['title'],
        "redirect"  =>  $this->flags['redirect'],
        "time"      =>  $this->flags['time']
      ]);

      $this->response->send();
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }

    public function AuthAction()
    {
      $this->response->setContentType("application/json");

      $username = preg_replace('/\s+/', '', $this->request->getPost("username","string"));
      $password = preg_replace('/\s+/', '', $this->request->getPost("password","string"));
      $user = ( !$this->isEmail( $username ) ) ? Users::findFirstByUsername($username) : Users::findFirstByEmail($username);

      try
      {
        if(!$this->request->isPost() || !$this->request->isAjax()):
          return RhynoException::InvalidRequestMethod();

        elseif(!$user->_):
          return RhynoException::AccessDenied();

        elseif(!in_array( $user->permission , range( $this->permissions->client, $this->permissions->admin ))):
          return RhynoException::PermissionDenied();

        elseif(!password_verify( $password , $user->password )):
          return RhynoException::CustomError("Sorry, the password is invalid. Please try again.");

        elseif(!$this->security->checkToken()):
          return RhynoException::InvalidCsrfToken();
        endif;
      }
      catch(\Exception $e)
      {
        $this->flags['status'] = false ;
        $this->flags['toast']  = "error";
        $this->flags['title']  = $e->getMessage();
      }

      if($this->flags['status']):
        try
        {
          $this->session->set($this->rhyno_token, $user->_);

          $team   = Team::findFirstByUid($user->_);
          $client = Clients::findFirstByUid($user->_);

          $name = ( $team->name ?: $client->name );

          if(!$team && !$client)
          {
            return RhynoException::Unknown();
          }

          $this->flags['toast']     = "success";
          $this->flags['title']     = "Wellcome, {$name} !";
          $this->flags['redirect']  = "{$this->rhyno_url}/";
        }
        catch(\Exception $e)
        {
          $this->flags['toast']  = "warning";
          $this->flags['title']  = $e->getMessage();
        }
      endif;

      return $this->response->setJsonContent([
        "toast"     =>  $this->flags['toast'],
        "title"     =>  $this->flags['title'],
        "redirect"  =>  $this->flags['redirect'],
        "time"      =>  $this->flags['time']
      ]);

      $this->response->send();
      $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

    }

    public function LogoutAction()
    {
      $this->session->destroy();
      return $this->response->redirect();
    }

}
