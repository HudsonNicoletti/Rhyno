<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users            as Users,
    Manager\Models\Projects         as Projects,
    Manager\Models\Tickets          as Tickets,
    Manager\Models\TicketCategories as TicketCategories,
    Manager\Models\TicketResponses  as TicketResponses;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Hidden;

class TicketCategoriesController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");

    $categories = TicketCategories::find([
      "order" =>  "category ASC"
    ]);

    $this->view->categories = $categories;
    $this->view->pick("tickets/admin/categories");
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");
    $title = $this->request->getPost("title","string");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$title):
        return RhynoException::EmptyInput("Category Title");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $category = new TicketCategories;
          $category->category = $title;
        if(!$category->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Ticket Category successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/ticket/categories";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");
    $unique = $this->dispatcher->getParam("category","int");
    $title  = $this->request->getPost("title","string");
    $category = TicketCategories::findfirst($unique);

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$unique):
        return RhynoException::WrongNumberOfParams();

      elseif(!$title):
        return RhynoException::EmptyInput("Category Title");

      elseif(!$category):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $category->category = $title;
        if(!$category->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Ticket Category successfully updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/ticket/categories";
      }
      catch (\Exception $e)
      {
        $this->flags['status'] = false ;
        $this->flags['toast']  = "error";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");
    $unique   = $this->dispatcher->getParam("category","int");
    $new_category = $this->request->getPost("category","int");
    $category = TicketCategories::findfirst($unique);
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$unique):
        return RhynoException::WrongNumberOfParams();

      elseif(!$new_category):
        return RhynoException::EmptyInput("Category");

      elseif(TicketCategories::find()->count() <= 1):
        return RhynoException::CustomError("Unable to remove category. At least one category must be active.");

      elseif(!$category):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        foreach (Tickets::findByCategory($unique) as $ticket)
        {
          $ticket->category = $new_category;
          if(!$ticket->save())
          {
            return RhynoException::CustomError("Unable to remove move tickets to category.");
          }
        }
        if(!$category->delete())
        {
          return RhynoException::CustomError("Unable to remove ticket category.");
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Ticket Category successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/ticket/categories";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form   = new Form;
      $inputs = [];
      $unique = $this->dispatcher->getParam("category","int");
      $method = $this->dispatcher->getParam("method","string");
      $category = TicketCategories::findFirst($unique);

      if($method == "remove"):
        $element['category'] = new Select("category", TicketCategories::find(["order"=>"category asc","_ != '{$category->_}' "]) ,[
          'using' => ["_","category"],
          'label' => "Category replacement"
        ]);
      else:

        $element['title'] = new Text("title",[
          'label' => "Category Title"
        ]);

      endif;
      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $method == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/ticket/categories/new";
        $modal_header = "Create new ticket category!";
      elseif( $method == "modify" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/ticket/categories/update/{$unique}";
        $modal_header = "Update ticket category!";

        $element['title']->setAttribute("active",true)->setAttribute("value",$category->category);
      elseif( $method == "remove" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/ticket/categories/delete/{$unique}";
        $modal_header = "Remove ticket category!";
      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
