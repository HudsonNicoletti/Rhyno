<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class SettingsEmailController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");
    $form = new Form;

    $element['host'] = new Text( "host" ,[
      'class'         => "validate",
      'value'         => $this->configuration->mail->host,
      'data-validate' => true
    ]);

    $element['username'] = new Text( "username" ,[
      'class'         => "validate",
      'value'         => $this->configuration->mail->username,
      'data-validate' => true
    ]);

    $element['password'] = new Password( "password" ,[
      'class' => "validate",
      'value' => $this->configuration->mail->password,
    ]);

    $element['port'] = new Text( "port" ,[
      'class'         => "validate",
      'value'         => $this->configuration->mail->port,
      'max-length'    => 4,
      'data-validate' => true
    ]);

    $element['type'] = new Select( "type" , ["tls"=>"TLS","ssl"=>"SSL"] ,[
      'value' => $this->configuration->mail->security,
      'class' => "validate",
    ]);

    $element['email'] = new Text( "email" ,[
      'class'         => "validate",
      'value'         => $this->configuration->mail->email,
      'data-validate' => true
    ]);

    $element['name'] = new Text( "name" ,[
      'value' => $this->configuration->mail->name,
      'class' => "validate",
    ]);

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]);

    foreach($element as $e)
    {
      $form->add($e);
    }

    $this->view->form = $form;
    $this->view->pick("settings/email");
  }

  public function SaveAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("host") || !$this->request->getPost("username") || !$this->request->getPost("port") || !$this->request->getPost("email") || !$this->request->getPost("name")):
        return RhynoException::EmptyInput(["SMTP Host","Username","Port","Name","E-Mail"]);

      elseif(!$this->isEmail($this->request->getPost("email","email"))):
        return RhynoException::InvalidEmailAddress();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $update = $this->UpdateConfigFile([
          "mail_host" => $this->request->getPost("host","string"),
          "mail_user" => $this->request->getPost("username","string"),
          "mail_pass" => $this->request->getPost("password","string"),
          "mail_secu" => $this->request->getPost("type","string"),
          "mail_port" => $this->request->getPost("port","string"),
          "mail_mail" => $this->request->getPost("email","email"),
          "mail_name" => $this->request->getPost("name","string"),
        ]);

        if(!$update):
          return RhynoException::CustomError("Unable to update application configuration file.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "E-Mail Configuration Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/settings/email";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function SendAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("send")):
        return RhynoException::EmptyInput("E-Mail");

      elseif(!$this->isEmail($this->request->getPost("send","email"))):
        return RhynoException::InvalidEmailAddress();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        # send email
        $this->mail->functions->From       = $this->configuration->mail->email;
        $this->mail->functions->FromName   = $this->configuration->mail->name;

        $this->mail->functions->addAddress($this->request->getPost("send","email"));
        $this->mail->functions->Subject = "E-Mail Test!";

        $this->mail->functions->Body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/mail.tpl"), [
          'TEST'    => true,
          'SENDER'  => $this->configuration->app->title,
          'LOGO'    => 'data: '.mime_content_type("assets/manager/images/logo.png").';base64,'.base64_encode(file_get_contents("assets/manager/images/logo.png")),
          'DATE'    => (new \DateTime())->format($this->configuration->app->date_format)
        ]);

        if($this->mail->functions->send())
        {
          $this->flags['toast']      = "success";
          $this->flags['title']      = "E-Mail Sent!";
          $this->flags['redirect']   = "{$this->rhyno_url}/settings/email";
        }
        else
        {
          return RhynoException::EmailSendError($this->request->getPost("send","email"));
        }
        $this->mail->functions->ClearAddresses();
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
