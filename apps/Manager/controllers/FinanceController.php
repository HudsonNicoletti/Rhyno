<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Currencies       as Currencies,
    Manager\Models\Expenses         as Expenses,
    Manager\Models\Projects         as Projects,
    Manager\Models\Clients          as Clients,
    Manager\Models\Users            as Users,
    Manager\Models\InvoicePayments  as InvoicePayments,
    Manager\Models\ProjectEstimates as ProjectEstimates,
    Manager\Models\EstimateRequests as EstimateRequests,
    Manager\Models\ProjectActivities as ProjectActivities,
    Manager\Models\ProjectInvoices  as ProjectInvoices;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden;

class FinanceController extends ControllerBase
{
  private $projects;
  private $rmethod;
  private $rtarget;

  public function onConstruct()
  {
    $this->rmethod = $this->dispatcher->getParam("method","string");
    $this->rtarget = $this->dispatcher->getParam("target","string");
    if($this->dispatcher->getActionName() == "invoices")
    {
      $this->projects = ($this->rhyno_user->permission == $this->permissions->client) ? Projects::findByClient($this->rhyno_user->_) : Projects::find() ;

      foreach($this->projects as $project)
      {
        # check recursions
        $this->RecurringInvoices($project);
        # check invoice status
        $this->VerifyInvoices($project);
      }
    }
  }

  public function IndexAction()
  {
    $income   = ["jan" => 0,"feb" => 0,"mar" => 0,"apr" => 0,"may" => 0,"jun" => 0,"jul" => 0,"agu" => 0,"sep" => 0,"oct" => 0,"nov" => 0,"dec" => 0];
    $expenses = ["jan" => 0,"feb" => 0,"mar" => 0,"apr" => 0,"may" => 0,"jun" => 0,"jul" => 0,"agu" => 0,"sep" => 0,"oct" => 0,"nov" => 0,"dec" => 0];

    switch ($this->rhyno_user->permission)
    {
      case $this->permissions->admin:

        $in = InvoicePayments::find([
          'conditions' => 'date BETWEEN :start: AND :end:',
          'bind' => [
            "start" => (new \DateTime("first day of January"))->format("Y-m-d H:i:s"),
            "end"   => (new \DateTime("last day of December"))->format("Y-m-d H:i:s")
          ]
        ]);
        $ex = Expenses::find([
          'conditions' => 'date BETWEEN :start: AND :end:',
          'bind' => [
            "start" => (new \DateTime("first day of January"))->format("Y-m-d H:i:s"),
            "end"   => (new \DateTime("last day of December"))->format("Y-m-d H:i:s")
          ]
        ]);
        foreach ($in as $i)
        {
          $d = strtolower((new \DateTime($i->date))->format("M"));
          $income[$d] = $income[$d] + ($i->amount / 100);
        }
        foreach ($ex as $e)
        {
          $d = strtolower((new \DateTime($e->date))->format("M"));
          $expenses[$d] = $expenses[$d] + ($e->amount / 100);
        }

        $this->view->income   = (object) $income;
        $this->view->expenses = (object) $expenses;
        $this->view->pick("finance/admin/index");
      break;
      case $this->permissions->team:
        $this->permissionHandler("admin");
      break;
      case $this->permissions->client:
        $this->response->redirect("finance/invoices",true);
      break;
    }
  }

  public function InvoicesAction()
  {
    switch ($this->rhyno_user->permission)
    {
      case $this->permissions->admin:

        $invoices = ProjectInvoices::query()
        ->columns([
          'Manager\Models\ProjectInvoices.unique',
          'Manager\Models\ProjectInvoices.title',
          'Manager\Models\ProjectInvoices.status',
          'Manager\Models\ProjectInvoices.due',
          '(SELECT SUM(Manager\Models\InvoiceItems.price * Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as value',
          "(SELECT SUM(Manager\Models\InvoicePayments.amount) FROM Manager\Models\InvoicePayments where Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._) as paid",
          'Manager\Models\Projects.title   as project',
          'Manager\Models\Projects.unique  as uniqueP',
          'Manager\Models\Clients.name     as client',
          'Manager\Models\Clients.company  as company',
          'Manager\Models\Clients.image    as clientimg',
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\ProjectInvoices.project")
        ->innerJoin("Manager\Models\Clients","Manager\Models\Clients._ = Manager\Models\Projects.client")
        ->orderBy("Manager\Models\ProjectInvoices.status=1 DESC,
                   Manager\Models\ProjectInvoices.status=4 DESC,
                   Manager\Models\ProjectInvoices.status=3 DESC,
                   Manager\Models\ProjectInvoices.status=2 DESC,
                   Manager\Models\ProjectInvoices.status=5 DESC,
                   Manager\Models\ProjectInvoices.due      ASC")
        ->execute();

        $this->view->invoices = $invoices;
        $this->view->pick("finance/admin/invoices");
      break;
      case $this->permissions->team:
        $this->permissionHandler("admin");
      break;
      case $this->permissions->client:

        $invoices = ProjectInvoices::query()
        ->columns([
          'Manager\Models\ProjectInvoices.unique',
          'Manager\Models\ProjectInvoices.title',
          'Manager\Models\ProjectInvoices.status',
          'Manager\Models\ProjectInvoices.due',
          '(SELECT SUM(Manager\Models\InvoiceItems.price * Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as value',
          "(SELECT SUM(Manager\Models\InvoicePayments.amount) FROM Manager\Models\InvoicePayments where Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._) as paid",
          'Manager\Models\Projects.title   as project',
          'Manager\Models\Projects.unique  as uniqueP',
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\ProjectInvoices.project")
        ->where("Manager\Models\Projects.client = :client:")
        ->orderBy("Manager\Models\ProjectInvoices.status=1 DESC,
                   Manager\Models\ProjectInvoices.status=4 DESC,
                   Manager\Models\ProjectInvoices.status=3 DESC,
                   Manager\Models\ProjectInvoices.status=2 DESC,
                   Manager\Models\ProjectInvoices.status=5 DESC,
                   Manager\Models\ProjectInvoices.due      ASC")
        ->bind([
          "client" => $this->rhyno_user_info->_
        ])
        ->execute();

        $this->view->invoices = $invoices;
        $this->view->currency = Currencies::findFirst($this->rhyno_user_info->currency);
        $this->view->crud = $this->configuration->client_crud;
        $this->view->pick("finance/client/invoices");
      break;
    }
  }

  public function EstimatesAction()
  {
    switch ($this->rhyno_user->permission)
    {
      case $this->permissions->admin:

        $estimates = ProjectEstimates::query()
        ->columns([
          "Manager\Models\ProjectEstimates._",
          "Manager\Models\ProjectEstimates.title",
          "Manager\Models\ProjectEstimates.unique",
          "Manager\Models\ProjectEstimates.status",
          "Manager\Models\ProjectEstimates.issue",
          "Manager\Models\ProjectEstimates.due",
          "Manager\Models\ProjectEstimates.recurring",
          'Manager\Models\Projects.title   as project',
          'Manager\Models\Projects.unique  as uniqueP',
          'Manager\Models\Clients.name     as client',
          'Manager\Models\Clients.company  as company',
          'Manager\Models\Clients.image    as clientimg',
          "(SELECT SUM(Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as items",
          "(SELECT SUM(Manager\Models\EstimateItems.price * Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as value",
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\ProjectEstimates.project")
        ->innerJoin("Manager\Models\Clients","Manager\Models\Clients._ = Manager\Models\Projects.client")
        ->orderBy("Manager\Models\ProjectEstimates.status=1 DESC,
                   Manager\Models\ProjectEstimates.status=4 DESC,
                   Manager\Models\ProjectEstimates.status=3 DESC,
                   Manager\Models\ProjectEstimates.status=2 DESC,
                   Manager\Models\ProjectEstimates.status=5 DESC,
                   Manager\Models\ProjectEstimates.due      ASC")
        ->execute();

        $this->view->estimates = $estimates;
        $this->view->pick("finance/admin/estimates");
      break;
      case $this->permissions->team:
        $this->permissionHandler("admin");
      break;
      case $this->permissions->client:

        $estimates = ProjectEstimates::query()
        ->columns([
          "Manager\Models\ProjectEstimates._",
          "Manager\Models\ProjectEstimates.title",
          "Manager\Models\ProjectEstimates.unique",
          "Manager\Models\ProjectEstimates.status",
          "Manager\Models\ProjectEstimates.issue",
          "Manager\Models\ProjectEstimates.due",
          "Manager\Models\ProjectEstimates.recurring",
          'Manager\Models\Projects.title   as project',
          'Manager\Models\Projects.unique  as uniqueP',
          "(SELECT SUM(Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as items",
          "(SELECT SUM(Manager\Models\EstimateItems.price * Manager\Models\EstimateItems.amount) FROM Manager\Models\EstimateItems WHERE Manager\Models\EstimateItems.estimate = Manager\Models\ProjectEstimates._) as value",
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\ProjectEstimates.project")
        ->where("Manager\Models\Projects.client = :client: AND Manager\Models\ProjectEstimates.status != :pending:")
        ->bind([
          "client" => $this->rhyno_user_info->_,
          "pending" => 1
        ])
        ->orderBy("Manager\Models\ProjectEstimates.due ASC")
        ->execute();

        $this->view->estimates = $estimates;
        $this->view->crud = $this->configuration->client_crud;
        $this->view->pick("finance/client/estimates");
      break;
    }
  }

  public function RequestsAction()
  {
    switch ($this->rhyno_user->permission)
    {
      case $this->permissions->team: $this->permissionHandler("admin"); break;
      case $this->permissions->admin:

        $requests = EstimateRequests::query()
        ->columns([
          'Manager\Models\EstimateRequests.unique',
          'Manager\Models\EstimateRequests.title',
          'Manager\Models\EstimateRequests.status',
          'Manager\Models\EstimateRequests.budget',
          'Manager\Models\EstimateRequests.date',
          'Manager\Models\Projects.unique as uniqueP',
          'Manager\Models\Projects.title  as project',
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\EstimateRequests.project")
        ->orderBy("Manager\Models\EstimateRequests.status=1 DESC,
                   Manager\Models\EstimateRequests.status=2 DESC,
                   Manager\Models\EstimateRequests.date     ASC")
        ->execute();

        $this->view->requests = $requests;
        $this->view->pick("finance/admin/requests");
      break;

      case $this->permissions->client:
        if($this->configuration->client_crud->projectrequests_page)
        {
          $requests = EstimateRequests::query()
          ->columns([
            'Manager\Models\EstimateRequests.unique',
            'Manager\Models\EstimateRequests.title',
            'Manager\Models\EstimateRequests.status',
            'Manager\Models\EstimateRequests.budget',
            'Manager\Models\EstimateRequests.date',
            'Manager\Models\Projects.unique as uniqueP',
            'Manager\Models\Projects.title  as project',
          ])
          ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\EstimateRequests.project")
          ->where("Manager\Models\Projects.client = :client:")
          ->orderBy("Manager\Models\EstimateRequests.status=1 DESC,
                     Manager\Models\EstimateRequests.status=2 DESC,
                     Manager\Models\EstimateRequests.date     ASC")
          ->bind([
            "client" => $this->rhyno_user_info->_
          ])
          ->execute();

          $this->view->requests = $requests;
          $this->view->crud = $this->configuration->client_crud;
          $this->view->pick("finance/client/requests");
        }
        else { $this->permissionHandler("team"); }
      break;
    }

    $form = new Form;
    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]);
    foreach($element as $e)
    {
      $form->add($e);
    }
    $this->view->form = $form;

  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    $issue = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("issue","string"));
    $due   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("due","string"));

    # catch any form errors
    try
    {

      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("project")):
        return RhynoException::EmptyInput("Project");

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("issue","string")):
        return RhynoException::EmptyInput("Issue Date");

      elseif(!$this->request->getPost("due","string")):
        return RhynoException::EmptyInput("Due Date");

      elseif($issue > $due):
        return RhynoException::CustomError("Woah! Issue date can't be after the due date!");

      elseif($email != $u->email && Users::findFirstByEmail($email)->_ != NULL):
        return RhynoException::RegisteredEmailAddress();

      elseif($username != $u->username && Users::findFirstByUsername($username)->_ != NULL):
        return RhynoException::RegisteredUsernameAddress();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $title  = $this->request->getPost("title","string");

        switch ($this->rtarget)
        {
          case 'invoice':

            $inv = new ProjectInvoices;
              $inv->unique         = $this->uniqueCode("INV");
              $inv->project        = $this->request->getPost("project","int");
              $inv->status         = 1;
              $inv->title          = $title;
              $inv->issue          = $issue->format("Y-m-d H:i:s");
              $inv->due            = $due->format("Y-m-d H:i:s");
              $inv->last_recursion = $issue->format("Y-m-d H:i:s");
              $inv->recurring      = ($this->request->getPost("recurring") == true ? 1 : 0);
            if(!$inv->save()){ return RhynoException::DBError(); }

            $txt = "Created a new invoice, '{$title}' to the project.";

          break;
          case 'estimate':

            $est = new ProjectEstimates;
              $est->unique    = $this->uniqueCode("EST");
              $est->project   = $this->request->getPost("project","int");
              $est->status    = 1;
              $est->title     = $title;
              $est->issue     = $issue->format("Y-m-d H:i:s");
              $est->due       = $due->format("Y-m-d H:i:s");
              $est->recurring = ($this->request->getPost("recurring") == true ? 1 : 0);
            if(!$est->save()){ return RhynoException::DBError(); }

            $txt = "Created a new estimate, '{$title}' to the project.";

          break;
        }

        $act = new ProjectActivities;
          $act->project = $this->request->getPost("project","int");
          $act->text    = $txt;
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Successfully created!";
        $this->flags['redirect']   = $this->request->getHTTPReferer();
      }
      catch (\Exception $e)
      {
        $this->flags['toast']      = "warning";
        $this->flags['title']      = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function SendAction()
  {
    $this->response->setContentType("application/json");

    # catch any form errors
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("project")):
        return RhynoException::EmptyInput("Project");

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("text","string")):
        return RhynoException::EmptyInput("Description");

      elseif(!$this->request->getPost("budget","string")):
        return RhynoException::EmptyInput("Budget");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      try
      {
        $currency = Currencies::findFirst($this->rhyno_user_info->currency);
        $convert  = $this->CurrencyConverter($this->request->getPost("budget","int"), $currency->code);
        $budget   = $convert->values->converted;

        $req = new EstimateRequests;
          $req->unique    = $this->uniqueCode("REQ");
          $req->project   = $this->request->getPost("project","int");
          $req->title     = $this->request->getPost("title","string");
          $req->text      = $this->request->getPost("text","string");
          $req->budget    = $budget;
          $req->reference = $this->request->getPost("reference","string");
          $req->date      = (new \DateTime())->format("Y-m-d H:i:s");
          $req->status    = 1;
        if(!$req->save()){ return RhynoException::DBError(); }

        $act = new ProjectActivities;
          $act->project = $this->request->getPost("project","int");
          $act->text    = "Requested an estimate '#{$req->unique}'.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Estimate request sent!";
        $this->flags['redirect']   = $this->request->getHTTPReferer();
      }
      catch (\Exception $e)
      {
        $this->flags['toast']      = "warning";
        $this->flags['title']      = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form    = new Form;
      $inputs  = [];

      if($this->rtarget == "invoice" && $this->rmethod == "create")
      {
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/finance/invoices/new";
        $modal_header = "Create a Invoice!";
        $modal_card   = ["content" => "<strong>Remember!</strong> The invoice value is defined by the item values within, creating a invoice will not generate a value ! All Items must be added inside the invoice."];

        $element['project'] = new Select( "project" , Projects::find() ,[
          'using' => ['_','title'],
          'label' => "Select a Project"
        ]);

        $element['title'] = new Text( "title" ,[
          'label' => "Invoice Title",
        ]);

        $element['issue'] = new Text( "issue" ,[
          'label' => "Select a issue date",
          'class' => "datepicker",
        ]);

        $element['due'] = new Text( "due" ,[
          'label' => "Select a due date",
          'class' => "datepicker",
        ]);

        $element['recurring'] = new Check( "recurring" ,[
          'label'  => "Recurrent Invoice?",
          'switch' => true,
        ]);

      }
      if($this->rtarget == "estimate" && $this->rmethod == "create")
      {
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/finance/estimates/new";
        $modal_header = "Create a Estimate!";
        $modal_card   = ["content" => "<strong>Remember!</strong> The estimate value is defined by the item values within, creating a estimate will not generate a value ! All Items must be added inside the estimate."];

        $element['project'] = new Select( "project" , Projects::find() ,[
          'using' => ['_','title'],
          'label' => "Select a Project"
        ]);

        $element['title'] = new Text( "title" ,[
          'label' => "Estimate Title",
        ]);

        $element['issue'] = new Text( "issue" ,[
          'label' => "Select a issue date",
          'class' => "datepicker",
        ]);

        $element['due'] = new Text( "due" ,[
          'label' => "Select a due date",
          'class' => "datepicker",
        ]);

        $element['recurring'] = new Check( "recurring" ,[
          'label'  => "Recurrent as Invoice?",
          'switch' => true,
        ]);

      }
      if($this->rtarget == "estimate" && $this->rmethod == "request")
      {
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/finance/estimates/send";
        $modal_header = "Request an Estimate!";

        $currency = Currencies::findFirst($this->rhyno_user_info->currency);

        $element['project'] = new Select( "project" , Projects::findByClient($this->rhyno_user_info->_) ,[
          'using' => ['_','title'],
          'label' => "Select a Project"
        ]);

        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Estimate Title",
        ]);

        $element['text'] = new Textarea( "text" ,[
          'class' => "materialize-textarea",
          'label' => "What you want done?",
        ]);

        $element['budget'] = new Text( "budget" ,[
          'label'             => "What budget do you have in mind? ({$currency->code})",
          'data-mask'         => "000.000.000.000.000,00",
          'data-mask-reverse' => true,
        ]);

        $element['reference'] = new Text( "reference" ,[
          'label' => "Do you have any reference link?",
        ]);
      }
      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl"), "switch" => $f->getAttribute("switch") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
