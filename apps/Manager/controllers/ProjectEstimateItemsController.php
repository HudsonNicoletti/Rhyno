<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users              as Users,
    Manager\Models\Projects           as Projects,
    Manager\Models\ProjectActivities  as ProjectActivities,
    Manager\Models\ProjectEstimates   as ProjectEstimates,
    Manager\Models\EstimateItems      as EstimateItems,
    Manager\Models\EstimateRequests   as EstimateRequests;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectEstimateItemsController extends ControllerBase
{
  private $uniqueP;
  private $uniqueE;
  private $uniqueT;
  private $project;
  private $estimate;
  private $rmethod;
  private $item;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueE = $this->dispatcher->getParam("estimate","string");
    $this->uniqueT = $this->dispatcher->getParam("item","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project  = Projects::findFirstByUnique($this->uniqueP);
    $this->estimate = ProjectEstimates::findFirstByUnique($this->uniqueE);
    $this->item     = EstimateItems::findFirstByUnique($this->uniqueT);
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueE):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->estimate):
        return RhynoException::Unreachable();

      elseif(!$this->estimate->project == $this->project->_):
        return RhynoException::CustomError("Whoah, this estimate is not part of this project!");

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Item Title");

      elseif(!$this->request->getPost("amount","int") || !$this->request->getPost("price","int")):
        return RhynoException::EmptyInput(["Amount / Hours","Unit Price"]);

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      $title = $this->request->getPost("title","string");
      try
      {
        $itm = new EstimateItems;
          $itm->unique   = $this->uniqueCode("ITM");
          $itm->estimate = $this->estimate->_;
          $itm->title    = $title;
          $itm->text     = $this->request->getPost("text","string");
          $itm->amount   = $this->request->getPost("amount","int");
          $itm->price    = $this->request->getPost("price","int");
        if(!$itm->save())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Estimate item successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/overview/{$this->uniqueE}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
      finally
      {
        $act = new ProjectActivities;
          $act->project = $project->_;
          $act->text    = "Added a new estimate item, '{$title}' to the project estimate '#{$this->uniqueE}'.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueE || !$this->uniqueT):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->estimate || !$this->item):
        return RhynoException::Unreachable();

      elseif(!$this->item->estimate == $this->estimate->_):
        return RhynoException::CustomError("Whoah, this estimate is not part of this project!");

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Item Title");

      elseif(!$this->request->getPost("amount","int") || !$this->request->getPost("price","int")):
        return RhynoException::EmptyInput(["Amount / Hours","Unit Price"]);

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      $title    = $this->request->getPost("title","string");

      try
      {
        $this->item->title   = $title;
        $this->item->text    = $this->request->getPost("text","string");
        $this->item->amount  = $this->request->getPost("amount","int");
        $this->item->price   = $this->request->getPost("price","int");
        if(!$this->item->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Updated a estimate item, '{$title}' to the project estimate '#{$this->uniqueE}'.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project estimate successfully updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/overview/{$this->uniqueE}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueE || !$this->uniqueT):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->estimate || !$this->item):
        return RhynoException::Unreachable();

      elseif(!$this->item->estimate == $this->estimate->_):
        return RhynoException::CustomError("Whoah, this estimate is not part of this project!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):

      try
      {
        if(!$this->item->delete())
        {
          return RhynoException::Unreachable();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Estimate item successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/overview/{$this->uniqueE}";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form   = new Form;
      $inputs = [];

      if($this->rmethod == "remove"):

      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Item Title",
        ]);
        $element['text'] = new Textarea( "text" ,[
          'label' => "Description",
          'class' => "materialize-textarea"
        ]);
        $element['amount'] = new Text( "amount" ,[
          'label' => "Amount / Hours",
        ]);
        $element['price'] = new Text( "price" ,[
          'label'             => "Unit Price",
          'data-mask'         => "000.000.000.000.000,00",
          'data-mask-reverse' => true,
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/{$this->uniqueE}/item/new";
        $modal_header = "Create a estimate item!";

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/{$this->uniqueE}/item/update/{$this->uniqueT}";
        $modal_header = "Update Project estimate item!";

        $element['title']->setAttribute("active",true)->setAttribute("value",$this->item->title);
        $element['text']->setAttribute("active",true)->setAttribute("value",$this->item->text);
        $element['amount']->setAttribute("active",true)->setAttribute("value",$this->item->amount);
        $element['price']->setAttribute("active",true)->setAttribute("value",$this->item->price);

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/estimates/{$this->uniqueE}/item/delete/{$this->uniqueT}";
        $modal_header = "Remove Project estimate item!";

        $modal_card = [
          "content"  => "Are you sure? The selected estimate item will me permanently removed from the invoice."
        ];

      endif;


      foreach($element as $e){$form->add($e);}
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
