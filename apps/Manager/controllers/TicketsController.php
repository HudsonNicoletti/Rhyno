<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users            as Users,
    Manager\Models\Team             as Team,
    Manager\Models\Clients          as Clients,
    Manager\Models\Projects         as Projects,
    Manager\Models\Tickets          as Tickets,
    Manager\Models\TicketCategories as TicketCategories,
    Manager\Models\TicketResponses  as TicketResponses;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Hidden;

class TicketsController extends ControllerBase
{
  private $unique;
  private $rmethod;
  private $ticket;

  private $team_ticket_created_email;
  private $team_ticket_created_inner;
  private $team_ticket_response_email;
  private $team_ticket_response_inner;
  private $team_ticket_status_email;
  private $team_ticket_status_inner;
  private $client_ticket_response_email;
  private $client_ticket_response_inner;
  private $client_ticket_status_email;
  private $client_ticket_status_inner;

  public function onConstruct()
  {
    $this->unique = $this->dispatcher->getParam("ticket","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");
    $this->ticket = Tickets::findFirstByUnique($this->unique);

    $this->team_ticket_created_email    = $this->configuration->team_notification->ticket_created_email;
    $this->team_ticket_created_inner    = $this->configuration->team_notification->ticket_created_inner;
    $this->team_ticket_response_email   = $this->configuration->team_notification->ticket_response_email;
    $this->team_ticket_response_inner   = $this->configuration->team_notification->ticket_response_inner;
    $this->team_ticket_status_email     = $this->configuration->team_notification->ticket_status_email;
    $this->team_ticket_status_inner     = $this->configuration->team_notification->ticket_status_inner;
    $this->client_ticket_response_email = $this->configuration->client_notification->ticket_response_email;
    $this->client_ticket_response_inner = $this->configuration->client_notification->ticket_response_inner;
    $this->client_ticket_status_email   = $this->configuration->client_notification->ticket_status_email;
    $this->client_ticket_status_inner   = $this->configuration->client_notification->ticket_status_inner;
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("tickets/admin/index");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->tickets_page)
        {
          $this->view->pick("tickets/team/index");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        if($this->configuration->client_crud->tickets_page)
        {
          $this->view->pick("tickets/client/index");
          $this->view->crud = $this->configuration->client_crud;
        }
        else
        {
          $this->permissionHandler("team");
        }
      break;
    }

    if($this->rhyno_user->permission >= $this->permissions->team):
      $tickets = Tickets::query()
      ->columns([
        'Manager\Models\Tickets._',
        'Manager\Models\Tickets.unique',
        'Manager\Models\Tickets.title',
        'Manager\Models\Tickets.updated',
        'Manager\Models\Tickets.status',
        'Manager\Models\TicketCategories.category',
        'Manager\Models\Clients.name',
        'Manager\Models\Clients.image',
        'Manager\Models\Clients.company',
      ])
      ->innerJoin('Manager\Models\TicketCategories', 'Manager\Models\Tickets.category = Manager\Models\TicketCategories._')
      ->innerJoin('Manager\Models\Clients', 'Manager\Models\Clients.uid = Manager\Models\Tickets.uid')
      ->orderBy("status=1 DESC,status=2 DESC,status=3 DESC,status=4 DESC,updated ASC")
      ->execute();

      $this->view->tickets = $tickets;

    elseif($this->rhyno_user->permission <= $this->permissions->client):
      $tickets = Tickets::query()
      ->columns([
        'Manager\Models\Tickets._',
        'Manager\Models\Tickets.unique',
        'Manager\Models\Tickets.title',
        'Manager\Models\Tickets.updated',
        'Manager\Models\Tickets.status',
        'Manager\Models\TicketCategories.category',
      ])
      ->innerJoin('Manager\Models\TicketCategories', 'Manager\Models\Tickets.category = Manager\Models\TicketCategories._')
      ->where("Manager\Models\Tickets.uid = :client:")
      ->orderBy("status=1 DESC,status=2 DESC,status=3 DESC,status=4 DESC,updated ASC")
      ->bind([
        "client" => $this->rhyno_user->_
      ])
      ->execute();


      $this->view->tickets = $tickets;
    endif;
  }

  public function OverviewAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("tickets/admin/overview");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->tickets_page)
        {
          $this->view->pick("tickets/team/overview");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        if($this->configuration->client_crud->tickets_page)
        {
          $this->view->pick("tickets/client/overview");
          $this->view->crud = $this->configuration->client_crud;
        }
        else
        {
          $this->permissionHandler("team");
        }
      break;
    }

    $this->assets->addCss("assets/manager/css/pages/profile.css")
                 ->addCss("assets/manager/css/pages/dashboard.css")
                 ->addJs("assets/manager/addons/slimscroll/jquery.slimscroll.js");

    $unique = $this->dispatcher->getParam("ticket","string");

    $ticket = Tickets::query()
    ->columns([
      'Manager\Models\Tickets._',
      'Manager\Models\Tickets.unique',
      'Manager\Models\Tickets.title',
      'Manager\Models\Tickets.updated',
      'Manager\Models\Tickets.created',
      'Manager\Models\Tickets.status',
      'Manager\Models\TicketCategories.category',
      'Manager\Models\Clients.name',
      'Manager\Models\Clients.image',
      'Manager\Models\Clients.company',
      'Manager\Models\Clients.role',
      'Manager\Models\Clients.domain',
      'Manager\Models\Clients.phone',
      'Manager\Models\Clients.vat',
      'Manager\Models\Clients.address',
      'Manager\Models\Clients.zip',
      'Manager\Models\Clients.city',
      'Manager\Models\Clients.state',
      'Manager\Models\Clients.country',
      'Manager\Models\Users.email',
    ])
    ->innerJoin('Manager\Models\TicketCategories', 'Manager\Models\Tickets.category = Manager\Models\TicketCategories._')
    ->innerJoin('Manager\Models\Clients', 'Manager\Models\Clients.uid = Manager\Models\Tickets.uid')
    ->innerJoin('Manager\Models\Users', 'Manager\Models\Tickets.uid = Manager\Models\Users._')
    ->where("Manager\Models\Tickets.unique = :unique:")
    ->bind([
      "unique" => $unique
    ])
    ->execute();

    $responses = TicketResponses::query()
    ->columns([
      'Manager\Models\TicketResponses._',
      'Manager\Models\TicketResponses.text',
      'Manager\Models\TicketResponses.date',
      'Manager\Models\TicketResponses.uid as user',
      'COALESCE(Manager\Models\Clients.name, Manager\Models\Team.name) as name',
      'COALESCE(Manager\Models\Clients.image, Manager\Models\Team.image) as image',
    ])
    ->leftJoin('Manager\Models\Clients', 'Manager\Models\Clients.uid = Manager\Models\TicketResponses.uid')
    ->leftJoin('Manager\Models\Team', 'Manager\Models\Team.uid = Manager\Models\TicketResponses.uid')
    ->where("Manager\Models\TicketResponses.ticket = :ticket:")
    ->bind([
      "ticket" => $ticket[0]->_
    ])
    ->orderBy("Manager\Models\TicketResponses.date ASC")
    ->execute();

    $form = new Form;
    $form->add(new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken(),
    ]));

    $this->view->me = $this->rhyno_user->_;
    $this->view->form = $form;
    $this->view->ticket = $ticket[0];
    $this->view->responses = $responses;
  }

  public function SendAction()
  {
    $this->response->setContentType("application/json");

    $message = $this->request->getPost("text","string");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->unique):
        return RhynoException::WrongNumberOfParams();

      elseif(!$message):
        return RhynoException::EmptyInput("Message");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $response = new TicketResponses;
          $response->ticket   = $this->ticket->_;
          $response->uid      = $this->rhyno_user->_;
          $response->text     = $message;
          $response->date     = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$response->save()){ return RhynoException::DBError(); }

        $this->ticket->updated = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$this->ticket->save()){ return RhynoException::DBError(); }

        # notifications
        # if the user is the ticket creator , send to team members notification
        if($this->rhyno_user->_ == $this->ticket->uid)
        {
          # if is enabled
          if($this->team_ticket_status_inner || $this->team_ticket_status_email)
          {
            $exceptions = ["inner"=>[],"email"=>[]];
            foreach(Team::find() as $m)
            {
              $receiver = $m;
              $user_inf = Users::findFirst($receiver->uid);
              # system notification
              if($this->team_ticket_response_inner)
              {
                $inner = $this->NewNotification([
                  "receiver" => $receiver->uid,
                  "text"     => "A response has been made to the ticket #{$this->unique}!",
                  "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}"
                ]);

                if(!$inner){ array_push($exceptions['inner'],$receiver->name); }
              }

              if($this->team_ticket_response_email)
              {
                $email = $this->NewEmailNotification([
                  "receiver" => $receiver->uid,
                  "title"    => "A response has been made to the ticket #{$this->unique}!",
                  "text"     => "You are receiving this notification because a response has been made to the ticket #{$this->unique}!",
                  "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}",
                ]);

                if(!$email){ array_push($exceptions['email'],$user_inf->email); }
              }
            }

            if(count($exceptions['inner']) > 0){ return RhynoException::NotificationError($exceptions['inner']); }
            if(count($exceptions['email']) > 0){ return RhynoException::EmailSendError($exceptions['email']); }
          }
        }
        else
        {
          if($this->client_ticket_response_inner || $this->client_ticket_response_email)
          {
            $receiver = Clients::findFirst($this->ticket->uid);
            $user_inf = Users::findFirst($receiver->uid);
            # system notification
            if($this->team_ticket_response_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "A response has been made to the ticket #{$this->unique}!",
                "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}"
              ]);

              if(!$inner){ return RhynoException::NotificationError($receiver->name); }
            }

            if($this->team_ticket_response_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "A response has been made to the ticket #{$this->unique}!",
                "text"     => "You are receiving this notification because a response has been made to the ticket #{$this->unique}!",
                "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}",
              ]);

              if(!$email){ return RhynoException::EmailSendError($user_inf->email); }
            }
          }
        }
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

      $this->flags['toast']      = "success";
      $this->flags['title']      = "Message Sent!";
      $this->flags['redirect']   = "{$this->rhyno_url}/ticket/overview/{$this->unique}";
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function StatusAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->unique || !$this->rmethod):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        switch ($this->rmethod)
        {
          case 'reopen':   $this->ticket->status = 2; $status = "reopened"; break;
          case 'progress': $this->ticket->status = 3; $status = "in progress"; break;
          case 'close':    $this->ticket->status = 4; $status = "closed"; break;
        }
        $this->ticket->updated = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$this->ticket->save())
        {
          return RhynoException::DBError();
        }

        if($this->team_ticket_status_inner || $this->team_ticket_status_email)
        {
          $exceptions = ["inner"=>[],"email"=>[]];
          foreach(Team::find() as $m)
          {
            $receiver = $m;
            $user_inf = Users::findFirst($receiver->uid);
            # system notification
            if($this->team_ticket_response_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "Status of the ticket #{$this->unique} has been set to {$status}!",
                "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}"
              ]);

              if(!$inner){ array_push($exceptions['inner'],$receiver->name); }
            }

            if($this->team_ticket_response_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "Status of the ticket #{$this->unique} has been updated!",
                "text"     => "You are receiving this notification because the ticket #{$this->unique} status has been set to {$status}!",
                "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}",
              ]);

              if(!$email){ array_push($exceptions['email'],$user_inf->email); }
            }
          }

          if(count($exceptions['inner']) > 0){ return RhynoException::NotificationError($exceptions['inner']); }
          if(count($exceptions['email']) > 0){ return RhynoException::EmailSendError($exceptions['email']); }
        }
        if($this->client_ticket_status_inner || $this->client_ticket_status_email)
        {
          $receiver = Clients::findFirstByUid($this->ticket->uid);
          $user_inf = Users::findFirst($receiver->uid);
          # system notification
          if($this->team_ticket_response_inner)
          {
            $inner = $this->NewNotification([
              "receiver" => $receiver->uid,
              "text"     => "Status of the ticket #{$this->unique} has been set to {$status}!",
              "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}"
            ]);

            if(!$inner){ return RhynoException::NotificationError($receiver->name); }
          }

          if($this->team_ticket_response_email)
          {
            $email = $this->NewEmailNotification([
              "receiver" => $receiver->uid,
              "title"    => "Status of the ticket #{$this->unique} has been updated!",
              "text"     => "You are receiving this notification because the ticket #{$this->unique} status has been set to {$status}!",
              "href"     => "{$this->rhyno_url}/ticket/overview/{$this->unique}",
            ]);

            if(!$email){ return RhynoException::EmailSendError($user_inf->email); }
          }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Ticket Status Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/ticket/overview/{$this->unique}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    $title = $this->request->getPost("title","string");
    $category = $this->request->getPost("category","int");
    $message = $this->request->getPost("text","string");
    # catch any form errors
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$title):
        return RhynoException::EmptyInput("Title");

      elseif(!$category):
        return RhynoException::EmptyInput("Category");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $ticket = new Tickets;
          $ticket->unique   = $this->uniqueCode("SUP");
          $ticket->uid      = $this->rhyno_user->_;
          $ticket->title    = $title;
          $ticket->category = $category;
          $ticket->updated  = (new \DateTime())->format("Y-m-d H:i:s");
          $ticket->created  = (new \DateTime())->format("Y-m-d H:i:s");
          $ticket->status   = 1;
        if(!$ticket->save()){ return RhynoException::DBError(); }

        if($message)
        {
          $response = new TicketResponses;
            $response->ticket   = $ticket->_;
            $response->uid      = $this->rhyno_user->_;
            $response->text     = $message;
            $response->date     = (new \DateTime())->format("Y-m-d H:i:s");
          if(!$response->save()){ return RhynoException::CustomError("Unable to create message."); }
        }

        if($this->team_ticket_status_inner || $this->team_ticket_status_email)
        {
          $exceptions = ["inner"=>[],"email"=>[]];
          foreach(Team::find() as $m)
          {
            $receiver = $m;
            $user_inf = Users::findFirst($receiver->uid);
            # system notification
            if($this->team_ticket_response_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "New ticket created! #{$ticket->unique}",
                "href"     => "{$this->rhyno_url}/ticket/overview/{$ticket->unique}"
              ]);

              if(!$inner){ array_push($exceptions['inner'],$receiver->name); }
            }

            if($this->team_ticket_response_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "New ticket created! #{$ticket->unique}",
                "text"     => "You are receiving this notification because a new ticket was created #{$ticket->unique}!",
                "href"     => "{$this->rhyno_url}/ticket/overview/{$ticket->unique}",
              ]);

              if(!$email){ array_push($exceptions['email'],$user_inf->email); }
            }
          }
          if(count($exceptions['inner']) > 0){ return RhynoException::NotificationError($exceptions['inner']); }
          if(count($exceptions['email']) > 0){ return RhynoException::EmailSendError($exceptions['email']); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Ticket successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/tickets";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    $title = $this->request->getPost("title","string");
    $category = $this->request->getPost("category","int");
    # catch any form errors
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$title):
        return RhynoException::EmptyInput("Title");

      elseif(!$category):
        return RhynoException::EmptyInput("Category");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $this->ticket->title    = $title;
        $this->ticket->category = $category;
        $this->ticket->updated  = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$this->ticket->save()){ return RhynoException::DBError(); }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Ticket successfully updated!";
        $this->flags['redirect']   =  $this->request->getHTTPReferer();
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");
    # catch any form errors
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->unique):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->ticket):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $tr = TicketResponses::findByTicket($this->ticket->_);

        if(!$tr->delete()):
          return RhynoException::CustomError("Unable to remove ticket responses.");
        elseif(!$this->ticket->delete()):
          return RhynoException::CustomError("Unable to remove ticket.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Ticket successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/tickets";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form   = new Form;
      $inputs = [];

      if($this->rmethod == "create"):

        $element['title'] = new Text("title",[
          'label' => "Ticket Title"
        ]);

        $element['text'] = new Textarea("text",[
          'class' => "materialize-textarea",
          'label' => "Ticket Message"
        ]);

        $element['category'] = new Select("category",TicketCategories::find(["order"=>"category"]),[
          'using' => ["_","category"],
          'label' => "Category"
        ]);

      elseif($this->rmethod == "modify"):
        $element['title'] = new Text("title",[
          'label' => "Ticket Title"
        ]);

        $element['category'] = new Select("category",TicketCategories::find(["order"=>"category"]),[
          'using' => ["_","category"],
          'label' => "Category"
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/ticket/new";
        $modal_header = "Create new Ticket!";

      elseif( $this->rmethod == "modify" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/ticket/update/{$this->unique}";
        $modal_header = "Update Ticket Information!";

        $element['title']->setAttribute("active",true)->setAttribute("value",$this->ticket->title);
        $element['category']->setAttribute("active",false)->setAttribute("value",$this->ticket->category);

      elseif( $this->rmethod == "remove" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/ticket/delete/{$this->unique}";
        $modal_header = "Remove Ticket";

        $modal_card = [ "content" => "Are you sure you want to delete this ticket?" ];
      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
