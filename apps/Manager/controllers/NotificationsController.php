<?php

namespace Manager\Controllers;

use Manager\Models\Notifications as Notifications;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class NotificationsController extends ControllerBase
{
  public function onConstruct()
  {
    $this->clearNewNotifications();
  }
  public function IndexAction()
  {
    $this->view->allnotifications = $this->Notifications(10);
  }
}
