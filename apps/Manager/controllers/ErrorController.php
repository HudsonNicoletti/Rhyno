<?php

namespace Manager\Controllers;

class ErrorController extends ControllerBase
{
  public function NotFoundAction()
  {
    $this->assets->addCss("assets/manager/css/error.css");
    $this->view->pick("error/404");
  }

  public function InternalAction()
  {
    $this->assets->addCss("assets/manager/css/error.css");
    $this->view->pick("error/500");
  }

  public function ForbiddenAction()
  {
    $this->assets->addCss("assets/manager/css/error.css");
    $this->view->pick("error/403");
  }
}
