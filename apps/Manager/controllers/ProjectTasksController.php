<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Team              as Team,
    Manager\Models\Users             as Users,
    Manager\Models\Tasks             as Tasks,
    Manager\Models\Projects          as Projects,
    Manager\Models\Assignments       as Assignments,
    Manager\Models\TaskResponses     as TaskResponses,
    Manager\Models\TaskCollaborators as TaskCollaborators;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden;

class ProjectTasksController extends ControllerBase
{
  private $uniqueP;
  private $uniqueT;
  private $rmethod;
  private $project;
  private $task;
  private $task_assigned_email;
  private $task_assigned_inner;
  private $task_info_email;
  private $task_info_inner;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueT = $this->dispatcher->getParam("task","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->task_assigned_email = $this->configuration->team_notification->task_assigned_email;
    $this->task_assigned_inner = $this->configuration->team_notification->task_assigned_inner;
    $this->task_info_email     = $this->configuration->team_notification->task_info_email;
    $this->task_info_inner     = $this->configuration->team_notification->task_info_inner;

    $this->project = Projects::findFirstByUnique($this->uniqueP);
    $this->task    = Tasks::findFirstByUnique($this->uniqueT);

    if($this->dispatcher->getActionName() == "index")
    {
      $this->updateTasks($this->project->_);
    }
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/tasks");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projecttasks_page)
        {
          $this->view->pick("projects/team/tasks");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->permissionHandler("team");
      break;
    }

    $tasks = Tasks::query()
    ->columns([
      'Manager\Models\Tasks._',
      'Manager\Models\Tasks.unique',
      'Manager\Models\Tasks.title',
      'Manager\Models\Tasks.created',
      'Manager\Models\Tasks.deadline',
      'Manager\Models\Tasks.status',
      'Manager\Models\Tasks.assigned',
      'Manager\Models\Team.image as managerimage',
      'Manager\Models\Team.name as manager'
    ])
    ->innerJoin('Manager\Models\Team', 'Manager\Models\Team._ = Manager\Models\Tasks.assigned')
    ->where("Manager\Models\Tasks.project = :project:")
    ->bind([
      "project" => $this->project->_
    ])
    ->orderBy("Manager\Models\Tasks.status=3 DESC,
               Manager\Models\Tasks.status=1 DESC,
               Manager\Models\Tasks.status=2 DESC,
               Manager\Models\Tasks.status=4 DESC,
               Manager\Models\Tasks.deadline ASC")
    ->execute();

    $collaborators = [];
    foreach($tasks as $task)
    {
      $tskc = TaskCollaborators::query()
      ->columns([
        'Manager\Models\Team.name',
        'Manager\Models\Team.image',
        'Manager\Models\TaskCollaborators.status'
      ])
      ->innerJoin('Manager\Models\Team', 'Manager\Models\Team._ = Manager\Models\TaskCollaborators.member')
      ->where("Manager\Models\TaskCollaborators.task = :task: AND Manager\Models\TaskCollaborators.member != :manager:")
      ->bind([
        "task"    => $task->_,
        "manager" => $task->assigned
      ])
      ->orderBy("Manager\Models\Team.name ASC")
      ->execute();

      $collaborators[$task->_] = [];
      foreach($tskc as $tc)
      {
        array_push($collaborators[$task->_],["member"=>$tc->name,"image"=>$tc->image,"status"=>$tc->status]);
      }
    }

    $this->view->tasks = $tasks;
    $this->view->collaborators = $collaborators;
    $this->view->project = $this->project;
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif($this->rhyno_user->permission <= $this->permissions->client):
        return RhynoException::PermissionDenied();

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("deadline","string")):
        return RhynoException::EmptyInput("Deadline date");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $manager = $this->request->getPost("manager","int");
        $members = $this->request->getPost("members");
        $exceptions = ["inner"=>[],"email"=>[]];

        $deadline   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("deadline","string"));

        $task = new Tasks;
          $task->unique       = $this->uniqueCode("TSK");
          $task->project      = $this->project->_;
          $task->title        = $this->request->getPost("title","string");
          $task->description  = $this->request->getPost("description","string");
          $task->status       = 1;
          $task->assigned     = $this->request->getPost("manager","int");
          $task->deadline     = $deadline->format("Y-m-d H:i:s");
          $task->created      = (new \DateTime())->format("Y-m-d H:i:s");
        if($task->save())
        {
          if(!in_array($manager,$members))
          {
            $tc = new TaskCollaborators;
              $tc->task   = $task->_;
              $tc->member = $manager;
              $tc->status = 1;
            if(!$tc->save())
            {
              return RhynoException::DBError();
            }
          }
          foreach($this->request->getPost("members") as $m)
          {
            $tc = new TaskCollaborators;
              $tc->task   = $task->_;
              $tc->member = $m;
              $tc->status = 1;
            if(!$tc->save())
            {
              return RhynoException::DBError();
            }
          }
          foreach($this->request->getPost("members") as $m)
          {
            $receiver = Team::findFirst($m);
            $user_inf = Users::findFirst($receiver->uid);

            if($this->task_assigned_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "You are now assigned to a task! #{$task->unique}",
                "href"     => "{$this->rhyno_url}/task/overview/{$task->unique}"
              ]);

              if(!$inner){ array_push($exceptions['inner'],$receiver->name); }
            }

            if($this->task_assigned_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "You Were Assigned To a Task !",
                "text"     => "You are receiving this e-mail because you were assigned to a project's task! #{$task->unique}",
                "href"     => "{$this->rhyno_url}/task/overview/{$task->unique}",
              ]);

              if(!$email){ array_push($exceptions['email'],$user_inf->email); }
            }
          }

          if(count($exceptions['inner']) > 0){ return RhynoException::NotificationError($exceptions['inner']); }
          if(count($exceptions['email']) > 0){ return RhynoException::EmailSendError($exceptions['email']); }

        }
        else
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project task successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/tasks";

      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueT):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->task):
        return RhynoException::Unreachable();

      elseif($this->rhyno_user->permission <= $this->permissions->client):
        return RhynoException::PermissionDenied();

      elseif(!$this->task->project == $this->project->_):
        return RhynoException::CustomError("Woah, thats not valid! This task is not part of this project!");

      elseif(!$this->request->getPost("title","string")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("deadline","string")):
        return RhynoException::EmptyInput("Deadline date");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $manager = $this->request->getPost("manager","int");
        $members = $this->request->getPost("members");
        $exceptions = ["inner"=>[],"email"=>[]];

        $deadline   = \DateTime::createFromFormat($this->rhyno_date, $this->request->getPost("deadline","string"));

        $this->task->title        = $this->request->getPost("title","string");
        $this->task->description  = $this->request->getPost("description","string");
        $this->task->assigned     = $this->request->getPost("manager","int");
        $this->task->deadline     = $deadline->format("Y-m-d H:i:s");
        if($this->task->save())
        {
          # GET ALL Collaborators
          $collabs = TaskCollaborators::findByTask($this->task->_);
          # Set array to push assigned collaborators
          $assignedCollabs = [];
          # Loop throgh already assigned collaborators
          foreach($collabs as $c)
          {
            # Verify if collaborator is not in the post array
            if(!in_array($c->member,$members))
            {
              # if this collaborator is not the manager remove him
              if($c->member != $manager)
              {
                $c->delete();
              }
            }
            # else push this user to array for comparision
            else {
              array_push($assignedCollabs, $c->member);
            }
          }
          # loop throgh post
          foreach($members as $member)
          {
            # if current member is not already collaborating , insert into database
            if(!in_array($member,$assignedCollabs))
            {
              $tc = new TaskCollaborators;
                $tc->task   = $this->task->_;
                $tc->member = $member;
                $tc->status = 1;
              $tc->save();
            }
          }
          foreach($members as $member)
          {
            $receiver = Team::findFirst($member);
            $user_inf = Users::findFirst($receiver->uid);

            if($this->task_info_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "Task information updated! #{$this->task->unique}",
                "href"     => "{$this->rhyno_url}/task/overview/{$this->task->unique}"
              ]);

              if(!$inner){ array_push($exceptions['inner'],$receiver->name); }
            }

            if($this->task_info_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "Task information updated!",
                "text"     => "You are receiving this e-mail because a task that you are assigned to has updated it's information! #{$this->task->unique}",
                "href"     => "{$this->rhyno_url}/task/overview/{$this->task->unique}",
              ]);

              if(!$email){ array_push($exceptions['email'],$user_inf->email); }
            }
          }

          if(count($exceptions['inner']) > 0){ return RhynoException::NotificationError($exceptions['inner']); }
          if(count($exceptions['email']) > 0){ return RhynoException::EmailSendError($exceptions['email']); }
        }
        else
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project task successfully updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/tasks";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueT):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->task):
        return RhynoException::Unreachable();

      elseif($this->rhyno_user->permission <= $this->permissions->client):
        return RhynoException::PermissionDenied();

      elseif(!$this->task->project == $this->project->_):
        return RhynoException::CustomError("Woah, thats not valid! This task is not part of this project!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if(!$tc->delete()):
          return RhynoException::CustomError("Unable to remove collaborators from task.");
        elseif(!$tr->delete()):
          return RhynoException::CustomError("Unable to remove responses from task.");
        elseif(!$this->task->delete()):
          return RhynoException::CustomError("Unable to remove task.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project task successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/tasks";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    $project = (!$this->uniqueP) ? Projects::findFirst($this->task->project) : $this->project;

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form    = new Form;
      $inputs  = [];

      $members = Assignments::query()
      ->columns([
        "Manager\Models\Team._",
        "Manager\Models\Team.name"
      ])
      ->innerJoin('Manager\Models\Team', 'Manager\Models\Team._ = Manager\Models\Assignments.member')
      ->where("Manager\Models\Assignments.project = :project:")
      ->bind([
        "project" => $project->_
      ])
      ->orderBy("Manager\Models\Team.name ASC")
      ->execute();

      if($this->rmethod == "remove"):

      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Title",
        ]);
        $element['description'] = new Textarea( "description" ,[
          'class' => "materialize-textarea",
          'label' => "Description",
        ]);
        $element['deadline'] = new Text( "deadline" ,[
          'label' => "Deadline",
          'class' => "datepicker"
        ]);
        $element['manager'] = new Select( "manager" , $members ,[
          'using' => ['_','name'],
          'label' => "Manager",
        ]);
        $element['_blank'] = new Hidden( "_blank" ,[
          'label' => "Collaborators",
          'active' => true
        ]);
        foreach($members as $team)
        {
          $element[$team->_] = new Check( $team->_ ,[
            'name'  => "members[]",
            'id'    => explode(" ",$team->name)[0],
            'value' => $team->_,
            'label' => $team->name ,
            'cl'    => "m6 l4" # Classes
          ]);
        }
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/tasks/new";
        $modal_header = "Create a new Task!";
        $modal_card = [ "content" => "Notice that only assigned members to the project are able to be selected as the task collaborators. Also it's not necessary to select the task manager in the project collaborators section, It will be done automaticly ;)" ];

        ($this->configuration->team_notification->task_assigned_email) ? array_push($modal_card,["content" => 'E-Mail Notification is enabled , this might take a sec depending on your connection.']) : null ;

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/tasks/update/{$this->uniqueT}";
        $modal_header = "Update Task Information!";
        $modal_card   = [];

        ($this->configuration->team_notification->task_info_email) ? array_push($modal_card,["content" => 'E-Mail Notification is enabled , this might take a sec depending on your connection.']) : null ;

        $element['title']       ->setAttribute("active",true)->setAttribute("value",$this->task->title);
        $element['description'] ->setAttribute("active",true)->setAttribute("value",$this->task->description);
        $element['deadline']    ->setAttribute("active",true)->setAttribute("value",(new \DateTime($this->task->deadline))->format($this->rhyno_date));
        $element['manager']     ->setAttribute("active",false)->setAttribute("value", $this->task->assigned);

        foreach(TaskCollaborators::findByTask($this->task->_) as $tc)
        {
          $element[$tc->member]->setAttribute("checked",true);
        }

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/tasks/delete/{$this->uniqueT}";
        $modal_header = "Remove Project Task!";

        $modal_card = [
          "content"  => "Are you sure? The selected task will me permanently removed from the project."
        ];

      # IF REQUEST IS TO VIEW POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "view"):
        $modal_header = "Task Information!";
        switch($this->task->status)
        {
          case 1 : $status = "<i class='material-icons tooltipped pointer indigo-text text-darken-2' data-tooltip='Pending'>update</i> Pending"; break;
          case 2 : $status = "<i class='material-icons tooltipped pointer green-text text-darken-2' data-tooltip='Completed'>check</i> Completed"; break;
          case 3 : $status = "<i class='material-icons tooltipped pointer small red-text text-darken-2' data-tooltip='Overdue'>report</i> overdue"; break;
          case 4 : $status = "<i class='material-icons small tooltipped pointer red-text text-darken-2' data-tooltip='Canceled'>clear</i> Canceled"; break;
        }
        $modal_info = [
          [
            "titles" => [
              ["title" => "Title"],
              ["title" => "Created"],
              ["title" => "Deadline"],
              ["title" => "Status"],
            ],
            "contents"  => [
              ["content" => $this->task->title],
              ["content" => (new \DateTime($this->task->created))->format($this->rhyno_date)],
              ["content" => (new \DateTime($this->task->deadline))->format($this->rhyno_date)],
              ["content" => $status],
            ]
          ],
          [
            "titles" => [
              ["title" => "Description"],
            ],
            "contents"  => [
              ["content" => $this->task->description],
            ]
          ]
        ];

      endif;


      if($this->rmethod != "view")
      {
        foreach($element as $e)
        {
          $form->add($e);
        }
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
