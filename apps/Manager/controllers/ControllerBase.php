<?php

namespace Manager\Controllers;

use Mustache_Engine as Mustache,
    abeautifulsite\SimpleImage as SimpleImage;

use Manager\Models\Team            as Team,
    Manager\Models\Users           as Users,
    Manager\Models\Tasks           as Tasks,
    Manager\Models\TaskCollaborators           as TaskCollaborators,
    Manager\Models\Clients         as Clients,
    Manager\Models\Currencies      as Currencies,
    Manager\Models\Notifications   as Notifications,
    Manager\Models\Projects        as Projects,
    Manager\Models\ProjectInvoices as ProjectInvoices,
    Manager\Models\ProjectRecursions as ProjectRecursions,
    Manager\Models\InvoicePayments as InvoicePayments,
    Manager\Models\InvoiceItems    as InvoiceItems;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
  protected $rhyno_url;
  protected $rhyno_token;
  protected $rhyno_id;
  protected $rhyno_date;
  protected $rhyno_datetime;
  protected $rhyno_user;
  protected $rhyno_user_info;
  protected $flags = [
    "status"    =>  true,
    "toast"     =>  "default",
    "title"     =>  null,
    "redirect"  =>  false,
    "time"      =>  2600
  ];

  public function Initialize()
  {
    $this->assets
         ->addCss('assets/manager/addons/Materialize/dist/css/materialize.css')
         ->addCss('assets/manager/addons/font-awesome/css/font-awesome.css')
         ->addCss('assets/manager/addons/code-prettify/src/prettify.css')
         ->addCss('assets/manager/css/admin.css')
         ->addCss('assets/manager/css/custom.css')
         ->addCss('assets/manager/css/themes/light.css')
         ->addCss('assets/manager/css/themes/main/materialize-red.css')
         ->addCss('assets/manager/css/themes/alternative/red.css')
         ->addCss("assets/manager/addons/Materialize/extras/noUiSlider/nouislider.css");

    $this->assets
         ->addJs('assets/manager/addons/jquery/dist/jquery.js')
         ->addJs('assets/manager/addons/Materialize/dist/js/materialize.js')
         ->addJs('assets/manager/addons/code-prettify/src/prettify.js')
         ->addJs("assets/manager/addons/Materialize/extras/noUiSlider/nouislider.js")
         ->addJs("assets/manager/addons/amcharts3/amcharts/amcharts.js")
         ->addJs("assets/manager/addons/amcharts3/amcharts/themes/light.js")
         ->addJs("assets/manager/addons/amcharts3/amcharts/pie.js")
         ->addJs("assets/manager/js/Chart.js")
         ->addJs('assets/manager/js/main.js');

    #    SETS SESSION TOKENS GLOBALY ACCESSABLE
    $this->rhyno_url      = rtrim($this->url->getStatic(),'/');
    $this->rhyno_token    = $this->configuration->app->token;
    $this->rhyno_id       = $this->session->get($this->rhyno_token);
    $this->rhyno_date     = $this->configuration->app->date_format;
    $this->rhyno_datetime = $this->configuration->app->datetime_format;

    if($this->session->has($this->rhyno_token))
    {
      if($this->router->getControllerName() == 'login'):
        return $this->response->redirect("{$this->rhyno_url}/index",true);
      endif;

      $id   = $this->session->get($this->rhyno_token);
      $user = Users::findFirst($id);

      # SET User data
      $this->rhyno_user = $user;

      $info = ( $this->rhyno_user->permission <= $this->permissions->client ) ? Clients::findFirstByUid($this->rhyno_user->_) : Team::findFirstByUid($this->rhyno_user->_);

      # set team or client info
      $this->rhyno_user_info = $info;

      $cont = $this->dispatcher->getControllerName();
      # Define as TeamController
      $team = ["team","departments"];
      $proj = ["projects","projectcategories","projectfiles","projectrequests","projectinvoices","projectestimates","projectpayments"];
      $sett = ["settings","settingsadministrators","settingspermissions","settingsnotifications","settingspayments","settingsemail","settingsserver"];
      $fina = ["finance","financeexpenses"];

      if($user->permission >= $this->permissions->admin)
      {
        $nav = [
          ["active"=> ($cont === "index"),          "href" => "{$this->rhyno_url}/",         "label" => "Dashboard"],
          ["active"=> ($cont === "tasks"),          "href" => "{$this->rhyno_url}/tasks",    "label" => "My Tasks" ],
          ["active"=> ($cont === "tickets"),        "href" => "{$this->rhyno_url}/tickets",  "label" => "Tickets"  ],
          ["active"=> (in_array($cont,$team,true)), "href" => "{$this->rhyno_url}/team",     "label" => "Team"     ],
          ["active"=> ($cont === "clients"),        "href" => "{$this->rhyno_url}/clients",  "label" => "Clients"  ],
          ["active"=> (in_array($cont,$proj,true)), "href" => "{$this->rhyno_url}/projects", "label" => "Projects" ],
          ["active"=> (in_array($cont,$fina,true)), "href" => "{$this->rhyno_url}/finance",  "label" => "Finance"  ],
          ["active"=> (in_array($cont,$sett,true)), "href" => "{$this->rhyno_url}/settings", "label" => "Settings" ],
        ];
      }
      elseif($user->permission == $this->permissions->team)
      {
        $nav = [
          ["active"=> ($cont === "index"),          "href" => "{$this->rhyno_url}/",         "label" => "Dashboard"],
          ["active"=> ($cont === "tasks"),          "href" => "{$this->rhyno_url}/tasks",    "label" => "My Tasks" ],
        ];
        # Handle Page Permissions
        (!$this->configuration->team_crud->tickets_page) ?: array_push($nav,["active"=> ($cont === "tickets"), "href" => "{$this->rhyno_url}/tickets",  "label" => "Tickets"  ]);
        (!$this->configuration->team_crud->clients_page) ?: array_push($nav,["active"=> ($cont === "clients"), "href" => "{$this->rhyno_url}/clients",  "label" => "Clients"  ]);
        (!$this->configuration->team_crud->projects_page) ?: array_push($nav,["active"=> (in_array($cont,$proj,true)), "href" => "{$this->rhyno_url}/projects", "label" => "Projects" ]);
      }
      elseif($user->permission <= $this->permissions->client)
      {
        $nav = [
          ["active"=> ($cont === "index"),          "href" => "{$this->rhyno_url}/",         "label" => "Dashboard"  ],
          ["active"=> ($cont === "projects"),       "href" => "{$this->rhyno_url}/projects", "label" => "My Projects"],
          ["active"=> (in_array($cont,$fina,true)), "href" => "{$this->rhyno_url}/finance",  "label" => "Finance"  ],
        ];

        (!$this->configuration->client_crud->tickets_page) ?: array_push($nav,["active"=> ($cont === "tickets"), "href" => "{$this->rhyno_url}/tickets",  "label" => "Tickets"  ]);
      }

      $this->view->user = $user;
      $this->view->info = $info;

      $this->view->newNotifications = Notifications::find(["new = '1' AND receiver = '{$this->rhyno_user->_}'"]);
      $this->view->notifications    = $this->Notifications(4);

      $this->view->date_format = $this->configuration->app->date_format;
      $this->view->navigation  = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/navigation.tpl"),[ 'nav' => $nav ]);
    }
    else
    {
      if($this->router->getControllerName() != 'login'):
        return $this->response->redirect("{$this->rhyno_url}/login",true);
      endif;
    }

    $this->view->logo      = $this->configuration->app->logo;
    $this->view->pagetitle = $this->configuration->app->title;
    $this->view->staticUrl = $this->rhyno_url;
  }

  protected function Notifications($limit,$offset=0)
  {
    $notifications = Notifications::query()
    ->columns([
      "COALESCE(Manager\Models\Team.name,Manager\Models\Clients.name) as modifier",
      "COALESCE(Manager\Models\Team.image,Manager\Models\Clients.image) as modifierimage",
      "Manager\Models\Notifications.text",
      "Manager\Models\Notifications.date",
      "Manager\Models\Notifications.href",
      "Manager\Models\Notifications.new",
    ])
    ->leftJoin("Manager\Models\Team","Manager\Models\Team.uid = Manager\Models\Notifications.modifier")
    ->leftJoin("Manager\Models\Clients","Manager\Models\Clients.uid = Manager\Models\Notifications.modifier")
    ->where("Manager\Models\Notifications.receiver = :user:")
    ->bind([
      "user" => $this->rhyno_user->_
    ])
    ->order("Manager\Models\Notifications.date DESC")
    ->limit($limit,$offset)
    ->execute();

    return $notifications;
  }

  protected function NewNotification(Array $data)
  {
    $n = new Notifications;
      $n->modifier = $this->rhyno_user->_;
      $n->receiver = $data["receiver"];
      $n->text     = $data["text"];
      $n->href     = $data["href"];
      $n->new      = 1;
      $n->date     = (new \DateTime("now"))->format("Y-m-d H:i:s");
    if($n->save())
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  protected function NewEmailNotification(Array $data)
  {
    $response = false;
    $r = $data["receiver"];
    $t = $data["title"];
    $x = $data["text"];
    $h = $data["href"];
    $u = Users::findFirst($r);

    if($u)
    {
      $this->mail->functions->From       = $this->configuration->mail->email;
      $this->mail->functions->FromName   = $this->configuration->mail->name;

      $this->mail->functions->addAddress($u->email);
      $this->mail->functions->Subject = $t;

      $this->mail->functions->Body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/mail.tpl"), [
        'NOTIFICATION' => true,
        'SENDER'  => $this->configuration->app->title,
        'LOGO'    => 'data: '.mime_content_type("assets/manager/images/{$this->configuration->app->logo}").';base64,'.base64_encode(file_get_contents("assets/manager/images/{$this->configuration->app->logo}")),
        'TITLE'   => $t,
        'TEXT'    => $x,
        'LINK'    => $this->request->getHttpHost().$h,
        'DATE'    => (new \DateTime())->format($this->configuration->app->date_format)
      ]);

      $response = ($this->mail->functions->send() ? true : false);
      $this->mail->functions->ClearAddresses();
    }

    return $response;
  }

  protected function isEmail($str)
  {
    return preg_match("/^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i", $str );
  }

  protected function uniqueCode($prefix = null, $limit = 10)
  {
    $limit = ($prefix ? $limit - strlen($prefix) : $limit);

    return $prefix.str_shuffle(substr(md5(round(time().uniqid())), 0, $limit));
  }

  protected function permissionHandler($permission)
  {
    switch ($permission)
    {
      case 'client': ($this->rhyno_user->permission >= $this->permissions->client) ? true : $this->response->redirect("error/forbidden",true); break;
      case 'team':   ($this->rhyno_user->permission >= $this->permissions->team)   ? true : $this->response->redirect("error/forbidden",true); break;
      case 'admin':  ($this->rhyno_user->permission >= $this->permissions->admin)  ? true : $this->response->redirect("error/forbidden",true); break;
    }
  }

  protected function CurrencyConverter($value, $currency)
  {
    settype($value, "float");
    $c = $this->CurrencyRateCheck();

    if(!array_key_exists($currency,$c->rates))
    {
      return (object) [
        "values"  => (object)[
          "original"   => $value,
          "converted"  => $value,
        ],
        "codes"  => (object)[
          "original"   => $this->configuration->app->currency,
          "converted"  => $this->configuration->app->currency,
        ]
      ];
    }
    else
    {
      return (object) [
        "values"  => (object)[
          "original"   => $value,
          "converted"  => ($c->rates[$currency] * $value),
        ],
        "codes"  => (object)[
          "original"   => $this->configuration->app->currency,
          "converted"  => $currency,
        ]
      ];
    }
  }

  protected function CurrencyRateCheck()
  {
    $json      = (object) json_decode(file_get_contents(__DIR__."/../../../config/rates.json"),true);
    $today     = new \DateTime("now");
    $json_date = new \DateTime($json->date);

    if(!$json->base ||$json_date < $today)
    {
      $api = "http://api.fixer.io/latest?base={$this->configuration->app->currency}";

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_URL,$api);
      $result = curl_exec($ch);
      curl_close($ch);

      file_put_contents(__DIR__."/../../../config/rates.json", $result);
      $json = (object) json_decode(file_get_contents(__DIR__."/../../../config/rates.json"),true);
    }

    return $json;
  }

  protected function UpdateConfigFile(array $line)
  {
    $template = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/config.tpl"), [
      'db_host'       => $line['db_host'] ?: $this->configuration->database->host,
      'db_user'       => $line['db_user'] ?: $this->configuration->database->username,
      'db_pass'       => $line['db_pass'] ?: $this->configuration->database->password,
      'db_name'       => $line['db_name'] ?: $this->configuration->database->dbname,

      'mail_host'     => $line['mail_host'] ?: $this->configuration->mail->host,
      'mail_user'     => $line['mail_user'] ?: $this->configuration->mail->username,
      'mail_pass'     => $line['mail_pass'] ?: $this->configuration->mail->password,
      'mail_secu'     => $line['mail_secu'] ?: $this->configuration->mail->security,
      'mail_port'     => $line['mail_port'] ?: $this->configuration->mail->port,
      'mail_mail'     => $line['mail_mail'] ?: $this->configuration->mail->email,
      'mail_name'     => $line['mail_name'] ?: $this->configuration->mail->name,

      'app_base'      => $line['app_base']      ?: $this->configuration->app->baseUri,
      'app_logo'      => $line['app_logo']      ?: $this->configuration->app->logo,
      'app_title'     => $line['app_title']     ?: $this->configuration->app->title,
      'app_token'     => $line['app_token']     ?: $this->configuration->app->token,
      'app_currency'  => $line['app_currency']  ?: $this->configuration->app->currency,
      'app_date'      => $line['app_date']      ?: $this->configuration->app->date_format,
      'app_datetime'  => $line['app_datetime']  ?: $this->configuration->app->datetime_format,

      'paypal'          => $line['paypal']          ?: ($this->configuration->paypal->active ? 'true' : 'false'),
      'paypal_mode'     => $line['paypal_mode']     ?: $this->configuration->paypal->mode,
      'paypal_clientID' => $line['paypal_clientID'] ?: $this->configuration->paypal->clientID,
      'paypal_secret'   => $line['paypal_secret']   ?: $this->configuration->paypal->secret,

      'stripe'              => $line['stripe']              ?: ($this->configuration->stripe->active ? 'true' : 'false'),
      'stripe_secret'       => $line['stripe_secret']       ?: $this->configuration->stripe->secret,
      'stripe_publishable'  => $line['stripe_publishable']  ?: $this->configuration->stripe->publishable,

      'taxes'   => $line['taxes']    ?: ($this->configuration->taxes->active ? 'true' : 'false'),
      'tax'     => $line['tax']      ?: $this->configuration->taxes->tax,
      'taxtype' => $line['taxtype']  ?: $this->configuration->taxes->type,
      'taxtime' => $line['taxtime']  ?: $this->configuration->taxes->taxtime,

      'members_page'            => $line['members_page']            ?: ($this->configuration->team_crud->members_page ? 'true' : 'false'),
      'members_create'          => $line['members_create']          ?: ($this->configuration->team_crud->members_create ? 'true' : 'false'),
      'members_update'          => $line['members_update']          ?: ($this->configuration->team_crud->members_update ? 'true' : 'false'),
      'members_delete'          => $line['members_delete']          ?: ($this->configuration->team_crud->members_delete ? 'true' : 'false'),
      'tickets_page'            => $line['tickets_page']            ?: ($this->configuration->team_crud->tickets_page ? 'true' : 'false'),
      'tickets_update'          => $line['tickets_update']          ?: ($this->configuration->team_crud->tickets_update ? 'true' : 'false'),
      'tickets_delete'          => $line['tickets_delete']          ?: ($this->configuration->team_crud->tickets_delete ? 'true' : 'false'),
      'clients_page'            => $line['clients_page']            ?: ($this->configuration->team_crud->clients_page ? 'true' : 'false'),
      'clients_create'          => $line['clients_create']          ?: ($this->configuration->team_crud->clients_create ? 'true' : 'false'),
      'clients_update'          => $line['clients_update']          ?: ($this->configuration->team_crud->clients_update ? 'true' : 'false'),
      'clients_delete'          => $line['clients_delete']          ?: ($this->configuration->team_crud->clients_delete ? 'true' : 'false'),
      'projects_page'           => $line['projects_page']           ?: ($this->configuration->team_crud->projects_page ? 'true' : 'false'),
      'projects_create'         => $line['projects_create']         ?: ($this->configuration->team_crud->projects_create ? 'true' : 'false'),
      'projects_update'         => $line['projects_update']         ?: ($this->configuration->team_crud->projects_update ? 'true' : 'false'),
      'projects_delete'         => $line['projects_delete']         ?: ($this->configuration->team_crud->projects_delete ? 'true' : 'false'),
      'projecttasks_page'       => $line['projecttasks_page']       ?: ($this->configuration->team_crud->projecttasks_page ? 'true' : 'false'),
      'projecttasks_create'     => $line['projecttasks_create']     ?: ($this->configuration->team_crud->projecttasks_create ? 'true' : 'false'),
      'projecttasks_update'     => $line['projecttasks_update']     ?: ($this->configuration->team_crud->projecttasks_update ? 'true' : 'false'),
      'projecttasks_delete'     => $line['projecttasks_delete']     ?: ($this->configuration->team_crud->projecttasks_delete ? 'true' : 'false'),
      'projectfiles_page'       => $line['projectfiles_page']       ?: ($this->configuration->team_crud->projectfiles_page ? 'true' : 'false'),
      'projectfiles_create'     => $line['projectfiles_create']     ?: ($this->configuration->team_crud->projectfiles_create ? 'true' : 'false'),
      'projectfiles_delete'     => $line['projectfiles_delete']     ?: ($this->configuration->team_crud->projectfiles_delete ? 'true' : 'false'),
      'projectrequests_page'    => $line['projectrequests_page']    ?: ($this->configuration->team_crud->projectrequests_page ? 'true' : 'false'),
      'projectrequests_delete'  => $line['projectrequests_delete']  ?: ($this->configuration->team_crud->projectrequests_delete ? 'true' : 'false'),
      'projectestimates_page'   => $line['projectestimates_page']   ?: ($this->configuration->team_crud->projectestimates_page ? 'true' : 'false'),
      'projectestimates_send'   => $line['projectestimates_send']   ?: ($this->configuration->team_crud->projectestimates_send ? 'true' : 'false'),
      'projectestimates_create' => $line['projectestimates_create'] ?: ($this->configuration->team_crud->projectestimates_create ? 'true' : 'false'),
      'projectestimates_update' => $line['projectestimates_update'] ?: ($this->configuration->team_crud->projectestimates_update ? 'true' : 'false'),
      'projectestimates_delete' => $line['projectestimates_delete'] ?: ($this->configuration->team_crud->projectestimates_delete ? 'true' : 'false'),
      'projectinvoices_page'    => $line['projectinvoices_page']    ?: ($this->configuration->team_crud->projectinvoices_page ? 'true' : 'false'),
      'projectinvoices_create'  => $line['projectinvoices_create']  ?: ($this->configuration->team_crud->projectinvoices_create ? 'true' : 'false'),
      'projectinvoices_update'  => $line['projectinvoices_update']  ?: ($this->configuration->team_crud->projectinvoices_update ? 'true' : 'false'),
      'projectinvoices_delete'  => $line['projectinvoices_delete']  ?: ($this->configuration->team_crud->projectinvoices_delete ? 'true' : 'false'),
      'projectpayments_page'    => $line['projectpayments_page']    ?: ($this->configuration->team_crud->projectpayments_page ? 'true' : 'false'),
      'projectpayments_create'  => $line['projectpayments_create']  ?: ($this->configuration->team_crud->projectpayments_create ? 'true' : 'false'),
      'projectpayments_delete'  => $line['projectpayments_delete']  ?: ($this->configuration->team_crud->projectpayments_delete ? 'true' : 'false'),

      'clienttickets_page'            => $line['clienttickets_page']           ?: ($this->configuration->client_crud->tickets_page ? 'true' : 'false'),
      'clienttickets_create'          => $line['clienttickets_create']         ?: ($this->configuration->client_crud->tickets_create ? 'true' : 'false'),
      'clienttickets_reopen'          => $line['clienttickets_reopen']         ?: ($this->configuration->client_crud->tickets_reopen ? 'true' : 'false'),
      'clienttickets_delete'          => $line['clienttickets_delete']         ?: ($this->configuration->client_crud->tickets_delete ? 'true' : 'false'),
      'clientprojectfiles_page'       => $line['clientprojectfiles_page']      ?: ($this->configuration->client_crud->projectfiles_page ? 'true' : 'false'),
      'clientprojectfiles_create'     => $line['clientprojectfiles_create']    ?: ($this->configuration->client_crud->projectfiles_create ? 'true' : 'false'),
      'clientprojectfiles_delete'     => $line['clientprojectfiles_delete']    ?: ($this->configuration->client_crud->projectfiles_delete ? 'true' : 'false'),
      'clientprojectrequests_page'    => $line['clientprojectrequests_page']   ?: ($this->configuration->client_crud->projectrequests_page ? 'true' : 'false'),
      'clientprojectrequests_create'  => $line['clientprojectrequests_create'] ?: ($this->configuration->client_crud->projectrequests_create ? 'true' : 'false'),
      'clientprojectrequests_delete'  => $line['clientprojectrequests_delete'] ?: ($this->configuration->client_crud->projectrequests_delete ? 'true' : 'false'),

      'member_info_email'         =>  $line['member_info_email']        ?: ($this->configuration->team_notification->member_info_email ? 'true' : 'false'),
      'member_info_inner'         =>  $line['member_info_inner']        ?: ($this->configuration->team_notification->member_info_inner ? 'true' : 'false'),
      'member_permission_email'   =>  $line['member_permission_email']  ?: ($this->configuration->team_notification->member_permission_email ? 'true' : 'false'),
      'member_permission_inner'   =>  $line['member_permission_inner']  ?: ($this->configuration->team_notification->member_permission_inner ? 'true' : 'false'),
      'project_assigned_email'    =>  $line['project_assigned_email']   ?: ($this->configuration->team_notification->project_assigned_email ? 'true' : 'false'),
      'project_assigned_inner'    =>  $line['project_assigned_inner']   ?: ($this->configuration->team_notification->project_assigned_inner ? 'true' : 'false'),
      'project_activities_email'  =>  $line['project_activities_email'] ?: ($this->configuration->team_notification->project_activities_email ? 'true' : 'false'),
      'project_activities_inner'  =>  $line['project_activities_inner'] ?: ($this->configuration->team_notification->project_activities_inner ? 'true' : 'false'),
      'task_assigned_email'       =>  $line['task_assigned_email']      ?: ($this->configuration->team_notification->task_assigned_email ? 'true' : 'false'),
      'task_assigned_inner'       =>  $line['task_assigned_inner']      ?: ($this->configuration->team_notification->task_assigned_inner ? 'true' : 'false'),
      'task_response_email'       =>  $line['task_response_email']      ?: ($this->configuration->team_notification->task_response_email ? 'true' : 'false'),
      'task_response_inner'       =>  $line['task_response_inner']      ?: ($this->configuration->team_notification->task_response_inner ? 'true' : 'false'),
      'task_memberstatus_email'   =>  $line['task_memberstatus_email']  ?: ($this->configuration->team_notification->task_memberstatus_email ? 'true' : 'false'),
      'task_memberstatus_inner'   =>  $line['task_memberstatus_inner']  ?: ($this->configuration->team_notification->task_memberstatus_inner ? 'true' : 'false'),
      'task_status_email'         =>  $line['task_status_email']        ?: ($this->configuration->team_notification->task_status_email ? 'true' : 'false'),
      'task_status_inner'         =>  $line['task_status_inner']        ?: ($this->configuration->team_notification->task_status_inner ? 'true' : 'false'),
      'task_info_email'           =>  $line['task_info_email']          ?: ($this->configuration->team_notification->task_info_email ? 'true' : 'false'),
      'task_info_inner'           =>  $line['task_info_inner']          ?: ($this->configuration->team_notification->task_info_inner ? 'true' : 'false'),
      'ticket_created_email'      =>  $line['ticket_created_email']     ?: ($this->configuration->team_notification->ticket_created_email ? 'true' : 'false'),
      'ticket_created_inner'      =>  $line['ticket_created_inner']     ?: ($this->configuration->team_notification->ticket_created_inner ? 'true' : 'false'),
      'ticket_response_email'     =>  $line['ticket_response_email']    ?: ($this->configuration->team_notification->ticket_response_email ? 'true' : 'false'),
      'ticket_response_inner'     =>  $line['ticket_response_inner']    ?: ($this->configuration->team_notification->ticket_response_inner ? 'true' : 'false'),
      'ticket_status_email'       =>  $line['ticket_status_email']      ?: ($this->configuration->team_notification->ticket_status_email ? 'true' : 'false'),
      'ticket_status_inner'       =>  $line['ticket_status_inner']      ?: ($this->configuration->team_notification->ticket_status_inner ? 'true' : 'false'),

      'client_info_email'               => $line['client_info_email']               ?: ($this->configuration->client_notification->client_info_email ? 'true' : 'false'),
      'client_info_inner'               => $line['client_info_inner']               ?: ($this->configuration->client_notification->client_info_inner ? 'true' : 'false'),
      'clientproject_create_email'      => $line['clientproject_create_email']      ?: ($this->configuration->client_notification->project_create_email ? 'true' : 'false'),
      'clientproject_create_inner'      => $line['clientproject_create_inner']      ?: ($this->configuration->client_notification->project_create_inner ? 'true' : 'false'),
      'clientproject_activities_email'  => $line['clientproject_activities_email']  ?: ($this->configuration->client_notification->project_activities_email ? 'true' : 'false'),
      'clientproject_activities_inner'  => $line['clientproject_activities_inner']  ?: ($this->configuration->client_notification->project_activities_inner ? 'true' : 'false'),
      'clientproject_status_email'      => $line['clientproject_status_email']      ?: ($this->configuration->client_notification->project_status_email ? 'true' : 'false'),
      'clientproject_status_inner'      => $line['clientproject_status_inner']      ?: ($this->configuration->client_notification->project_status_inner ? 'true' : 'false'),
      'projectinvoice_status_email'     => $line['projectinvoice_status_email']     ?: ($this->configuration->client_notification->projectinvoice_status_email ? 'true' : 'false'),
      'projectinvoice_status_inner'     => $line['projectinvoice_status_inner']     ?: ($this->configuration->client_notification->projectinvoice_status_inner ? 'true' : 'false'),
      'clientticket_response_email'     => $line['clientticket_response_email']     ?: ($this->configuration->client_notification->ticket_response_email ? 'true' : 'false'),
      'clientticket_response_inner'     => $line['clientticket_response_inner']     ?: ($this->configuration->client_notification->ticket_response_inner ? 'true' : 'false'),
      'clientticket_status_email'       => $line['clientticket_status_email']       ?: ($this->configuration->client_notification->ticket_status_email ? 'true' : 'false'),
      'clientticket_status_inner'       => $line['clientticket_status_inner']       ?: ($this->configuration->client_notification->ticket_status_inner ? 'true' : 'false'),

      'debug'     => $line['debug'] ?: ($this->configuration->debug->active ? 'true' : 'false'),
      'installer' => $line['installer'] ?: ($this->configuration->debug->installer ? 'true' : 'false'),
    ]);

    if(file_put_contents(__DIR__."/../../../config/config.ini",$template))
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  protected function RecurringInvoices($project)
  {

    $recursions = ProjectRecursions::findByProject($project->_);
    $today = new \DateTime("NOW");

    foreach($recursions as $r)
    {
      $invoice = ProjectInvoices::findFirst($r->invoice);
      $items   = InvoiceItems::findByInvoice($r->invoice);

      # duplicate invoice
      $copy    = new ProjectInvoices;
        $copy->unique         = $this->uniqueCode("REC");
        $copy->project        = $invoice->project;
        $copy->status         = 1;
        $copy->title          = "Recursion from #{$invoice->unique} ";
        $copy->issue          = $today->format("Y-m-d H:i:s");
        $copy->due            = (new \DateTime("+1 month"))->format("Y-m-d H:i:s");
        $copy->recurring      = 0;
        $copy->last_recursion = $today->format("Y-m-d H:i:s");
      $copy->save();

      # duplicate invoice items
      foreach($items as $item)
      {
        $newI = new InvoiceItems;
          $newI->unique     = $this->uniqueCode("ITM");
          $newI->invoice    = $copy->_;
          $newI->title      = $item->title;
          $newI->text       = $item->text;
          $newI->amount     = $item->amount;
          $newI->price      = $item->price;
          $newI->issued     = $today->format("Y-m-d H:i:s");
        $newI->save();
      }

      # update original invoice last recursion date
      $invoice->last_recursion = $today->format("Y-m-d H:i:s");
      $invoice->save();

      # remove recursion history
      $r->delete();
    }

  }

  protected function VerifyInvoices($project)
  {
    $status  = 1;
    // 1 Pending
    // 2 Partially Paid
    // 3 Paid
    // 4 Overdue
    // 5 Canceled
    #  SELECT ALL INVOICES AND IGNORE Canceled AND Paid

    $client = Clients::findFirst($project->client);
    $invoices = ProjectInvoices::query()
    ->columns([
      'Manager\Models\ProjectInvoices._',
      'Manager\Models\ProjectInvoices.status',
      'Manager\Models\ProjectInvoices.issue',
      'Manager\Models\ProjectInvoices.due',
      "(SELECT SUM(Manager\Models\InvoiceItems.price * Manager\Models\InvoiceItems.amount) FROM Manager\Models\InvoiceItems WHERE Manager\Models\InvoiceItems.invoice = Manager\Models\ProjectInvoices._) as total",
      "(SELECT SUM(Manager\Models\InvoicePayments.amount) FROM Manager\Models\InvoicePayments where Manager\Models\InvoicePayments.invoice = Manager\Models\ProjectInvoices._) as paid",
    ])
    ->where("project = :project: AND status != :canceled:")
    ->bind([
      "project"  => $project->_,
      "canceled" => 5,
    ])
    ->execute();

    $today = new \DateTime("now");

    if($invoices):
      foreach($invoices as $i)
      {
        $due = new \DateTime($i->due);
        ($i->paid == null) ? $status = 1 : $status = $status;
        ($i->paid != null && $i->paid < $i->total) ? $status = 2 : $status = $status;
        ($i->paid != null && $i->paid >= $i->total) ? $status = 3 : $status = $status;
        ($i->paid == null && $today >= $due || $i->paid != null && $i->paid < $i->total && $today >= $due) ? $status = 4 : $status = $status;

        $inv = ProjectInvoices::findFirst($i->_);
        $inv->status = $status;
        $inv->save();

        switch ($status)
        {
          case 1: $st = "Pending"; break;
          case 2: $st = "Partially Paid"; break;
          case 3: $st = "Paid"; break;
          case 4: $st = "Overdue"; break;
        }

        ($this->configuration->client_notification->projectinvoice_status_inner) ? $this->NewNotification([
          "receiver" => $client->uid,
          "text"     => "The Invoice #{$this->unique} status has been updated to {$st}!",
          "href"     => "{$this->rhyno_url}/project/{$project->unique}/invoices/overview/{$i->unique}"
        ]) : null ;

        ($this->configuration->client_notification->projectinvoice_status_email) ? $this->NewEmailNotification([
          "receiver" => $client->uid,
          "title"    => "The Invoice #{$this->unique} status has been updated!",
          "text"     => "You are receiving this notification because the invoice #{$i->unique} status has been updated to {$st}!",
          "href"     => "{$this->rhyno_url}/project/{$project->unique}/invoices/overview/{$i->unique}",
        ]) : null ;
      }
    endif;

  }

  protected function calcTax($value,$duedate,$paydate = false)
  {
    $final_tax = 0;
    if($this->configuration->taxes->active)
    {
      $today = new \DateTime("now");
      $due   = new \DateTime($duedate);
      $pay   = new \DateTime($paydate);
      $diff  = ($paydate) ? $pay->diff($due) : $today->diff($due);

      switch ($this->configuration->taxes->taxtime)
      {
        case 'daily':
          $mult = $diff->days;
        break;
        case 'monthly':
          $mult = round($diff->days / 30);
        break;
        case 'annually':
          $mult = round($diff->days / 365);
        break;
      }

      if($this->configuration->taxes->type == "percent"):
        $tax = (($this->configuration->taxes->tax * $value) / 100);
        $tax = (($tax * $mult) /100);
      elseif($this->configuration->taxes->type == "exact"):
        $tax = (($this->configuration->taxes->taxtax * $mult) / 100);
      endif;

      $final_tax= ($diff->format('%R') == "-") ? $tax : 0;
    }

    return round($final_tax);
  }

  protected function updateTasks($project = false)
  {
    $tasks = ($project) ? Tasks::findByProject($project) : Tasks::find();
    $today = new \DateTime("now");
    $ar = [];
    foreach($tasks as $t)
    {
      $deadline = new \DateTime($t->deadline);
      #  if not completed or canceled
      if($today > $deadline && !in_array($t->status,[2,4]))
      {
        $t->status = 3;
      }
      if($today < $deadline && $t->status == 3)
      {
         $t->status = 1;
      }
      $t->save();

    }
  }

  protected function clearNewNotifications()
  {
    foreach(Notifications::find(["new = '1' AND receiver = '{$this->rhyno_user->_}' "]) as $n)
    {
      $n->new = 0;
      $n->save();
    }
  }

  protected function ResizeImage($src,$dest=false,$params=["w"=>128,"h"=>128])
  {
    $image = new SimpleImage($src);
    $image->adaptive_resize($params['w'], $params['h']);
    (!$dest) ? $image->save() : $image->save($dest);
  }
}
