<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Users            as Users,
    Manager\Models\Clients          as Clients,
    Manager\Models\Currencies       as Currencies,
    Manager\Models\Projects         as Projects,
    Manager\Models\Tasks            as Tasks,
    Manager\Models\Tickets          as Tickets,
    Manager\Models\TicketResponses  as TicketResponses;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Hidden;

class ClientsController extends ControllerBase
{
  private $client_info_email;
  private $client_info_inner;

  public function onConstruct()
  {
    $this->client_info_email = $this->configuration->client_notification->client_info_email;
    $this->client_info_inner = $this->configuration->client_notification->client_info_inner;
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("clients/admin/index");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->clients_page)
        {
          $this->view->pick("clients/team/index");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        $this->permissionHandler("team");
      break;
    }

    $clients = Clients::query()
    ->columns([
      'Manager\Models\Clients._',
      'Manager\Models\Clients.name',
      'Manager\Models\Clients.phone',
      'Manager\Models\Clients.domain',
      'Manager\Models\Clients.image',
      'Manager\Models\Clients.company',
      'Manager\Models\Users.email',
      'Manager\Models\Currencies.code as currency',
    ])
    ->innerJoin('Manager\Models\Users', 'Manager\Models\Users._ = Manager\Models\Clients.uid')
    ->innerJoin('Manager\Models\Currencies', 'Manager\Models\Currencies._ = Manager\Models\Clients.currency')
    ->execute();

    $this->view->clients = $clients;
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    $username = preg_replace('/\s+/', '', $this->request->getPost("username","string"));
    $password = preg_replace('/\s+/', '', $this->request->getPost("password","string"));
    $email    = $this->request->getPost("email","email");
    # catch any form errors
    try
    {
      $check_email    = Users::findFirstByEmail($email);
      $check_username = Users::findFirstByUsername($username);

      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("name")):
        return RhynoException::EmptyInput("Name");

      elseif(!$this->request->getPost("email")):
        return RhynoException::EmptyInput("e-mail");

      elseif(!$username || !$password):
        return RhynoException::EmptyInput(["Username","Password"]);

      elseif(!$this->isEmail($email)):
        return RhynoException::InvalidEmailAddress();

      elseif($check_email):
        return RhynoException::RegisteredEmailAddress();

      elseif($check_username):
        return RhynoException::RegisteredUsername();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      # try to save
      try
      {
        if($this->request->hasFiles()):
          foreach($this->request->getUploadedFiles() as $file):
            if($file->getError() != 0):
              $this->flags['redirect']   = "{$this->rhyno_url}/clients";
              return RhynoException::UploadError($file->getError());
            endif;
            $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
            $file->moveTo("assets/manager/images/avatar/{$filename}");
            $this->ResizeImage("assets/manager/images/avatar/{$filename}");
          endforeach;
        else:
          $filename = null;
        endif;

        $user = new Users;
          $user->username   = $username;
          $user->password   = password_hash($password, PASSWORD_BCRYPT );
          $user->email      = $email;
          $user->permission = $this->permissions->client;
        if(!$user->save()){ return RhynoException::DBError(); }

        $client = new Clients;
          $client->uid      = $user->_;
          $client->name     = $this->request->getPost("name","string");
          $client->phone    = $this->request->getPost("phone","string");
          $client->domain   = $this->request->getPost("domain","string");
          $client->address  = $this->request->getPost("address","string");
          $client->zip      = $this->request->getPost("zip","string");
          $client->country  = $this->request->getPost("country","string");
          $client->city     = $this->request->getPost("city","string");
          $client->state    = $this->request->getPost("state","string");
          $client->image    = $filename;
          $client->vat      = $this->request->getPost("vat","string");
          $client->currency = $this->request->getPost("currency","int");
          $client->company  = $this->request->getPost("company","string");
          $client->role     = $this->request->getPost("role","string");
        if(!$client->save()){ return RhynoException::DBError(); }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Client successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/clients";

      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    $c = Clients::findFirst($this->dispatcher->getParam("client","int"));
    $u = Users::findFirst($c->uid);

    $username = preg_replace('/\s+/', '', $this->request->getPost("username","string"));
    $password = preg_replace('/\s+/', '', $this->request->getPost("password","string"));
    $email    = $this->request->getPost("email","email");
    # catch any form errors
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->request->getPost("name")):
        return RhynoException::EmptyInput("Name");

      elseif(!$this->request->getPost("email")):
        return RhynoException::EmptyInput("e-mail");

      elseif(!$username):
        return RhynoException::EmptyInput("Username");

      elseif(!$this->isEmail($email)):
        return RhynoException::InvalidEmailAddress();

      elseif($email != $u->email && Users::findFirstByEmail($email)->_ != NULL):
        return RhynoException::RegisteredEmailAddress();

      elseif($username != $u->username && Users::findFirstByUsername($username)->_ != NULL):
        return RhynoException::RegisteredUsername();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    # if form is ok
    if($this->flags['status']):

      # try to save
      try
      {
        if($this->request->hasFiles()):
          foreach($this->request->getUploadedFiles() as $file):
            if($file->getError() != 0):
              $this->flags['redirect']  = "{$this->rhyno_url}/clients";
              return RhynoException::UploadError($file->getError());
            endif;
            $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
            $file->moveTo("assets/manager/images/avatar/{$filename}");
            $this->ResizeImage("assets/manager/images/avatar/{$filename}");
          endforeach;
          @unlink("assets/manager/images/avatar/{$c->image}");
        else:
          $filename = $c->image;
        endif;

        $u->username   = $username;
        $u->password   = ($password != null ) ? password_hash($password, PASSWORD_BCRYPT ) : $u->password;
        $u->email      = $email;

        $c->name     = $this->request->getPost("name","string");
        $c->phone    = $this->request->getPost("phone","string");
        $c->domain   = $this->request->getPost("domain","string");
        $c->address  = $this->request->getPost("address","string");
        $c->zip      = $this->request->getPost("zip","string");
        $c->country  = $this->request->getPost("country","string");
        $c->city     = $this->request->getPost("city","string");
        $c->state    = $this->request->getPost("state","string");
        $c->image    = $filename;
        $c->vat      = $this->request->getPost("vat","string");
        $c->currency = $this->request->getPost("currency","int");
        $c->company  = $this->request->getPost("company","string");
        $c->role     = $this->request->getPost("role","string");

        # error if can't save
        if(!$u->save()){ return RhynoException::DBError(); }
        if(!$c->save()){ return RhynoException::DBError(); }

        # system notification
        if($this->client_info_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $u->_,
            "text"     => "Your account information was updated!",
            "href"     => "{$this->rhyno_url}/account"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->client_info_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $u->_,
            "title"    => "Your account information was updated!",
            "text"     => "You are receiving this e-mail because your account information was updated by a admin!",
            "href"     => "{$this->rhyno_url}/account",
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']     = "success";
        $this->flags['title']     = "Client information successfully updated!";
        $this->flags['redirect']  = "{$this->rhyno_url}/clients";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);


    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);

  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    $client = $this->dispatcher->getParam("client","int");
    # catch any erros
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$client):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $c = Clients::findFirst($client);
        $u = Users::findFirst($c->uid);

        if(!$c || !$u) { return RhynoException::Unreachable(); }

        # remove image from server
        @unlink("assets/manager/images/avatar/{$c->image}");

        # update projects assignments table
        foreach (Projects::findByClient($client) as $project)
        {
          $project->delete();
          foreach (Tasks::findByProject($project->project) as $task)
          {
            $task->delete();
          }
        }
        foreach(Tickets::findByUid($client) as $ticket)
        {
          $ticket->delete();
          foreach (TicketResponses::findByTicket($ticket->_) as $response)
          {
            $response->delete();
          }
        }

        $c->delete();
        $u->delete();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Client deleted!";
        $this->flags['redirect']   = "{$this->rhyno_url}/clients";
      }
      catch (\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form = new Form();
      $inputs = [];
      $uid = $this->dispatcher->getParam("client","int");
      $method = $this->dispatcher->getParam("method","string");

      if($uid)
      {
        $client = Clients::query()
        ->columns([
          'Manager\Models\Clients.name',
          'Manager\Models\Clients.phone',
          'Manager\Models\Clients.domain',
          'Manager\Models\Clients.address',
          'Manager\Models\Clients.country',
          'Manager\Models\Clients.state',
          'Manager\Models\Clients.state',
          'Manager\Models\Clients.city',
          'Manager\Models\Clients.zip',
          'Manager\Models\Clients.vat',
          'Manager\Models\Clients.currency',
          'Manager\Models\Clients.company',
          'Manager\Models\Clients.role',
          'Manager\Models\Currencies.code as code',
          'Manager\Models\Users.username',
          'Manager\Models\Users.email',
        ])
        ->innerJoin('Manager\Models\Users', 'Manager\Models\Users._ = Manager\Models\Clients.uid')
        ->innerJoin('Manager\Models\Currencies', 'Manager\Models\Currencies._ = Manager\Models\Clients.currency')
        ->where("Manager\Models\Clients._ = :client:")
        ->bind([
          "client"  => $uid
        ])
        ->execute();
      }

      if($method == "remove"):

      else:

      # CREATING ELEMENTS
      $element['name'] = new Text( "name" ,[
        'class' => "validate",
        'label' => "Full Name",
        'cl'    => "m6 l4"
      ]);

      $element['email'] = new Text( "email" ,[
        'class' => "validate",
        'label' => "E-Mail",
        'cl'    => "m6 l4"
      ]);

      $element['phone'] = new Text( "phone" ,[
        'class' => "validate",
        'label' => "Phone Number",
        'cl'    => "m6 l4"
      ]);

      $element['domain'] = new Text( "domain" ,[
        'class' => "validate",
        'label' => "Domain",
        'cl'    => "m6 l4"
      ]);

      $element['address'] = new Text( "address" ,[
        'class' => "validate",
        'label' => "Address",
        'cl'    => "m6 l4"
      ]);

      $element['country'] = new Text( "country" ,[
        'class' => "validate",
        'label' => "Country",
        'cl'    => "m6 l4"
      ]);

      $element['state'] = new Text( "state" ,[
        'class' => "validate",
        'label' => "State",
        'cl'    => "m6 l4"
      ]);

      $element['city'] = new Text( "city" ,[
        'class' => "validate",
        'label' => "City",
        'cl'    => "m6 l4"
      ]);

      $element['zip'] = new Text( "zip" ,[
        'class' => "validate",
        'label' => "ZIP Code",
        'cl'    => "m6 l4"
      ]);

      $element['vat'] = new Text( "vat" ,[
        'class' => "validate",
        'label' => "VAT Number",
        'cl'    => "m6 l4"
      ]);

      $element['currency'] = new Select( "currency" , Currencies::find(["order"=>"code asc"]) ,[
        'using' => ['_','code'],
        'class' => "validate",
        'label' => "Currency Type",
        'cl'    => "m6 l2"
      ]);

      $element['company'] = new Text( "company" ,[
        'class' => "validate",
        'label' => "Company Name",
        'cl'    => "m6 l3"
      ]);

      $element['role'] = new Text( "role" ,[
        'class' => "validate",
        'label' => "Role in Company",
        'cl'    => "m6 l3",
        'break' => true
      ]);

      $element['username'] = new Text( "username" ,[
        'class'         => "validate",
        'label'         => "Username",
        'data-validate' => true,
        'cl'    => "m6 l6"
      ]);

      $element['password'] = new Password( "password" ,[
        'class' => "validate",
        'label' => "Password",
        'cl'    => "m6 l6"
      ]);

      $element['image'] = new File( "image" ,[
        'class' => "validate",
        'label' => "User Image",
        'file'  => true
      ]);

      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $method == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/client/new";
        $modal_header = "Create a new client!";

        $currency = Currencies::findFirstByCode($this->configuration->app->currency);
        if($currency)
        {
          $element['currency']->setAttribute("active",false)->setAttribute("value",$currency->_);
        }

        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO UPDATE POPULATE WITH VALJUE TO ELEMENT
      elseif ($method == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/client/update/{$uid}";
        $modal_header = "Update client information!";

        $modal_card = [
          "content"  => "Please note that the fields are optional for updating: <i>Password</i> , <i>User Image</i> , <i>Phone</i>"
        ];

        $element['name']      ->setAttribute("active",true)->setAttribute("value",$client[0]->name);
        $element['email']     ->setAttribute("active",true)->setAttribute("value",$client[0]->email);
        $element['phone']     ->setAttribute("active",true)->setAttribute("value",$client[0]->phone);
        $element['domain']    ->setAttribute("active",true)->setAttribute("value",$client[0]->domain);
        $element['address']   ->setAttribute("active",true)->setAttribute("value",$client[0]->address);
        $element['country']   ->setAttribute("active",true)->setAttribute("value",$client[0]->country);
        $element['state']     ->setAttribute("active",true)->setAttribute("value",$client[0]->state);
        $element['city']      ->setAttribute("active",true)->setAttribute("value",$client[0]->city);
        $element['zip']       ->setAttribute("active",true)->setAttribute("value",$client[0]->zip);
        $element['vat']       ->setAttribute("active",true)->setAttribute("value",$client[0]->vat);
        $element['currency']  ->setAttribute("active",false)->setAttribute("value",$client[0]->currency);
        $element['company']   ->setAttribute("active",true)->setAttribute("value",$client[0]->company);
        $element['role']      ->setAttribute("active",true)->setAttribute("value",$client[0]->role);
        $element['username']  ->setAttribute("active",true)->setAttribute("value",$client[0]->username);

        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($method == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/client/delete/{$uid}";
        $modal_header = "Remove Client!";

        $modal_card = [
          "content"  => "Are you sure? Apon removal all data such as Projects, Tickets, Tasks etc. associated with this client will be also removed!"
        ];

        foreach($element as $e)
        {
          $form->add($e);
        }

      # IF REQUEST IS TO VIEW POPULATE WITH VALJUE TO ELEMENT
      elseif ($method == "view"):
        $modal_header = "Client Information!";
        $modal_info = [
          [
            "titles" => [
              ["title" => "Username"],
              ["title" => "Full Name"],
              ["title" => "E-Mail"],
              ["title" => "Phone"],
              ["title" => "Domain"],
            ],
            "contents"  => [
              ["content" => $client[0]->username],
              ["content" => $client[0]->name],
              ["content" => $client[0]->email],
              ["content" => $client[0]->phone],
              ["content" => $client[0]->domain],
            ]
          ],
          [
            "titles" => [
              ["title" => "Address"],
              ["title" => "Country"],
              ["title" => "State"],
              ["title" => "City"],
              ["title" => "Currency"],
            ],
            "contents"  => [
              ["content" => ($client[0]->address ?: "n/a")],
              ["content" => ($client[0]->country ?: "n/a")],
              ["content" => ($client[0]->state ?: "n/a")],
              ["content" => ($client[0]->city ?: "n/a")],
              ["content" => $client[0]->code],
            ]
          ],
          [
            "titles" => [
              ["title" => "Company Name"],
              ["title" => "Role"],
            ],
            "contents"  => [
              ["content" => ($client[0]->company ?: "n/a")],
              ["content" => ($client[0]->role ?: "n/a")],
            ]
          ],
        ];

      endif;
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
