<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Hidden;

class SettingsPermissionsController extends ControllerBase
{
  public function IndexAction()
  {
    $this->permissionHandler("admin");
    $form = new Form;

    $element['members_page'] = new Check("members_page",[
      "checked" => ($this->configuration->team_crud->members_page ?: null),
      "data-disabler" => "members"
    ]);
    $element['members_create'] = new Check("members_create",[
      "checked" => ($this->configuration->team_crud->members_create ?: null),
      "data-disable" => "members"
    ]);
    $element['members_update'] = new Check("members_update",[
      "checked" => ($this->configuration->team_crud->members_update ?: null),
      "data-disable" => "members"
    ]);
    $element['members_delete'] = new Check("members_delete",[
      "checked" => ($this->configuration->team_crud->members_delete ?: null),
      "data-disable" => "members"
    ]);
    $element['tickets_page'] = new Check("tickets_page",[
      "checked" => ($this->configuration->team_crud->tickets_page ?: null),
      "data-disabler" => "tickets"
    ]);
    $element['tickets_update'] = new Check("tickets_update",[
      "checked" => ($this->configuration->team_crud->tickets_update ?: null),
      "data-disable" => "tickets"
    ]);
    $element['tickets_delete'] = new Check("tickets_delete",[
      "checked" => ($this->configuration->team_crud->tickets_delete ?: null),
      "data-disable" => "tickets"
    ]);
    $element['clients_page'] = new Check("clients_page",[
      "checked" => ($this->configuration->team_crud->clients_page ?: null),
      "data-disabler" => "clients"
    ]);
    $element['clients_create'] = new Check("clients_create",[
      "checked" => ($this->configuration->team_crud->clients_create ?: null),
      "data-disable" => "clients"
    ]);
    $element['clients_update'] = new Check("clients_update",[
      "checked" => ($this->configuration->team_crud->clients_update ?: null),
      "data-disable" => "clients"
    ]);
    $element['clients_delete'] = new Check("clients_delete",[
      "checked" => ($this->configuration->team_crud->clients_delete ?: null),
      "data-disable" => "clients"
    ]);
    $element['projects_page'] = new Check("projects_page",[
      "checked" => ($this->configuration->team_crud->projects_page ?: null),
      "data-disabler" => "projects"
    ]);
    $element['projects_create'] = new Check("projects_create",[
      "checked" => ($this->configuration->team_crud->projects_create ?: null),
      "data-disable" => "projects"
    ]);
    $element['projects_update'] = new Check("projects_update",[
      "checked" => ($this->configuration->team_crud->projects_update ?: null),
      "data-disable" => "projects"
    ]);
    $element['projects_delete'] = new Check("projects_delete",[
      "checked" => ($this->configuration->team_crud->projects_delete ?: null),
      "data-disable" => "projects"
    ]);
    $element['projecttasks_page'] = new Check("projecttasks_page",[
      "checked" => ($this->configuration->team_crud->projecttasks_page ?: null),
      "data-disabler" => "projecttasks"
    ]);
    $element['projecttasks_create'] = new Check("projecttasks_create",[
      "checked" => ($this->configuration->team_crud->projecttasks_create ?: null),
      "data-disable" => "projecttasks"
    ]);
    $element['projecttasks_update'] = new Check("projecttasks_update",[
      "checked" => ($this->configuration->team_crud->projecttasks_update ?: null),
      "data-disable" => "projecttasks"
    ]);
    $element['projecttasks_delete'] = new Check("projecttasks_delete",[
      "checked" => ($this->configuration->team_crud->projecttasks_delete ?: null),
      "data-disable" => "projecttasks"
    ]);
    $element['projectfiles_page'] = new Check("projectfiles_page",[
      "checked" => ($this->configuration->team_crud->projectfiles_page ?: null),
      "data-disabler" => "projectfiles"
    ]);
    $element['projectfiles_create']     = new Check("projectfiles_create",[
      "checked" => ($this->configuration->team_crud->projectfiles_create ?: null),
      "data-disable" => "projectfiles"
    ]);
    $element['projectfiles_delete']     = new Check("projectfiles_delete",[
      "checked" => ($this->configuration->team_crud->projectfiles_delete ?: null),
      "data-disable" => "projectfiles"
    ]);
    $element['projectrequests_page']    = new Check("projectrequests_page",[
      "checked" => ($this->configuration->team_crud->projectrequests_page ?: null),
      "data-disabler" => "projectrequests"
    ]);
    $element['projectrequests_delete']  = new Check("projectrequests_delete",[
      "checked" => ($this->configuration->team_crud->projectrequests_delete ?: null),
      "data-disable" => "projectrequests"
    ]);
    $element['projectestimates_page']   = new Check("projectestimates_page",[
      "checked" => ($this->configuration->team_crud->projectestimates_page ?: null),
      "data-disabler" => "projectestimates"
    ]);
    $element['projectestimates_send']   = new Check("projectestimates_send",[
      "checked" => ($this->configuration->team_crud->projectestimates_send ?: null),
      "data-disable" => "projectestimates"
    ]);
    $element['projectestimates_create'] = new Check("projectestimates_create",[
      "checked" => ($this->configuration->team_crud->projectestimates_create ?: null),
      "data-disable" => "projectestimates"
    ]);
    $element['projectestimates_update'] = new Check("projectestimates_update",[
      "checked" => ($this->configuration->team_crud->projectestimates_update ?: null),
      "data-disable" => "projectestimates"
    ]);
    $element['projectestimates_delete'] = new Check("projectestimates_delete",[
      "checked" => ($this->configuration->team_crud->projectestimates_delete ?: null),
      "data-disable" => "projectestimates"
    ]);
    $element['projectinvoices_page']    = new Check("projectinvoices_page",[
      "checked" => ($this->configuration->team_crud->projectinvoices_page ?: null),
      "data-disabler" => "projectinvoices"
    ]);
    $element['projectinvoices_create']  = new Check("projectinvoices_create",[
      "checked" => ($this->configuration->team_crud->projectinvoices_create ?: null),
      "data-disable" => "projectinvoices"
    ]);
    $element['projectinvoices_update']  = new Check("projectinvoices_update",[
      "checked" => ($this->configuration->team_crud->projectinvoices_update ?: null),
      "data-disable" => "projectinvoices"
    ]);
    $element['projectinvoices_delete']  = new Check("projectinvoices_delete",[
      "checked" => ($this->configuration->team_crud->projectinvoices_delete ?: null),
      "data-disable" => "projectinvoices"
    ]);
    $element['projectpayments_page'] = new Check("projectpayments_page",[
      "checked" => ($this->configuration->team_crud->projectpayments_page ?: null),
      "data-disabler" => "projectpayments"
    ]);
    $element['projectpayments_create']  = new Check("projectpayments_create",[
      "checked" => ($this->configuration->team_crud->projectpayments_create ?: null),
      "data-disable" => "projectpayments"
    ]);
    $element['projectpayments_delete']  = new Check("projectpayments_delete",[
      "checked" => ($this->configuration->team_crud->projectpayments_delete ?: null),
      "data-disable" => "projectpayments"
    ]);

    $element['clienttickets_page']  = new Check("clienttickets_page",[
      "checked" => ($this->configuration->client_crud->tickets_page ?: null),
      "data-disabler" => "clienttickets"
    ]);
    $element['clienttickets_create']  = new Check("clienttickets_create",[
      "checked" => ($this->configuration->client_crud->tickets_create ?: null),
      "data-disable" => "clienttickets"
    ]);
    $element['clienttickets_reopen']  = new Check("clienttickets_reopen",[
      "checked" => ($this->configuration->client_crud->tickets_reopen ?: null),
      "data-disable" => "clienttickets"
    ]);
    $element['clienttickets_delete']  = new Check("clienttickets_delete",[
      "checked" => ($this->configuration->client_crud->tickets_delete ?: null),
      "data-disable" => "clienttickets"
    ]);
    $element['clientprojectfiles_page']  = new Check("clientprojectfiles_page",[
      "checked" => ($this->configuration->client_crud->projectfiles_page ?: null),
      "data-disabler" => "clientprojectfiles"
    ]);
    $element['clientprojectfiles_create']  = new Check("clientprojectfiles_create",[
      "checked" => ($this->configuration->client_crud->projectfiles_create ?: null),
      "data-disable" => "clientprojectfiles"
    ]);
    $element['clientprojectfiles_delete']  = new Check("clientprojectfiles_delete",[
      "checked" => ($this->configuration->client_crud->projectfiles_delete ?: null),
      "data-disable" => "clientprojectfiles"
    ]);
    $element['clientprojectrequests_page']  = new Check("clientprojectrequests_page",[
      "checked" => ($this->configuration->client_crud->projectrequests_page ?: null),
      "data-disabler" => "clientprojectreques"
    ]);
    $element['clientprojectrequests_create']  = new Check("clientprojectrequests_create",[
      "checked" => ($this->configuration->client_crud->projectrequests_create ?: null),
      "data-disable" => "clientprojectrequests"
    ]);
    $element['clientprojectrequests_delete']  = new Check("clientprojectrequests_delete",[
      "checked" => ($this->configuration->client_crud->projectrequests_delete ?: null),
      "data-disable" => "clientprojectrequests"
    ]);

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    foreach($element as $e){ $form->add($e); }

    $this->view->form = $form;
    $this->view->pick("settings/permissions");
  }

  public function SaveAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->dispatcher->getParam("method")):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if($this->dispatcher->getParam("method","string") == "members"):
          $parameters = [
            "members_page"             => ($this->request->getPost("members_page")            ? "true" : "false" ),
            "members_create"           => ($this->request->getPost("members_create")          ? "true" : "false" ),
            "members_update"           => ($this->request->getPost("members_update")          ? "true" : "false" ),
            "members_delete"           => ($this->request->getPost("members_delete")          ? "true" : "false" ),
            "tickets_page"             => ($this->request->getPost("tickets_page")            ? "true" : "false" ),
            "tickets_update"           => ($this->request->getPost("tickets_update")          ? "true" : "false" ),
            "tickets_delete"           => ($this->request->getPost("tickets_delete")          ? "true" : "false" ),
            "clients_page"             => ($this->request->getPost("clients_page")            ? "true" : "false" ),
            "clients_create"           => ($this->request->getPost("clients_create")          ? "true" : "false" ),
            "clients_update"           => ($this->request->getPost("clients_update")          ? "true" : "false" ),
            "clients_delete"           => ($this->request->getPost("clients_delete")          ? "true" : "false" ),
            "projects_page"            => ($this->request->getPost("projects_page")           ? "true" : "false" ),
            "projects_create"          => ($this->request->getPost("projects_create")         ? "true" : "false" ),
            "projects_update"          => ($this->request->getPost("projects_update")         ? "true" : "false" ),
            "projects_delete"          => ($this->request->getPost("projects_delete")         ? "true" : "false" ),
            "projecttasks_page"        => ($this->request->getPost("projecttasks_page")       ? "true" : "false" ),
            "projecttasks_create"      => ($this->request->getPost("projecttasks_create")     ? "true" : "false" ),
            "projecttasks_update"      => ($this->request->getPost("projecttasks_update")     ? "true" : "false" ),
            "projecttasks_delete"      => ($this->request->getPost("projecttasks_delete")     ? "true" : "false" ),
            "projectfiles_page"        => ($this->request->getPost("projectfiles_page")       ? "true" : "false" ),
            "projectfiles_create"      => ($this->request->getPost("projectfiles_create")     ? "true" : "false" ),
            "projectfiles_delete"      => ($this->request->getPost("projectfiles_delete")     ? "true" : "false" ),
            "projectrequests_page"     => ($this->request->getPost("projectrequests_page")    ? "true" : "false" ),
            "projectrequests_delete"   => ($this->request->getPost("projectrequests_delete")  ? "true" : "false" ),
            "projectestimates_page"    => ($this->request->getPost("projectestimates_page")   ? "true" : "false" ),
            "projectestimates_send"    => ($this->request->getPost("projectestimates_send")   ? "true" : "false" ),
            "projectestimates_create"  => ($this->request->getPost("projectestimates_create") ? "true" : "false" ),
            "projectestimates_update"  => ($this->request->getPost("projectestimates_update") ? "true" : "false" ),
            "projectestimates_delete"  => ($this->request->getPost("projectestimates_delete") ? "true" : "false" ),
            "projectinvoices_page"     => ($this->request->getPost("projectinvoices_page")    ? "true" : "false" ),
            "projectinvoices_create"   => ($this->request->getPost("projectinvoices_create")  ? "true" : "false" ),
            "projectinvoices_update"   => ($this->request->getPost("projectinvoices_update")  ? "true" : "false" ),
            "projectinvoices_delete"   => ($this->request->getPost("projectinvoices_delete")  ? "true" : "false" ),
            "projectpayments_page"     => ($this->request->getPost("projectpayments_page")    ? "true" : "false" ),
            "projectpayments_create"   => ($this->request->getPost("projectpayments_create")  ? "true" : "false" ),
            "projectpayments_delete"   => ($this->request->getPost("projectpayments_delete")  ? "true" : "false" ),
          ];
        elseif($this->dispatcher->getParam("method","string") == "clients"):
          $parameters = [
            "clienttickets_page"      => ($this->request->getPost("clienttickets_page")    ? "true" : "false" ),
            "clienttickets_create"    => ($this->request->getPost("clienttickets_create")  ? "true" : "false" ),
            "clienttickets_update"    => ($this->request->getPost("clienttickets_update")  ? "true" : "false" ),
            "clienttickets_delete"    => ($this->request->getPost("clienttickets_delete")  ? "true" : "false" ),
            "clientprojectfiles_page"      => ($this->request->getPost("clientprojectfiles_page")    ? "true" : "false" ),
            "clientprojectfiles_create"    => ($this->request->getPost("clientprojectfiles_create")  ? "true" : "false" ),
            "clientprojectfiles_delete"    => ($this->request->getPost("clientprojectfiles_delete")  ? "true" : "false" ),
            "clientprojectrequests_page"   => ($this->request->getPost("clientprojectrequests_page")    ? "true" : "false" ),
            "clientprojectrequests_create" => ($this->request->getPost("clientprojectrequests_create")  ? "true" : "false" ),
            "clientprojectrequests_delete" => ($this->request->getPost("clientprojectrequests_delete")  ? "true" : "false" ),
          ];
        endif;

        $update = $this->UpdateConfigFile($parameters);

        if(!$update):
          return RhynoException::CustomError("Unable to update application configuration file.");
        endif;

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Permissions Updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/settings/permissions";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
