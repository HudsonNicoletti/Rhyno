<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Team              as Team,
    Manager\Models\Users             as Users,
    Manager\Models\Tasks             as Tasks,
    Manager\Models\Projects          as Projects,
    Manager\Models\Assignments       as Assignments,
    Manager\Models\TaskResponses     as TaskResponses,
    Manager\Models\TaskCollaborators as TaskCollaborators;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden;

class TasksController extends ControllerBase
{
  private $task_assigned_email;
  private $task_assigned_inner;
  private $task_response_email;
  private $task_response_inner;
  private $task_memberstatus_email;
  private $task_memberstatus_inner;
  private $task_status_email;
  private $task_status_inner;

  public function onConstruct()
  {
    $this->task_assigned_email     = $this->configuration->team_notification->task_assigned_email;
    $this->task_assigned_inner     = $this->configuration->team_notification->task_assigned_inner;
    $this->task_response_email     = $this->configuration->team_notification->task_response_email;
    $this->task_response_inner     = $this->configuration->team_notification->task_response_inner;
    $this->task_memberstatus_email = $this->configuration->team_notification->task_memberstatus_email;
    $this->task_memberstatus_inner = $this->configuration->team_notification->task_memberstatus_inner;
    $this->task_status_email       = $this->configuration->team_notification->task_status_email;
    $this->task_status_inner       = $this->configuration->team_notification->task_status_inner;

    if($this->dispatcher->getActionName() == "index")
    {
      $this->updateTasks();
    }
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("tasks/admin/index");
      break;
      case $this->permissions->team:
        $this->view->pick("tasks/team/index");
      break;
      case $this->permissions->client:
        $this->permissionHandler("team");
      break;
    }

    $id = $this->dispatcher->getParam("project","int");
    $team = Team::findFirstByUid($this->session->get($this->rhyno_token));

    $tasks = TaskCollaborators::query()
    ->columns([
      'Manager\Models\Tasks._',
      'Manager\Models\Tasks.unique',
      'Manager\Models\Tasks.title',
      'Manager\Models\Tasks.description',
      'Manager\Models\Tasks.deadline',
      'Manager\Models\Tasks.created',
      'Manager\Models\Tasks.completed',
      'Manager\Models\Tasks.status',
      'Manager\Models\Tasks.assigned',
      'Manager\Models\Projects.title as project',
      'Manager\Models\TaskCollaborators.status as mystatus',
      'Manager\Models\Team.name as manager',
      'Manager\Models\Team.image as managerimage',
    ])
    ->innerJoin('Manager\Models\Tasks', 'Manager\Models\Tasks._ = Manager\Models\TaskCollaborators.task')
    ->innerJoin('Manager\Models\Team', 'Manager\Models\Team._ = Manager\Models\Tasks.assigned')
    ->leftJoin('Manager\Models\Projects', 'Manager\Models\Tasks.project = Manager\Models\Projects._')
    ->where("Manager\Models\TaskCollaborators.member = :user: ")
    ->bind([
      "user" => $team->_
    ])
    ->orderBy("Manager\Models\Tasks.status=3 DESC,
               Manager\Models\Tasks.status=1 DESC,
               Manager\Models\Tasks.status=2 DESC,
               Manager\Models\Tasks.status=4 DESC,
               Manager\Models\Tasks.deadline ASC")
    ->execute();

    $collaborators = [];
    foreach($tasks as $task)
    {
      $tskc = TaskCollaborators::query()
      ->columns([
        'Manager\Models\Team.name',
        'Manager\Models\Team.image',
        'Manager\Models\TaskCollaborators.status'
      ])
      ->innerJoin('Manager\Models\Team', 'Manager\Models\Team._ = Manager\Models\TaskCollaborators.member')
      ->where("Manager\Models\TaskCollaborators.task = :task: AND Manager\Models\TaskCollaborators.member != :manager:")
      ->bind([
        "task"=>$task->_,
        "manager"=>$task->assigned
      ])
      ->orderBy("Manager\Models\Team.name ASC")
      ->execute();

      $collaborators[$task->_] = [];
      foreach($tskc as $tc)
      {
        array_push($collaborators[$task->_],["member"=>$tc->name,"image"=>$tc->image,"status"=>$tc->status]);
      }
    }

    $this->view->tasks = $tasks;
    $this->view->collaborators = $collaborators;
  }

  public function OverviewAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("tasks/admin/overview");
      break;
      case $this->permissions->team:
        $this->view->pick("tasks/team/overview");
      break;
      case $this->permissions->client:
        $this->permissionHandler("team");
      break;
    }

    $this->assets->addCss("assets/manager/css/pages/dashboard.css");
    $this->assets->addJs("assets/manager/addons/slimscroll/jquery.slimscroll.js");

    $unique = $this->dispatcher->getParam("task","string");
    $task = Tasks::query()
    ->columns([
      'Manager\Models\Tasks._',
      'Manager\Models\Tasks.unique',
      'Manager\Models\Tasks.title',
      'Manager\Models\Tasks.description',
      'Manager\Models\Tasks.status',
      'Manager\Models\Tasks.created',
      'Manager\Models\Tasks.deadline',
      'Manager\Models\Tasks.assigned',
      'Manager\Models\Projects.title as project',
      'Manager\Models\Projects.description as p_description',
      'Manager\Models\Projects.created as p_created',
      'Manager\Models\Projects.deadline as p_deadline',
    ])
    ->innerJoin('Manager\Models\Projects', 'Manager\Models\Tasks.project = Manager\Models\Projects._')
    ->where("Manager\Models\Tasks.unique = :task:")
    ->bind([
      "task" => $unique,
    ])
    ->execute();
    $form = new Form();

    $collaborators = TaskCollaborators::query()
    ->columns([
      'Manager\Models\Team._',
      'Manager\Models\Team.name as member',
      'Manager\Models\Team.image',
      'Manager\Models\TaskCollaborators.status'
    ])
    ->innerJoin('Manager\Models\Team', 'Manager\Models\Team._ = Manager\Models\TaskCollaborators.member')
    ->where("Manager\Models\TaskCollaborators.task = :task:")
    ->bind([
      "task" => $task[0]->_,
    ])
    ->orderBy("Manager\Models\Team.name ASC")
    ->execute();

    $responses = TaskResponses::query()
    ->columns([
      "Manager\Models\Team.name",
      "Manager\Models\Team.image",
      "Manager\Models\TaskResponses.member",
      "Manager\Models\TaskResponses.text",
      "Manager\Models\TaskResponses.date",
    ])
    ->innerJoin("Manager\Models\Team","Manager\Models\Team._ = Manager\Models\TaskResponses.member")
    ->where("Manager\Models\TaskResponses.task = :task: ")
    ->orderBy("Manager\Models\TaskResponses.date ASC")
    ->bind([
      "task" => $task[0]->_
    ])
    ->execute();

    $me = Team::findFirstByUid($this->session->get($this->rhyno_token))->_;

    $element['security'] = new Hidden( "security" ,[
      'name'  => $this->security->getTokenKey(),
      'value' => $this->security->getToken()
    ]);

    foreach($element as $e){ $form->add($e); }

    $this->view->me = $me;
    $this->view->mystatus = TaskCollaborators::findFirst(["task = '{$task[0]->_}' AND member = '{$me}' "])->status;
    $this->view->task = $task[0];
    $this->view->responses = $responses;
    $this->view->collaborators = $collaborators;
    $this->view->form = $form;
  }

  public function CollaboratorsAction()
  {
    $this->response->setContentType("application/json");
    $unique  = $this->dispatcher->getParam("task","string");
    $task    = Tasks::findFirstByUnique($unique);

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        // return RhynoException::InvalidRequestMethod();

      elseif(!$unique):
        return RhynoException::WrongNumberOfParams();

      elseif(!$task):
        return RhynoException::Unreachable();

      elseif(!$this->dispatcher->getParam("task","string")):
        return RhynoException::EmptyInput("Task");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $members = $this->request->getPost("members");
        $manager = $task->assigned;

        # GET ALL Collaborators
        $collabs = TaskCollaborators::findByTask($task->_);
        # Set array to push assigned collaborators
        $assignedCollabs = [];
        $exceptions = ["inner"=>[],"email"=>[]];
        # Loop throgh already assigned collaborators
        foreach($collabs as $c)
        {
          # Verify if collaborator is not in the post array
          if(!in_array($c->member,$members))
          {
            # if this collaborator is not the manager remove him
            if($c->member != $manager)
            {
              if(!$c->delete())
              {
                return RhynoException::DBError();
              }
            }
          }
          # else push this user to array for comparision
          else {
            array_push($assignedCollabs, $c->member);
          }
        }
        # loop throgh post
        foreach($members as $member)
        {
          # if current member is not already collaborating , insert into database
          if(!in_array($member,$assignedCollabs))
          {
            $tc = new TaskCollaborators;
              $tc->task   = $task->_;
              $tc->member = $member;
              $tc->status = 1;
            if(!$tc->save())
            {
              return RhynoException::DBError();
            }
          }
        }
        foreach($members as $member)
        {
          # if current member is not already collaborating , insert into database
          if(!in_array($member,$assignedCollabs))
          {
            $reciever = Team::findFirst($member);
            $user_inf = Users::findFirst($reciever->uid);
            # system notification
            if($this->task_assigned_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $reciever->uid,
                "text"     => "You are now assigned to a task! #{$unique}",
                "href"     => "{$this->rhyno_url}/task/overview/{$unique}"
              ]);

              if(!$inner){ array_push($exceptions['inner'],$reciever->name); }
            }

            if($this->task_assigned_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $reciever->uid,
                "title"    => "You Were Assigned To a Task! #{$unique}",
                "text"     => "You are receiving this e-mail because you were assigned to the task #{$unique}!",
                "href"     => "{$this->rhyno_url}/task/overview/{$unique}",
              ]);

              if(!$email){ array_push($exceptions['email'],$user_inf->email); }
            }
          }
        }

        if(count($exceptions['inner']) > 0){ return RhynoException::NotificationError($exceptions['inner']); }
        if(count($exceptions['email']) > 0){ return RhynoException::EmailSendError($exceptions['email']); }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project task successfully updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/task/overview/{$unique}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function StatusAction()
  {
    $this->response->setContentType("application/json");
    $unique = $this->dispatcher->getParam("task","string");
    $method = $this->dispatcher->getParam("method","string");
    $task   = Tasks::findFirstByUnique($unique);

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$unique || !$method):
        return RhynoException::WrongNumberOfParams();

      elseif(!$task):
        return RhynoException::Unreachable();

      elseif(!$this->dispatcher->getParam("task","string")):
        return RhynoException::EmptyInput("Task");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $member = Team::findFirstByUid($this->rhyno_user->_);
        $exceptions = ["inner"=>[],"email"=>[]];

        if($method == "update")
        {
          switch($task->status)
          {
            case 1 : $task->status = 2; break;
            case 2 : $task->status = 1; break;
            case 3 : $task->status = 2; break;
            case 4 : $task->status = 1; break;
          }
          if(!$task->save()){ return RhynoException::DBError(); }
        }
        elseif($method == "cancel")
        {
          $task->status = 4;
          if(!$task->save()){ return RhynoException::DBError(); }
        }
        elseif($method == "job")
        {
          $tskc = TaskCollaborators::findFirst(["task = '{$task->_}' AND member = '{$member->_}'"]);
          switch ($tskc->status)
          {
            case 1 : $tskc->status = 2; break;
            case 2 : $tskc->status = 1; break;
          }
          if(!$tskc->save()){ return RhynoException::DBError(); }
        }

        if($this->task_status_inner || $this->task_status_email || $this->task_memberstatus_inner || $this->task_memberstatus_email)
        {
          foreach(TaskCollaborators::findByTask($task->_) as $tc)
          {
            $receiver = Team::findFirst($tc->member);
            $user_inf = Users::findFirst($receiver->uid);

            switch ($method)
            {
              case 'update':
                # system notification
                if($this->task_status_inner)
                {
                  $inner = $this->NewNotification([
                    "receiver" => $receiver->uid,
                    "text"     => "Updated Task #{$unique} status!",
                    "href"     => "{$this->rhyno_url}/task/overview/{$unique}"
                  ]);

                  if(!$inner){ array_push($exceptions["inner"],$receiver->name); }
                }

                if($this->task_status_email)
                {
                  $email = $this->NewEmailNotification([
                    "receiver" => $receiver->uid,
                    "title"    => "{$member->name}, Updated Task #{$unique} status!",
                    "text"     => "{$member->name}, Updated Task #{$unique} status!",
                    "href"     => "{$this->rhyno_url}/task/overview/{$unique}",
                  ]);

                  if(!$email){ array_push($exceptions["email"],$user_inf->email); }
                }
              break;
              case 'cancel':
                # system notification
                if($this->task_status_inner)
                {
                  $inner = $this->NewNotification([
                    "receiver" => $receiver->uid,
                    "text"     => "Canceled Task #{$unique}!",
                    "href"     => "{$this->rhyno_url}/task/overview/{$unique}"
                  ]);

                  if(!$inner){ array_push($exceptions["inner"],$receiver->name); }
                }

                if($this->task_status_email)
                {
                  $email = $this->NewEmailNotification([
                    "receiver" => $receiver->uid,
                    "title"    => "{$member->name}, Canceled Task #{$unique}!",
                    "text"     => "{$member->name}, Canceled Task #{$unique}!",
                    "href"     => "{$this->rhyno_url}/task/overview/{$unique}",
                  ]);

                  if(!$email){ array_push($exceptions["email"],$user_inf->email); }
                }
              break;
              case 'job':
                # system notification
                if($this->task_memberstatus_inner)
                {
                  $inner = $this->NewNotification([
                    "receiver" => $receiver->uid,
                    "text"     => "Updated personal status for the task #{$unique}!",
                    "href"     => "{$this->rhyno_url}/task/overview/{$unique}"
                  ]);

                  if(!$inner){ array_push($exceptions["inner"],$receiver->name); }
                }

                if($this->task_memberstatus_email)
                {
                  $email = $this->NewEmailNotification([
                    "receiver" => $receiver->uid,
                    "title"    => "{$member->name}, updated personal status for the task #{$unique}!",
                    "text"     => "{$member->name}, updated personal status for the task #{$unique}!",
                    "href"     => "{$this->rhyno_url}/task/overview/{$unique}",
                  ]);

                  if(!$email){ array_push($exceptions["email"],$user_inf->email); }
                }
              break;
            }
          }
        }

        if(count($exceptions["inner"]) > 0) { return RhynoException::NotificationError($exceptions["inner"]); }
        if(count($exceptions["email"]) > 0) { return RhynoException::EmailSendError($exceptions["email"]); }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Status updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/task/overview/{$unique}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function SendAction()
  {
    $this->response->setContentType("application/json");
    $unique  = $this->dispatcher->getParam("task","string");
    $member  = Team::findFirstByUid($this->rhyno_user->_);
    $message = $this->request->getPost("text","string");
    $task    = Tasks::findFirstByUnique($unique);

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$unique):
        return RhynoException::WrongNumberOfParams();

      elseif(!$task || !$member):
        return RhynoException::Unreachable();

      elseif(!$message):
        return RhynoException::EmptyInput("Message");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $exceptions = ["inner"=>[],"email"=>[]];
        $response = new TaskResponses;
          $response->task     = $task->_;
          $response->member   = $member->_;
          $response->text     = $message;
          $response->date     = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$response->save())
        {
          return RhynoException::DBError();
        }

        if($this->task_response_inner || $this->task_response_email)
        {
          foreach(TaskCollaborators::findByTask($task->_) as $tc)
          {
            $receiver = Team::findFirst($tc->member);
            $user_inf = Users::findFirst($receiver->uid);

            # system notification
            if($this->task_memberstatus_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "Responded in Task #{$unique}!",
                "href"     => "{$this->rhyno_url}/task/overview/{$unique}"
              ]);

              if(!$inner){ array_push($exceptions["inner"],$receiver->name); }
            }

            if($this->task_memberstatus_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "{$member->name}, responded in Task #{$unique}!",
                "text"     => "{$member->name}, responded in Task #{$unique}: {$message}",
                "href"     => "{$this->rhyno_url}/task/overview/{$unique}",
              ]);

              if(!$email){ array_push($exceptions["email"],$user_inf->email); }
            }
          }
        }

        if(count($exceptions["inner"]) > 0) { return RhynoException::NotificationError($exceptions["inner"]); }
        if(count($exceptions["email"]) > 0) { return RhynoException::EmailSendError($exceptions["email"]); }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Message Sent!";
        $this->flags['redirect']   = "{$this->rhyno_url}/task/overview/{$unique}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form   = new Form;
      $inputs = [];
      $unique = $this->dispatcher->getParam("task","string");
      $method = $this->dispatcher->getParam("method","string");
      $task   = Tasks::findFirstByUnique($unique);

      if($method == "collaborators"):
        $members = Assignments::query()
        ->columns([
          "Manager\Models\Team._",
          "Manager\Models\Team.name"
        ])
        ->innerJoin('Manager\Models\Team', 'Manager\Models\Team._ = Manager\Models\Assignments.member')
        ->where("Manager\Models\Assignments.project = :project:")
        ->bind([
          "project" =>  $task->project
        ])
        ->orderBy("Manager\Models\Team.name ASC")
        ->execute();

        foreach($members as $team)
        {
          $element[$team->_] = new Check( $team->_ ,[
            'name'  => "members[]",
            'id'    => explode(" ",$team->name)[0],
            'value' => $team->_,
            'label' => $team->name ,
            'cl'    => "m6 l4" # Classes
          ]);
        }
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $method == "collaborators" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/task/collaborators/update/{$unique}";
        $modal_header = "Manage Task Collaborators!";
        $modal_card = [ "content" => "Remember, only assigned members to this task's project are able to be selected as the task collaborators." ];

        foreach(TaskCollaborators::findByTask($task->_) as $tc)
        {
          $element[$tc->member]->setAttribute("checked",true);
        }
      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
