<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Clients           as Clients,
    Manager\Models\Projects          as Projects,
    Manager\Models\ProjectFiles      as ProjectFiles,
    Manager\Models\ProjectTypes      as ProjectTypes,
    Manager\Models\ProjectActivities as ProjectActivities;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectFilesController extends ControllerBase
{
  private $uniqueP;
  private $project;
  private $rmethod;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueF = $this->dispatcher->getParam("file","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project  = Projects::findFirstByUnique($this->uniqueP);
    $this->file     = ProjectFiles::findFirstByUnique($this->uniqueF);
  }

  public function IndexAction()
  {
    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:
        $this->view->pick("projects/admin/files");
      break;
      case $this->permissions->team:
        if($this->configuration->team_crud->projectfiles_page)
        {
          $this->view->pick("projects/team/files");
          $this->view->crud = $this->configuration->team_crud;
        }
        else
        {
          $this->permissionHandler("admin");
        }
      break;
      case $this->permissions->client:
        if($this->configuration->client_crud->projectfiles_page)
        {
          $this->view->pick("projects/client/files");
          $this->view->crud = $this->configuration->client_crud;
        }
        else
        {
          $this->permissionHandler("team");
        }
      break;
    }

    $files = ProjectFiles::query()
    ->columns([
      'Manager\Models\ProjectFiles._',
      'Manager\Models\ProjectFiles.unique',
      'Manager\Models\ProjectFiles.project',
      'Manager\Models\ProjectFiles.title',
      'Manager\Models\ProjectFiles.file',
      'Manager\Models\ProjectFiles.uploaded',
      'Manager\Models\ProjectFiles.user',
      'CONCAT(COALESCE(Manager\Models\Team.name,""),COALESCE(Manager\Models\Clients.name,"")) AS name',
      'CONCAT(COALESCE(Manager\Models\Team.image,""),COALESCE(Manager\Models\Clients.image,"")) AS image',
    ])
    ->leftJoin('Manager\Models\Team', 'Manager\Models\Team.uid = Manager\Models\ProjectFiles.user')
    ->leftJoin('Manager\Models\Clients', 'Manager\Models\Clients.uid = Manager\Models\ProjectFiles.user')
    ->where("Manager\Models\ProjectFiles.project = :project:")
    ->orderBy("uploaded DESC ,  title ASC")
    ->bind(["project" => $this->project->_ ])
    ->execute();
    # use uid because clients can upload to

    $this->view->me      = $this->rhyno_user->_;
    $this->view->files   = $files;
    $this->view->project = $this->project;
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->request->hasFiles()):
        return RhynoException::UploadError();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $title = $this->request->getPost("title","string");

        foreach($this->request->getUploadedFiles() as $file):
          $filename = substr(sha1(uniqid()), 0, 12).'.'.$file->getExtension();
          $file->moveTo("assets/manager/files/{$filename}");
        endforeach;

        $file = new ProjectFiles;
          $file->unique    = $this->uniqueCode("FLS");
          $file->project   = $this->project->_;
          $file->user      = $this->rhyno_user->_;
          $file->title     = $title;
          $file->file      = $filename;
          $file->uploaded  = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$file->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Uploaded '{$title}' to the project's files.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $client = Clients::findFirst($this->project->client);

        # system notification
        if($this->configuration->client_notification->project_activities_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => "Uploaded file '{$title}' to the project #{$this->uniqueP}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/files"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->configuration->client_notification->project_activities_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => "Uploaded file to project #{$this->uniqueP}!",
            "text"     => "You are receiving this notification because a file '{$title}' was uploaded to the project #{$this->uniqueP}!",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/files",
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project file successfully uploaded!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/files";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueF):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->file):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        unlink("assets/manager/files/{$this->file->file}");
        if(!$this->file->delete())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Removed '{$this->file->title}' from the project's files.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $client = Clients::findFirst($this->project->client);

        # system notification
        if($this->configuration->client_notification->project_activities_inner)
        {
          $inner = $this->NewNotification([
            "receiver" => $client->uid,
            "text"     => "Removed file '{$this->file->title}' from the project #{$this->uniqueP}.",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/files"
          ]);

          if(!$inner){ return RhynoException::NotificationError(); }
        }

        if($this->configuration->client_notification->project_activities_email)
        {
          $email = $this->NewEmailNotification([
            "receiver" => $client->uid,
            "title"    => "Removed file from project #{$this->uniqueP}!",
            "text"     => "You are receiving this notification because the file '{$this->file->title}' was removed from the project #{$this->uniqueP}!",
            "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/files",
          ]);

          if(!$email){ return RhynoException::EmailSendError($u->email); }
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project file successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/files";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }
    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form = new Form();
      $inputs = [];

      if($this->rmethod == "remove"):

      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "File Title",
        ]);
        $element['file'] = new File( "file" ,[
          'label' => "Select File",
          'file'  => true
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/files/new";
        $modal_header = "Upload project files!";

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/files/delete/{$this->uniqueF}";
        $modal_header = "Remove Project files!";

        $modal_card = [
          "content"  => "Are you sure? The selected file will me permanently removed from the server."
        ];

      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function RequestFileAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";

    elseif(!$this->dispatcher->getParam("file")):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Wrong number of parameters.";

    elseif(!file_exists("assets/manager/files/".$this->dispatcher->getParam("file"))):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "File does not exits.";
    endif;

    if($this->flags['status'])
    {
      $this->flags['toast']    = "success";
      $this->flags['title']    = "File Downloading!";
      $this->flags['redirect'] = "{$this->rhyno_url}/download/file/".$this->dispatcher->getParam("file");
    }

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DownloadFileAction()
  {
    # maybe do a auth
    $filename = $this->dispatcher->getParam("file","string");
    $mime     = mime_content_type("assets/manager/files/{$filename}");

    $this->response->setContentType($mime);
    $this->response->setHeader("Content-Disposition", "attachment; filename='{$filename}'");
    $file = file_get_contents("assets/manager/files/{$filename}");
    return $this->response->setContent($file);
  }
}
