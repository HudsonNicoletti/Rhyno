<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Team         as Team,
    Manager\Models\Users        as Users,
    Manager\Models\Tasks        as Tasks,
    Manager\Models\TaskCollaborators        as TaskCollaborators,
    Manager\Models\Clients      as Clients,
    Manager\Models\Departments  as Departments,
    Manager\Models\Assignments  as Assignments,
    Manager\Models\ProjectTypes as ProjectTypes,
    Manager\Models\Projects     as Projects;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Check,
    Phalcon\Forms\Element\Hidden;

class AssignmentsController extends ControllerBase
{
  private $uniqueP;
  private $project;
  private $assignments;
  private $rmethod;
  private $project_assigned_email;
  private $project_assigned_inner;


  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project_assigned_email = $this->configuration->team_notification->project_assigned_email;
    $this->project_assigned_inner = $this->configuration->team_notification->project_assigned_inner;

    $this->project  = Projects::findFirstByUnique($this->uniqueP);
    $this->assignments = Assignments::findByProject($this->project->_);
  }

  public function AssignAction()
  {
    $this->response->setContentType("application/json");
    # catch any form errors
    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project):
        return RhynoException::Unreachable();

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $members = $this->request->getPost("members");

        $assigned = [];
        $exceptions = ["inner"=>[],"email"=>[]];

        # any member that is not in the post will be removed, else push to assigned array if not already pushed
        foreach($this->assignments as $a)
        {
          if(!in_array($a->member,$members))
          {
            $a->delete();
          }
          else
          {
            (!in_array($a->member,$assigned)) ? array_push($assigned, $a->member) : null;
          }
        }

        # any collaborator that is not in the post will be removed, else push to assigned array if not already pushed
        foreach(Tasks::findByProject($this->project->_) as $t)
        {
          $collabs  = TaskCollaborators::findByTask($t->_);

          foreach($collabs as $c)
          {
            if(!in_array($c->member,$members))
            {
              $c->delete();
            }
            else {
              (!in_array($c->member,$assigned)) ? array_push($assigned, $c->member) : null;
            }
          }
        }

        # assign all members in post to project or error if cant save
        foreach($members as $m)
        {
          if(!in_array($m,$assigned))
          {
            $assign = new Assignments;
              $assign->project = $this->project->_;
              $assign->member  = $m;
            if(!$assign->save()) { return RhynoException::DBError(); }
          }
        }

        # send notification & email if enabled , in case of error push to exception array and display after execution
        foreach($members as $m)
        {
          if(!in_array($m,$assigned))
          {
            # system notification
            $receiver = Team::findFirst($m);
            $user_inf = Users::findFirst($receiver->uid);
            if($this->project_assigned_inner)
            {
              $inner = $this->NewNotification([
                "receiver" => $receiver->uid,
                "text"     => "You are now assigned to a project! #{$this->uniqueP}",
                "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/overview"
              ]);

              if(!$inner){ array_push($exceptions["inner"],$receiver->name); }
            }

            if($this->project_assigned_email)
            {
              $email = $this->NewEmailNotification([
                "receiver" => $receiver->uid,
                "title"    => "You Were Assigned To a Project! #{$this->uniqueP}",
                "text"     => "You are receiving this e-mail because you were assigned to a project #{$this->uniqueP}!",
                "href"     => "{$this->rhyno_url}/project/{$this->uniqueP}/overview",
              ]);

              if(!$email){ array_push($exceptions["email"],$user_inf->email); }
            }
          }
        }

        if(count($exceptions["inner"]) > 0){ return RhynoException::NotificationError($exceptions["inner"]); }
        if(count($exceptions["email"]) > 0){ return RhynoException::EmailSendError($exceptions["email"]); }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Members successfully assigned to the project!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/overview";

      }
      catch (\Exception $e)
      {
        $this->flags['toast'] = "warning";
        $this->flags['title'] = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form   = new Form;
      $inputs = [];

      $element['departments'] = new Select( "departments[]" , Departments::find(["order"=>"department asc"]) ,[
        'using'     => ["_","department"],
        'label'     => "Team Departments ",
        'multiple'  => true ,
        'checkbox'  => "members[]",
        'data-ajax' => "{$this->rhyno_url}/department/members",
      ]);

      foreach(Team::find(["order"=>"name asc"]) as $team)
      {
        $element[$team->_] = new Check( $team->_ ,[
          'name'  => "members[]",
          'id'    => explode(" ",$team->name)[0],
          'value' => $team->_,
          'label' => $team->name ,
          'cl'    => "m6 l4" # Classes
        ]);
      }

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "assign" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/assign";
        $modal_header = "Manage assigned members to the project!";
        $modal_card = [
          ["content" => 'Hey! You can select multiple members by selecting departments or individual team members to assign to the project!'],
          ["content" => '<strong>Warning!</strong>, all members assigned to a task will be unassigned if removed from project assignments!'],
        ];

        ($this->configuration->team_notification->project_assigned_email) ? array_push($modal_card,["content" => 'E-Mail Notification is enabled , this might take a sec depending on your connection.']) : null;

        foreach($this->assignments as $d)
        {
          $element[$d->member]->setAttribute("checked",true);
        }
      endif;

      foreach($element as $e)
      {
        $form->add($e);
      }
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }
}
