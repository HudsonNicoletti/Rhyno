<?php

namespace Manager\Controllers;

use Manager\Models\Team               as Team,
    Manager\Models\Tasks              as Tasks,
    Manager\Models\Tickets            as Tickets,
    Manager\Models\Projects           as Projects,
    Manager\Models\ProjectActivities           as ProjectActivities,
    Manager\Models\Assignments        as Assignments,
    Manager\Models\InvoicePayments    as InvoicePayments,
    Manager\Models\TaskCollaborators  as TaskCollaborators;

class IndexController extends ControllerBase
{
  public function IndexAction()
  {

    $sun = new \DateTime("last Sunday");
    $sat = new \DateTime("next Saturday");

    $days   = ["sun" => 0,"mon" => 0,"tue" => 0,"wed" => 0,"thu" => 0,"fri" => 0,"sat" => 0,];
    $months = ["jan" => 0,"feb" => 0,"mar" => 0,"apr" => 0,"may" => 0,"jun" => 0,"jul" => 0,"agu" => 0,"sep" => 0,"oct" => 0,"nov" => 0,"dec" => 0];

    switch($this->rhyno_user->permission)
    {
      case $this->permissions->admin:

        $weekly = InvoicePayments::find([
          'conditions' => 'date BETWEEN :start: AND :end:',
          'bind' => [
            "start" => $sun->format("Y-m-d H:i:s"),
            "end" => $sat->format("Y-m-d H:i:s"),
          ]
        ]);

        $monthly = InvoicePayments::find([
          'conditions' => 'date BETWEEN :start: AND :end:',
          'bind' => [
            "start" => (new \DateTime("first day of January"))->format("Y-m-d H:i:s"),
            "end"   => (new \DateTime("last day of December"))->format("Y-m-d H:i:s")
          ]
        ]);

        foreach ($weekly as $w)
        {
          $d = strtolower((new \DateTime($w->date))->format("D"));
          $days[$d] = $days[$d] + ($w->amount / 100);
        }
        foreach ($monthly as $m)
        {
          $d = strtolower((new \DateTime($m->date))->format("M"));
          $months[$d] = $months[$d] + ($m->amount / 100);
        }

        $project_activities = ProjectActivities::query()
        ->columns([
          "Manager\Models\ProjectActivities.text as activity",
          "Manager\Models\ProjectActivities.date as date",
          "Manager\Models\Projects.unique as project",
          "COALESCE(Manager\Models\Team.name,Manager\Models\Clients.name) as name",
          "COALESCE(Manager\Models\Team.image,Manager\Models\Clients.image) as image",
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\ProjectActivities.project")
        ->leftJoin("Manager\Models\Team","Manager\Models\Team.uid = Manager\Models\ProjectActivities.user")
        ->leftJoin("Manager\Models\Clients","Manager\Models\Clients.uid = Manager\Models\ProjectActivities.user")
        ->orderBy("date DESC")
        ->limit(15,0)
        ->execute();

        $projects = Projects::find();
        $tasks    = Tasks::find();
        $tickets  = Tickets::find();

        $this->view->weekly  = (object) $days;
        $this->view->monthly = (object) $months;
        $this->view->project_activities = $project_activities;
        $this->view->pick("index/admin/index");
      break;
      case $this->permissions->team:

        $projects = Assignments::query()
        ->columns([
          'Manager\Models\Projects.status',
        ])
        ->innerJoin('Manager\Models\Projects', 'Manager\Models\Projects._ = Manager\Models\Assignments.project')
        ->where("Manager\Models\Assignments.member = :user: ")
        ->bind([
          "user" => $this->rhyno_user_info->_
        ])
        ->execute();

        $tasks    = TaskCollaborators::query()
        ->columns([
          'Manager\Models\Tasks.status',
        ])
        ->innerJoin('Manager\Models\Tasks', 'Manager\Models\Tasks._ = Manager\Models\TaskCollaborators.task')
        ->where("Manager\Models\TaskCollaborators.member = :user: ")
        ->bind([
          "user" => $this->rhyno_user_info->_
        ])
        ->execute();

        $project_activities = Assignments::query()
        ->columns([
          "Manager\Models\ProjectActivities.text as activity",
          "Manager\Models\ProjectActivities.date as date",
          "Manager\Models\Projects.unique as project",
          "COALESCE(Manager\Models\Team.name,Manager\Models\Clients.name) as name",
          "COALESCE(Manager\Models\Team.image,Manager\Models\Clients.image) as image",
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\Assignments.project")
        ->innerJoin("Manager\Models\ProjectActivities","Manager\Models\Projects._ = Manager\Models\ProjectActivities.project")
        ->leftJoin("Manager\Models\Team","Manager\Models\Team.uid = Manager\Models\ProjectActivities.user")
        ->leftJoin("Manager\Models\Clients","Manager\Models\Clients.uid = Manager\Models\ProjectActivities.user")
        ->where("Manager\Models\Assignments.member = :member:")
        ->bind([
          "member" => $this->rhyno_user_info->_
        ])
        ->orderBy("date DESC")
        ->limit(15,0)
        ->execute();


        $tickets  = Tickets::find();

        $this->view->pick("index/team/index");
        $this->view->project_activities = $project_activities;
        $this->view->crud = $this->configuration->team_crud;
      break;
      case $this->permissions->client:

        $projects = Projects::findByClient($this->rhyno_user_info->_);
        $tickets  = Tickets::findByUid($this->rhyno_user->_);

        $monthly = InvoicePayments::find([
          'conditions' => 'payer = :payer: AND date BETWEEN :start: AND :end:',
          'bind' => [
            "payer" => $this->rhyno_user->_,
            "start" => (new \DateTime("first day of January"))->format("Y-m-d H:i:s"),
            "end"   => (new \DateTime("last day of December"))->format("Y-m-d H:i:s")
          ]
        ]);

        foreach ($monthly as $m)
        {
          $d = strtolower((new \DateTime($m->date))->format("M"));
          $months[$d] = $months[$d] + ($m->amount / 100);
        }

        $project_activities = ProjectActivities::query()
        ->columns([
          "Manager\Models\ProjectActivities.text as activity",
          "Manager\Models\ProjectActivities.date as date",
          "Manager\Models\Projects.unique as project",
          "COALESCE(Manager\Models\Team.name,Manager\Models\Clients.name) as name",
          "COALESCE(Manager\Models\Team.image,Manager\Models\Clients.image) as image",
        ])
        ->innerJoin("Manager\Models\Projects","Manager\Models\Projects._ = Manager\Models\ProjectActivities.project")
        ->leftJoin("Manager\Models\Team","Manager\Models\Team.uid = Manager\Models\ProjectActivities.user")
        ->leftJoin("Manager\Models\Clients","Manager\Models\Clients.uid = Manager\Models\ProjectActivities.user")
        ->where("Manager\Models\Projects.client = :client:")
        ->bind([
          "client" => $this->rhyno_user_info->_
        ])
        ->orderBy("date DESC")
        ->limit(15,0)
        ->execute();

        $this->view->monthly = (object) $months;
        $this->view->pick("index/client/index");
        $this->view->project_activities = $project_activities;
        $this->view->crud = $this->configuration->client_crud;
      break;
    }

    $this->assets->addCss("assets/manager/css/pages/dashboard.css");
      // create a redirect for clients

      $p = ["pending"=>0,"completed"=>0,"canceled"=>0];
      foreach ($projects as $project) {
        switch ($project->status){
          case 1 : $p['pending']    = $p['pending'] + 1 ; break;
          case 2 : $p['completed']  = $p['completed'] + 1 ; break;
          case 3 : $p['canceled']   = $p['canceled'] + 1 ; break;
        }
      }

      $t = ["pending"=>0,"completed"=>0,"canceled"=>0];
      foreach ($tasks as $task) {
        switch ($task->status){
          case 1 : $t['pending']    = $t['pending'] + 1 ; break;
          case 2 : $t['completed']  = $t['completed'] + 1 ; break;
          case 3 : $t['canceled']   = $t['canceled'] + 1 ; break;
        }
      }

      $k = ["open"=>0,"pending"=>0,"closed"=>0];
      foreach ($tickets as $ticket) {
        switch ($ticket->status){
          case 1 : $k['open']    = $k['open'] + 1 ; break;
          case 2 : $k['open']    = $k['open'] + 1 ; break;
          case 3 : $k['pending'] = $k['pending'] + 1 ; break;
          case 4 : $k['closed']  = $k['closed'] + 1 ; break;
        }
      }

      $this->view->project = [ "pending" => $p['pending'],"completed" => $p['completed'],"canceled" => $p['canceled']];
      $this->view->tasks   = [ "pending" => $t['pending'],"completed" => $t['completed'],"canceled" => $t['canceled']];
      $this->view->tickets = [ "open"    => $k['open'],   "pending"   => $k['pending'],  "closed"   => $k['closed']];

  }
}
