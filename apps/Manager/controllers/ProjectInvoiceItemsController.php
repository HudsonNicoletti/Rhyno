<?php

namespace Manager\Controllers;

use Manager\Controllers\RhynoException;

use Manager\Models\Projects          as Projects,
    Manager\Models\ProjectInvoices   as ProjectInvoices,
    Manager\Models\ProjectActivities as ProjectActivities,
    Manager\Models\InvoicePayments   as InvoicePayments,
    Manager\Models\InvoiceItems      as InvoiceItems;

use Mustache_Engine as Mustache;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Forms\Element\Textarea,
    Phalcon\Forms\Element\Password,
    Phalcon\Forms\Element\Select,
    Phalcon\Forms\Element\File,
    Phalcon\Forms\Element\Hidden;

class ProjectInvoiceItemsController extends ControllerBase
{
  private $uniqueP;
  private $uniqueI;
  private $uniqueT;
  private $rmethod;
  private $project;
  private $invoice;
  private $item;

  public function onConstruct()
  {
    # set all variables before layout
    $this->uniqueP = $this->dispatcher->getParam("project","string");
    $this->uniqueI = $this->dispatcher->getParam("invoice","string");
    $this->uniqueT = $this->dispatcher->getParam("item","string");
    $this->rmethod = $this->dispatcher->getParam("method","string");

    $this->project = Projects::findFirstByUnique($this->uniqueP);
    $this->invoice = ProjectInvoices::findFirstByUnique($this->uniqueI);
    $this->item    = InvoiceItems::findFirstByUnique($this->uniqueT);
  }

  public function NewAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueI):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->invoice):
        return RhynoException::Unreachable();

      elseif(!$this->invoice->project == $this->project->_):
        return RhynoException::CustomError("Whoah, this invoice is not part of this project!");

      elseif(!$this->request->getPost("title")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("amount") || !$this->request->getPost("price")):
        return RhynoException::EmptyInput(["Amount","Unite Price"]);

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $title = $this->request->getPost("title","string");

        $itm = new InvoiceItems;
          $itm->unique  = $this->uniqueCode("ITM");
          $itm->invoice = $this->invoice->_;
          $itm->title   = $title;
          $itm->text    = $this->request->getPost("text","string");
          $itm->amount  = $this->request->getPost("amount","int");
          $itm->price   = $this->request->getPost("price","int");
          $itm->issued  = (new \DateTime())->format("Y-m-d H:i:s");
        if(!$itm->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Created a new invoice item, '{$title}' to the project invoce '#{$this->uniqueI}'.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Invoice item successfully created!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/overview/{$this->uniqueI}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function UpdateAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueI || !$this->uniqueT):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->invoice|| !$this->item):
        return RhynoException::Unreachable();

      elseif(!$this->item->invoice == $this->invoice->_):
        return RhynoException::CustomError("Whoah, this item is not part of this project's invoice!");

      elseif(!$this->request->getPost("title")):
        return RhynoException::EmptyInput("Title");

      elseif(!$this->request->getPost("amount") || !$this->request->getPost("price")):
        return RhynoException::EmptyInput(["Amount","Unite Price"]);

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        $title    = $this->request->getPost("title","string");

        $this->item->title   = $title;
        $this->item->text    = $this->request->getPost("text","string");
        $this->item->amount  = $this->request->getPost("amount","int");
        $this->item->price   = $this->request->getPost("price","int");
        if(!$this->item->save())
        {
          return RhynoException::DBError();
        }

        $act = new ProjectActivities;
          $act->project = $this->project->_;
          $act->text    = "Updated a invoice item, '{$title}' to the project invoce '#{$this->uniqueI}'.";
          $act->user    = $this->rhyno_user->_;
          $act->date    = (new \DateTime())->format("Y-m-d H:i:s");
        $act->save();

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Project invoice successfully updated!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/overview/{$this->uniqueI}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function DeleteAction()
  {
    $this->response->setContentType("application/json");

    try
    {
      if(!$this->request->isPost() || !$this->request->isAjax()):
        return RhynoException::InvalidRequestMethod();

      elseif(!$this->uniqueP || !$this->uniqueI || !$this->uniqueT):
        return RhynoException::WrongNumberOfParams();

      elseif(!$this->project || !$this->invoice|| !$this->item):
        return RhynoException::Unreachable();

      elseif(!$this->item->invoice == $this->invoice->_):
        return RhynoException::CustomError("Whoah, this item is not part of this project's invoice!");

      elseif(!$this->security->checkToken()):
        return RhynoException::InvalidCsrfToken();
      endif;
    }
    catch (\Exception $e)
    {
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = $e->getMessage();
    }

    if($this->flags['status']):
      try
      {
        if(!$this->item->delete())
        {
          return RhynoException::DBError();
        }

        $this->flags['toast']      = "success";
        $this->flags['title']      = "Invoice item successfully removed!";
        $this->flags['redirect']   = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/overview/{$this->uniqueI}";
      }
      catch(\Exception $e)
      {
        $this->flags['toast']  = "warning";
        $this->flags['title']  = $e->getMessage();
      }

    endif;

    return $this->response->setJsonContent([
      "toast"     =>  $this->flags['toast'],
      "title"     =>  $this->flags['title'],
      "redirect"  =>  $this->flags['redirect'],
      "time"      =>  $this->flags['time']
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

  public function ModalAction()
  {
    $this->response->setContentType("application/json");

    if(!$this->request->isGet()):
      $this->flags['status'] = false ;
      $this->flags['toast']  = "error";
      $this->flags['title']  = "Invalid Method.";
    endif;

    if($this->flags['status']):

      $form    = new Form;
      $inputs  = [];

      if($this->rmethod == "remove"):

      else:
        # CREATING ELEMENTS
        $element['title'] = new Text( "title" ,[
          'label' => "Invoice Title",
        ]);
        $element['text'] = new Textarea( "text" ,[
          'label' => "Description",
          'class' => "materialize-textarea"
        ]);
        $element['amount'] = new Text( "amount" ,[
          'label' => "Amount / Hours",
          'data-mask'         => "000.000.000.000.000",
          'data-mask-reverse' => true,
        ]);
        $element['price'] = new Text( "price" ,[
          'label'             => "Unit Price",
          'data-mask'         => "000.000.000.000.000,00",
          'data-mask-reverse' => true,
        ]);
      endif;

      $element['security'] = new Hidden( "security" ,[
        'name'  => $this->security->getTokenKey(),
        'value' => $this->security->getToken(),
      ]);

      # IF REQUEST IS TO CREATE JUST POPULATE WITH DEFAULT ELEMENTS
      if( $this->rmethod == "create" ):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/{$this->uniqueI}/item/new";
        $modal_header = "Create a invoice item!";

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "modify"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/{$this->uniqueI}/item/update/{$this->uniqueT}";
        $modal_header = "Update Project invoice item!";

        $element['title']->setAttribute("active",true)->setAttribute("value",$this->item->title);
        $element['text']->setAttribute("active",true)->setAttribute("value",$this->item->text);
        $element['amount']->setAttribute("active",true)->setAttribute("value",$this->item->amount);
        $element['price']->setAttribute("active",true)->setAttribute("value",$this->item->price);

      # IF REQUEST IS TO DELETE POPULATE WITH VALJUE TO ELEMENT
      elseif ($this->rmethod == "remove"):
        $modal_form   = true;
        $modal_action = "{$this->rhyno_url}/project/{$this->uniqueP}/invoices/{$this->uniqueI}/item/delete/{$this->uniqueT}";
        $modal_header = "Remove Project invoice item!";

        $modal_card = [
          "content"  => "Are you sure? The selected invoice item will me permanently removed from the invoice."
        ];

      endif;


      foreach($element as $e){$form->add($e);}
      # POPULATE ARRAY WITH TITLE AND INPUTS FOR RENDERING
      foreach($form as $f)
      {
        array_push($inputs,[ "label" => $f->getAttribute("label") , "input" => $f->render($f->getName()) , "file" => $f->getAttribute("file") , "active" => $f->getAttribute("active") , "cl" => $f->getAttribute("cl") ]);
      }

      # RENDER
      $body = (new Mustache)->render(file_get_contents(__DIR__."/../../../templates/modal.tpl"),[
        "form"    => $modal_form,
        "header"  => $modal_header,
        "action"  => $modal_action,
        "card"    => $modal_card,
        "info"    => $modal_info,
        "bottom"  => $modal_bottom,
        "inputs"  => $inputs
      ]);

    endif;

    return $this->response->setJsonContent([
      "status"    =>  $this->flags['status'],
      "data"      =>  $body
    ]);

    $this->response->send();
    $this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
  }

}
