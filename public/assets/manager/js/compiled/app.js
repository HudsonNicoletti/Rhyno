(function ($) {

  function AjaxForm(form) {
    var $this = $(form),
        action = $this.attr("action"),
        method = $this.attr("method"),
        inputs = $this.find("input:not(:file):not(:submit):not(:checkbox) , textarea, select, input:hidden, input:checked"),
        files = $this.find("input:file"),
        flags = {},
        content = new FormData();

    for (var i = 0; i < inputs.length; ++i) {
      content.append($(inputs[i]).attr("name"), $(inputs[i]).val()); // Add all fields automatic
    }

    //  Loop & append files with file data
    if (files.length) {
      for (var i = 0; i < files.length; ++i) {
        if (files[i].files[i] != undefined) {
          content.append(files.eq(i).attr("name"), files[i].files[i], files[i].files[i].name); // add files if exits
        }
      }
    }

    $.ajax({
      url: action,
      type: method,
      data: content,
      processData: false,
      contentType: false,
      dataType: "json",
      cache: false,
      beforeSend: function () {
        if ($this.data("before") != false || $this.data("before") == undefined) {
          Materialize.toast("Processing Your Request. This will take a few seconds ;)", 2200, "info");
        }
      },
      success: function (response) {

        Materialize.toast(response.title, response.time, response.toast, function () {
          response.redirect ? document.location = response.redirect : null;
        });

        return false;
      },
      error: function () {
        Materialize.toast(response.title, response.time, response.toast);
      }
    });
  }

  function currencyFormat(n, currency) {
    return currency + " " + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1.");
  }

  $("body").delegate("form[role='AjaxForm']", "submit", function () {
    AjaxForm(this);
    return false;
  }).delegate(".input-field label", "click", function (e) {
    $(this).parent().find("input:checkbox").click();
  }).delegate("select[checkbox]", "change", function (e) {
    var checkbox = $(this).attr("checkbox"),
        chs = $("input:checkbox[name='" + checkbox + "']");
    $.ajax({
      url: $(this).data("ajax"),
      type: "POST",
      data: { 'content': $(this).val() },
      success: function (response) {
        chs.removeAttr("checked");
        response.data.forEach(function (val) {
          chs.filter("[value='" + val + "']").attr("checked", true);
        });
      }
    });
  }).delegate("[data-href]", "click", function (e) {
    document.location = $(this).data("href");
  }).delegate("[data-download]", "click", function (e) {
    $.ajax({
      url: _baseUri + "/request/file/" + $(this).data("download"),
      type: "GET",
      success: function (response) {
        if (response) {
          if (response.redirect) {
            Materialize.toast(response.title, response.time, response.toast, function () {
              document.location = response.redirect;
            });
          } else {
            Materialize.toast(response.title, response.time, response.toast);
          }
        }
        return false;
      },
      error: function () {
        Materialize.toast(response.title, response.time, response.toast);
      }
    });
  }).delegate("[data-disabler]", "click", function () {
    var $this = $(this),
        target = $this.data("disabler"),
        sib = $("[data-disable='" + target + "']");

    if ($this.is(":checked")) {
      sib.each(function (i) {
        $(this).removeAttr("disabled");
      });
    } else {
      sib.each(function (i) {
        $(this).prop('checked', false).attr("disabled", "disabled");
      });
    }
  }).delegate("[data-submit]", "click", function () {
    $(this).parent("form").submit();
  }).delegate("[data-activates='dropdown-notifications']", "click", function () {
    $.post(_baseUri + '/clsnn', function () {});
    $("[data-activates='dropdown-notifications']").find("span.new-notifications").text("");
  }).delegate("[data-more]", "click", function () {
    $.get(_baseUri + $(this).data("more"), { amount: $("#dynamic-content").children().length }, function (response) {
      if (response.status) {
        for (var i = 0; i < response.data.length; i++) {
          var $template = $('#template').clone(),
              node = $template.prop('content'),
              $node = $(node).children(),
              $date = $node.find('#date'),
              $text = $node.find('#text'),
              $image = $node.find('#image');

          var img = response.data[i].image == null ? "placeholder/" + response.data[i].name.substring(0, 1).toLowerCase() + ".jpg" : response.data[i].image;

          $date.html(response.data[i].date);
          $text.html(response.data[i].text);
          $image.attr("src", $image.attr("src") + img);
          $image.attr("data-tooltip", response.data[i].name);

          $node.appendTo("#dynamic-content");
          $('.tooltipped').tooltip();
        }
      } else {
        $(this).hide();
      }
    });
    return false;
  });

  if ($("[data-convert]").length) {

    $.ajax({
      url: _baseUri + "/currencyRates",
      type: "GET",
      success: function (r) {
        var currency = $("[data-convert]").eq(0).data('currency'),
            rate = r.rates[currency];

        $("[data-convert]").each(function () {
          var $this = $(this);
          $this.ready(function () {
            var value = $this.data("convert") / 100;

            if (!rate) {
              $this.attr("data-tooltip", currencyFormat(value, "$") + " " + r.base);
              $this.text(currencyFormat(value, "$") + " " + r.base);
            } else {
              $this.attr("data-tooltip", currencyFormat(value, "$") + " " + r.base);
              $this.text(currencyFormat(value * rate, "$") + " " + currency);
            }
          });
        });
      },
      error: function (r) {
        Materialize.toast(response.title, response.time, response.toast);
      }
    });
  }

  if (window.location.hash) {
    var title,
        time = 2200,
        toast = "error";
    switch (window.location.hash) {
      case "#card-error":
        title = "Sorry, there was a problem with your card.";
        break;
      case "#api-error":
        title = "Sorry, there was a problem with the API.";
        break;
      case "#error":
        title = "Sorry, there was an internal error.";
        break;
      case "#not-approved":
        title = "Sorry, there was a problem. Your request was not approved.";
        break;
      case "#success":
        title = "Processed successfully!";
        toast = "success";
        break;
    }
    Materialize.toast(title, time, toast);
  }

  var $ModalTrigger = $('[data-ajax]');

  /********************************
  Modal Templating
  ********************************/
  if ($ModalTrigger.length) {
    $ModalTrigger.each(function () {
      var $this = $(this);

      $this.on("click", function () {
        $.ajax({
          url: $(this).data("ajax"),
          type: "GET",
          processData: false,
          contentType: false,
          dataType: "json",
          cache: false,
          success: function (response) {
            $('#modal-results').html(response.data);
            $('select').material_select(); // fixing select
            $('#dynamic-modal').openModal();
            datepikr();
            maskup();
          }
        });
        return false;
      });
    });
  }

  if ($(".progress-bar").length) {
    $(".progress-bar").loading();
  }

  if ($('.datepicker').length) {
    datepikr();
  }

  if ($("[data-mask]").length) {
    maskup();
  }

  if ($(".stripe-toggle").length) {
    $(".stripe-toggle").on("click", function () {
      $(".stripe-button-el").trigger("click");
    });
  }

  if ($('[data-search]').length) {
    $('[data-search]').filtr({
      target: "[data-searchable]", // Object to be filterd
      caseSensitive: false, // Filter strictly  ( true || false , default is false )
      invert: false });
  }

  if ($('[data-filter]').length) {
    $('[data-filter]').on("change", function () {
      var filt = $(this).val();
      if (filt != 0) {
        $('[data-filterable]').addClass("hidden").filter("[data-filterable='" + filt + "']").removeClass("hidden");
      } else {
        $('[data-filterable]').removeClass("hidden");
      }
    });
  }

  function datepikr() {
    $('.datepicker').pickadate({
      selectMonths: true,
      selectYears: 15,
      format: dateformat
    });
  }

  function maskup() {
    var $this = $("[data-mask]");

    $this.each(function () {
      var mask = $(this).data("mask"),
          reverse = $(this).data("mask-reverse");
      $(this).mask(mask, {
        "reverse": reverse
      });
    });
  }

  if ($("[data-chart]").length) {
    $("[data-chart]").each(function () {

      var $this = $(this),
          comparison = $this.data("comparison"),
          type = $this.data("chart"),
          title = $this.data("title"),
          labels = $this.data("labels").split(","),
          values = $this.data("values").split(",");

      if (comparison) {
        var title2 = $this.data("title2"),
            values2 = $this.data("values2").split(",");
        var Lchart = new Chart($this, {
          type: type,
          data: {
            labels: labels,
            datasets: [{
              label: title,
              data: values,
              fill: true,
              backgroundColor: "rgba(78,182,173,0.2)",
              borderColor: "rgba(78,182,173,1)"
            }, {
              label: title2,
              data: values2,
              fill: true,
              backgroundColor: "rgba(183,28,28,0.2)",
              borderColor: "rgba(183,28,28,1)"
            }]
          }
        });
      } else {
        var Lchart = new Chart($this, {
          type: type,
          data: {
            labels: labels,
            datasets: [{
              label: title,
              data: values,
              backgroundColor: ['#26a69a', '#0d47a1', '#b71c1c', '#29b6f6', '#f57f17', '#2e7d32', '#0097a7', '#26a69a', '#0d47a1', '#b71c1c', '#29b6f6', '#f57f17', '#2e7d32', '#0097a7']
            }]
          }
        });
      }
    });
  }
})(jQuery);