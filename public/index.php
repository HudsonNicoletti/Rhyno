<?php

use Phalcon\Mvc\Application;

function throwException() {
  throw new \Exception("Couldn't require file.");
}

try {

  (file_exists(__DIR__ . '/../libraries/autoload.php')) ? require __DIR__ . '/../libraries/autoload.php' : $error = true;

  #   Include services
  require __DIR__ . '/../config/services.php';

  #   Handle the request
  $application = new Application($di);

  #   Include modules
  require __DIR__ . '/../config/modules.php';

  echo $application->handle()->getContent();

}
catch (\Exception $e) {

  $uri = rtrim($cf->application->baseUri,'/');

  if($cf->debug->active)
  {
    echo $e->getMessage();
  }
  else
  {
    header("Location: {$uri}/error/internal");
  }

}
