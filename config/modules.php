<?php

$application->registerModules([
    'Manager' => [
        'className' => 'Manager\Module',
        'path'      => __DIR__ . '/../apps/Manager/Module.php'
    ],
    'Installer' => [
        'className' => 'Installer\Module',
        'path'      => __DIR__ . '/../apps/Installer/Module.php'
    ],
]);
