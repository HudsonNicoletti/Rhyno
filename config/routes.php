<?php
# START - Installer
$router->add("/installer", [
  'module'     => 'Installer',
  'namespace'  => 'Installer\Controllers',
  'controller' => 'index',
  'action'     => 'index'
]);
$router->add("/installer/application", [
  'module'     => 'Installer',
  'namespace'  => 'Installer\Controllers',
  'controller' => 'index',
  'action'     => 'application'
]);
$router->add("/installer/account", [
  'module'     => 'Installer',
  'namespace'  => 'Installer\Controllers',
  'controller' => 'index',
  'action'     => 'account'
]);

$router->add("/installer/save", [
  'module'     => 'Installer',
  'namespace'  => 'Installer\Controllers',
  'controller' => 'index',
  'action'     => 'save'
])->via(["POST"]);

$router->add("/installer/createAccount", [
  'module'     => 'Installer',
  'namespace'  => 'Installer\Controllers',
  'controller' => 'account',
  'action'     => 'create'
])->via(["POST"]);

# END   - Installer

# START - LoginController
$router->add("/login/request/confirmation", [
  'controller' => 'login',
  'action'     => 'RequestCode'
])->via(["POST"]);

$router->add("/login/confirmation/{hash:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'login',
  'action'     => 'confirmation'
]);

$router->add("/login/verify/{hash:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'login',
  'action'     => 'verify'
])->via(["POST"]);
# END   - LoginController

# START - IndexController
$router->add("/logout", [
  'controller' => 'login',
  'action'     => 'logout',
]);
# END   - IndexController

# START - AjaxRequestController
$router->add("/currencyRates", [
  'controller' => 'ajaxrequest',
  'action'     => 'currencyRates',
]);
$router->add("/request/stripe/publishable", [
  'controller' => 'ajaxrequest',
  'action'     => 'stripe',
]);
$router->add("/request/project/activities/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'ajaxrequest',
  'action'     => 'ProjectActivities',
]);
$router->add("/clsnn", [
  'controller' => 'ajaxrequest',
  'action'     => 'clsnn',
]);
# START - AjaxRequestController

# START - AccountController
$router->add("/account", [
  'controller' => 'account',
  'action'     => 'index',
]);
$router->add("/account/update", [
  'controller' => 'account',
  'action'     => 'update',
])->via(["POST"]);
# END   - AccountController

# START - FinanceController
$router->add("/finance", [
  'controller' => 'finance',
  'action'     => 'index',
]);
$router->add("/finance/invoices", [
  'controller' => 'finance',
  'action'     => 'invoices',
]);
$router->add("/finance/estimates", [
  'controller' => 'finance',
  'action'     => 'estimates',
]);
$router->add("/finance/requests", [
  'controller' => 'finance',
  'action'     => 'requests',
]);

$router->add("/finance/invoices/create", [
  'controller' => 'finance',
  'action'     => 'modal',
  'method'     => 'create',
  'target'     => 'invoice',
]);

$router->add("/finance/estimates/create", [
  'controller' => 'finance',
  'action'     => 'modal',
  'method'     => 'create',
  'target'     => 'estimate',
]);

$router->add("/finance/estimates/request", [
  'controller' => 'finance',
  'action'     => 'modal',
  'method'     => 'request',
  'target'     => 'estimate',
]);

$router->add("/finance/invoices/new", [
  'controller' => 'finance',
  'action'     => 'new',
  'target'     => 'invoice',
])->via(["POST"]);

$router->add("/finance/estimates/new", [
  'controller' => 'finance',
  'action'     => 'new',
  'target'     => 'estimate',
])->via(["POST"]);

$router->add("/finance/estimates/send", [
  'controller' => 'finance',
  'action'     => 'send',
])->via(["POST"]);

$router->add("/finance/expenses", [
  'controller' => 'financeexpenses',
  'action'     => 'index',
]);
$router->add("/finance/expenses/create", [
  'controller' => 'financeexpenses',
  'action'     => 'modal',
  'method'     => 'create'
]);
$router->add("/finance/expenses/modify/{expense:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'financeexpenses',
  'action'     => 'modal',
  'method'     => 'modify'
]);
$router->add("/finance/expenses/remove/{expense:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'financeexpenses',
  'action'     => 'modal',
  'method'     => 'remove'
]);
$router->add("/finance/expenses/new", [
  'controller' => 'financeexpenses',
  'action'     => 'new',
])->via(["POST"]);
$router->add("/finance/expenses/update/{expense:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'financeexpenses',
  'action'     => 'update',
])->via(["POST"]);
$router->add("/finance/expenses/delete/{expense:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'financeexpenses',
  'action'     => 'delete',
])->via(["POST"]);
# END   - FinanceController

# START - NotificationController
$router->add("/notifications", [
  'controller' => 'notifications',
  'action'     => 'index',
]);
# END   - NotificationController

# START - TasksController
$router->add("/task/create", [
  'controller' => 'tasks',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/task/overview/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tasks',
  'action'     => 'overview',
]);

$router->add("/task/collaborators/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tasks',
  'action'     => 'modal',
  'method'     => 'collaborators'
]);

$router->add("/task/status/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tasks',
  'action'     => 'status',
  'method'     => 'update'
])->via(["POST"]);

$router->add("/task/cancel/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tasks',
  'action'     => 'status',
  'method'     => 'cancel'
])->via(["POST"]);

$router->add("/task/job/status/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tasks',
  'action'     => 'status',
  'method'     => 'job'
])->via(["POST"]);

$router->add("/task/collaborators/update/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tasks',
  'action'     => 'collaborators',
])->via(["POST"]);

$router->add("/task/send/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tasks',
  'action'     => 'send',
])->via(["POST"]);
# END   - TasksController
# START - TicketsController
$router->add("/tickets/create", [
  'controller' => 'tickets',
  'action'     => 'modal',
  'method'     => 'create'
]);
$router->add("/ticket/modify/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'modal',
  'method'     => 'modify'
]);
$router->add("/ticket/remove/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'modal',
  'method'     => 'remove'
]);
$router->add("/ticket/overview/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'overview',
]);
$router->add("/ticket/new", [
  'controller' => 'tickets',
  'action'     => 'new',
])->via(["POST"]);
$router->add("/ticket/send/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'send',
])->via(["POST"]);
$router->add("/ticket/status/progress/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'status',
  'method'     => 'progress'
])->via(["POST"]);
$router->add("/ticket/status/reopen/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'status',
  'method'     => 'reopen'
])->via(["POST"]);
$router->add("/ticket/status/close/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'status',
  'method'     => 'close'
])->via(["POST"]);
$router->add("/ticket/update/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'update',
])->via(["POST"]);
$router->add("/ticket/delete/{ticket:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'tickets',
  'action'     => 'delete',
])->via(["POST"]);
# END   - TicketsController
# START - TicketCategoriesController
$router->add("/ticket/categories", [
  'controller' => 'ticketcategories',
  'action'     => 'index'
]);
$router->add("/ticket/categories/create", [
  'controller' => 'ticketcategories',
  'action'     => 'modal',
  'method'     => 'create'
]);
$router->add("/ticket/categories/modify/{category:[0-9]+}", [
  'controller' => 'ticketcategories',
  'action'     => 'modal',
  'method'     => 'modify'
]);
$router->add("/ticket/categories/remove/{category:[0-9]+}", [
  'controller' => 'ticketcategories',
  'action'     => 'modal',
  'method'     => 'remove'
]);
$router->add("/ticket/categories/new", [
  'controller' => 'ticketcategories',
  'action'     => 'new',
])->via(["POST"]);
$router->add("/ticket/categories/update/{category:[0-9]+}", [
  'controller' => 'ticketcategories',
  'action'     => 'update',
])->via(["POST"]);
$router->add("/ticket/categories/delete/{category:[0-9]+}", [
  'controller' => 'ticketcategories',
  'action'     => 'delete',
])->via(["POST"]);
# END   - TicketCategoriesController
# START - TeamController
$router->add("/team/create", [
  'controller' => 'team',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/team/view/{member:[0-9]+}", [
  'controller' => 'team',
  'action'     => 'modal',
  'method'     => 'view',
]);

$router->add("/team/modify/{member:[0-9]+}", [
  'controller' => 'team',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/team/remove/{member:[0-9]+}", [
  'controller' => 'team',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/team/new", [
  'controller' => 'team',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/team/update/{member:[0-9]+}", [
  'controller' => 'team',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/team/delete/{member:[0-9]+}", [
  'controller' => 'team',
  'action'     => 'delete',
])->via(["POST"]);

##  TEAM DEPARMENTS
$router->add("/department/members", [
  'controller' => 'departments',
  'action'     => 'members',
]);

$router->add("/departments", [
  'controller' => 'departments',
  'action'     => 'index',
]);

$router->add("/department/create", [
  'controller' => 'departments',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/department/modify/{department:[0-9]+}", [
  'controller' => 'departments',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/department/remove/{department:[0-9]+}", [
  'controller' => 'departments',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/department/new", [
  'controller' => 'departments',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/department/update/{department:[0-9]+}", [
  'controller' => 'departments',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/department/delete/{department:[0-9]+}", [
  'controller' => 'departments',
  'action'     => 'delete',
])->via(["POST"]);
# END   - TeamController
# START - ClientsController
$router->add("/client/create", [
  'controller' => 'clients',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/client/view/{client:[0-9]+}", [
  'controller' => 'clients',
  'action'     => 'modal',
  'method'     => 'view',
]);

$router->add("/client/modify/{client:[0-9]+}", [
  'controller' => 'clients',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/client/remove/{client:[0-9]+}", [
  'controller' => 'clients',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/client/new", [
  'controller' => 'clients',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/client/update/{client:[0-9]+}", [
  'controller' => 'clients',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/client/delete/{client:[0-9]+}", [
  'controller' => 'clients',
  'action'     => 'delete',
])->via(["POST"]);
# END   - ClientsController
# START - AssignmentsController
$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/assignments", [
  'controller' => 'assignments',
  'action'     => 'modal',
  'method'     => 'assign',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/assign", [
  'controller' => 'assignments',
  'action'     => 'assign',
])->via(["POST"]);
# END   - AssignmentsController
# START - ProjectsController
$router->add("/project/create", [
  'controller' => 'projects',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/modify/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/overview", [
  'controller' => 'projects',
  'action'     => 'overview',
]);

$router->add("/project/remove/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/new", [
  'controller' => 'projects',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/update/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/delete/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'delete',
])->via(["POST"]);

$router->add("/project/complete/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'status',
  'method'     => 'complete'
])->via(["POST"]);

$router->add("/project/incomplete/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'status',
  'method'     => 'incomplete'
])->via(["POST"]);

$router->add("/project/cancel/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'status',
  'method'     => 'cancel'
])->via(["POST"]);

$router->add("/project/note/{project:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projects',
  'action'     => 'notes',
])->via(["POST"]);

# END   - ProjectsController
# START - ProjectTasksController
$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks", [
  'controller' => 'projecttasks',
  'action'     => 'index',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks/create", [
  'controller' => 'projecttasks',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks/modify/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projecttasks',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks/overview/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projecttasks',
  'action'     => 'modal',
  'method'     => 'view',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks/remove/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projecttasks',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks/new", [
  'controller' => 'projecttasks',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks/update/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projecttasks',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/tasks/delete/{task:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projecttasks',
  'action'     => 'delete',
])->via(["POST"]);

# END   - ProjectTasksController
# START - ProjectFilesController

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/files", [
  'controller' => 'projectfiles',
  'action'     => 'index',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/files/create", [
  'controller' => 'projectfiles',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/files/remove/{file:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectfiles',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/request/file/{file:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectfiles',
  'action'     => 'requestfile',
]);

$router->add("/download/file/{file:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectfiles',
  'action'     => 'downloadfile',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/files/new", [
  'controller' => 'projectfiles',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/files/delete/{file:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectfiles',
  'action'     => 'delete',
])->via(["POST"]);

# END   - ProjectFilesController
# START - ProjectCategoriesController

$router->add("/project/categories", [
  'controller' => 'projectcategories',
  'action'     => 'index',
]);

$router->add("/project/categories/create", [
  'controller' => 'projectcategories',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/categories/modify/{category:[0-9]+}", [
  'controller' => 'projectcategories',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/project/categories/remove/{category:[0-9]+}", [
  'controller' => 'projectcategories',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/categories/new", [
  'controller' => 'projectcategories',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/categories/update/{category:[0-9]+}", [
  'controller' => 'projectcategories',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/categories/delete/{category:[0-9]+}", [
  'controller' => 'projectcategories',
  'action'     => 'delete',
])->via(["POST"]);
# END   - ProjectCategoriesController

# START - ProjectEstimateRequestsController
$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/requests", [
  'controller' => 'projectestimaterequests',
  'action'     => 'index',
]);
$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/requests/overview/{request:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimaterequests',
  'action'     => 'modal',
  'method'     => 'view',
]);
$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/requests/remove/{request:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimaterequests',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/requests/update/{request:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimaterequests',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/requests/delete/{request:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimaterequests',
  'action'     => 'delete',
])->via(["POST"]);
# END   - ProjectEstimateRequestsController

# START - ProjectEstimatesController
$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates", [
  'controller' => 'projectestimates',
  'action'     => 'index',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/create", [
  'controller' => 'projectestimates',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/modify/{estimate:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimates',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/create/request", [
  'controller' => 'projectestimates',
  'action'     => 'modal',
  'method'     => 'request',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/request", [
  'controller' => 'projectestimates',
  'action'     => 'request',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/remove/{estimate:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimates',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/overview/{estimate:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimates',
  'action'     => 'overview',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/decline/{estimate:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimates',
  'action'     => 'modal',
  'method'     => 'decline',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/accept/{estimate:[a-zA-Z0-9\_\-\.]+}/submit", [
  'controller' => 'projectestimates',
  'action'     => 'submit',
  'method'     => 'accept',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/decline/{estimate:[a-zA-Z0-9\_\-\.]+}/submit", [
  'controller' => 'projectestimates',
  'action'     => 'submit',
  'method'     => 'decline',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/new", [
  'controller' => 'projectestimates',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/update/{estimate:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimates',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/delete/{estimate:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimates',
  'action'     => 'delete',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/send/{estimate:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimates',
  'action'     => 'send',
])->via(["POST"]);

# END   - ProjectEstimatesController
# START - ProjectEstimateItemsController

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/{estimate:[a-zA-Z0-9\_\-\.]+}/item/create", [
  'controller' => 'projectestimateitems',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/{estimate:[a-zA-Z0-9\_\-\.]+}/item/modify/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimateitems',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/{estimate:[a-zA-Z0-9\_\-\.]+}/item/remove/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimateitems',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/{estimate:[a-zA-Z0-9\_\-\.]+}/item/new", [
  'controller' => 'projectestimateitems',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/{estimate:[a-zA-Z0-9\_\-\.]+}/item/update/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimateitems',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/estimates/{estimate:[a-zA-Z0-9\_\-\.]+}/item/delete/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectestimateitems',
  'action'     => 'delete',
])->via(["POST"]);

# END   - ProjectEstimateItemsController
# START - ProjectInvoicesController

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices", [
  'controller' => 'projectinvoices',
  'action'     => 'index',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/create", [
  'controller' => 'projectinvoices',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/modify/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoices',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/remove/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoices',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/overview/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoices',
  'action'     => 'overview',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/new", [
  'controller' => 'projectinvoices',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/update/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoices',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/delete/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoices',
  'action'     => 'delete',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/cancel/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoices',
  'action'     => 'status',
  'method'     => 'cancel'
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/set/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoices',
  'action'     => 'status',
  'method'     => 'set'
])->via(["POST"]);

# END   - ProjectInvoicesController
# START - ProjectInvoiceItemsController

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/item/create", [
  'controller' => 'projectinvoiceitems',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/item/modify/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoiceitems',
  'action'     => 'modal',
  'method'     => 'modify',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/item/remove/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoiceitems',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/item/new", [
  'controller' => 'projectinvoiceitems',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/item/update/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoiceitems',
  'action'     => 'update',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/item/delete/{item:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectinvoiceitems',
  'action'     => 'delete',
])->via(["POST"]);

# END   - ProjectInvoiceItemsController
# START - ProjectPaymentsController

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/payments", [
  'controller' => 'projectpayments',
  'action'     => 'index',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/payments/create", [
  'controller' => 'projectpayments',
  'action'     => 'modal',
  'method'     => 'create',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/payments/remove/{payment:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectpayments',
  'action'     => 'modal',
  'method'     => 'remove',
]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/payments/new", [
  'controller' => 'projectpayments',
  'action'     => 'new',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/payments/delete/{payment:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectpayments',
  'action'     => 'delete',
])->via(["POST"]);

$router->add("/payments/verification/paypal/{project:[a-zA-Z0-9\_\-\.]+}/{invoice:[a-zA-Z0-9\_\-\.]+}", [
  'controller' => 'projectpayments',
  'action'     => 'paypalverification',
])->via(["GET"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/pay/paypal", [
  'controller' => 'projectpayments',
  'action'     => 'paypal',
])->via(["POST"]);

$router->add("/project/{project:[a-zA-Z0-9\_\-\.]+}/invoices/{invoice:[a-zA-Z0-9\_\-\.]+}/pay/stripe", [
  'controller' => 'projectpayments',
  'action'     => 'stripe',
])->via(["POST"]);
# END   - ProjectPaymentsController

# START - SettingsAdministratorsController
$router->add("/settings/administrators", [
  'controller' => 'settingsadministrators',
  'action'     => 'index',
]);
$router->add("/settings/administrators/manage", [
  'controller' => 'settingsadministrators',
  'action'     => 'modal',
  'method'     => 'manage',
]);
$router->add("/settings/administrators/save", [
  'controller' => 'settingsadministrators',
  'action'     => 'save',
])->via(["POST"]);
# END   - SettingsAdministratorsController

# START - SettingsEmailController
$router->add("/settings/email", [
  'controller' => 'settingsemail',
  'action'     => 'index',
]);
$router->add("/settings/email/save", [
  'controller' => 'settingsemail',
  'action'     => 'save',
])->via(["POST"]);
$router->add("/settings/email/send", [
  'controller' => 'settingsemail',
  'action'     => 'send',
])->via(["POST"]);
# END   - SettingsEmailController

# START - SettingsServerController
$router->add("/settings/server", [
  'controller' => 'settingsserver',
  'action'     => 'index',
]);
$router->add("/settings/server/save", [
  'controller' => 'settingsserver',
  'action'     => 'save',
])->via(["POST"]);
# END   - SettingsServerController

# START - SettingsPermissionsController
$router->add("/settings/permissions", [
  'controller' => 'settingspermissions',
  'action'     => 'index',
]);
$router->add("/settings/permissions/save/members", [
  'controller' => 'settingspermissions',
  'action'     => 'save',
  'method'     => 'members'
])->via(["POST"]);
$router->add("/settings/permissions/save/clients", [
  'controller' => 'settingspermissions',
  'action'     => 'save',
  'method'     => 'clients'
])->via(["POST"]);
# END   - SettingsPermissionsController

# START - SettingsNotificationsController
$router->add("/settings/notifications", [
  'controller' => 'settingsnotifications',
  'action'     => 'index',
]);
$router->add("/settings/notifications/save/members", [
  'controller' => 'settingsnotifications',
  'action'     => 'save',
  'method'     => 'members'
])->via(["POST"]);
$router->add("/settings/notifications/save/clients", [
  'controller' => 'settingsnotifications',
  'action'     => 'save',
  'method'     => 'clients'
])->via(["POST"]);
# END   - SettingsNotificationsController

# START - SettingsNotificationsController
$router->add("/settings/payments", [
  'controller' => 'settingspayments',
  'action'     => 'index',
]);
$router->add("/settings/payments/paypal/save", [
  'controller' => 'settingspayments',
  'action'     => 'save',
  'method'     => 'paypal'
])->via(["POST"]);
$router->add("/settings/payments/stripe/save", [
  'controller' => 'settingspayments',
  'action'     => 'save',
  'method'     => 'stripe'
])->via(["POST"]);
$router->add("/settings/payments/taxes/save", [
  'controller' => 'settingspayments',
  'action'     => 'save',
  'method'     => 'taxes'
])->via(["POST"]);
# END   - SettingsNotificationsController
