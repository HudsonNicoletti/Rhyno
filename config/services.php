<?php

use Phalcon\Mvc\Dispatcher\Exception  as DispatchException,
    Phalcon\Session\Adapter\Files     as SessionAdapter,
    Phalcon\Db\Adapter\Pdo\Mysql      as DbAdapter,
    Phalcon\Mvc\Dispatcher            as PhDispatcher,
    Phalcon\Events\Manager            as EventsManager,
    Phalcon\Mvc\Url                   as UrlResolver,
    Phalcon\Mvc\Model\Manager         as ModelsManager,
    Phalcon\Config\Adapter\Ini,
    Phalcon\DI\FactoryDefault,
    Phalcon\Mvc\Router,
    Phalcon\Dispatcher;

$di = new FactoryDefault();
$cf = new Ini("config.ini");

#    The URL component is used to generate all kinds of URLs in the application
$di['url'] = function() use ($cf){
    $url = new UrlResolver();
    $url->setBaseUri(rtrim($cf->application->baseUri,'/').'/');

    return $url;
};

#   Starts the session the first time some component requests the session service
$di['session'] = function (){
    $session = new SessionAdapter();
    $session->start();

    return $session;
};

#   Handles 404
$di['dispatcher'] = function () {
    $eventsManager = new EventsManager();
    $eventsManager->attach("dispatch:beforeException", function($event, $dispatcher, $exception) {

      if ($exception instanceof DispatchException) {
        $dispatcher->forward([
          'controller' => 'error',
          'action'     => 'NotFound'
        ]);
        return false;
      }

    });

    $dispatcher = new PhDispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
};

$di['configuration'] = function () use ($cf) {
    return (object)[
      "database"      => $cf->database ,
      "mail"          => $cf->mail,
      "app"           => $cf->application,
      "taxes"         => $cf->taxes,
      "paypal"        => $cf->paypal,
      "stripe"        => $cf->stripe,
      "team_crud"     => $cf->team_crud,
      "client_crud"   => $cf->client_crud,
      "debug"         => $cf->debug,
      "team_notification"   => $cf->team_notification,
      "client_notification" => $cf->client_notification,
    ];
};

#   Configure PHPMailer ( loaded by composer ) , returning the mail ini config & PHPMailer functions
$di['mail'] = function () use ($cf) {
    $mail = new PHPMailer;

    $mail->isSMTP();
    $mail->isHTML(true);

    $mail->CharSet      = $cf->mail->charset;
    $mail->Host         = $cf->mail->host;
    $mail->SMTPAuth     = true;
    $mail->Username     = $cf->mail->username;
    $mail->Password     = $cf->mail->password;
    $mail->SMTPSecure   = $cf->mail->security;
    $mail->Port         = $cf->mail->port;

    #   Pass as object only MAIL config and PHPMailer Functions.
    return (object)[
      "name"      => $cf->mail->name ,
      "email"     => $cf->mail->email ,
      "functions" => $mail
    ];
};

#   Configure system login permissions
$di['paypal'] = function () use ($cf) {
  if($cf->paypal->active)
  {
    $api = new \PayPal\Rest\ApiContext(new \PayPal\Auth\OAuthTokenCredential(
      $cf->paypal->clientID,
      $cf->paypal->secret
    ));

    $api->setConfig([
      'mode'                     =>  $cf->paypal->mode,
      'http.ConnectionTimeOut'   =>  100,
      'log.LogEnabled'           =>  false,
      'log.FileName'             =>  '',
      'log.LogLevel'             =>  'FINE',
      'validation.level'         =>  'log'
    ]);

    return $api;
  }
  else
  {
    return false;
  }
};

#   Configure system login permissions
$di['permissions'] = function () use ($cf) {
    return $cf->permissions;
};

#   Configure system Logs
$di['logs'] = function () use ($cf) {
    return $cf->logs;
};

#    Registering a router
$di['router'] = function () {
    $router = new Router();

    $router->setDefaultModule('Manager');
    $router->setDefaultNamespace('Manager\Controllers');
    $router->removeExtraSlashes(true);

    require( "routes.php" );

    return $router;
};
