module.exports = function(grunt){
    grunt.initConfig({

        concat: {
          options: {
            separator: ';',
          },
          target: {
           files: [
             { dest:  'public/assets/manager/js/main.js', src: 'public/assets/manager/js/compiled/*.js' }
           ]
          }
        },

        uglify: {
            my_target: {
                files: {
                    'public/assets/manager/js/main.js': ['public/assets/manager/js/main.js']
                }
            }
        },
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.registerTask('default', ['concat','uglify']);
}
