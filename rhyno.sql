/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : rhyno

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-11-09 17:23:41
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for assignments
-- ----------------------------
DROP TABLE IF EXISTS `assignments`;
CREATE TABLE `assignments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `project` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of assignments
-- ----------------------------

-- ----------------------------
-- Table structure for clients
-- ----------------------------
DROP TABLE IF EXISTS `clients`;
CREATE TABLE `clients` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `name` varchar(60) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `country` varchar(60) DEFAULT NULL,
  `city` varchar(60) DEFAULT NULL,
  `state` varchar(60) DEFAULT NULL,
  `image` varchar(60) DEFAULT NULL,
  `vat` varchar(255) DEFAULT NULL,
  `currency` int(1) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of clients
-- ----------------------------

-- ----------------------------
-- Table structure for currencies
-- ----------------------------
DROP TABLE IF EXISTS `currencies`;
CREATE TABLE `currencies` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of currencies
-- ----------------------------
INSERT INTO `currencies` VALUES ('1', 'AUD', 'Australia Dollar');
INSERT INTO `currencies` VALUES ('2', 'BGN', 'Bulgaria Lev');
INSERT INTO `currencies` VALUES ('3', 'BRL', 'Brazil Real');
INSERT INTO `currencies` VALUES ('4', 'CAD', 'Canada Dollar');
INSERT INTO `currencies` VALUES ('5', 'CHF', 'Switzerland Franc');
INSERT INTO `currencies` VALUES ('6', 'CNY', 'China Yuan Renminbi');
INSERT INTO `currencies` VALUES ('7', 'CZK', 'Czech Republic Koruna');
INSERT INTO `currencies` VALUES ('8', 'DKK', 'Denmark Krone');
INSERT INTO `currencies` VALUES ('9', 'EUR', 'Euro Member Countries');
INSERT INTO `currencies` VALUES ('10', 'GBP', 'United Kingdom Pound');
INSERT INTO `currencies` VALUES ('11', 'HKD', 'Hong Kong Dollar');
INSERT INTO `currencies` VALUES ('12', 'HRK', 'Croatia Kuna');
INSERT INTO `currencies` VALUES ('13', 'HUF', 'Hungary Forint');
INSERT INTO `currencies` VALUES ('14', 'IDR', 'Indonesia Rupiah');
INSERT INTO `currencies` VALUES ('15', 'ILS', 'Israel Shekel');
INSERT INTO `currencies` VALUES ('16', 'INR', 'India Rupee');
INSERT INTO `currencies` VALUES ('17', 'JPY', 'Japan Yen');
INSERT INTO `currencies` VALUES ('18', 'KRW', 'Korea (South) Won');
INSERT INTO `currencies` VALUES ('19', 'MXN', 'Mexico Peso');
INSERT INTO `currencies` VALUES ('20', 'MYR', 'Malaysia Ringgit');
INSERT INTO `currencies` VALUES ('21', 'NOK', 'Norway Krone');
INSERT INTO `currencies` VALUES ('22', 'NZD', 'New Zealand Dollar');
INSERT INTO `currencies` VALUES ('23', 'PHP', 'Philippines Peso');
INSERT INTO `currencies` VALUES ('24', 'PLN', 'Poland Zloty');
INSERT INTO `currencies` VALUES ('25', 'RON', 'Romania New Leu');
INSERT INTO `currencies` VALUES ('26', 'RUB', 'Russia Ruble');
INSERT INTO `currencies` VALUES ('27', 'SEK', 'Sweden Krona');
INSERT INTO `currencies` VALUES ('28', 'SGD', 'Singapore Dollar');
INSERT INTO `currencies` VALUES ('29', 'USD', 'American Dollar');

-- ----------------------------
-- Table structure for departments
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `department` varchar(60) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO `departments` VALUES ('1', 'All Members');

-- ----------------------------
-- Table structure for department_members
-- ----------------------------
DROP TABLE IF EXISTS `department_members`;
CREATE TABLE `department_members` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `department` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of department_members
-- ----------------------------

-- ----------------------------
-- Table structure for estimate_items
-- ----------------------------
DROP TABLE IF EXISTS `estimate_items`;
CREATE TABLE `estimate_items` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `estimate` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of estimate_items
-- ----------------------------

-- ----------------------------
-- Table structure for estimate_requests
-- ----------------------------
DROP TABLE IF EXISTS `estimate_requests`;
CREATE TABLE `estimate_requests` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) DEFAULT NULL,
  `project` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `budget` int(10) DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estimate_requests
-- ----------------------------

-- ----------------------------
-- Table structure for estimate_reviews
-- ----------------------------
DROP TABLE IF EXISTS `estimate_reviews`;
CREATE TABLE `estimate_reviews` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `estimate` int(10) NOT NULL,
  `text` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of estimate_reviews
-- ----------------------------

-- ----------------------------
-- Table structure for expenses
-- ----------------------------
DROP TABLE IF EXISTS `expenses`;
CREATE TABLE `expenses` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `amount` int(60) NOT NULL,
  `category` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `file` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of expenses
-- ----------------------------

-- ----------------------------
-- Table structure for expense_categories
-- ----------------------------
DROP TABLE IF EXISTS `expense_categories`;
CREATE TABLE `expense_categories` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of expense_categories
-- ----------------------------
INSERT INTO `expense_categories` VALUES ('1', 'Miscellaneous');
INSERT INTO `expense_categories` VALUES ('2', 'Salary');
INSERT INTO `expense_categories` VALUES ('3', 'Software');

-- ----------------------------
-- Table structure for invoice_items
-- ----------------------------
DROP TABLE IF EXISTS `invoice_items`;
CREATE TABLE `invoice_items` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `invoice` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` varchar(255) DEFAULT NULL,
  `amount` int(10) DEFAULT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `issued` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of invoice_items
-- ----------------------------

-- ----------------------------
-- Table structure for invoice_payments
-- ----------------------------
DROP TABLE IF EXISTS `invoice_payments`;
CREATE TABLE `invoice_payments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `project` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `amount` int(60) NOT NULL,
  `method` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `payer` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of invoice_payments
-- ----------------------------

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `modifier` int(10) NOT NULL,
  `receiver` int(10) NOT NULL,
  `text` text,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `href` varchar(255) DEFAULT NULL,
  `new` int(10) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notifications
-- ----------------------------

-- ----------------------------
-- Table structure for paypal_payments
-- ----------------------------
DROP TABLE IF EXISTS `paypal_payments`;
CREATE TABLE `paypal_payments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `ppid` varchar(255) NOT NULL,
  `value` int(60) DEFAULT NULL,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paypal_payments
-- ----------------------------

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `type` int(10) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `client` int(10) DEFAULT NULL,
  `created` date DEFAULT NULL,
  `deadline` date DEFAULT NULL,
  `finished` date DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of projects
-- ----------------------------

-- ----------------------------
-- Table structure for project_activities
-- ----------------------------
DROP TABLE IF EXISTS `project_activities`;
CREATE TABLE `project_activities` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `project` int(10) NOT NULL,
  `text` text,
  `user` int(11) NOT NULL COMMENT 'user because client can be here',
  `date` datetime NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_activities
-- ----------------------------

-- ----------------------------
-- Table structure for project_estimates
-- ----------------------------
DROP TABLE IF EXISTS `project_estimates`;
CREATE TABLE `project_estimates` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(60) NOT NULL,
  `project` int(10) NOT NULL,
  `status` int(10) NOT NULL COMMENT 'open , invoiced , reviesd , accepted ',
  `title` varchar(255) DEFAULT NULL,
  `issue` datetime DEFAULT NULL,
  `due` datetime DEFAULT NULL,
  `recurring` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of project_estimates
-- ----------------------------

-- ----------------------------
-- Table structure for project_files
-- ----------------------------
DROP TABLE IF EXISTS `project_files`;
CREATE TABLE `project_files` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `project` int(10) NOT NULL,
  `user` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `file` varchar(60) NOT NULL,
  `uploaded` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_files
-- ----------------------------

-- ----------------------------
-- Table structure for project_invoices
-- ----------------------------
DROP TABLE IF EXISTS `project_invoices`;
CREATE TABLE `project_invoices` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(60) NOT NULL,
  `project` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `issue` datetime DEFAULT NULL,
  `due` datetime DEFAULT NULL,
  `recurring` int(10) NOT NULL,
  `last_recursion` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_invoices
-- ----------------------------

-- ----------------------------
-- Table structure for project_recursions
-- ----------------------------
DROP TABLE IF EXISTS `project_recursions`;
CREATE TABLE `project_recursions` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `project` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  `due` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `last_recursion` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of project_recursions
-- ----------------------------

-- ----------------------------
-- Table structure for project_types
-- ----------------------------
DROP TABLE IF EXISTS `project_types`;
CREATE TABLE `project_types` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_types
-- ----------------------------
INSERT INTO `project_types` VALUES ('1', 'Web Development');
INSERT INTO `project_types` VALUES ('2', 'Graphic Design');
INSERT INTO `project_types` VALUES ('3', 'Marketing');
INSERT INTO `project_types` VALUES ('4', 'Social Media');
INSERT INTO `project_types` VALUES ('5', 'Others');
INSERT INTO `project_types` VALUES ('6', 'Mobile Development');

-- ----------------------------
-- Table structure for request_codes
-- ----------------------------
DROP TABLE IF EXISTS `request_codes`;
CREATE TABLE `request_codes` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `code` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `expires` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of request_codes
-- ----------------------------

-- ----------------------------
-- Table structure for stripe_payments
-- ----------------------------
DROP TABLE IF EXISTS `stripe_payments`;
CREATE TABLE `stripe_payments` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `user` int(10) NOT NULL,
  `invoice` int(10) NOT NULL,
  `spid` varchar(255) NOT NULL,
  `value` int(60) DEFAULT NULL,
  `date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of stripe_payments
-- ----------------------------

-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `unique` varchar(255) NOT NULL,
  `project` int(10) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `deadline` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `assigned` int(10) DEFAULT NULL,
  `completed` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tasks
-- ----------------------------

-- ----------------------------
-- Table structure for task_collaborators
-- ----------------------------
DROP TABLE IF EXISTS `task_collaborators`;
CREATE TABLE `task_collaborators` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `task` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  `status` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_collaborators
-- ----------------------------

-- ----------------------------
-- Table structure for task_responses
-- ----------------------------
DROP TABLE IF EXISTS `task_responses`;
CREATE TABLE `task_responses` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `task` int(10) NOT NULL,
  `member` int(10) NOT NULL,
  `text` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task_responses
-- ----------------------------

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `image` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of team
-- ----------------------------

-- ----------------------------
-- Table structure for tickets
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `unique` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `category` int(10) DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tickets
-- ----------------------------

-- ----------------------------
-- Table structure for ticket_categories
-- ----------------------------
DROP TABLE IF EXISTS `ticket_categories`;
CREATE TABLE `ticket_categories` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket_categories
-- ----------------------------
INSERT INTO `ticket_categories` VALUES ('1', 'General');
INSERT INTO `ticket_categories` VALUES ('2', 'Bug Report');
INSERT INTO `ticket_categories` VALUES ('3', 'Invoice');

-- ----------------------------
-- Table structure for ticket_responses
-- ----------------------------
DROP TABLE IF EXISTS `ticket_responses`;
CREATE TABLE `ticket_responses` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `ticket` int(10) NOT NULL,
  `uid` int(10) NOT NULL,
  `text` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket_responses
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `_` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `email` varchar(255) NOT NULL,
  `permission` int(10) NOT NULL,
  PRIMARY KEY (`_`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Event structure for Check Project Recursions
-- ----------------------------
DROP EVENT IF EXISTS `Check Project Recursions`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `Check Project Recursions` ON SCHEDULE EVERY 1 DAY STARTS '2016-09-29 14:26:41' ON COMPLETION NOT PRESERVE ENABLE DO INSERT INTO project_recursions (invoice,project,status,due,last_recursion) SELECT _,project,status,due,last_recursion from project_invoices where recurring = 1 and  NOW() >= last_recursion + INTERVAL 1 MONTH
;
;;
DELIMITER ;

-- ----------------------------
-- Event structure for Request Codes Reset
-- ----------------------------
DROP EVENT IF EXISTS `Request Codes Reset`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` EVENT `Request Codes Reset` ON SCHEDULE EVERY 1 MINUTE STARTS '2016-09-26 16:28:15' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM request_codes WHERE NOW() <= expires
;;
DELIMITER ;
