# REQUIREMENTS  
  - Composer
  - Mysql
  - Phalcon
  - PHP ^5.5.*
  - Apache AllowOverride  

# Default Password
  > $2a$10$xIvm9Lf8vcsKpBMjnhpWy.h1YJ7i4P3WCQb52HfAtH5l1zp062lqa

# INSTALLATION UBUNTU 12.04

- sudo apt-get update
- sudo apt-get install python-software-properties
- sudo apt-get curl
- curl -s https://packagecloud.io/install/repositories/phalcon/stable/script.deb.sh | sudo bash

- sudo apt-get install apache2
- sudo apt-get install mysql-server php5-mysql
- sudo mysql_install_db
- sudo mysql_secure_installation

- sudo apt-get install php5 php5-dev gcc libpcre3-dev libapache2-mod-php5 php5-mcrypt php5-mysql php5-phalcon
- sudo a2enmod rewrite
- sudo service apache2 restart

- curl -sS https://getcomposer.org/installer | php
- sudo mv composer.phar /usr/local/bin/composer

- sudo nano /etc/apache2/sites-available/default
    > set DocumentRoot to : /var/www
- sudo nano /etc/apache2/apache2.conf
    > Set AllowOverride All to directories : / & /var/www

- sudo service apache2 restart

# INSTALLATION UBUNTU 12.04
  > Phalcon 3.0.0

- sudo apt-get update
- sudo apt-get install python-software-properties
- sudo apt-add-repository ppa:phalcon/stable
- sudo apt-get update

- sudo apt-get install apache2
- sudo apt-get install mysql-server php5-mysql
- sudo mysql_install_db
- sudo mysql_secure_installation
- sudo apt-get curl
- sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt php5-dev php5-mysql gcc libpcre3-dev php5-phalcon
- sudo a2enmod rewrite
- sudo service apache2 restart

- curl -sS https://getcomposer.org/installer | php
- sudo mv composer.phar /usr/local/bin/composer

- sudo nano /etc/apache2/sites-available/default
    > set DocumentRoot to : /var/www
- sudo nano /etc/apache2/apache2.conf
    > Set AllowOverride All to directories : / & /var/www

- sudo service apache2 restart



# http://api.fixer.io/latest?base=USD

# How it Works ?
  - [Client's Guide](client.md)
  - [Team's Guide](team.md)
  - [Admin's Guide](admin.md)
